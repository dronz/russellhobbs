<?php echo $header; ?>
 
<?php global $session; ?>
                    
<?php 

if(empty($t1o_text_logo_color)) $t1o_text_logo_color ="222222";
if(empty($t1o_text_logo_awesome_color)) $t1o_text_logo_awesome_color ="16B778";
if(empty($t1o_header_logo_bg_color)) $t1o_header_logo_bg_color ="16B778";
if(empty($t1o_top_custom_block_title_bar_bg_color)) $t1o_top_custom_block_title_bar_bg_color ="16B778";
if(empty($t1o_top_custom_block_awesome_color)) $t1o_top_custom_block_awesome_color ="FFFFFF";
if(empty($t1o_top_custom_block_title_color)) $t1o_top_custom_block_title_color ="FFFFFF";
if(empty($t1o_top_custom_block_bg_color)) $t1o_top_custom_block_bg_color ="222222";
if(empty($t1o_top_custom_block_text_color)) $t1o_top_custom_block_text_color ="FFFFFF";
if(empty($t1o_news_bg_color)) $t1o_news_bg_color ="363636";
if(empty($t1o_news_icons_color)) $t1o_news_icons_color ="FFFFFF";
if(empty($t1o_news_word_bg_color)) $t1o_news_word_bg_color ="DA2042";
if(empty($t1o_news_word_color)) $t1o_news_word_color ="FFFFFF";
if(empty($t1o_news_color)) $t1o_news_color ="FFFFFF";
if(empty($t1o_news_hover_color)) $t1o_news_hover_color ="FFFFFF";
if(empty($t1o_custom_bar_below_menu_bg_color)) $t1o_custom_bar_below_menu_bg_color ="363636";
if(empty($t1o_custom_bar_below_menu_text_color)) $t1o_custom_bar_below_menu_text_color ="FFFFFF";
if(empty($t1o_category_title_above_color)) $t1o_category_title_above_color ="222222";
if(empty($t1o_snapchat_box_bg)) $t1o_snapchat_box_bg ="000000";
if(empty($t1o_video_box_bg)) $t1o_video_box_bg ="E22C29";
if(empty($t1o_custom_box_bg)) $t1o_custom_box_bg ="222222";

    $menu_link_target = array(
		'_self' => 'in the same frame',
		'_blank' => 'in a new tab'
	);    
?>
<?php for ($i = 1; $i <= 15; $i++) { ?>
<?php if(empty($t1o_menu_labels_color[$i])) $t1o_menu_labels_color[$i] ="16B778"; ?>
<?php } ?>

<?php for ($language_id = 1; $language_id <= 20; $language_id++) { ?>
<?php if(empty($t1o_text_logo[$language_id])) $t1o_text_logo[$language_id] ="Your Store"; ?>
<?php if(empty($t1o_text_sale[$language_id])) $t1o_text_sale[$language_id] ="Sale"; ?>
<?php if(empty($t1o_text_new_prod[$language_id])) $t1o_text_new_prod[$language_id] ="New"; ?>
<?php if(empty($t1o_text_quickview[$language_id])) $t1o_text_quickview[$language_id] ="Quick View"; ?>
<?php if(empty($t1o_text_share[$language_id])) $t1o_text_share[$language_id] ="Share"; ?>
<?php if(empty($t1o_text_shop_now[$language_id])) $t1o_text_shop_now[$language_id] ="Shop Now"; ?>
<?php if(empty($t1o_text_view[$language_id])) $t1o_text_view[$language_id] ="View Now"; ?>
<?php if(empty($t1o_text_next_product[$language_id])) $t1o_text_next_product[$language_id] ="Next"; ?>
<?php if(empty($t1o_text_previous_product[$language_id])) $t1o_text_previous_product[$language_id] ="Previous"; ?>
<?php if(empty($t1o_text_product_viewed[$language_id])) $t1o_text_product_viewed[$language_id] ="Product viewed:"; ?>
<?php if(empty($t1o_text_special_price[$language_id])) $t1o_text_special_price[$language_id] ="Special price:"; ?>
<?php if(empty($t1o_text_old_price[$language_id])) $t1o_text_old_price[$language_id] ="Old price:"; ?>
<?php if(empty($t1o_text_percent_saved[$language_id])) $t1o_text_percent_saved[$language_id] ="You save:"; ?>
<?php if(empty($t1o_text_product_friend[$language_id])) $t1o_text_product_friend[$language_id] ="Send to a friend"; ?>
<?php if(empty($t1o_text_menu_categories[$language_id])) $t1o_text_menu_categories[$language_id] ="Shop by Category"; ?>
<?php if(empty($t1o_text_menu_brands[$language_id])) $t1o_text_menu_brands[$language_id] ="Brands"; ?>
<?php if(empty($t1o_text_contact_us[$language_id])) $t1o_text_contact_us[$language_id] ="Contact us"; ?>
<?php if(empty($t1o_text_menu_contact_address[$language_id])) $t1o_text_menu_contact_address[$language_id] ="Address"; ?>
<?php if(empty($t1o_text_menu_contact_email[$language_id])) $t1o_text_menu_contact_email[$language_id] ="E-mail"; ?>
<?php if(empty($t1o_text_menu_contact_tel[$language_id])) $t1o_text_menu_contact_tel[$language_id] ="Telephone"; ?>
<?php if(empty($t1o_text_menu_contact_fax[$language_id])) $t1o_text_menu_contact_fax[$language_id] ="Fax"; ?>
<?php if(empty($t1o_text_menu_contact_hours[$language_id])) $t1o_text_menu_contact_hours[$language_id] ="Opening Times"; ?>
<?php if(empty($t1o_text_menu_contact_form[$language_id])) $t1o_text_menu_contact_form[$language_id] ="Contact Form"; ?>
<?php if(empty($t1o_text_menu_menu[$language_id])) $t1o_text_menu_menu[$language_id] ="Menu"; ?>
<?php if(empty($t1o_text_see_all_products_by[$language_id])) $t1o_text_see_all_products_by[$language_id] ="See all products by"; ?>
<?php if(empty($t1o_text_bestseller[$language_id])) $t1o_text_bestseller[$language_id] ="Bestseller"; ?>
<?php if(empty($t1o_text_featured[$language_id])) $t1o_text_featured[$language_id] ="Featured"; ?>
<?php if(empty($t1o_text_latest[$language_id])) $t1o_text_latest[$language_id] ="Latest"; ?>
<?php if(empty($t1o_text_special[$language_id])) $t1o_text_special[$language_id] ="Specials"; ?>
<?php if(empty($t1o_text_most_viewed[$language_id])) $t1o_text_most_viewed[$language_id] ="Most Viewed"; ?>
<?php if(empty($t1o_text_news[$language_id])) $t1o_text_news[$language_id] ="News"; ?>
<?php if(empty($t1o_text_share[$language_id])) $t1o_text_share[$language_id] ="Share:"; ?>
<?php if(empty($t1o_text_gallery[$language_id])) $t1o_text_gallery[$language_id] ="Gallery"; ?>
<?php if(empty($t1o_text_small_list[$language_id])) $t1o_text_small_list[$language_id] ="Small List"; ?>
<?php if(empty($t1o_text_your_cart[$language_id])) $t1o_text_your_cart[$language_id] ="Your Cart:"; ?>
<?php if(empty($t1o_text_popular_search[$language_id])) $t1o_text_popular_search[$language_id] ="Popular Search:"; ?>
<?php if(empty($t1o_text_advanced_search[$language_id])) $t1o_text_advanced_search[$language_id] ="Advanced Search"; ?>
<?php if(empty($t1o_newsletter_promo_text[$language_id])) $t1o_newsletter_promo_text[$language_id] ="Subscribe to our newsletter to get special offers and receive the latest news, sales and updates"; ?>
<?php if(empty($t1o_newsletter_email[$language_id])) $t1o_newsletter_email[$language_id] ="Your email address"; ?>
<?php if(empty($t1o_newsletter_subscribe[$language_id])) $t1o_newsletter_subscribe[$language_id] ="Subscribe"; ?>
<?php } ?>





<style type="text/css">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
.k_help {color:#999;background-color:#F5F5F5;font-size:10px;font-weight:normal;text-transform:uppercase;padding:15px;width:auto;display:block;margin-top:10px;margin-bottom:10px;border-radius:3px;}
span.k_help_tip {margin-left:10px;padding:4px 9px 3px;width:24px;display:inline;border-radius:2px;background-color:#16B778;color:#FFF;font-weight:bold;transition: all 0.15s ease-in 0s;opacity: 0.9; display: none;}
span.k_help_tip_support {display:block;}
span.k_help_tip:hover {opacity: 1;}
span.k_help_tip a {color:#FFF;font-size:12px;font-weight:bold;text-decoration:none;}
span.k_tooltip {cursor:pointer;}
.k_sep {background-color:#F7F7F7;}
.ptn {position:relative;width:40px;height:40px;float:left;margin-right:5px;margin-bottom:5px;}
.ptn_nr {position:absolute;bottom:0px;right:3px;}
.prod_l {position:relative;width:134px;height:134px;float:left;margin-right:25px;margin-bottom:30px;}
.prod_l_nr {position:absolute;bottom:-17px;right:0px;}
.header_s {position:relative;width:300px;height:auto;float:left;margin-right:25px;margin-bottom:45px;}
.header_s_nr {position:absolute;bottom:-22px;right:0px;}
.header_s:nth-child(2n+1) {clear: both;}
table.form {margin-bottom:0;}
table.form div {text-align:left}
table.form b {color:#003A88;font-size:13px}
table.form > tbody > tr > td:first-child {text-align:right}
a.btn-default.link {text-decoration:none;margin-left:5px;margin-right:5px;color:#222222;background-color:#F0F0F0;border-color:#F0F0F0;transition: all 0.15s ease-in 0s;position:relative;}
a.btn-default.link:hover {background-color:#DEDEDE;border-color:#DEDEDE;}
a.btn-default.link:last-child {padding-right:45px;}
a.btn-default.link i {font-size:14px;margin-right:5px;}
a.btn-default.link span.k_help_tip {position:absolute;top:5px;right:5px;}
.htabs {margin-top:15px;}
a.button-sellmore-theme {background:#4BB8E2;color:#FFFFFF;padding:4px 12px;margin-left:5px;text-decoration:none;border-radius:3px;cursor:pointer;}
a.button-sellmore-theme:hover {background:#ED5053;}
table.form {
	width: 100%;
	border-collapse: collapse;
	margin-bottom: 20px;
}
table.form > tbody > tr > td:first-child {
	width: 200px;
}
table.form > tbody > tr > td {
	padding: 10px;
	color: #000000;
	border-bottom: 1px dotted #CCCCCC;
}
label.control-label span:after {
	display: none;
}
legend {
	background-color: #EEEEEE;
	border: none;
	border-radius: 3px;
}
fieldset legend {
	margin-top: 20px;
	padding: 20px 30px;
}
legend.bn {
	border-color: #FFFFFF;
	padding: 0;
	margin: 0;
}
legend span {
	font-size: 12px;
}
.nav-tabs {
	border-bottom: 3px solid #16B778;
}
.nav-tabs > li {
	margin-bottom: 0;
}
.nav-tabs > li > a:hover {
	background-color: #E9E9E9;
	border-color: #E9E9E9;
}
.nav-tabs > li > a {
	padding: 15px 20px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
	background-color: #16B778;
	border-color: #16B778;
	color: #FFFFFF;
	font-weight: normal;
}
.nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
	background-color: #16B778;
}
.nav > li > a {
	background-color: #EEEEEE;
	padding: 15px 20px;
}
.nav > li > a:hover {
	background-color: #E9E9E9;
	color: inherit;
}
html, body {color:#222222;}
a {color:#666666;transition: all 0.15s ease-in 0s;}
a:hover, a:focus {color:#16B778;}
label {font-weight:normal;}
.form-control {display:inline-block;width:auto;min-width:77px;}
.form-horizontal .form-group {
	margin-left: 0;
	margin-right: 0;
}
.form-group + .form-group {border-top: 1px dotted #EDEDED;}
.tab-content .form-horizontal label.col-sm-2{
	padding-left: 0;
}
textarea.form-control {width:100%;}
.panel-heading i {font-size: 14px;}
</style>    

<?php echo $column_left; ?>




<div id="content">


  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-sellmore-theme" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="panel panel-default">
    <div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
	</div>
    <div class="panel-body">
    
    <div class="row form-horizontal">
		<div class="col-sm-6 col-md-4">
			<?php $strqty=count($stores); if ($strqty>1) { ?>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="t1o_store_id">Store:</label>
					<div class="col-sm-10">
						<select name="t1o_store_id" id="t1o_store_id" class="form-control input">
							<?php foreach ($stores as $store): ?>
							    <option value="<?php echo HTTPS_SERVER . 'index.php?route=module/sellmore_theme_options&store_id=' . $store['store_id'] . '&token=' . $session->data['token']; ?>"<?php if($store_id == $store['store_id']) echo ' selected="selected"'; ?>><?php echo $store['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>					
				<script type="text/javascript">
					$(document).ready(function (){
						$("#t1o_store_id").bind("change", function() {
							window.location = $(this).val();
						});
					});
				</script>
			<?php } ?>
		</div>
	</div>

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sellmore-theme">
    
    <input type="hidden" name="store_id" value="<?php echo $store_id; ?>" />

        <div style="margin-top:10px; margin-bottom:25px;">
            <span style="margin-left:0px;">Useful links:</span> 
            <a href="http://sellmore.321cart.com/documentation/" class="btn btn-default link" target="_blank"><i class="fa fa-book"></i> SELLMORE Documentation</a>
            <a href="http://sellmore.321cart.com/landing/" class="btn btn-default link" target="_blank"><i class="fa fa-television"></i> SELLMORE Demos</a>
            <a href="http://sellmore.321cart.com/support/" class="btn btn-default link" target="_blank"><i class="fa fa-support"></i> SELLMORE Support <span class="k_help_tip_support k_help_tip k_tooltip" title="<br>If you need help, please contact us. We provide support only through our SELLMORE Support System.<br><br>Create a ticket and our support developer will respond as soon as possible.<br><br>Support requests are being processed on Monday to Friday.<br><br>" data-toggle="tooltip">?</span></a>
            

		</div>

    
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-options" data-toggle="tab">General Options</a></li>
          <li><a href="#tab-header" data-toggle="tab">Header</a></li>
          <li><a href="#tab-menu" data-toggle="tab">Main Menu</a></li>
          <li><a href="#tab-midsection" data-toggle="tab">Midsection</a></li>
          <li><a href="#tab-footer" data-toggle="tab">Footer</a></li>
          <li><a href="#tab-widgets" data-toggle="tab">Widgets</a></li>
          <li><a href="#tab-css" data-toggle="tab">Custom CSS/JavaScript</a></li>
          <li><a href="#tab-translate" data-toggle="tab">Theme Translate</a></li>
        </ul>
        
        <div class="tab-content">
        <!-- -->
        
        <div class="tab-pane active" id="tab-options"> 
        <div class="row form-horizontal">  
        
        <div class="col-sm-2">    
        <ul id="store_features_tabs" class="nav nav-pills nav-stacked">
             <li class="active"><a href="#tab-options-layout" data-toggle="tab">Layout</a></li>
             <li><a href="#tab-options-sliders" data-toggle="tab">Products Layout</a></li>
             <li><a href="#tab-options-others" data-toggle="tab">Others</a></li>                                       
        </ul> 
        </div>
        
        <div class="col-sm-10">
        <div class="tab-content">
        
        <div id="tab-options-layout" class="tab-pane fade in active"> 
        
                    <fieldset>

						<div class="form-group">
							<label class="col-sm-2 control-label">Layout style:</label>
							<div class="col-sm-10">
								<select name="t1o_layout_style" class="form-control">
                                    <option value="boxed"<?php if($t1o_layout_style == 'boxed') echo ' selected="selected"';?>>Boxed</option>
									<option value="framed"<?php if($t1o_layout_style == 'framed') echo ' selected="selected"';?>>Framed</option>
                                    <option value="full-width"<?php if($t1o_layout_style == 'full-width') echo ' selected="selected"';?><?php if($t1o_layout_style == '') echo ' selected="selected"';?>>Full Width</option>
                                    <option value="full-width-border"<?php if($t1o_layout_style == 'full-width-border') echo ' selected="selected"';?>>Full Width + Border</option> 
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_01.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Maximum width:<br /><span class="k_help">for "Framed" layout style</span>
                            </label>
							<div class="col-sm-10">
								<select name="t1o_layout_l" class="form-control">
									<option value="1"<?php if($t1o_layout_l == '1') echo ' selected="selected"';?>>1170px</option>
                                    <option value="2"<?php if($t1o_layout_l == '2') echo ' selected="selected"';?>>980px</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Align layout to:<br /><span class="k_help">for "Framed" layout style</span>
                            </label>
							<div class="col-sm-10">
								<select name="t1o_layout_framed_align" class="form-control">
                                    <option value="0"<?php if($t1o_layout_framed_align == '0') echo ' selected="selected"';?>><?php echo $text_position_c; ?></option>
									<option value="1"<?php if($t1o_layout_framed_align == '1') echo ' selected="selected"';?>><?php echo $text_position_l; ?></option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Product Blocks maximum width:
                            <span class="k_help">for "Full Width"<br />and "Full Width + Border"<br />layout styles</span>
                            </label>
							<div class="col-sm-10">
								<select name="t1o_layout_full_width_max" class="form-control">
                                    <option value="0"<?php if($t1o_layout_full_width_max == '0') echo ' selected="selected"';?>>Full Width</option>
									<option value="1"<?php if($t1o_layout_full_width_max == '1') echo ' selected="selected"';?>>1440px</option>
                                    <option value="2"<?php if($t1o_layout_full_width_max == '2') echo ' selected="selected"';?>>1170px</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_01.jpg" target="_blank"><span class="k_help_tip">?</span></a> 
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Align Headings to:</label>
							<div class="col-sm-10">
								<select name="t1o_layout_h_align" class="form-control">
									<option value="0"<?php if($t1o_layout_h_align == '0') echo ' selected="selected"';?>><?php echo $text_position_c; ?></option>
                                    <option value="1"<?php if($t1o_layout_h_align == '1') echo ' selected="selected"';?>><?php echo $text_position_l; ?></option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Catalog Mode:<br /><span class="k_help">excludes purchase options</span>
                            </label>
							<div class="col-sm-10">
								<select name="t1o_layout_catalog_mode" class="form-control">
								    <option value="0"<?php if($t1o_layout_catalog_mode == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_layout_catalog_mode == '1') echo ' selected="selected"';?>>Yes</option> 
								</select>  
							    </select>
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-options-sliders" class="tab-pane">  
        
                    <fieldset>
                    
                        <legend class="bn"></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Bestseller view:</label>
							<div class="col-sm-10">
								<select name="t1o_bestseller_style" class="form-control">
									<option value="0"<?php if($t1o_bestseller_style == '0') echo ' selected="selected"';?>>Grid</option>
                                    <option value="1"<?php if($t1o_bestseller_style == '1') echo ' selected="selected"';?><?php if($t1o_bestseller_style == '') echo ' selected="selected"';?>>Slider</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Featured view:</label>
							<div class="col-sm-10">
								<select name="t1o_featured_style" class="form-control">
									<option value="0"<?php if($t1o_featured_style == '0') echo ' selected="selected"';?>>Grid</option>
                                    <option value="1"<?php if($t1o_featured_style == '1') echo ' selected="selected"';?><?php if($t1o_featured_style == '') echo ' selected="selected"';?>>Slider</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Latest view:</label>
							<div class="col-sm-10">
								<select name="t1o_latest_style" class="form-control">
									<option value="0"<?php if($t1o_latest_style == '0') echo ' selected="selected"';?>>Grid</option>
                                    <option value="1"<?php if($t1o_latest_style == '1') echo ' selected="selected"';?><?php if($t1o_latest_style == '') echo ' selected="selected"';?>>Slider</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Specials view:</label>
							<div class="col-sm-10">
								<select name="t1o_specials_style" class="form-control">
									<option value="0"<?php if($t1o_specials_style == '0') echo ' selected="selected"';?>>Grid</option>
                                    <option value="1"<?php if($t1o_specials_style == '1') echo ' selected="selected"';?><?php if($t1o_specials_style == '') echo ' selected="selected"';?>>Slider</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">SELLMORE Theme Most Viewed view:</label>
							<div class="col-sm-10">
								<select name="t1o_most_viewed_style" class="form-control">
									<option value="0"<?php if($t1o_most_viewed_style == '0') echo ' selected="selected"';?>>Grid</option>
                                    <option value="1"<?php if($t1o_most_viewed_style == '1') echo ' selected="selected"';?><?php if($t1o_most_viewed_style == '') echo ' selected="selected"';?>>Slider</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">SELLMORE Theme Product Tabs view:</label>
							<div class="col-sm-10">
								<select name="t1o_product_tabs_style" class="form-control">
									<option value="0"<?php if($t1o_product_tabs_style == '0') echo ' selected="selected"';?>>Grid</option>
                                    <option value="1"<?php if($t1o_product_tabs_style == '1') echo ' selected="selected"';?><?php if($t1o_product_tabs_style == '') echo ' selected="selected"';?>>Slider</option>
								</select>
							</div>
						</div>

						<legend>Grid View <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_02.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>

						
                        <div class="form-group">
							<label class="col-sm-2 control-label">Products per row:</label>
							<div class="col-sm-10">
								<select name="t1o_product_grid_per_row" class="form-control">
									<option value="12"<?php if($t1o_product_grid_per_row == '12') echo ' selected="selected"';?>>1</option>
                           			<option value="6"<?php if($t1o_product_grid_per_row == '6') echo ' selected="selected"';?>>2</option>
                           			<option value="4"<?php if($t1o_product_grid_per_row == '4') echo ' selected="selected"';?>>3</option>
                           			<option value="3"<?php if($t1o_product_grid_per_row == '3') echo ' selected="selected"';?><?php if($t1o_product_grid_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="15"<?php if($t1o_product_grid_per_row == '15') echo ' selected="selected"';?>>5</option>
                           			<option value="2"<?php if($t1o_product_grid_per_row == '2') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        
                        <legend>Slider View <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_02.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Bestsellers per row:</label>
							<div class="col-sm-10">
								<select name="t1o_bestseller_per_row" class="form-control">
									<option value="1"<?php if($t1o_bestseller_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_bestseller_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_bestseller_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_bestseller_per_row == '4') echo ' selected="selected"';?><?php if($t1o_bestseller_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_bestseller_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_bestseller_per_row == '6') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Featured per row:</label>
							<div class="col-sm-10">
								<select name="t1o_featured_per_row" class="form-control">
									<option value="1"<?php if($t1o_featured_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_featured_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_featured_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_featured_per_row == '4') echo ' selected="selected"';?><?php if($t1o_featured_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_featured_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_featured_per_row == '6') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Latest per row:</label>
							<div class="col-sm-10">
								<select name="t1o_latest_per_row" class="form-control">
									<option value="1"<?php if($t1o_latest_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_latest_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_latest_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_latest_per_row == '4') echo ' selected="selected"';?><?php if($t1o_latest_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_latest_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_latest_per_row == '6') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Specials per row:</label>
							<div class="col-sm-10">
								<select name="t1o_specials_per_row" class="form-control">
									<option value="1"<?php if($t1o_specials_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_specials_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_specials_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_specials_per_row == '4') echo ' selected="selected"';?><?php if($t1o_specials_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_specials_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_specials_per_row == '6') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">SELLMORE Theme Most Viewed per row:</label>
							<div class="col-sm-10">
								<select name="t1o_most_viewed_per_row" class="form-control">
									<option value="1"<?php if($t1o_most_viewed_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_most_viewed_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_most_viewed_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_most_viewed_per_row == '4') echo ' selected="selected"';?><?php if($t1o_most_viewed_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_most_viewed_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_most_viewed_per_row == '6') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">SELLMORE Theme Products Tabs - Products per row:</label>
							<div class="col-sm-10">
								<select name="t1o_product_tabs_per_row" class="form-control">
									<option value="1"<?php if($t1o_product_tabs_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_product_tabs_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_product_tabs_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_product_tabs_per_row == '4') echo ' selected="selected"';?><?php if($t1o_product_tabs_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_product_tabs_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_product_tabs_per_row == '6') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Related Products per row:</label>
							<div class="col-sm-10">
								<select name="t1o_related_per_row" class="form-control">
									<option value="1"<?php if($t1o_related_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_related_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_related_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_related_per_row == '4') echo ' selected="selected"';?><?php if($t1o_related_per_row == '') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_related_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_related_per_row == '6') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Carousel Items per row:</label>
							<div class="col-sm-10">
								<select name="t1o_carousel_items_per_row" class="form-control">
									<option value="1"<?php if($t1o_carousel_items_per_row == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_carousel_items_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_carousel_items_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_carousel_items_per_row == '4') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_carousel_items_per_row == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_carousel_items_per_row == '6') echo ' selected="selected"';?><?php if($t1o_carousel_items_per_row == '') echo ' selected="selected"';?>>6</option>
								</select>  
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-options-others" class="tab-pane">  
        
                    <fieldset>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show "Scroll To Top" button:</label>
							<div class="col-sm-10">
								<select name="t1o_others_totop" class="form-control">
                                    <option value="0"<?php if($t1o_others_totop == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_others_totop == '1') echo ' selected="selected"';?><?php if($t1o_others_totop == '') echo ' selected="selected"';?>>Yes</option>    
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_03.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>

					</fieldset>        
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        <div class="tab-pane" id="tab-header"> 
        <div class="row form-horizontal">  
        
        <div class="col-sm-2">    
        <ul id="store_features_tabs" class="nav nav-pills nav-stacked">
             <li class="active"><a href="#tab-header-general" data-toggle="tab">General</a></li>
             <li><a href="#tab-header-logo-creator" data-toggle="tab">Logo Creator</a></li>
             <li><a href="#tab-header-fixed-header" data-toggle="tab">Fixed Header</a></li>
             <li><a href="#tab-header-top-bar" data-toggle="tab">Top Bar</a></li>
             <li><a href="#tab-header-top-promo-message-slider" data-toggle="tab">Top Promo Message Slider</a></li>
             <li><a href="#tab-header-news" data-toggle="tab">News</a></li>
             <li><a href="#tab-header-custom-block" data-toggle="tab">Header Custom Block</a></li>
             <li><a href="#tab-header-search" data-toggle="tab">Search</a></li>                         
        </ul> 
        </div>
        
        <div class="col-sm-10">
        <div class="tab-content">
        
        <div id="tab-header-general" class="tab-pane fade in active">
        
                    <fieldset>
                    
                        <legend class="bn"></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Header Style:</label>
							<div class="col-sm-10">
								<select name="t1o_header_style" class="form-control">
									<option value="header-style-1"<?php if($t1o_header_style == 'header-style-1') echo ' selected="selected"';?>>Style 1</option>
                                    <option value="header-style-2"<?php if($t1o_header_style == 'header-style-2') echo ' selected="selected"';?><?php if($t1o_header_style == '') echo ' selected="selected"';?>>Style 2</option>
                                    <option value="header-style-3"<?php if($t1o_header_style == 'header-style-3') echo ' selected="selected"';?>>Style 3</option>
                                    <option value="header-style-4"<?php if($t1o_header_style == 'header-style-4') echo ' selected="selected"';?>>Style 4</option>
                                    <option value="header-style-5"<?php if($t1o_header_style == 'header-style-5') echo ' selected="selected"';?>>Style 5</option>
                                    <option value="header-style-6"<?php if($t1o_header_style == 'header-style-6') echo ' selected="selected"';?>>Style 6</option>
                                    <option value="header-style-7"<?php if($t1o_header_style == 'header-style-7') echo ' selected="selected"';?>>Style 7</option>
                                    <option value="header-style-8"<?php if($t1o_header_style == 'header-style-8') echo ' selected="selected"';?>>Style 8</option>
                                    <option value="header-style-9"<?php if($t1o_header_style == 'header-style-9') echo ' selected="selected"';?>>Style 9</option> 
                                    <option value="header-style-10"<?php if($t1o_header_style == 'header-style-10') echo ' selected="selected"';?>>Style 10</option>                  
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php for ($hs = 1; $hs <= 10; $hs++) { ?>
                                <div class="header_s"><img src="view/image/theme_img/hs_<?php echo $hs; ?>.png"><span class="t1o_help header_s_nr">Style <?php echo $hs; ?></span></div> 
						        <?php } ?>	
						    </div>   
                        </div>
                        
                    </fieldset>
                    
        </div>
        
        <div id="tab-header-logo-creator" class="tab-pane">
        
                    <fieldset>
                    
                        <span class="k_help">Here you can create your own simple logo. If you have a logo in a graphical format, you can add it here: <b>System > Settings > Image > Store Logo</b></span>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Logo Text:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_logo[<?php echo $language_id; ?>]" id="t1o_text_logo_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_logo[$language_id])) echo $t1o_text_logo[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Logo Text color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_text_logo_color" id="t1o_text_logo_color" value="<?php echo $t1o_text_logo_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Font Awesome Icon:</label>
							<div class="col-sm-10">
								<select name="t1o_text_logo_awesome_status" class="form-control">
									<option value="0"<?php if($t1o_text_logo_awesome_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_text_logo_awesome_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon:</label>
                            <div class="col-sm-10">
								<input type="text" name="t1o_text_logo_awesome" value="<?php echo $t1o_text_logo_awesome; ?>" class="form-control" />
                                <span class="k_help">Enter the name of an icon, for example: <b>shopping-bag</b></span>
                                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_text_logo_awesome_color" id="t1o_text_logo_awesome_color" value="<?php echo $t1o_text_logo_awesome_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Logo background color:<br /><span class="k_help">for Header Style 5</span>
                            </label>
							<div class="col-sm-10">
								<input type="text" name="t1o_header_logo_bg_color" id="t1o_header_logo_bg_color" value="<?php echo $t1o_header_logo_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>

                    </fieldset>
                    
        </div>

        <div id="tab-header-fixed-header" class="tab-pane">
        
                    <fieldset>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Fixed Header:<br /><span class="k_help">Not available for Header Style 6</span>
                            </label>
							<div class="col-sm-10">
								<select name="t1o_header_fixed_header_status" class="form-control">
									<option value="0"<?php if($t1o_header_fixed_header_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_header_fixed_header_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_05.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-header-top-bar" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Top Bar <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_04.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Top Bar:</label>
							<div class="col-sm-10">
								<select name="t1o_top_bar_status" class="form-control">
									<option value="0"<?php if($t1o_top_bar_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_top_bar_status == '1') echo ' selected="selected"';?><?php if($t1o_top_bar_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show "Shopping Cart" link:</label>
							<div class="col-sm-10">
								<select name="t1o_top_bar_cart_link_status" class="form-control">
                                    <option value="0"<?php if($t1o_top_bar_cart_link_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_top_bar_cart_link_status == '1') echo ' selected="selected"';?>>Yes</option>  
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Welcome Message:</label>
							<div class="col-sm-10">
								<select name="t1o_top_bar_welcome_status" class="form-control">
									<option value="0"<?php if($t1o_top_bar_welcome_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_top_bar_welcome_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon for Welcome Message:</label>
                            <div class="col-sm-10">
								<input type="text" name="t1o_top_bar_welcome_awesome" value="<?php echo $t1o_top_bar_welcome_awesome; ?>" class="form-control" />
                                <span class="k_help">Enter the name of an icon, for example: <b>flag</b></span>
                                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
							</div>
						</div>
                        <div class="form-group">
						    <label class="col-sm-2 control-label">Welcome Message:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
								<?php $language_id=$language['language_id']; ?>
									<div class="input-group">
										<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                        <input type="text" name="t1o_top_bar_welcome[<?php echo $language_id; ?>]" id="t1o_top_bar_welcome_<?php echo $language_id; ?>" value="<?php if(isset($t1o_top_bar_welcome[$language_id])) echo $t1o_top_bar_welcome[$language_id]; ?>" placeholder="Message" class="form-control" />  
									</div>
								<?php } ?>
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-header-top-promo-message-slider" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Top Promo Message Slider <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_07.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show Promo Message Slider:</label>
							<div class="col-sm-10">
								<select name="t1o_top_custom_block_status" class="form-control">
									<option value="0"<?php if($t1o_top_custom_block_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_top_custom_block_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Promo Bar background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_top_custom_block_title_bar_bg_color" id="t1o_top_custom_block_title_bar_bg_color" value="<?php echo $t1o_top_custom_block_title_bar_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                        <div class="form-group">
					        <label class="col-sm-2 control-label">Promo Bar background image:</label>
					        <div class="col-sm-10">                                
                                <a href="" id="t1o_top_custom_block_bar_bg_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_top_custom_block_bar_bg_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_top_custom_block_bar_bg" value="<?php echo $t1o_top_custom_block_bar_bg; ?>" id="t1o_top_custom_block_bar_bg" />
					        </div>
				        </div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Font Awesome Icon:</label>
							<div class="col-sm-10">
								<select name="t1o_top_custom_block_awesome_status" class="form-control">
									<option value="0"<?php if($t1o_top_custom_block_awesome_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_top_custom_block_awesome_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon:</label>
                            <div class="col-sm-10">
								<input type="text" name="t1o_top_custom_block_awesome" value="<?php echo $t1o_top_custom_block_awesome; ?>" class="form-control" />
                                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span>
                                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_top_custom_block_awesome_color" id="t1o_top_custom_block_awesome_color" value="<?php echo $t1o_top_custom_block_awesome_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                        <div class="form-group">
						    <label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
								<?php $language_id=$language['language_id']; ?>
									<div class="input-group">
										<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                        <input type="text" name="t1o_top_custom_block_title[<?php echo $language_id; ?>]" id="t1o_top_custom_block_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_top_custom_block_title[$language_id])) echo $t1o_top_custom_block_title[$language_id]; ?>" placeholder="Title" class="form-control" />  
									</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_top_custom_block_title_color" id="t1o_top_custom_block_title_color" value="<?php echo $t1o_top_custom_block_title_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_top_custom_block_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_top_custom_block_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_top_custom_block_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_top_custom_block_content[<?php echo $language_id; ?>]" id="t1o_top_custom_block_content-<?php echo $language_id; ?>"><?php if(isset($t1o_top_custom_block_content[$language_id])) echo $t1o_top_custom_block_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        <div class="form-group">
					        <label class="col-sm-2 control-label">Background Image:</label>
					        <div class="col-sm-10">                                
                                <a href="" id="t1o_top_custom_block_bg_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_top_custom_block_bg_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_top_custom_block_bg" value="<?php echo $t1o_top_custom_block_bg; ?>" id="t1o_top_custom_block_bg" />
					        </div>
				        </div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_top_custom_block_bg_color" id="t1o_top_custom_block_bg_color" value="<?php echo $t1o_top_custom_block_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Text color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_top_custom_block_text_color" id="t1o_top_custom_block_text_color" value="<?php echo $t1o_top_custom_block_text_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-header-news" class="tab-pane">
        
                    <fieldset>

						<legend>News <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_43.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show News Block:</label>
							<div class="col-sm-10">
								<select name="t1o_news_status" class="form-control">
									<option value="0"<?php if($t1o_news_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_news_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="table-responsive">
                        
							<table class="table table-hover">
								<thead>
									<tr>
                                    <th class="left" width="10%">News</th>
									<th class="left" width="10%">Show</th>
									<th class="left" width="40%">Title</th>
									<th class="left" width="40%">URL</th>
									</tr>
								</thead>
                                <tbody>
									<?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <tr>
									<td>News <?php echo $i; ?>:</td>
									<td>
                                    <select name="t1o_news[<?php echo $i; ?>][status]" class="form-control">
									    <option value="0"<?php if($t1o_news[$i]['status'] == '0') echo ' selected="selected"';?>>No</option> 
                                        <option value="1"<?php if($t1o_news[$i]['status'] == '1') echo ' selected="selected"';?>>Yes</option>      
							    	</select>
                                    </td>
                                    <td>
                                    <?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_news[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_news_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_news[$i][$language_id]['title'])) echo $t1o_news[$i][$language_id]['title']; ?>" placeholder="Title" class="form-control" /> 
										</div>
									<?php } ?>
                                    </td>
                                    <td>
                                    <input type="text" name="t1o_news[<?php echo $i; ?>][url]" value="<?php echo $t1o_news[$i]['url']; ?>" class="form-control" />
                                    </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
							</table>
                        
                        </div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_news_bg_color" id="t1o_news_bg_color" value="<?php echo $t1o_news_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Icons color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_news_icons_color" id="t1o_news_icons_color" value="<?php echo $t1o_news_icons_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">"News" word background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_news_word_bg_color" id="t1o_news_word_bg_color" value="<?php echo $t1o_news_word_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">"News" word color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_news_word_color" id="t1o_news_word_color" value="<?php echo $t1o_news_word_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">News color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_news_color" id="t1o_news_color" value="<?php echo $t1o_news_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">News color hover:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_news_hover_color" id="t1o_news_hover_color" value="<?php echo $t1o_news_hover_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-header-custom-block" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Header Custom Block (for Header Style 2) <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_07.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Header Custom Block: </label>
							<div class="col-sm-10">
								<select name="t1o_header_custom_block_1_status" class="form-control">
								    <option value="0"<?php if($t1o_header_custom_block_1_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_header_custom_block_1_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_header_custom_block_1_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_header_custom_block_1_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_header_custom_block_1_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_header_custom_block_1_content[<?php echo $language_id; ?>]" id="t1o_header_custom_block_1_content-<?php echo $language_id; ?>"><?php if(isset($t1o_header_custom_block_1_content[$language_id])) echo $t1o_header_custom_block_1_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-header-search" class="tab-pane">
        
                    <fieldset>
                    
                        <legend class="bn"></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Quick Search Auto-Suggest:</label>
							<div class="col-sm-10">
								<select name="t1o_header_auto_suggest_status" class="form-control">
									<option value="0"<?php if($t1o_header_auto_suggest_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_header_auto_suggest_status == '1') echo ' selected="selected"';?><?php if($t1o_header_auto_suggest_status == '') echo ' selected="selected"';?>>Yes</option>    
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_06.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
   
                        <legend>Popular Search <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_07.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Popular Search: </label>
							<div class="col-sm-10">
								<select name="t1o_header_popular_search_status" class="form-control"> 
                                    <option value="0"<?php if($t1o_header_popular_search_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_header_popular_search_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>

                        <?php for ($i = 1; $i <= 20; $i++) { ?>
                        <div class="form-group">
						    <label class="col-sm-2 control-label">Popular Search <?php echo $i; ?>:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>	
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_header_popular_search[<?php echo $i; ?>][<?php echo $language_id; ?>][word]" id="t1o_header_popular_search_<?php echo $i; ?>_<?php echo $language_id; ?>_word" value="<?php if(isset($t1o_header_popular_search[$i][$language_id]['word'])) echo $t1o_header_popular_search[$i][$language_id]['word']; ?>" placeholder="eg. Jeans" class="form-control" />
										</div>
									<?php } ?>
							</div>
						</div>
                        <?php } ?>

					</fieldset>   
        </div>
     
        </div>
        </div>
        
        </div>
        </div>

        
        
        

        <div class="tab-pane" id="tab-menu"> 
        <div class="row form-horizontal"> 

        <div class="col-sm-2">    
        <ul id="store_features_tabs" class="nav nav-pills nav-stacked">
             <li class="active"><a href="#tab-menu-general" data-toggle="tab">General</a></li>
             <li><a href="#tab-menu-home-link" data-toggle="tab">Home Link</a></li>
             <li><a href="#tab-menu-categories" data-toggle="tab">Categories</a></li>
             <li><a href="#tab-menu-brands" data-toggle="tab">Brands</a></li>
             <li><a href="#tab-menu-custom-blocks" data-toggle="tab">Custom Blocks</a></li>
             <li><a href="#tab-menu-custom-dropdown-menu" data-toggle="tab">Custom Dropdown Menus</a></li>
             <li><a href="#tab-menu-custom-links" data-toggle="tab">Custom Links</a></li>
             <li><a href="#tab-menu-labels" data-toggle="tab">Menu Labels</a></li>
             <li><a href="#tab-menu-custom-bar" data-toggle="tab">Custom Bar</a></li>                        
        </ul> 
        </div>
        
        <div class="col-sm-10">
        <div class="tab-content">
        
        <div id="tab-menu-general" class="tab-pane fade in active">
        
                    <fieldset>
                        
                        <legend class="bn"></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Align Menu Items to:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_align" class="form-control">
                                    <option value="left"<?php if($t1o_menu_align == 'left') echo ' selected="selected"';?>>Left</option>
                                    <option value="center"<?php if($t1o_menu_align == 'center') echo ' selected="selected"';?>>Center</option>
								</select>
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-menu-home-link" class="tab-pane">
        
                    <fieldset>                        
                    
                        <legend>Home Page Link</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Home Page Link style:
                            </label>
							<div class="col-sm-10">
								<select name="t1o_menu_homepage" class="form-control">
                                    <option value="dontshow"<?php if($t1o_menu_homepage == 'dontshow') echo ' selected="selected"';?>>Don't show</option>
                                    <option value="text"<?php if($t1o_menu_homepage == 'text') echo ' selected="selected"';?><?php if($t1o_menu_homepage == '') echo ' selected="selected"';?>>Text</option>
									<option value="icon"<?php if($t1o_menu_homepage == 'icon') echo ' selected="selected"';?>>Icon</option> 
                                    <option value="icontext"<?php if($t1o_menu_homepage == 'icontext') echo ' selected="selected"';?>>Icon + Text</option>          
								</select>
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-menu-categories" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Categories</legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show Categories:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_status" class="form-control">
									<option value="0"<?php if($t1o_menu_categories_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_menu_categories_status == '1') echo ' selected="selected"';?><?php if($t1o_menu_categories_status == '') echo ' selected="selected"';?>>Yes</option>   
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Categories display style:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_style" class="form-control">
									<option value="1"<?php if($t1o_menu_categories_style == '1') echo ' selected="selected"';?>><?php echo $text_opencart; ?></option>
                                    <option value="2"<?php if($t1o_menu_categories_style == '2') echo ' selected="selected"';?>><?php echo $text_vertical; ?></option>
                                    <option value="5"<?php if($t1o_menu_categories_style == '5') echo ' selected="selected"';?>>Vertical 2</option>
                                    <option value="3"<?php if($t1o_menu_categories_style == '3') echo ' selected="selected"';?><?php if($t1o_menu_categories_style == '') echo ' selected="selected"';?>><?php echo $text_horizontal; ?></option>
                                    <option value="4"<?php if($t1o_menu_categories_style == '4') echo ' selected="selected"';?>>Inline</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_08.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show category images:<br /><span class="k_help">for Horizontal, Vertical, Vertical 2 and OpenCart styles</span></label>
							<div class="col-sm-10">
								<select name="t1o_menu_main_category_icon_status" class="form-control">
									<option value="0"<?php if($t1o_menu_main_category_icon_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_menu_main_category_icon_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Category dropdowns padding right:<br /><span class="k_help">for Vertical and OpenCart styles</span></label>
							<div class="col-sm-10">
								<select name="t1o_menu_main_category_padding_right" class="form-control">
									<option value="0"<?php if($t1o_menu_main_category_padding_right == '0') echo ' selected="selected"';?>>0px</option>
                                    <option value="10"<?php if($t1o_menu_main_category_padding_right == '10') echo ' selected="selected"';?>>10px</option>
                                    <option value="20"<?php if($t1o_menu_main_category_padding_right == '20') echo ' selected="selected"';?>>20px</option>
                                    <option value="30"<?php if($t1o_menu_main_category_padding_right == '30') echo ' selected="selected"';?>>30px</option>
                                    <option value="40"<?php if($t1o_menu_main_category_padding_right == '40') echo ' selected="selected"';?>>40px</option>
                                    <option value="50"<?php if($t1o_menu_main_category_padding_right == '50') echo ' selected="selected"';?>>50px</option>
                                    <option value="60"<?php if($t1o_menu_main_category_padding_right == '60') echo ' selected="selected"';?>>60px</option>
                                    <option value="70"<?php if($t1o_menu_main_category_padding_right == '70') echo ' selected="selected"';?>>70px</option>
                                    <option value="80"<?php if($t1o_menu_main_category_padding_right == '80') echo ' selected="selected"';?>>80px</option>
                                    <option value="90"<?php if($t1o_menu_main_category_padding_right == '90') echo ' selected="selected"';?>>90px</option>
                                    <option value="100"<?php if($t1o_menu_main_category_padding_right == '100') echo ' selected="selected"';?>>100px</option>
                                    <option value="110"<?php if($t1o_menu_main_category_padding_right == '110') echo ' selected="selected"';?>>110px</option>
                                    <option value="120"<?php if($t1o_menu_main_category_padding_right == '120') echo ' selected="selected"';?>>120px</option>
                                    <option value="130"<?php if($t1o_menu_main_category_padding_right == '130') echo ' selected="selected"';?>>130px</option>
                                    <option value="140"<?php if($t1o_menu_main_category_padding_right == '140') echo ' selected="selected"';?>>140px</option>
                                    <option value="150"<?php if($t1o_menu_main_category_padding_right == '150') echo ' selected="selected"';?>>150px</option>
                                    <option value="160"<?php if($t1o_menu_main_category_padding_right == '160') echo ' selected="selected"';?>>160px</option>
                                    <option value="170"<?php if($t1o_menu_main_category_padding_right == '170') echo ' selected="selected"';?>>170px</option>
                                    <option value="180"<?php if($t1o_menu_main_category_padding_right == '180') echo ' selected="selected"';?>>180px</option>
                                    <option value="190"<?php if($t1o_menu_main_category_padding_right == '190') echo ' selected="selected"';?>>190px</option>
                                    <option value="200"<?php if($t1o_menu_main_category_padding_right == '200') echo ' selected="selected"';?><?php if($t1o_menu_main_category_padding_right == '') echo ' selected="selected"';?>>200px</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_09.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Category dropdowns padding bottom:<br /><span class="k_help">for Vertical and OpenCart styles</span></label>
							<div class="col-sm-10">
								<select name="t1o_menu_main_category_padding_bottom" class="form-control">
									<option value="0"<?php if($t1o_menu_main_category_padding_bottom == '0') echo ' selected="selected"';?>>0px</option>
                                    <option value="10"<?php if($t1o_menu_main_category_padding_bottom == '10') echo ' selected="selected"';?>>10px</option>
                                    <option value="20"<?php if($t1o_menu_main_category_padding_bottom == '20') echo ' selected="selected"';?>>20px</option>
                                    <option value="30"<?php if($t1o_menu_main_category_padding_bottom == '30') echo ' selected="selected"';?>>30px</option>
                                    <option value="40"<?php if($t1o_menu_main_category_padding_bottom == '40') echo ' selected="selected"';?>>40px</option>
                                    <option value="50"<?php if($t1o_menu_main_category_padding_bottom == '50') echo ' selected="selected"';?>>50px</option>
                                    <option value="60"<?php if($t1o_menu_main_category_padding_bottom == '60') echo ' selected="selected"';?>>60px</option>
                                    <option value="70"<?php if($t1o_menu_main_category_padding_bottom == '70') echo ' selected="selected"';?>>70px</option>
                                    <option value="80"<?php if($t1o_menu_main_category_padding_bottom == '80') echo ' selected="selected"';?>>80px</option>
                                    <option value="90"<?php if($t1o_menu_main_category_padding_bottom == '90') echo ' selected="selected"';?>>90px</option>
                                    <option value="100"<?php if($t1o_menu_main_category_padding_bottom == '100') echo ' selected="selected"';?>>100px</option>
                                    <option value="110"<?php if($t1o_menu_main_category_padding_bottom == '110') echo ' selected="selected"';?><?php if($t1o_menu_main_category_padding_bottom == '') echo ' selected="selected"';?>>110px</option>
                                    <option value="120"<?php if($t1o_menu_main_category_padding_bottom == '120') echo ' selected="selected"';?>>120px</option>
                                    <option value="130"<?php if($t1o_menu_main_category_padding_bottom == '130') echo ' selected="selected"';?>>130px</option>
                                    <option value="140"<?php if($t1o_menu_main_category_padding_bottom == '140') echo ' selected="selected"';?>>140px</option>
                                    <option value="150"<?php if($t1o_menu_main_category_padding_bottom == '150') echo ' selected="selected"';?>>150px</option>
                                    <option value="160"<?php if($t1o_menu_main_category_padding_bottom == '160') echo ' selected="selected"';?>>160px</option>
                                    <option value="170"<?php if($t1o_menu_main_category_padding_bottom == '170') echo ' selected="selected"';?>>170px</option>
                                    <option value="180"<?php if($t1o_menu_main_category_padding_bottom == '180') echo ' selected="selected"';?>>180px</option>
                                    <option value="190"<?php if($t1o_menu_main_category_padding_bottom == '190') echo ' selected="selected"';?>>190px</option>
                                    <option value="200"<?php if($t1o_menu_main_category_padding_bottom == '200') echo ' selected="selected"';?>>200px</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_09.jpg" target="_blank"><span class="k_help_tip">?</span></a> 
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Categories per row:<br /><span class="k_help">for Horizontal style</span></label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_per_row" class="form-control">
									<option value="col-sm-4"<?php if($t1o_menu_categories_per_row == 'col-sm-4') echo ' selected="selected"';?>>3</option>
                                    <option value="col-sm-3"<?php if($t1o_menu_categories_per_row == 'col-sm-3') echo ' selected="selected"';?><?php if($t1o_menu_categories_per_row == '') echo ' selected="selected"';?>>4</option>
                                    <option value="col-sm-5-pr"<?php if($t1o_menu_categories_per_row == 'col-sm-5-pr') echo ' selected="selected"';?>>5</option>
                                    <option value="col-sm-2"<?php if($t1o_menu_categories_per_row == 'col-sm-2') echo ' selected="selected"';?>>6</option>
                                    <option value="col-sm-7-pr"<?php if($t1o_menu_categories_per_row == 'col-sm-7-pr') echo ' selected="selected"';?>>7</option>
                                    <option value="col-sm-8-pr"<?php if($t1o_menu_categories_per_row == 'col-sm-8-pr') echo ' selected="selected"';?>>8</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show 3 level category:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_3_level" class="form-control">
									<option value="0"<?php if($t1o_menu_categories_3_level == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_menu_categories_3_level == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Category dropdown on Home Page:<br /><span class="k_help">for "Vertical 2" style</span></label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_home_visibility" class="form-control">
                                    <option value="0"<?php if($t1o_menu_categories_home_visibility == '0') echo ' selected="selected"';?>>On Hover</option>
                                    <option value="1"<?php if($t1o_menu_categories_home_visibility == '1') echo ' selected="selected"';?><?php if($t1o_menu_categories_home_visibility == '') echo ' selected="selected"';?>>Always Visible</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_08.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <legend>Custom Blocks for Horizontal style <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_11.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Left Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_custom_block_left_status" class="form-control">
									<option value="0"<?php if($t1o_menu_categories_custom_block_left_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_menu_categories_custom_block_left_status == '1') echo ' selected="selected"';?>>Yes</option> 
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Left Custom Block Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_menu_categories_custom_block_left_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_menu_categories_custom_block_left_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_menu_categories_custom_block_left_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_menu_categories_custom_block_left_content[<?php echo $language_id; ?>]" id="t1o_menu_categories_custom_block_left_content-<?php echo $language_id; ?>"><?php if(isset($t1o_menu_categories_custom_block_left_content[$language_id])) echo $t1o_menu_categories_custom_block_left_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Right Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_custom_block_right_status" class="form-control">
									<option value="0"<?php if($t1o_menu_categories_custom_block_right_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_menu_categories_custom_block_right_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Right Custom Block Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_menu_categories_custom_block_right_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_menu_categories_custom_block_right_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_menu_categories_custom_block_right_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_menu_categories_custom_block_right_content[<?php echo $language_id; ?>]" id="t1o_menu_categories_custom_block_right_content-<?php echo $language_id; ?>"><?php if(isset($t1o_menu_categories_custom_block_right_content[$language_id])) echo $t1o_menu_categories_custom_block_right_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show Bottom Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_categories_custom_block_status" class="form-control">
									<option value="0"<?php if($t1o_menu_categories_custom_block_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_menu_categories_custom_block_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Bottom Custom Block Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_menu_categories_custom_block_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_menu_categories_custom_block_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_menu_categories_custom_block_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_menu_categories_custom_block_content[<?php echo $language_id; ?>]" id="t1o_menu_categories_custom_block_content-<?php echo $language_id; ?>"><?php if(isset($t1o_menu_categories_custom_block_content[$language_id])) echo $t1o_menu_categories_custom_block_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-menu-brands" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Brands <span class="k_help_tip k_tooltip" title="Before you turn on this option, add at least one manufacturer." data-toggle="tooltip">?</span></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show Brands:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_brands_status" class="form-control">
									<option value="0"<?php if($t1o_menu_brands_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_menu_brands_status == '1') echo ' selected="selected"';?><?php if($t1o_menu_brands_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Brands display style:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_brands_style" class="form-control">
									<option value="logoname"<?php if($t1o_menu_brands_style == 'logoname') echo ' selected="selected"';?>><?php echo $text_brand_logo_name; ?></option>  
                                    <option value="logo"<?php if($t1o_menu_brands_style == 'logo') echo ' selected="selected"';?>><?php echo $text_brand_logo; ?></option>
                                    <option value="name"<?php if($t1o_menu_brands_style == 'name') echo ' selected="selected"';?><?php if($t1o_menu_brands_style == '') echo ' selected="selected"';?>><?php echo $text_brand_name; ?></option>
                                             
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Brands per row:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_brands_per_row" class="form-control">
									<option value="4"<?php if($t1o_menu_brands_per_row == '4') echo ' selected="selected"';?>>3</option>
                                    <option value="3"<?php if($t1o_menu_brands_per_row == '3') echo ' selected="selected"';?>>4</option>
                                    <option value="2"<?php if($t1o_menu_brands_per_row == '2') echo ' selected="selected"';?><?php if($t1o_menu_brands_per_row == '') echo ' selected="selected"';?>>6</option>
                                    <option value="8-pr"<?php if($t1o_menu_brands_per_row == '8-pr') echo ' selected="selected"';?>>8</option>
                                    <option value="10-pr"<?php if($t1o_menu_brands_per_row == '10-pr') echo ' selected="selected"';?>>10</option>
                                    <option value="1"<?php if($t1o_menu_brands_per_row == '1') echo ' selected="selected"';?>>12</option>         
								</select>
							</div>
						</div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-menu-custom-blocks" class="tab-pane">
        
                    <fieldset>
                        
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        
                        <legend>Custom Block <?php echo $i; ?><a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_13.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Block <?php echo $i; ?>:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_custom_block[<?php echo $i; ?>][status]" class="form-control">
								    <option value="0"<?php if($t1o_menu_custom_block[$i]['status'] == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_menu_custom_block[$i]['status'] == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_menu_custom_block[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_menu_custom_block_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_menu_custom_block[$i][$language_id]['title'])) echo $t1o_menu_custom_block[$i][$language_id]['title']; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_menu_custom_block_<?php echo $i; ?>" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_menu_custom_block_<?php echo $i; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_menu_custom_block_<?php echo $i; ?>_<?php echo $language_id; ?>" class="tab-pane">
                                            <textarea name="t1o_menu_custom_block[<?php echo $i ?>][<?php echo $language_id; ?>][content]" id="t1o_menu_custom_block_<?php echo $i ?>_<?php echo $language_id; ?>_content"><?php if(isset($t1o_menu_custom_block[$i][$language_id]['content'])) echo $t1o_menu_custom_block[$i][$language_id]['content']; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <?php } ?>
                        
                    </fieldset>
                    
        </div>
        
        <div id="tab-menu-custom-dropdown-menu" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Custom Dropdown Menu 1<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_12.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Menu 1:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_cm_status" class="form-control">
									<option value="0"<?php if($t1o_menu_cm_status == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_menu_cm_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
						    <label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
								<?php $language_id=$language['language_id']; ?>
									<div class="input-group">
										<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                        <input type="text" name="t1o_menu_cm_title[<?php echo $language_id; ?>]" id="t1o_menu_cm_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_menu_cm_title[$language_id])) echo $t1o_menu_cm_title[$language_id]; ?>" placeholder="Title" class="form-control" />  
									</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="table-responsive">
                        
							<table class="table table-hover">
								<thead>
									<tr>
                                    <th class="left" width="10%">Link</th>
									<th class="left" width="10%">Show</th>
									<th class="left" width="30%">Title</th>
									<th class="left" width="30%">URL</th>
									<th class="left" width="20%">Open</th>
									</tr>
								</thead>
                                <tbody>
									<?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <tr>
									<td>Link <?php echo $i; ?>:</td>
									<td>
    
                                    <select name="t1o_menu_cm_link[<?php echo $i; ?>][status]" class="form-control">
									    <option value="0"<?php if($t1o_menu_cm_link[$i]['status'] == '0') echo ' selected="selected"';?>>No</option> 
                                        <option value="1"<?php if($t1o_menu_cm_link[$i]['status'] == '1') echo ' selected="selected"';?>>Yes</option>      
							    	</select>

                                    </td>
                                    <td>
                                    
                                    <?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_menu_cm_link[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_menu_cm_link_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_menu_cm_link[$i][$language_id]['title'])) echo $t1o_menu_cm_link[$i][$language_id]['title']; ?>" placeholder="Title" class="form-control" />
										</div>
									<?php } ?>
                                    
                                    </td>
                                    <td>
                                    
                                    <input type="text" name="t1o_menu_cm_link[<?php echo $i; ?>][url]" value="<?php echo $t1o_menu_cm_link[$i]['url']; ?>" class="form-control" />
                                    
                                    </td>
                                    <td>
                                    
                                    <select name="t1o_menu_cm_link[<?php echo $i; ?>][target]" class="form-control">
										<?php foreach ($menu_link_target as $fv => $fc) { ?>
											<?php ($fv ==  $t1o_menu_cm_link[$i]['target']) ? $current = 'selected' : $current=''; ?>
											<option value="<?php echo $fv; ?>" <?php echo $current; ?> ><?php echo $fc; ?></option>	
										<?php } ?>
									</select>
                                    
                                    </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
							</table>
                        
                        </div>
                        
                        <legend>Custom Dropdown Menu 2</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Menu 2:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_cm_2_status" class="form-control">
									<option value="0"<?php if($t1o_menu_cm_2_status == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_menu_cm_2_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
						    <label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
								<?php $language_id=$language['language_id']; ?>
									<div class="input-group">
										<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                        <input type="text" name="t1o_menu_cm_2_title[<?php echo $language_id; ?>]" id="t1o_menu_cm_2_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_menu_cm_2_title[$language_id])) echo $t1o_menu_cm_2_title[$language_id]; ?>" placeholder="Title" class="form-control" />  
									</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="table-responsive">
                        
							<table class="table table-hover">
								<thead>
									<tr>
                                    <th class="left" width="10%">Link</th>
									<th class="left" width="10%">Show</th>
									<th class="left" width="30%">Title</th>
									<th class="left" width="30%">URL</th>
									<th class="left" width="20%">Open</th>
									</tr>
								</thead>
                                <tbody>
									<?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <tr>
									<td>Link <?php echo $i; ?>:</td>
									<td>
    
                                    <select name="t1o_menu_cm_2_link[<?php echo $i; ?>][status]" class="form-control">
									    <option value="0"<?php if($t1o_menu_cm_2_link[$i]['status'] == '0') echo ' selected="selected"';?>>No</option> 
                                        <option value="1"<?php if($t1o_menu_cm_2_link[$i]['status'] == '1') echo ' selected="selected"';?>>Yes</option>      
							    	</select>

                                    </td>
                                    <td>
                                    
                                    <?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_menu_cm_2_link[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_menu_cm_2_link_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_menu_cm_2_link[$i][$language_id]['title'])) echo $t1o_menu_cm_2_link[$i][$language_id]['title']; ?>" placeholder="Title" class="form-control" />
										</div>
									<?php } ?>
                                    
                                    </td>
                                    <td>
                                    
                                    <input type="text" name="t1o_menu_cm_2_link[<?php echo $i; ?>][url]" value="<?php echo $t1o_menu_cm_2_link[$i]['url']; ?>" class="form-control" />
                                    
                                    </td>
                                    <td>
                                    
                                    <select name="t1o_menu_cm_2_link[<?php echo $i; ?>][target]" class="form-control">
										<?php foreach ($menu_link_target as $fv => $fc) { ?>
											<?php ($fv ==  $t1o_menu_cm_2_link[$i]['target']) ? $current = 'selected' : $current=''; ?>
											<option value="<?php echo $fv; ?>" <?php echo $current; ?> ><?php echo $fc; ?></option>	
										<?php } ?>
									</select>
                                    
                                    </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
							</table>
                        
                        </div>
                        
                        <legend>Custom Dropdown Menu 3</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Menu 3:</label>
							<div class="col-sm-10">
								<select name="t1o_menu_cm_3_status" class="form-control">
									<option value="0"<?php if($t1o_menu_cm_3_status == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_menu_cm_3_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
						    <label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
								<?php $language_id=$language['language_id']; ?>
									<div class="input-group">
										<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                        <input type="text" name="t1o_menu_cm_3_title[<?php echo $language_id; ?>]" id="t1o_menu_cm_3_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_menu_cm_3_title[$language_id])) echo $t1o_menu_cm_3_title[$language_id]; ?>" placeholder="Title" class="form-control" />  
									</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="table-responsive">
                        
							<table class="table table-hover">
								<thead>
									<tr>
                                    <th class="left" width="10%">Link</th>
									<th class="left" width="10%">Show</th>
									<th class="left" width="30%">Title</th>
									<th class="left" width="30%">URL</th>
									<th class="left" width="20%">Open</th>
									</tr>
								</thead>
                                <tbody>
									<?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <tr>
									<td>Link <?php echo $i; ?>:</td>
									<td>
    
                                    <select name="t1o_menu_cm_3_link[<?php echo $i; ?>][status]" class="form-control">
									    <option value="0"<?php if($t1o_menu_cm_3_link[$i]['status'] == '0') echo ' selected="selected"';?>>No</option> 
                                        <option value="1"<?php if($t1o_menu_cm_3_link[$i]['status'] == '1') echo ' selected="selected"';?>>Yes</option>      
							    	</select>

                                    </td>
                                    <td>
                                    
                                    <?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_menu_cm_3_link[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_menu_cm_3_link_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_menu_cm_3_link[$i][$language_id]['title'])) echo $t1o_menu_cm_3_link[$i][$language_id]['title']; ?>" placeholder="Title" class="form-control" />
										</div>
									<?php } ?>
                                    
                                    </td>
                                    <td>
                                    
                                    <input type="text" name="t1o_menu_cm_3_link[<?php echo $i; ?>][url]" value="<?php echo $t1o_menu_cm_3_link[$i]['url']; ?>" class="form-control" />
                                    
                                    </td>
                                    <td>
                                    
                                    <select name="t1o_menu_cm_3_link[<?php echo $i; ?>][target]" class="form-control">
										<?php foreach ($menu_link_target as $fv => $fc) { ?>
											<?php ($fv ==  $t1o_menu_cm_3_link[$i]['target']) ? $current = 'selected' : $current=''; ?>
											<option value="<?php echo $fv; ?>" <?php echo $current; ?> ><?php echo $fc; ?></option>	
										<?php } ?>
									</select>
                                    
                                    </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
							</table>
                        
                        </div>
                        
                    </fieldset>
                    
        </div>
        
        <div id="tab-menu-custom-links" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Custom Links <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_11.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="table-responsive">
                        
							<table class="table table-hover">
								<thead>
									<tr>
                                    <th class="left" width="10%">Link</th>
									<th class="left" width="10%">Show</th>
									<th class="left" width="30%">Title</th>
									<th class="left" width="30%">URL</th>
									<th class="left" width="20%">Open</th>
									</tr>
								</thead>
                                <tbody>
									<?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <tr>
									<td>Link <?php echo $i; ?>:</td>
									<td>
                                    <select name="t1o_menu_link[<?php echo $i; ?>][status]" class="form-control">
									    <option value="0"<?php if($t1o_menu_link[$i]['status'] == '0') echo ' selected="selected"';?>>No</option> 
                                        <option value="1"<?php if($t1o_menu_link[$i]['status'] == '1') echo ' selected="selected"';?>>Yes</option>      
							    	</select>
                                    </td>
                                    <td>
                                    <?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_menu_link[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_menu_link_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_menu_link[$i][$language_id]['title'])) echo $t1o_menu_link[$i][$language_id]['title']; ?>" placeholder="Title" class="form-control" /> 
										</div>
									<?php } ?>
                                    </td>
                                    <td>
                                    <input type="text" name="t1o_menu_link[<?php echo $i; ?>][url]" value="<?php echo $t1o_menu_link[$i]['url']; ?>" class="form-control" />
                                    </td>
                                    <td>
                                    
                                    <select name="t1o_menu_link[<?php echo $i; ?>][target]" class="form-control">
										<?php foreach ($menu_link_target as $fv => $fc) { ?>
											<?php ($fv ==  $t1o_menu_link[$i]['target']) ? $current = 'selected' : $current=''; ?>
											<option value="<?php echo $fv; ?>" <?php echo $current; ?> ><?php echo $fc; ?></option>	
										<?php } ?>
									</select>
                                    
                                    </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
							</table>
                        
                        </div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-menu-labels" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Menu Labels <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_14.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="table-responsive">
                        
							<table class="table table-hover">
									<?php for ($i = 1; $i <= 15; $i++) { ?>
                                    <tr>
									<td width="15%" style="text-align:right;">Menu Label <?php echo $i; ?>:</td>
									<td width="35%">
                                    <?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>	
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_menu_labels[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_menu_labels_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_menu_labels[$i][$language_id]['title'])) echo $t1o_menu_labels[$i][$language_id]['title']; ?>" placeholder="Label" class="form-control" />
										</div>
									<?php } ?>
                                    </td>
                                    <td width="15%" style="text-align:right;">Background color:</td>
                                    <td width="35%">
                                    <input type="text" name="t1o_menu_labels_color[<?php echo $i; ?>]" id="t1o_menu_labels_color_[<?php echo $i; ?>]" value="<?php echo $t1o_menu_labels_color[$i]; ?>" class="color {required:false,hash:true}" size="8" />
                                    </td>
                                    </tr>
                                    <?php } ?>
							</table>
                        
                        </div>
                        
                    </fieldset>
                    
        </div>

        <div id="tab-menu-custom-bar" class="tab-pane">
        
                    <fieldset>
                        
                        <legend>Custom Bar below Main Menu <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_15.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Bar:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_bar_below_menu_status" class="form-control">
									<option value="0"<?php if($t1o_custom_bar_below_menu_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_bar_below_menu_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_custom_bar_below_menu_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_custom_bar_below_menu_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_custom_bar_below_menu_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_custom_bar_below_menu_content[<?php echo $language_id; ?>]" id="t1o_custom_bar_below_menu_content-<?php echo $language_id; ?>"><?php if(isset($t1o_custom_bar_below_menu_content[$language_id])) echo $t1o_custom_bar_below_menu_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        <div class="form-group">
					        <label class="col-sm-2 control-label">Background Image:</label>
					        <div class="col-sm-10">                                
                                <a href="" id="t1o_custom_bar_below_menu_bg_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_custom_bar_below_menu_bg_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_custom_bar_below_menu_bg" value="<?php echo $t1o_custom_bar_below_menu_bg; ?>" id="t1o_custom_bar_below_menu_bg" />
					        </div>
				        </div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Background Image Animation:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_bar_below_menu_bg_animation" class="form-control">
									<option value="0"<?php if($t1o_custom_bar_below_menu_bg_animation == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_bar_below_menu_bg_animation == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_custom_bar_below_menu_bg_color" id="t1o_custom_bar_below_menu_bg_color" value="<?php echo $t1o_custom_bar_below_menu_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Text color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_custom_bar_below_menu_text_color" id="t1o_custom_bar_below_menu_text_color" value="<?php echo $t1o_custom_bar_below_menu_text_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>

					</fieldset> 
     
        </div>
        
        </div>
        </div>
        
        </div>
        </div>




        <div class="tab-pane" id="tab-midsection"> 
        <div class="row form-horizontal">  
        
        <div class="col-sm-2">    
        <ul id="midsection_tabs" class="nav nav-pills nav-stacked">
             <li class="active"><a href="#tab-midsection-category" data-toggle="tab">Category Page</a></li>
             <li><a href="#tab-midsection-product" data-toggle="tab">Product Page</a></li>
             <li><a href="#tab-midsection-contact" data-toggle="tab">Contact Page</a></li>
             <li><a href="#tab-midsection-lf" data-toggle="tab">Left/Right Column</a></li>                                      
        </ul> 
        </div>
        
        <div class="col-sm-10">
        <div class="tab-content">
        
        <div id="tab-midsection-category" class="tab-pane fade in active">  
        
                    <fieldset>
                    
                        <legend>Category Title <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_16.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Category Title position:</label>
							<div class="col-sm-10">
								<select name="t1o_category_title_position" class="form-control">
									<option value="0"<?php if($t1o_category_title_position == '0') echo ' selected="selected"';?>>Above Content Column</option>
                                    <option value="1"<?php if($t1o_category_title_position == '1') echo ' selected="selected"';?><?php if($t1o_category_title_position == '') echo ' selected="selected"';?>>Content Column</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Category Title color:<br /><span class="k_help">for "Above Content Column" position</span></label>
							<div class="col-sm-10">
								<input type="text" name="t1o_category_title_above_color" id="t1o_category_title_above_color" value="<?php echo $t1o_category_title_above_color; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>
                        
                        <legend>Category Info <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_17.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show category description:</label>
							<div class="col-sm-10">
								<select name="t1o_category_desc_status" class="form-control">
									<option value="0"<?php if($t1o_category_desc_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_desc_status == '1') echo ' selected="selected"';?><?php if($t1o_category_desc_status == '') echo ' selected="selected"';?>>Yes</option>  
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show category image:</label>
							<div class="col-sm-10">
								<select name="t1o_category_img_status" class="form-control">
									<option value="0"<?php if($t1o_category_img_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_img_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Parallax scrolling effect:<br /><span class="k_help">for "Above Content Column" category title position</span></label>
                            <div class="col-sm-10">
								<select name="t1o_category_img_parallax" class="form-control">
									<option value="0"<?php if($t1o_category_img_parallax == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_img_parallax == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
				        </div>
                        
                        <legend>Subcategories <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_19.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show subcategories:</label>
							<div class="col-sm-10">
								<select name="t1o_category_subcategories_status" class="form-control">
									<option value="0"<?php if($t1o_category_subcategories_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_subcategories_status == '1') echo ' selected="selected"';?><?php if($t1o_category_subcategories_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Subcategories style:</label>
							<div class="col-sm-10">
								<select name="t1o_category_subcategories_style" class="form-control">
									<option value="0"<?php if($t1o_category_subcategories_style == '0') echo ' selected="selected"';?>>SELLMORE Theme</option>
                                    <option value="1"<?php if($t1o_category_subcategories_style == '1') echo ' selected="selected"';?>>OpenCart</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Subcategories per row:<br /><span class="k_help">for "SELLMORE Theme" style</span></label>
							<div class="col-sm-10">
								<select name="t1o_category_subcategories_per_row" class="form-control">
                           			<option value="2"<?php if($t1o_category_subcategories_per_row == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_category_subcategories_per_row == '3') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_category_subcategories_per_row == '4') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_category_subcategories_per_row == '5') echo ' selected="selected"';?><?php if($t1o_category_subcategories_per_row == '') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_category_subcategories_per_row == '6') echo ' selected="selected"';?>>6</option>
                                    <option value="7"<?php if($t1o_category_subcategories_per_row == '7') echo ' selected="selected"';?>>7</option>
                                    <option value="8"<?php if($t1o_category_subcategories_per_row == '8') echo ' selected="selected"';?>>8</option>
								</select>  
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">AutoPlay:</label>
							<div class="col-sm-10">
								<select name="t1o_category_subcategories_autoplay" class="form-control">
									<option value="4000"<?php if($t1o_category_subcategories_autoplay == '4000') echo ' selected="selected"';?>>Yes</option>
                                    <option value="false"<?php if($t1o_category_subcategories_autoplay == 'false') echo ' selected="selected"';?>>No</option>
								</select>
							</div>
						</div>

						<legend>Product Box <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_20.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Product Box style:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_box_style" class="form-control">
                                    <option value="product-box-style-1"<?php if($t1o_category_prod_box_style == 'product-box-style-1') echo ' selected="selected"';?>>Style 1</option>
									<option value="product-box-style-2"<?php if($t1o_category_prod_box_style == 'product-box-style-2') echo ' selected="selected"';?><?php if($t1o_category_prod_box_style == '') echo ' selected="selected"';?>>Style 2</option>
                                    <option value="product-box-style-3"<?php if($t1o_category_prod_box_style == 'product-box-style-3') echo ' selected="selected"';?>>Style 3</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_01.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show sale badge:</label>
							<div class="col-sm-10">
								<select name="t1o_sale_badge_status" class="form-control">
									<option value="0"<?php if($t1o_sale_badge_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_sale_badge_status == '1') echo ' selected="selected"';?><?php if($t1o_sale_badge_status == '') echo ' selected="selected"';?>>Yes</option>
                                    
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Sale badge type:</label>
							<div class="col-sm-10">
								<select name="t1o_sale_badge_type" class="form-control">
									<option value="0"<?php if($t1o_sale_badge_type == '0') echo ' selected="selected"';?>>SALE text</option>
                                    <option value="1"<?php if($t1o_sale_badge_type == '1') echo ' selected="selected"';?><?php if($t1o_sale_badge_type == '') echo ' selected="selected"';?>>Percent</option>
								</select>
							</div>
						</div>
                                       
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show new product badge:</label>
							<div class="col-sm-10">
								<select name="t1o_new_badge_status" class="form-control">
									<option value="0"<?php if($t1o_new_badge_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_new_badge_status == '1') echo ' selected="selected"';?><?php if($t1o_new_badge_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show "Out of Stock" badge:</label>
							<div class="col-sm-10">
								<select name="t1o_out_of_stock_badge_status" class="form-control">
									<option value="0"<?php if($t1o_out_of_stock_badge_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_out_of_stock_badge_status == '1') echo ' selected="selected"';?><?php if($t1o_out_of_stock_badge_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show product name:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_name_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_name_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_name_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_name_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show product brand:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_brand_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_brand_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_brand_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_brand_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show product price:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_price_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_price_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_price_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_price_status == '') echo ' selected="selected"';?>>Yes</option> 
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show "Quick View" button:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_quickview_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_quickview_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_quickview_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_quickview_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show "Add to Cart" button:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_cart_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_cart_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_cart_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_cart_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show rating stars:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_ratings_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_ratings_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_ratings_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_ratings_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show "Add to Wishlist", "Add to Compare" buttons:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_wis_com_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_wis_com_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_wis_com_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_wis_com_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show zoom image effect:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_zoom_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_zoom_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_zoom_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_zoom_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show swap image effect:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_swap_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_swap_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_swap_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_swap_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Box hover shadow:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_shadow_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_shadow_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_shadow_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_shadow_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Box hover lift up:</label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_lift_up_status" class="form-control">
									<option value="0"<?php if($t1o_category_prod_lift_up_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_category_prod_lift_up_status == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_lift_up_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Align items to:<br /><span class="k_help">for Grid view</span></label>
							<div class="col-sm-10">
								<select name="t1o_category_prod_align" class="form-control">
                                    <option value="0"<?php if($t1o_category_prod_align == '0') echo ' selected="selected"';?>>Center</option>
                                    <option value="1"<?php if($t1o_category_prod_align == '1') echo ' selected="selected"';?><?php if($t1o_category_prod_align == '') echo ' selected="selected"';?>>Left</option>
								</select>
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-midsection-product" class="tab-pane">  
        
                    <fieldset>
                        
                        <legend class="bn"></legend> 
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Prev/Next products:</label>
							<div class="col-sm-10">
								<select name="t1o_product_prev_next_status" class="form-control">
									<option value="0"<?php if($t1o_product_prev_next_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_product_prev_next_status == '1') echo ' selected="selected"';?><?php if($t1o_product_prev_next_status == '') echo ' selected="selected"';?>>Yes</option>  
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_21.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <legend>Product Page Layout</legend> 
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Product Page Layout:</label>
							<div class="col-sm-10">
								<select name="t1o_layout_product_page" class="form-control">
									<option value="1"<?php if($t1o_layout_product_page == '1') echo ' selected="selected"';?>>Layout 1</option>
                           			<option value="2"<?php if($t1o_layout_product_page == '2') echo ' selected="selected"';?>>Layout 2</option>
                           			<option value="3"<?php if($t1o_layout_product_page == '3') echo ' selected="selected"';?>>Layout 3</option>
                           			<option value="4"<?php if($t1o_layout_product_page == '4') echo ' selected="selected"';?>>Layout 4</option> 
                           			<option value="5"<?php if($t1o_layout_product_page == '5') echo ' selected="selected"';?><?php if($t1o_layout_product_page == '') echo ' selected="selected"';?>>Layout 5</option>
                           			<option value="6"<?php if($t1o_layout_product_page == '6') echo ' selected="selected"';?>>Layout 6</option>
                           			<option value="7"<?php if($t1o_layout_product_page == '7') echo ' selected="selected"';?>>Layout 7</option>    
                           			<option value="8"<?php if($t1o_layout_product_page == '8') echo ' selected="selected"';?>>Layout 8</option>
                           			<option value="9"<?php if($t1o_layout_product_page == '9') echo ' selected="selected"';?>>Layout 9</option>
                           			<option value="10"<?php if($t1o_layout_product_page == '10') echo ' selected="selected"';?>>Layout 10</option>
                                    <option value="11"<?php if($t1o_layout_product_page == '11') echo ' selected="selected"';?>>Layout 11</option>
                                    <option value="12"<?php if($t1o_layout_product_page == '12') echo ' selected="selected"';?>>Layout 12</option>
                                    <option value="13"<?php if($t1o_layout_product_page == '13') echo ' selected="selected"';?>>Layout 13</option>
								</select>  
							</div>
						</div>
                        
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php for ($ppl = 1; $ppl <= 4; $ppl++) { ?>
                                <div class="prod_l"><img src="view/image/theme_img/pl_<?php echo $ppl; ?>.png"><span class="t1o_help prod_l_nr">Layout <?php echo $ppl; ?></span></div> 
						        <?php } ?>	
						    </div>   
                            <div class="col-sm-12">
                                <?php for ($ppl = 5; $ppl <= 8; $ppl++) { ?>
                                <div class="prod_l"><img src="view/image/theme_img/pl_<?php echo $ppl; ?>.png"><span class="t1o_help prod_l_nr">Layout <?php echo $ppl; ?></span></div> 
						        <?php } ?>	
						    </div>
                            <div class="col-sm-12">
                                <?php for ($ppl = 9; $ppl <= 12; $ppl++) { ?>
                                <div class="prod_l"><img src="view/image/theme_img/pl_<?php echo $ppl; ?>.png"><span class="t1o_help prod_l_nr">Layout <?php echo $ppl; ?></span></div> 
						        <?php } ?>	
						    </div>
                            <div class="col-sm-12">
                                <?php for ($ppl = 13; $ppl <= 13; $ppl++) { ?>
                                <div class="prod_l"><img src="view/image/theme_img/pl_<?php echo $ppl; ?>.png"><span class="t1o_help prod_l_nr">Layout <?php echo $ppl; ?></span></div> 
						        <?php } ?>	
						    </div>
                        </div>

						<legend>Product Name <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_22.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Position:</label>
							<div class="col-sm-10">
								<select name="t1o_product_name_position" class="form-control">
                                    <option value="0"<?php if($t1o_product_name_position == '0') echo ' selected="selected"';?>>Buy Column</option>
									<option value="1"<?php if($t1o_product_name_position == '1') echo ' selected="selected"';?>>Content Column</option>
                                    <option value="2"<?php if($t1o_product_name_position == '2') echo ' selected="selected"';?>>Above Content Column</option>
								</select>
							</div>
						</div>
                        
                        <legend>Product Images</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Thumbnails in a row:</label>
							<div class="col-sm-10">
								<select name="t1o_additional_images" class="form-control">
									<option value="1"<?php if($t1o_additional_images == '1') echo ' selected="selected"';?>>1</option>
                           			<option value="2"<?php if($t1o_additional_images == '2') echo ' selected="selected"';?>>2</option>
                           			<option value="3"<?php if($t1o_additional_images == '3') echo ' selected="selected"';?><?php if($t1o_additional_images == '') echo ' selected="selected"';?>>3</option>
                           			<option value="4"<?php if($t1o_additional_images == '4') echo ' selected="selected"';?>>4</option> 
                           			<option value="5"<?php if($t1o_additional_images == '5') echo ' selected="selected"';?>>5</option>
                           			<option value="6"<?php if($t1o_additional_images == '6') echo ' selected="selected"';?>>6</option>
                           			<option value="7"<?php if($t1o_additional_images == '7') echo ' selected="selected"';?>>7</option>    
                           			<option value="8"<?php if($t1o_additional_images == '8') echo ' selected="selected"';?>>8</option>
                           			<option value="9"<?php if($t1o_additional_images == '9') echo ' selected="selected"';?>>9</option>
                           			<option value="10"<?php if($t1o_additional_images == '10') echo ' selected="selected"';?>>10</option>
								</select>  
							</div>
						</div>
                        
                        <legend>Buy Section</legend>
		                    
                        <div class="form-group">
							<label class="col-sm-2 control-label">Align items to:</label>
							<div class="col-sm-10">
								<select name="t1o_product_align" class="form-control">
                                    <option value="0"<?php if($t1o_product_align == '0') echo ' selected="selected"';?>>Center</option>
                                    <option value="1"<?php if($t1o_product_align == '1') echo ' selected="selected"';?><?php if($t1o_product_align == '') echo ' selected="selected"';?>>Left</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_23.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show manufacturer logo:</label>
							<div class="col-sm-10">
								<select name="t1o_product_manufacturer_logo_status" class="form-control">
									<option value="0"<?php if($t1o_product_manufacturer_logo_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_product_manufacturer_logo_status == '1') echo ' selected="selected"';?><?php if($t1o_product_manufacturer_logo_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_24.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Save Percent:</label>
							<div class="col-sm-10">
								<select name="t1o_product_save_percent_status" class="form-control">
									<option value="0"<?php if($t1o_product_save_percent_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_product_save_percent_status == '1') echo ' selected="selected"';?><?php if($t1o_product_save_percent_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_25.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Tax:</label>
							<div class="col-sm-10">
								<select name="t1o_product_tax_status" class="form-control">
									<option value="0"<?php if($t1o_product_tax_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_product_tax_status == '1') echo ' selected="selected"';?><?php if($t1o_product_tax_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_26.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show product viewed:</label>
							<div class="col-sm-10">
								<select name="t1o_product_viewed_status" class="form-control">
									<option value="0"<?php if($t1o_product_viewed_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_product_viewed_status == '1') echo ' selected="selected"';?><?php if($t1o_product_viewed_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_27.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Increment/Decrement a Quantity:</label>
							<div class="col-sm-10">
								<select name="t1o_product_i_c_status" class="form-control">
									<option value="0"<?php if($t1o_product_i_c_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_product_i_c_status == '1') echo ' selected="selected"';?><?php if($t1o_product_i_c_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_28.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <legend>Related Products</legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show related products:</label>
							<div class="col-sm-10">
								<select name="t1o_product_related_status" class="form-control">
									<option value="0"<?php if($t1o_product_related_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_product_related_status == '1') echo ' selected="selected"';?><?php if($t1o_product_related_status == '') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Related products position:</label>
							<div class="col-sm-10">
								<select name="t1o_product_related_position" class="form-control">
									<option value="0"<?php if($t1o_product_related_position == '0') echo ' selected="selected"';?>>Right</option>
                                    <option value="1"<?php if($t1o_product_related_position == '1') echo ' selected="selected"';?><?php if($t1o_product_related_position == '') echo ' selected="selected"';?>>Bottom</option>
								</select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_29.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Related products style:<br /><span class="k_help">only bottom position</span></label>
							<div class="col-sm-10">
								<select name="t1o_product_related_style" class="form-control">
									<option value="0"<?php if($t1o_product_related_style == '0') echo ' selected="selected"';?>>Grid</option>
                                    <option value="1"<?php if($t1o_product_related_style == '1') echo ' selected="selected"';?><?php if($t1o_product_related_style == '') echo ' selected="selected"';?>>Slider</option>
								</select>
							</div>
						</div>
                        
                        <legend>Feature Box 1<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_30.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
					        <label class="col-sm-2 control-label">Custom Icon:<br /><span class="k_help">Recommended dimensions<br>38 x 38px</span></label>
					        <div class="col-sm-10">                                
                                <a href="" id="t1o_product_fb1_icon_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_product_fb1_icon_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_product_fb1_icon" value="<?php echo $t1o_product_fb1_icon; ?>" id="t1o_product_fb1_icon" />
					        </div>
				        </div> 
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon:</label>
                            <div class="col-sm-10">
								<input type="text" name="t1o_product_fb1_awesome" value="<?php echo $t1o_product_fb1_awesome; ?>" class="form-control" />
                                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span>
                                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">    
                                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_fb1_title[<?php echo $language_id; ?>]" id="t1o_product_fb1_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_fb1_title[$language_id])) echo $t1o_product_fb1_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Subtitle:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_fb1_subtitle[<?php echo $language_id; ?>]" id="t1o_product_fb1_subtitle_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_fb1_subtitle[$language_id])) echo $t1o_product_fb1_subtitle[$language_id]; ?>" placeholder="Subtitle" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_product_fb1_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_product_fb1_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_product_fb1_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_product_fb1_content[<?php echo $language_id; ?>]" id="t1o_product_fb1_content-<?php echo $language_id; ?>"><?php if(isset($t1o_product_fb1_content[$language_id])) echo $t1o_product_fb1_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <legend>Feature Box 2</legend>
 
                        <div class="form-group">
					        <label class="col-sm-2 control-label">Custom Icon:<br /><span class="k_help">Recommended dimensions<br>38 x 38px</span></label>
					        <div class="col-sm-10">                                
                                <a href="" id="t1o_product_fb2_icon_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_product_fb2_icon_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_product_fb2_icon" value="<?php echo $t1o_product_fb2_icon; ?>" id="t1o_product_fb2_icon" />
					        </div>
				        </div> 
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon:</label>
                            <div class="col-sm-10">
								<input type="text" name="t1o_product_fb2_awesome" value="<?php echo $t1o_product_fb2_awesome; ?>" class="form-control" />
                                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span>
                                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">    
                                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_fb2_title[<?php echo $language_id; ?>]" id="t1o_product_fb2_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_fb2_title[$language_id])) echo $t1o_product_fb2_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Subtitle:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_fb2_subtitle[<?php echo $language_id; ?>]" id="t1o_product_fb2_subtitle_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_fb2_subtitle[$language_id])) echo $t1o_product_fb2_subtitle[$language_id]; ?>" placeholder="Subtitle" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_product_fb2_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_product_fb2_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_product_fb2_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_product_fb2_content[<?php echo $language_id; ?>]" id="t1o_product_fb2_content-<?php echo $language_id; ?>"><?php if(isset($t1o_product_fb2_content[$language_id])) echo $t1o_product_fb2_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <legend>Feature Box 3</legend>

                        <div class="form-group">
					        <label class="col-sm-2 control-label">Custom Icon:<br /><span class="k_help">Recommended dimensions<br>38 x 38px</span></label>
					        <div class="col-sm-10">                                
                                <a href="" id="t1o_product_fb3_icon_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_product_fb3_icon_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_product_fb3_icon" value="<?php echo $t1o_product_fb3_icon; ?>" id="t1o_product_fb3_icon" />
					        </div>
				        </div> 
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Font Awesome Icon:</label>
                            <div class="col-sm-10">
								<input type="text" name="t1o_product_fb3_awesome" value="<?php echo $t1o_product_fb3_awesome; ?>" class="form-control" />
                                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span>
                                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">    
                                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_fb3_title[<?php echo $language_id; ?>]" id="t1o_product_fb3_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_fb3_title[$language_id])) echo $t1o_product_fb3_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Subtitle:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_fb3_subtitle[<?php echo $language_id; ?>]" id="t1o_product_fb3_subtitle_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_fb3_subtitle[$language_id])) echo $t1o_product_fb3_subtitle[$language_id]; ?>" placeholder="Subtitle" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_product_fb3_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_product_fb3_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>

								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_product_fb3_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_product_fb3_content[<?php echo $language_id; ?>]" id="t1o_product_fb3_content-<?php echo $language_id; ?>"><?php if(isset($t1o_product_fb3_content[$language_id])) echo $t1o_product_fb3_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
          
							</div>
						</div>
                        
                        <legend>Custom Block - Under Main Image<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_31.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_product_custom_block_1_status" class="form-control">
								    <option value="0"<?php if($t1o_product_custom_block_1_status == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_product_custom_block_1_status == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
						</div>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">    
                                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_custom_block_1_title[<?php echo $language_id; ?>]" id="t1o_product_custom_block_1_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_custom_block_1_title[$language_id])) echo $t1o_product_custom_block_1_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_product_custom_block_1_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_product_custom_block_1_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_product_custom_block_1_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_product_custom_block_1_content[<?php echo $language_id; ?>]" id="t1o_product_custom_block_1_content-<?php echo $language_id; ?>"><?php if(isset($t1o_product_custom_block_1_content[$language_id])) echo $t1o_product_custom_block_1_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <legend>Custom Block - Buy Section<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_32.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_product_custom_block_2_status" class="form-control">
								    <option value="0"<?php if($t1o_product_custom_block_2_status == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_product_custom_block_2_status == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
						</div>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">    
                                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_custom_block_2_title[<?php echo $language_id; ?>]" id="t1o_product_custom_block_2_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_custom_block_2_title[$language_id])) echo $t1o_product_custom_block_2_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_product_custom_block_2_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_product_custom_block_2_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_product_custom_block_2_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_product_custom_block_2_content[<?php echo $language_id; ?>]" id="t1o_product_custom_block_2_content-<?php echo $language_id; ?>"><?php if(isset($t1o_product_custom_block_2_content[$language_id])) echo $t1o_product_custom_block_2_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
          
							</div>
						</div>  
                        
                        <legend>Custom Block - Right Sidebar<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_33.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_product_custom_block_3_status" class="form-control">
								    <option value="0"<?php if($t1o_product_custom_block_3_status == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_product_custom_block_3_status == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
						</div>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">    
                                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_custom_block_3_title[<?php echo $language_id; ?>]" id="t1o_product_custom_block_3_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_product_custom_block_3_title[$language_id])) echo $t1o_product_custom_block_3_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_product_custom_block_3_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_product_custom_block_3_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_product_custom_block_3_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_product_custom_block_3_content[<?php echo $language_id; ?>]" id="t1o_product_custom_block_3_content-<?php echo $language_id; ?>"><?php if(isset($t1o_product_custom_block_3_content[$language_id])) echo $t1o_product_custom_block_3_content[$language_id]; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <?php for ($i = 1; $i <= 3; $i++) { ?>
                        
                        <legend>Custom Tab <?php echo $i; ?><a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_34.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Tab <?php echo $i; ?>:</label>
							<div class="col-sm-10">
								<select name="t1o_product_custom_tab[<?php echo $i; ?>][status]" class="form-control">
								    <option value="0"<?php if($t1o_product_custom_tab[$i]['status'] == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_product_custom_tab[$i]['status'] == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_product_custom_tab[<?php echo $i; ?>][<?php echo $language_id; ?>][title]" id="t1o_product_custom_tab_<?php echo $i; ?>_<?php echo $language_id; ?>_title" value="<?php if(isset($t1o_product_custom_tab[$i][$language_id]['title'])) echo $t1o_product_custom_tab[$i][$language_id]['title']; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_product_custom_tab_<?php echo $i; ?>" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_product_custom_tab_<?php echo $i; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_product_custom_tab_<?php echo $i; ?>_<?php echo $language_id; ?>" class="tab-pane">
                                            <textarea name="t1o_product_custom_tab[<?php echo $i ?>][<?php echo $language_id; ?>][content]" id="t1o_product_custom_tab_<?php echo $i ?>_<?php echo $language_id; ?>_content"><?php if(isset($t1o_product_custom_tab[$i][$language_id]['content'])) echo $t1o_product_custom_tab[$i][$language_id]['content']; ?></textarea>
										</div>				
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <?php } ?>

					</fieldset>        
        
        </div>
        
        <div id="tab-midsection-contact" class="tab-pane">  
        
                    <fieldset>
                        
                        <legend>Google Map <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_35.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Google Map:</label>
							<div class="col-sm-10">
								<select name="t1o_contact_map_status" class="form-control">
									<option value="0"<?php if($t1o_contact_map_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_contact_map_status == '1') echo ' selected="selected"';?>>Yes</option>
								</select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Latitude, Longitude:<br /><span class="k_help">For example:<br />51.5224954,-0.1720996</span></label>
							<div class="col-sm-10">
								<input type="text" name="t1o_contact_map_ll" value="<?php echo $t1o_contact_map_ll; ?>" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="http://itouchmap.com/latlong.html" target="_blank" class="link">How to find Latitude and Longitude?</a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Your Google Maps API key:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_contact_map_api" value="<?php echo $t1o_contact_map_api; ?>" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank" class="link">How to get your Google Maps API key?</a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Map type:</label>
							<div class="col-sm-10">
								<select name="t1o_contact_map_type" class="form-control">
									<option value="ROADMAP"<?php if($t1o_contact_map_type == 'ROADMAP') echo ' selected="selected"';?>>ROADMAP</option>
                            		<option value="SATELLITE"<?php if($t1o_contact_map_type == 'SATELLITE') echo ' selected="selected"';?>>SATELLITE</option>
                            		<option value="HYBRID"<?php if($t1o_contact_map_type == 'HYBRID') echo ' selected="selected"';?>>HYBRID</option>
                            		<option value="TERRAIN"<?php if($t1o_contact_map_type == 'TERRAIN') echo ' selected="selected"';?>>TERRAIN</option> 
								</select>
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-midsection-lf" class="tab-pane">  
        
                    <fieldset> 
        
                        <legend>Categories</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Categories display type:</label>
							<div class="col-sm-10">
								<select name="t1o_left_right_column_categories_type" class="form-control">
								    <option value="0"<?php if($t1o_left_right_column_categories_type == '0') echo ' selected="selected"';?>><?php echo $text_opencart; ?></option> 
                                    <option value="1"<?php if($t1o_left_right_column_categories_type == '1') echo ' selected="selected"';?><?php if($t1o_left_right_column_categories_type == '') echo ' selected="selected"';?>><?php echo $text_accordion; ?></option>  
                                    <option value="2"<?php if($t1o_left_right_column_categories_type == '2') echo ' selected="selected"';?>>Dropdown</option> 
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_36.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
        
                    </fieldset>      
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        <div class="tab-pane" id="tab-footer"> 
        <div class="row form-horizontal">  
        
        <div class="col-sm-2">    
        <ul id="footer_tabs" class="nav nav-pills nav-stacked">
             <li class="active"><a href="#tab-footer-top-custom-1" data-toggle="tab">Top Custom Block</a></li>
             <li><a href="#tab-footer-custom-column-1" data-toggle="tab">Custom Column 1</a></li>
             <li><a href="#tab-footer-information" data-toggle="tab">Information Block</a></li>
             <li><a href="#tab-footer-custom-column-2" data-toggle="tab">Custom Column 2 / Newsletter</a></li>
             <li><a href="#tab-footer-payment" data-toggle="tab">Payment Images</a></li>
             <li><a href="#tab-footer-powered" data-toggle="tab">Powered by</a></li>
             <li><a href="#tab-footer-follow" data-toggle="tab">Follow us</a></li>
             <li><a href="#tab-footer-bottom-custom-1" data-toggle="tab">Bottom Custom Block</a></li>
             <li><a href="#tab-footer-bottom-custom-2" data-toggle="tab">Sliding Bottom Custom Block</a></li>                                  
        </ul> 
        </div>
        
        <div class="col-sm-10">
        <div class="tab-content">
        
        <div id="tab-footer-top-custom-1" class="tab-pane fade in active">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Top Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_top_1_status" class="form-control">
								    <option value="0"<?php if($t1o_custom_top_1_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_top_1_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_custom_top_1_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_custom_top_1_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_custom_top_1_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_custom_top_1_content[<?php echo $language_id; ?>]" id="t1o_custom_top_1_content-<?php echo $language_id; ?>"><?php if(isset($t1o_custom_top_1_content[$language_id])) echo $t1o_custom_top_1_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>
                    
                    </fieldset>        
        
        </div>
        
        <div id="tab-footer-custom-column-1" class="tab-pane">  
        
                    <fieldset>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Column 1:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_1_status" class="form-control">
								    <option value="0"<?php if($t1o_custom_1_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_1_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Custom Column 1 width:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_1_column_width" class="form-control">
								    <option value="1"<?php if($t1o_custom_1_column_width == '1') echo ' selected="selected"';?>>1/12</option>
                                    <option value="2"<?php if($t1o_custom_1_column_width == '2') echo ' selected="selected"';?>>2/12</option>
                                    <option value="3"<?php if($t1o_custom_1_column_width == '3') echo ' selected="selected"';?>>3/12</option>
                                    <option value="4"<?php if($t1o_custom_1_column_width == '4') echo ' selected="selected"';?><?php if($t1o_custom_1_column_width == '') echo ' selected="selected"';?>>4/12</option>
                                    <option value="5"<?php if($t1o_custom_1_column_width == '5') echo ' selected="selected"';?>>5/12</option>
                                    <option value="6"<?php if($t1o_custom_1_column_width == '6') echo ' selected="selected"';?>>6/12</option>
                                    <option value="7"<?php if($t1o_custom_1_column_width == '7') echo ' selected="selected"';?>>7/12</option>
                                    <option value="8"<?php if($t1o_custom_1_column_width == '8') echo ' selected="selected"';?>>8/12</option>
                                    <option value="9"<?php if($t1o_custom_1_column_width == '9') echo ' selected="selected"';?>>9/12</option>
                                    <option value="10"<?php if($t1o_custom_1_column_width == '10') echo ' selected="selected"';?>>10/12</option>
                                    <option value="11"<?php if($t1o_custom_1_column_width == '11') echo ' selected="selected"';?>>11/12</option>
                                    <option value="12"<?php if($t1o_custom_1_column_width == '12') echo ' selected="selected"';?>>12/12</option>
							    </select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_custom_1_title[<?php echo $language_id; ?>]" id="t1o_custom_1_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_custom_1_title[$language_id])) echo $t1o_custom_1_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_custom_1_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_custom_1_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_custom_1_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_custom_1_content[<?php echo $language_id; ?>]" id="t1o_custom_1_content-<?php echo $language_id; ?>"><?php if(isset($t1o_custom_1_content[$language_id])) echo $t1o_custom_1_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-footer-information" class="tab-pane">  
        
                    <fieldset>
                    
                        <legend>Information Block<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_38.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Information Block width:</label>
							<div class="col-sm-10">
								<select name="t1o_information_block_width" class="form-control">
								    <option value="1"<?php if($t1o_information_block_width == '1') echo ' selected="selected"';?>>1/12</option>
                                    <option value="2"<?php if($t1o_information_block_width == '2') echo ' selected="selected"';?>>2/12</option>
                                    <option value="3"<?php if($t1o_information_block_width == '3') echo ' selected="selected"';?>>3/12</option>
                                    <option value="4"<?php if($t1o_information_block_width == '4') echo ' selected="selected"';?>>4/12</option>
                                    <option value="5"<?php if($t1o_information_block_width == '5') echo ' selected="selected"';?><?php if($t1o_information_block_width == '') echo ' selected="selected"';?>>5/12</option>
                                    <option value="6"<?php if($t1o_information_block_width == '6') echo ' selected="selected"';?>>6/12</option>
                                    <option value="7"<?php if($t1o_information_block_width == '7') echo ' selected="selected"';?>>7/12</option>
                                    <option value="8"<?php if($t1o_information_block_width == '8') echo ' selected="selected"';?>>8/12</option>
                                    <option value="9"<?php if($t1o_information_block_width == '9') echo ' selected="selected"';?>>9/12</option>
                                    <option value="10"<?php if($t1o_information_block_width == '10') echo ' selected="selected"';?>>10/12</option>
                                    <option value="11"<?php if($t1o_information_block_width == '11') echo ' selected="selected"';?>>11/12</option>
                                    <option value="12"<?php if($t1o_information_block_width == '12') echo ' selected="selected"';?>>12/12</option>
							    </select>
							</div>
						</div>

						<legend>Information Column<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_38.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Information Column:</label>
							<div class="col-sm-10">
								<select name="t1o_information_column_1_status" class="form-control">
								    <option value="0"<?php if($t1o_information_column_1_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_information_column_1_status == '1') echo ' selected="selected"';?><?php if($t1o_information_column_1_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <span class="k_help">To disable the default OpenCart links, like <b>About Us</b>, <b>Delivery Information</b> or <b>Privacy Policy</b>, go to OpenCart Admin > Catalog > Information.</span>
                        
                        
                        <legend>Customer Service Column<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_39.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Customer Service Column:</label>
							<div class="col-sm-10">
								<select name="t1o_information_column_2_status" class="form-control">
								    <option value="0"<?php if($t1o_information_column_2_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_information_column_2_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Contact Us link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_2_1_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_2_1_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_2_1_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_2_1_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">My Account link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_2_2_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_2_2_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_2_2_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_2_2_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Returns link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_2_3_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_2_3_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_2_3_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_2_3_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Order History link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_2_4_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_2_4_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_2_4_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_2_4_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Wish List link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_2_5_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_2_5_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_2_5_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_2_5_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        
                        <legend>Extras Column<a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_40.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Extras Column:</label>
							<div class="col-sm-10">
								<select name="t1o_information_column_3_status" class="form-control">
								    <option value="0"<?php if($t1o_information_column_3_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_information_column_3_status == '1') echo ' selected="selected"';?><?php if($t1o_information_column_3_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Brands link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_3_1_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_3_1_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_3_1_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_3_1_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Gift Vouchers link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_3_2_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_3_2_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_3_2_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_3_2_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Affiliates link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_3_3_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_3_3_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_3_3_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_3_3_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Specials link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_3_4_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_3_4_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_3_4_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_3_4_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Newsletter link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_3_5_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_3_5_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_3_5_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_3_5_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Site Map link:</label>
							<div class="col-sm-10">
								<select name="t1o_i_c_3_6_status" class="form-control">
								    <option value="0"<?php if($t1o_i_c_3_6_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_i_c_3_6_status == '1') echo ' selected="selected"';?><?php if($t1o_i_c_3_6_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-footer-custom-column-2" class="tab-pane">  
        
                    <fieldset>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Column 2 / Newsletter:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_2_status" class="form-control">
								    <option value="0"<?php if($t1o_custom_2_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_2_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Custom Column 2 / Newsletter width:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_2_column_width" class="form-control">
								    <option value="1"<?php if($t1o_custom_2_column_width == '1') echo ' selected="selected"';?>>1/12</option>
                                    <option value="2"<?php if($t1o_custom_2_column_width == '2') echo ' selected="selected"';?>>2/12</option>
                                    <option value="3"<?php if($t1o_custom_2_column_width == '3') echo ' selected="selected"';?><?php if($t1o_custom_2_column_width == '') echo ' selected="selected"';?>>3/12</option>
                                    <option value="4"<?php if($t1o_custom_2_column_width == '4') echo ' selected="selected"';?>>4/12</option>
                                    <option value="5"<?php if($t1o_custom_2_column_width == '5') echo ' selected="selected"';?>>5/12</option>
                                    <option value="6"<?php if($t1o_custom_2_column_width == '6') echo ' selected="selected"';?>>6/12</option>
                                    <option value="7"<?php if($t1o_custom_2_column_width == '7') echo ' selected="selected"';?>>7/12</option>
                                    <option value="8"<?php if($t1o_custom_2_column_width == '8') echo ' selected="selected"';?>>8/12</option>
                                    <option value="9"<?php if($t1o_custom_2_column_width == '9') echo ' selected="selected"';?>>9/12</option>
                                    <option value="10"<?php if($t1o_custom_2_column_width == '10') echo ' selected="selected"';?>>10/12</option>
                                    <option value="11"<?php if($t1o_custom_2_column_width == '11') echo ' selected="selected"';?>>11/12</option>
                                    <option value="12"<?php if($t1o_custom_2_column_width == '12') echo ' selected="selected"';?>>12/12</option>
							    </select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_custom_2_title[<?php echo $language_id; ?>]" id="t1o_custom_2_title_<?php echo $language_id; ?>" value="<?php if(isset($t1o_custom_2_title[$language_id])) echo $t1o_custom_2_title[$language_id]; ?>" placeholder="Title" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Newsletter Block:</label>
							<div class="col-sm-10">
								<select name="t1o_newsletter_status" class="form-control">
								    <option value="0"<?php if($t1o_newsletter_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_newsletter_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Your MailChimp Campaign URL:</label>
							<div class="col-sm-10">
                                <input type="text" name="t1o_newsletter_campaign_url" value="<?php echo $t1o_newsletter_campaign_url; ?>" class="form-control" style="width: 100%;" />
                                <br /><span class="k_help"><a href="https://mailchimp.com/" target="_blank" class="link">MailChimp Website &raquo;</a></span>
                                <span class="k_help"><a href="http://kb.mailchimp.com/" target="_blank" class="link">MailChimp Knowledge Base &raquo;</a></span>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Newsletter Promo text:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_newsletter_promo_text[<?php echo $language_id; ?>]" id="t1o_newsletter_promo_text_<?php echo $language_id; ?>" value="<?php if(isset($t1o_newsletter_promo_text[$language_id])) echo $t1o_newsletter_promo_text[$language_id]; ?>" placeholder="Newsletter Promo text" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">"Your email address" text:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_newsletter_email[<?php echo $language_id; ?>]" id="t1o_newsletter_email_<?php echo $language_id; ?>" value="<?php if(isset($t1o_newsletter_email[$language_id])) echo $t1o_newsletter_email[$language_id]; ?>" placeholder="Your email address" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">"Subscribe" text:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_newsletter_subscribe[<?php echo $language_id; ?>]" id="t1o_newsletter_subscribe_<?php echo $language_id; ?>" value="<?php if(isset($t1o_newsletter_subscribe[$language_id])) echo $t1o_newsletter_subscribe[$language_id]; ?>" placeholder="Subscribe" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Custom Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_custom_2_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_custom_2_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_custom_2_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_custom_2_content[<?php echo $language_id; ?>]" id="t1o_custom_2_content-<?php echo $language_id; ?>"><?php if(isset($t1o_custom_2_content[$language_id])) echo $t1o_custom_2_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-footer-payment" class="tab-pane">  
        
                    <fieldset>

						<legend class="bn"></legend>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show payment images:</label>
							<div class="col-sm-10">
								<select name="t1o_payment_block_status" class="form-control">
								    <option value="0"<?php if($t1o_payment_block_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_payment_block_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_45.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>

                        <legend>Custom payment image</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show:</label>
							<div class="col-sm-10">
								<select name="t1o_payment_block_custom_status" class="form-control">
								    <option value="0"<?php if($t1o_payment_block_custom_status == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_block_custom_status == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Upload your payment image:<br /><span class="k_help">Recommended height: 36px</span></label>
							<div class="col-sm-10">
								<a href="" id="t1o_payment_block_custom_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_payment_block_custom_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_payment_block_custom" value="<?php echo $t1o_payment_block_custom; ?>" id="t1o_payment_block_custom" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">url:</label>
							<div class="col-sm-10">
                                <input type="text" name="t1o_payment_block_custom_url" value="<?php echo $t1o_payment_block_custom_url; ?>" class="form-control" />
							</div>
						</div>  
                        
                        <legend>SELLMORE Theme payment images:</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">PayPal:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_paypal" class="form-control">
								    <option value="0"<?php if($t1o_payment_paypal == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_paypal == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_paypal-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_paypal_url" value="<?php echo $t1o_payment_paypal_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Visa:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_visa" class="form-control">
								    <option value="0"<?php if($t1o_payment_visa == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_visa == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_visa-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_visa_url" value="<?php echo $t1o_payment_visa_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">MasterCard:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_mastercard" class="form-control">
								    <option value="0"<?php if($t1o_payment_mastercard == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_mastercard == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_mastercard-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_mastercard_url" value="<?php echo $t1o_payment_mastercard_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Maestro:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_maestro" class="form-control">
								    <option value="0"<?php if($t1o_payment_maestro == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_maestro == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_maestro-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_maestro_url" value="<?php echo $t1o_payment_maestro_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Discover:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_discover" class="form-control">
								    <option value="0"<?php if($t1o_payment_discover == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_discover == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_discover-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_discover_url" value="<?php echo $t1o_payment_discover_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Skrill:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_skrill" class="form-control">
								    <option value="0"<?php if($t1o_payment_skrill == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_skrill == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_skrill-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_skrill_url" value="<?php echo $t1o_payment_skrill_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">American Express:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_american_express" class="form-control">
								    <option value="0"<?php if($t1o_payment_american_express == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_american_express == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_american_express-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_american_express_url" value="<?php echo $t1o_payment_american_express_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Cirrus:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_cirrus" class="form-control">
								    <option value="0"<?php if($t1o_payment_cirrus == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_cirrus == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_cirrus-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_cirrus_url" value="<?php echo $t1o_payment_cirrus_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Delta:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_delta" class="form-control">
								    <option value="0"<?php if($t1o_payment_delta == '0') echo ' selected="selected"';?>>No</option> 

                                    <option value="1"<?php if($t1o_payment_delta == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_delta-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_delta_url" value="<?php echo $t1o_payment_delta_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Google:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_google" class="form-control">
								    <option value="0"<?php if($t1o_payment_google == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_google == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_google-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_google_url" value="<?php echo $t1o_payment_google_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">2CheckOut:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_2co" class="form-control">
								    <option value="0"<?php if($t1o_payment_2co == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_2co == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_2co-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_2co_url" value="<?php echo $t1o_payment_2co_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Sage:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_sage" class="form-control">
								    <option value="0"<?php if($t1o_payment_sage == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_sage == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_sage-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_sage_url" value="<?php echo $t1o_payment_sage_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Solo:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_solo" class="form-control">
								    <option value="0"<?php if($t1o_payment_solo == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_solo == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_solo-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_solo_url" value="<?php echo $t1o_payment_solo_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Amazon Payments:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_amazon" class="form-control">
								    <option value="0"<?php if($t1o_payment_amazon == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_amazon == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_amazon-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_amazon_url" value="<?php echo $t1o_payment_amazon_url; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Western Union:</label>
							<div class="col-sm-2">
								<select name="t1o_payment_western_union" class="form-control">
								    <option value="0"<?php if($t1o_payment_western_union == '0') echo ' selected="selected"';?>>No</option> 
                                    <option value="1"<?php if($t1o_payment_western_union == '1') echo ' selected="selected"';?>>Yes</option>      
							    </select>
							</div>
                            <div class="col-sm-1"><img src="<?php echo HTTP_CATALOG; ?>catalog/view/theme/sellmore/image/payment/payment_image_western_union-1.png"></div>
                            <div class="col-sm-1 control-label">url:</div>
                            <div class="col-sm-6">
								<input type="text" name="t1o_payment_western_union_url" value="<?php echo $t1o_payment_western_union_url; ?>" class="form-control" />
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-footer-powered" class="tab-pane">  
        
                    <fieldset>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show powered by:</label>
							<div class="col-sm-10">
								<select name="t1o_powered_status" class="form-control">
								    <option value="0"<?php if($t1o_powered_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_powered_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_42.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_powered_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_powered_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_powered_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_powered_content[<?php echo $language_id; ?>]" id="t1o_powered_content-<?php echo $language_id; ?>"><?php if(isset($t1o_powered_content[$language_id])) echo $t1o_powered_content[$language_id]; ?></textarea>
										</div>		
									<?php } ?>
								</div>
                                
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-footer-follow" class="tab-pane">  
        
                    <fieldset>

						<legend>Follow Us</legend>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Follow Us Block:</label>
							<div class="col-sm-10">
								<select name="t1o_follow_us_status" class="form-control">
								    <option value="0"<?php if($t1o_follow_us_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_follow_us_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_44.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Facebook:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_facebook" value="<?php echo $t1o_facebook; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Twitter:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_twitter" value="<?php echo $t1o_twitter; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Google+:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_googleplus" value="<?php echo $t1o_googleplus; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">RSS:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_rss" value="<?php echo $t1o_rss; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Pinterest:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_pinterest" value="<?php echo $t1o_pinterest; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Vimeo:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_vimeo" value="<?php echo $t1o_vimeo; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Flickr:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_flickr" value="<?php echo $t1o_flickr; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">LinkedIn:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_linkedin" value="<?php echo $t1o_linkedin; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">YouTube:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_youtube" value="<?php echo $t1o_youtube; ?>" class="form-control" />

							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Dribbble:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_dribbble" value="<?php echo $t1o_dribbble; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Instagram:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_instagram" value="<?php echo $t1o_instagram; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Behance:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_behance" value="<?php echo $t1o_behance; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Skype username:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_skype" value="<?php echo $t1o_skype; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Tumblr:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_tumblr" value="<?php echo $t1o_tumblr; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Reddit:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_reddit" value="<?php echo $t1o_reddit; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">VK:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_vk" value="<?php echo $t1o_vk; ?>" class="form-control" />
							</div>
						</div>

					</fieldset>        
        
        </div>
        
        <div id="tab-footer-bottom-custom-1" class="tab-pane">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Bottom Custom Block:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_bottom_1_status" class="form-control">
								    <option value="0"<?php if($t1o_custom_bottom_1_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_bottom_1_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_custom_bottom_1_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_custom_bottom_1_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_custom_bottom_1_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_custom_bottom_1_content[<?php echo $language_id; ?>]" id="t1o_custom_bottom_1_content-<?php echo $language_id; ?>"><?php if(isset($t1o_custom_bottom_1_content[$language_id])) echo $t1o_custom_bottom_1_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>
                    
                    </fieldset>        
        
        </div>
        
        <div id="tab-footer-bottom-custom-2" class="tab-pane">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Sliding Bottom Custom Block:
                            <br /><span class="k_help">for "Full Width" layout style</span>
                            </label>
							<div class="col-sm-10">
								<select name="t1o_custom_bottom_2_status" class="form-control">
								    <option value="0"<?php if($t1o_custom_bottom_2_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_bottom_2_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_custom_bottom_2_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_custom_bottom_2_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_custom_bottom_2_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_custom_bottom_2_content[<?php echo $language_id; ?>]" id="t1o_custom_bottom_2_content-<?php echo $language_id; ?>"><?php if(isset($t1o_custom_bottom_2_content[$language_id])) echo $t1o_custom_bottom_2_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                <span class="k_help">To add background image or pattern, go to <b>SELLMORE Theme Settings - Design > Background Images > Footer > Sliding Bottom Custom Block</b>.</span>
                                
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Height:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_bottom_2_footer_margin" class="form-control">
								    <option value="50"<?php if($t1o_custom_bottom_2_footer_margin == '50') echo ' selected="selected"';?>>50px</option>
                                    <option value="100"<?php if($t1o_custom_bottom_2_footer_margin == '100') echo ' selected="selected"';?>>100px</option>
                                    <option value="150"<?php if($t1o_custom_bottom_2_footer_margin == '150') echo ' selected="selected"';?>>150px</option>
                                    <option value="200"<?php if($t1o_custom_bottom_2_footer_margin == '200') echo ' selected="selected"';?>>200px</option>
                                    <option value="250"<?php if($t1o_custom_bottom_2_footer_margin == '250') echo ' selected="selected"';?>>250px</option>
                                    <option value="300"<?php if($t1o_custom_bottom_2_footer_margin == '300') echo ' selected="selected"';?>>300px</option>
                                    <option value="350"<?php if($t1o_custom_bottom_2_footer_margin == '350') echo ' selected="selected"';?>>350px</option>
                                    <option value="400"<?php if($t1o_custom_bottom_2_footer_margin == '400') echo ' selected="selected"';?>>400px</option>
                                    <option value="450"<?php if($t1o_custom_bottom_2_footer_margin == '450') echo ' selected="selected"';?>>450px</option>
                                    <option value="500"<?php if($t1o_custom_bottom_2_footer_margin == '500') echo ' selected="selected"';?>>500px</option>
                                    <option value="550"<?php if($t1o_custom_bottom_2_footer_margin == '550') echo ' selected="selected"';?><?php if($t1o_custom_bottom_2_footer_margin == '') echo ' selected="selected"';?>>550px</option>
							    </select>
                                <a href="<?php echo HTTP_CATALOG; ?>admin/view/image/theme_img/help_sellmore_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>
							</div>
						</div>
                    
                    </fieldset> 
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        <div class="tab-pane" id="tab-widgets"> 
        <div class="row form-horizontal">  
        
        <div class="col-sm-2">    
        <ul id="widgets_tabs" class="nav nav-pills nav-stacked">
             <li class="active"><a href="#tab-widgets-facebook" data-toggle="tab">Facebook Widget</a></li>
             <li><a href="#tab-widgets-twitter" data-toggle="tab">Twitter Widget</a></li>
             <li><a href="#tab-widgets-googleplus" data-toggle="tab">Google+ Widget</a></li>
             <li><a href="#tab-widgets-pinterest" data-toggle="tab">Pinterest Widget</a></li>
             <li><a href="#tab-widgets-snapchat" data-toggle="tab">Snapchat Widget</a></li>
             <li><a href="#tab-widgets-video-box" data-toggle="tab">Video Box</a></li>
             <li><a href="#tab-widgets-custom-box" data-toggle="tab">Custom Content Box</a></li>
             <li><a href="#tab-widgets-cookie" data-toggle="tab">EU Cookie Message Widget</a></li>
        </ul> 
        </div>
        
        <div class="col-sm-10">
        <div class="tab-content">
        
        <div id="tab-widgets-facebook" class="tab-pane fade in active">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Facebook Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_facebook_likebox_status" class="form-control">
								    <option value="0"<?php if($t1o_facebook_likebox_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_facebook_likebox_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Facebook FanPage ID:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_facebook_likebox_id" value="<?php echo $t1o_facebook_likebox_id; ?>" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="http://findmyfacebookid.com/" target="_blank" class="link">Find your Facebook ID &raquo;</a>
							</div>
						</div>

                    </fieldset>          

        </div>
 
        <div id="tab-widgets-twitter" class="tab-pane"> 
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Twitter Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_twitter_block_status" class="form-control">
								    <option value="0"<?php if($t1o_twitter_block_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_twitter_block_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Twitter username:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_twitter_block_user" value="<?php echo $t1o_twitter_block_user; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Widget ID:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_twitter_block_widget_id" value="<?php echo $t1o_twitter_block_widget_id; ?>" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="http://321cart.com/oxy/documentation/assets/images/screen_14.png" target="_blank" class="link">Find your Widget ID &raquo;</a>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Tweet limit:</label>
							<div class="col-sm-10">
								<select name="t1o_twitter_block_tweets" class="form-control">
								    <option value="1"<?php if($t1o_twitter_block_tweets == '1') echo ' selected="selected"';?>>1</option>
                                    <option value="2"<?php if($t1o_twitter_block_tweets == '2') echo ' selected="selected"';?><?php if($t1o_twitter_block_tweets == '') echo ' selected="selected"';?>>2</option>
                                    <option value="3"<?php if($t1o_twitter_block_tweets == '3') echo ' selected="selected"';?>>3</option>
							    </select>
							</div>
						</div>

                    </fieldset>         

        </div>
        
        <div id="tab-widgets-googleplus" class="tab-pane">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Google+ Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_googleplus_box_status" class="form-control">
								    <option value="0"<?php if($t1o_googleplus_box_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_googleplus_box_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Google+ User:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_googleplus_box_user" value="<?php echo $t1o_googleplus_box_user; ?>" class="form-control" />
							</div>
						</div>

                    </fieldset>          

        </div>
        
        <div id="tab-widgets-pinterest" class="tab-pane">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Pinterest Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_pinterest_box_status" class="form-control">
								    <option value="0"<?php if($t1o_pinterest_box_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_pinterest_box_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Pinterest User:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_pinterest_box_user" value="<?php echo $t1o_pinterest_box_user; ?>" class="form-control" />
							</div>
						</div>

                    </fieldset>          

        </div>
        
        <div id="tab-widgets-snapchat" class="tab-pane">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Snapchat Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_snapchat_box_status" class="form-control">
								    <option value="0"<?php if($t1o_snapchat_box_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_snapchat_box_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Upload your Snapchat Code image:</label>
							<div class="col-sm-10">
								<a href="" id="t1o_snapchat_box_code_custom_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $t1o_snapchat_box_code_custom_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="t1o_snapchat_box_code_custom" value="<?php echo $t1o_snapchat_box_code_custom; ?>" id="t1o_snapchat_box_code_custom" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_snapchat_box_title" value="<?php echo $t1o_snapchat_box_title; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Subtitle:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_snapchat_box_subtitle" value="<?php echo $t1o_snapchat_box_subtitle; ?>" class="form-control" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_snapchat_box_bg" id="t1o_snapchat_box_bg" value="<?php echo $t1o_snapchat_box_bg; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>

                    </fieldset> 
        
        </div>
  
        <div id="tab-widgets-video-box" class="tab-pane">
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Video Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_video_box_status" class="form-control">
								    <option value="0"<?php if($t1o_video_box_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_video_box_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_video_box_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_video_box_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_video_box_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_video_box_content[<?php echo $language_id; ?>]" id="t1o_video_box_content-<?php echo $language_id; ?>"><?php if(isset($t1o_video_box_content[$language_id])) echo $t1o_video_box_content[$language_id]; ?></textarea>
										</div>
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_video_box_bg" id="t1o_video_box_bg" value="<?php echo $t1o_video_box_bg; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>

                    </fieldset>          

        </div>
        
        <div id="tab-widgets-custom-box" class="tab-pane"> 
        
                    <fieldset>

                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Custom Content Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_custom_box_status" class="form-control">
								    <option value="0"<?php if($t1o_custom_box_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_custom_box_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Content:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_custom_box_content" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_custom_box_content_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_custom_box_content_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_custom_box_content[<?php echo $language_id; ?>]" id="t1o_custom_box_content-<?php echo $language_id; ?>"><?php if(isset($t1o_custom_box_content[$language_id])) echo $t1o_custom_box_content[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Background color:</label>
							<div class="col-sm-10">
								<input type="text" name="t1o_custom_box_bg" id="t1o_custom_box_bg" value="<?php echo $t1o_custom_box_bg; ?>" class="color {required:false,hash:true}" size="8" />
							</div>
						</div>

                    </fieldset>              

        </div>
        
        
        <div id="tab-widgets-cookie" class="tab-pane">  
        
                    <fieldset>

						<div class="form-group">
							<label class="col-sm-2 control-label">Show EU Cookie Message Widget:</label>
							<div class="col-sm-10">
								<select name="t1o_eu_cookie_status" class="form-control">
								    <option value="0"<?php if($t1o_eu_cookie_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_eu_cookie_status == '1') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Show Cookie Icon:</label>
							<div class="col-sm-10">
								<select name="t1o_eu_cookie_icon_status" class="form-control">
								    <option value="0"<?php if($t1o_eu_cookie_icon_status == '0') echo ' selected="selected"';?>>No</option>
                                    <option value="1"<?php if($t1o_eu_cookie_icon_status == '1') echo ' selected="selected"';?><?php if($t1o_eu_cookie_icon_status == '') echo ' selected="selected"';?>>Yes</option>
							    </select>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">EU Cookie Message:</label>
							<div class="col-sm-10">
								
                                <ul id="t1o_eu_cookie_message" class="nav nav-tabs">
									<?php foreach ($languages as $language) { ?>
										<li><a href="#t1o_eu_cookie_message_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" width="16px" height="11px" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content">					
									<?php foreach ($languages as $language) { ?>
										<?php $language_id = $language['language_id']; ?>				
										<div id="t1o_eu_cookie_message_<?php echo $language_id; ?>" class="tab-pane">
											<textarea name="t1o_eu_cookie_message[<?php echo $language_id; ?>]" id="t1o_eu_cookie_message-<?php echo $language_id; ?>"><?php if(isset($t1o_eu_cookie_message[$language_id])) echo $t1o_eu_cookie_message[$language_id]; ?></textarea>
										</div>			
									<?php } ?>
								</div>
                                
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-sm-2 control-label">"Close" text:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_eu_cookie_close[<?php echo $language_id; ?>]" id="t1o_eu_cookie_close_<?php echo $language_id; ?>" value="<?php if(isset($t1o_eu_cookie_close[$language_id])) echo $t1o_eu_cookie_close[$language_id]; ?>" placeholder="Close" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>

					</fieldset>        
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
       

 
 
        
        
        <div class="tab-pane" id="tab-css"> 
        <div class="row form-horizontal">  

        
        <div class="col-sm-12">
        <div class="tab-content">
        
                    <fieldset>         
        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Custom CSS:</label>
							<div class="col-sm-10">
                                <textarea name="t1o_custom_css" rows="10" class="form-control" /><?php echo $t1o_custom_css; ?></textarea>
							</div>
						</div> 
                        <div class="form-group">
							<label class="col-sm-2 control-label">Custom JavaScript:</label>
							<div class="col-sm-10">
                                <textarea name="t1o_custom_js" rows="10" class="form-control" /><?php echo $t1o_custom_js; ?></textarea>
							</div>
						</div> 
 
                    </fieldset>                          
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        
        <div class="tab-pane" id="tab-translate"> 
        <div class="row form-horizontal">  

        
        <div class="col-sm-12">
        <div class="tab-content">
        
                    <fieldset>         
        
                        <div class="form-group">
							<label class="col-sm-2 control-label">Sale</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_sale[<?php echo $language_id; ?>]" id="t1o_text_sale_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_sale[$language_id])) echo $t1o_text_sale[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">New</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_new_prod[<?php echo $language_id; ?>]" id="t1o_text_new_prod_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_new_prod[$language_id])) echo $t1o_text_new_prod[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Quick View</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_quickview[<?php echo $language_id; ?>]" id="t1o_text_quickview_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_quickview[$language_id])) echo $t1o_text_quickview[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Shop Now</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_shop_now[<?php echo $language_id; ?>]" id="t1o_text_shop_now_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_shop_now[$language_id])) echo $t1o_text_shop_now[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">View Now</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_view[<?php echo $language_id; ?>]" id="t1o_text_view_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_view[$language_id])) echo $t1o_text_view[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Next</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_next_product[<?php echo $language_id; ?>]" id="t1o_text_next_product_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_next_product[$language_id])) echo $t1o_text_next_product[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Previous</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_previous_product[<?php echo $language_id; ?>]" id="t1o_text_previous_product_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_previous_product[$language_id])) echo $t1o_text_previous_product[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Product viewed:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_product_viewed[<?php echo $language_id; ?>]" id="t1o_text_product_viewed_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_product_viewed[$language_id])) echo $t1o_text_product_viewed[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Special price:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_special_price[<?php echo $language_id; ?>]" id="t1o_text_special_price_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_special_price[$language_id])) echo $t1o_text_special_price[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Old price:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_old_price[<?php echo $language_id; ?>]" id="t1o_text_old_price_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_old_price[$language_id])) echo $t1o_text_old_price[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">You save:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_percent_saved[<?php echo $language_id; ?>]" id="t1o_text_percent_saved_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_percent_saved[$language_id])) echo $t1o_text_percent_saved[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Send to a friend</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_product_friend[<?php echo $language_id; ?>]" id="t1o_text_product_friend_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_product_friend[$language_id])) echo $t1o_text_product_friend[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Shop by Category</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_categories[<?php echo $language_id; ?>]" id="t1o_text_menu_categories_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_categories[$language_id])) echo $t1o_text_menu_categories[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Brands</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_brands[<?php echo $language_id; ?>]" id="t1o_text_menu_brands_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_brands[$language_id])) echo $t1o_text_menu_brands[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Contact us</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_contact_us[<?php echo $language_id; ?>]" id="t1o_text_contact_us_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_contact_us[$language_id])) echo $t1o_text_contact_us[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Address</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_contact_address[<?php echo $language_id; ?>]" id="t1o_text_menu_contact_address_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_contact_address[$language_id])) echo $t1o_text_menu_contact_address[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">E-mail</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_contact_email[<?php echo $language_id; ?>]" id="t1o_text_menu_contact_email_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_contact_email[$language_id])) echo $t1o_text_menu_contact_email[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Telephone</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_contact_tel[<?php echo $language_id; ?>]" id="t1o_text_menu_contact_tel_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_contact_tel[$language_id])) echo $t1o_text_menu_contact_tel[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Fax</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_contact_fax[<?php echo $language_id; ?>]" id="t1o_text_menu_contact_fax_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_contact_fax[$language_id])) echo $t1o_text_menu_contact_fax[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Opening Times</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_contact_hours[<?php echo $language_id; ?>]" id="t1o_text_menu_contact_hours_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_contact_hours[$language_id])) echo $t1o_text_menu_contact_hours[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Contact Form</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_contact_form[<?php echo $language_id; ?>]" id="t1o_text_menu_contact_form_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_contact_form[$language_id])) echo $t1o_text_menu_contact_form[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Menu</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_menu_menu[<?php echo $language_id; ?>]" id="t1o_text_menu_menu_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_menu_menu[$language_id])) echo $t1o_text_menu_menu[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">See all products by</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_see_all_products_by[<?php echo $language_id; ?>]" id="t1o_text_see_all_products_by_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_see_all_products_by[$language_id])) echo $t1o_text_see_all_products_by[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Bestseller</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_bestseller[<?php echo $language_id; ?>]" id="t1o_text_bestseller_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_bestseller[$language_id])) echo $t1o_text_bestseller[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Featured</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_featured[<?php echo $language_id; ?>]" id="t1o_text_featured_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_featured[$language_id])) echo $t1o_text_featured[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Latest</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_latest[<?php echo $language_id; ?>]" id="t1o_text_latest_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_latest[$language_id])) echo $t1o_text_latest[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Specials</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_special[<?php echo $language_id; ?>]" id="t1o_text_special_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_special[$language_id])) echo $t1o_text_special[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Most Viewed</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_most_viewed[<?php echo $language_id; ?>]" id="t1o_text_most_viewed_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_most_viewed[$language_id])) echo $t1o_text_most_viewed[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">News</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_news[<?php echo $language_id; ?>]" id="t1o_text_news_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_news[$language_id])) echo $t1o_text_news[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Share:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_share[<?php echo $language_id; ?>]" id="t1o_text_share_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_share[$language_id])) echo $t1o_text_share[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Gallery</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_gallery[<?php echo $language_id; ?>]" id="t1o_text_gallery_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_gallery[$language_id])) echo $t1o_text_gallery[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Small List</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_small_list[<?php echo $language_id; ?>]" id="t1o_text_small_list_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_small_list[$language_id])) echo $t1o_text_small_list[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Your Cart:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_your_cart[<?php echo $language_id; ?>]" id="t1o_text_your_cart_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_your_cart[$language_id])) echo $t1o_text_your_cart[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Popular Search:</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_popular_search[<?php echo $language_id; ?>]" id="t1o_text_popular_search_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_popular_search[$language_id])) echo $t1o_text_popular_search[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-2 control-label">Advanced Search</label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $language_id = $language['language_id']; ?>
										<div class="input-group">
											<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                            <input type="text" name="t1o_text_advanced_search[<?php echo $language_id; ?>]" id="t1o_text_advanced_search_<?php echo $language_id; ?>" value="<?php if(isset($t1o_text_advanced_search[$language_id])) echo $t1o_text_advanced_search[$language_id]; ?>" placeholder="" class="form-control" />
										</div>
								<?php } ?>
							</div>
						</div>
 
                    </fieldset>                          
     
        </div>
        </div>
        
        </div>
        </div>


        <!-- -->         
        </div>  
        
        
    </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>
<script type="text/javascript" src="view/javascript/poshytip/jquery.poshytip.js"></script>
<link rel="stylesheet" type="text/css" href="view/javascript/poshytip/tip-twitter/tip-twitter.css" />

<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>  

<script type="text/javascript"><!--					
<?php foreach ($languages as $language) { ?>
$('#t1o_top_custom_block_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_header_custom_block_1_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_custom_bar_below_menu_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_menu_categories_custom_block_left_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_menu_categories_custom_block_right_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_menu_categories_custom_block_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_menu_custom_block_1_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_menu_custom_block_2_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_menu_custom_block_3_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_menu_custom_block_4_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_menu_custom_block_5_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_product_fb1_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_product_fb2_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_product_fb3_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_product_custom_block_1_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_product_custom_block_2_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_product_custom_block_3_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_product_custom_tab_1_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_product_custom_tab_2_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_product_custom_tab_3_<?php echo $language['language_id']; ?>_content').summernote({height: 300});
$('#t1o_contact_custom_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_fp_fb1_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_fp_fb2_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_fp_fb3_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_fp_fb4_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_custom_top_1_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_custom_1_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_custom_2_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_custom_bottom_1_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_custom_bottom_2_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_powered_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_video_box_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_custom_box_content-<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#t1o_eu_cookie_message-<?php echo $language['language_id']; ?>').summernote({height: 300});
<?php } ?>
//--></script>
<script type="text/javascript"><!--
$('#t1o_top_custom_block_content li:first-child a').tab('show');
$('#t1o_header_custom_block_1_content li:first-child a').tab('show');
$('#t1o_custom_bar_below_menu_content li:first-child a').tab('show');
$('#t1o_menu_categories_custom_block_left_content li:first-child a').tab('show');
$('#t1o_menu_categories_custom_block_right_content li:first-child a').tab('show');
$('#t1o_menu_categories_custom_block_content li:first-child a').tab('show');
$('#t1o_menu_custom_block_1 li:first-child a').tab('show');
$('#t1o_menu_custom_block_2 li:first-child a').tab('show');
$('#t1o_menu_custom_block_3 li:first-child a').tab('show');
$('#t1o_menu_custom_block_4 li:first-child a').tab('show');
$('#t1o_menu_custom_block_5 li:first-child a').tab('show');
$('#t1o_product_fb1_content li:first-child a').tab('show');
$('#t1o_product_fb2_content li:first-child a').tab('show');
$('#t1o_product_fb3_content li:first-child a').tab('show');
$('#t1o_product_custom_block_1_content li:first-child a').tab('show');
$('#t1o_product_custom_block_2_content li:first-child a').tab('show');
$('#t1o_product_custom_block_3_content li:first-child a').tab('show');
$('#t1o_product_custom_tab_1 li:first-child a').tab('show');
$('#t1o_product_custom_tab_2 li:first-child a').tab('show');
$('#t1o_product_custom_tab_3 li:first-child a').tab('show');
$('#t1o_contact_custom_content li:first-child a').tab('show');
$('#t1o_fp_fb1_content li:first-child a').tab('show');
$('#t1o_fp_fb2_content li:first-child a').tab('show');
$('#t1o_fp_fb3_content li:first-child a').tab('show');
$('#t1o_fp_fb4_content li:first-child a').tab('show');
$('#t1o_custom_top_1_content li:first-child a').tab('show');
$('#t1o_custom_1_content li:first-child a').tab('show');
$('#t1o_custom_2_content li:first-child a').tab('show');
$('#t1o_custom_bottom_1_content li:first-child a').tab('show');
$('#t1o_custom_bottom_2_content li:first-child a').tab('show');
$('#t1o_powered_content li:first-child a').tab('show');
$('#t1o_video_box_content li:first-child a').tab('show');
$('#t1o_custom_box_content li:first-child a').tab('show');
$('#t1o_eu_cookie_message li:first-child a').tab('show');
//--></script>
</div>

<?php echo $footer; ?>
