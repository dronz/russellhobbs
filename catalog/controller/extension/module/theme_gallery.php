<?php
class ControllerExtensionModuleThemeGallery extends Controller {
	public function index($setting) {
		static $module = 0;
		
		$data['gallery'] = $setting['gallery'][$this->config->get('config_language_id')];

		$this->load->model('design/banner');
		$this->load->model('tool/image');
				
		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);
		
		$data['pr_id'] = $setting['pr_id'];
		$data['module_style'] = $setting['module_style'];
		$data['module_title_position'] = $setting['module_title_position'];
		$data['module_title_width'] = $setting['module_title_width'];
		$data['module_items_width'] = $setting['module_items_width'];
		$data['module_title_color'] = $setting['module_title_color'];
		$data['module_subtitle_color'] = $setting['module_subtitle_color'];
		$data['module_bg_color'] = $setting['module_bg_color'];
		$data['module_image_custom'] = $setting['module_image_custom'];
		
		$data['config_ssl'] = $this->config->get('config_ssl');
		$data['config_url'] = $this->config->get('config_url');

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		$data['module'] = $module++;
		
		return $this->load->view('extension/module/theme_gallery', $data);

	}
}