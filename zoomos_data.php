<?php

/* Zoomos получение данных прайслиста и сохранение их в текстовый файл */
error_reporting(E_ERROR) ;
ini_set('display_errors', 'On');

require "config.php";
ini_set('memory_limit', '1024M');
set_time_limit(0);

$ZOOMOS_KEY = 'opto.by-6V3IssAf2';
$ZOOMOS_JSON_URL = "http://api.export.zoomos.by/pricelist?key=$ZOOMOS_KEY";

file_put_contents('zoomos_data/script_on.txt', 0);
file_put_contents('categories.txt', '');

$mysqli = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}
mysqli_set_charset($mysqli, 'utf8');

$log = 'zoomos_data/log/'.date("d-m-Y H-i-s").'.txt';

// / GET PRICES ZOOMOS JSON
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 60 * 60);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60 * 60);
curl_setopt($ch, CURLOPT_URL, $ZOOMOS_JSON_URL);

$zoomos_data = json_decode(curl_exec($ch));
if($zoomos_data){
    //$sql = "UPDATE * FROM ".DB_PREFIX."product SET stock_status_id=5 AND status=0";
    $sql = "UPDATE ".DB_PREFIX."product SET updated=0";
    $up = mysqli_query($mysqli, $sql);
}
file_put_contents($log, date("d-m-Y H-i-s")."\n");

$error_array = array();

$arr_add = array();
$c = 0; $new = 0; $need_update = 0; $isset = 0; $not_update = 0;
foreach($zoomos_data as $zoomos){
		if(isset($zoomos->id)){
			$sql = "SELECT * FROM ".DB_PREFIX."product WHERE zoomos_id='".$zoomos->id."'";
		}else{		
			continue;
		}
		
	
        $res = mysqli_query($mysqli, $sql);         

        if(isset($res->num_rows) && count($res->num_rows) > 1){
			//2 товара!!!
			file_put_contents($log, 'ОШИБКА! zoomos_id='.$zoomos->id. ", - найдено ".count($res->num_rows)." товаров \n", FILE_APPEND);
        }elseif(isset($res->num_rows) && $res->num_rows){
			
			$isset++;

			$row = mysqli_fetch_assoc($res);
			
			$sql_add = '';
				if(isset($zoomos->supplierInfo)){
					$sql_add = ", supplier_id='".$zoomos->supplierInfo->id."', supplier_name='".$zoomos->supplierInfo->name."', supplier_price='".$zoomos->supplierInfo->priceConverted."'";
				}
			
			$sql = "UPDATE ".DB_PREFIX."product SET price = '".(float)$zoomos->price."', status=1, updated=1, quantity=1000, stock_status_id=7 $sql_add WHERE zoomos_id='".$zoomos->id."'";
			$up = mysqli_query($mysqli, $sql);			
			file_put_contents($log, $sql. "\n", FILE_APPEND);

			//Если не нужно обновлять пропускаем
			if($row['not_update']){
				continue;
			}
			
			//нужно обновить товар полностью
			if(isset($zoomos->itemDateUpdMillis) && ($row['dateUpdMillis'] != $zoomos->itemDateUpdMillis)){
				$arr_add[] = $zoomos;
				file_put_contents($log, $zoomos->id. "- нужно обновить \n", FILE_APPEND);
				$need_update++;
			}
        }else{
            file_put_contents($log, $zoomos->id. "- не найден \n", FILE_APPEND);

            $arr_add[] = $zoomos;
            $new++;   
        }

continue;
}


if($zoomos_data){
    //$sql = "UPDATE * FROM ".DB_PREFIX."product SET stock_status_id=5 AND status=0";
    $sql = "UPDATE ".DB_PREFIX."product SET stock_status_id=5, quantity=0 WHERE updated=0";
    $up = mysqli_query($mysqli, $sql);
}

echo $new.'- новых <br>';
echo $isset.'- существующих <br>';
echo $need_update.'- нужно обновить <br>';
echo $not_update.'- не обновлять <br>';

file_put_contents($log, "\n".$new." - новых \n", FILE_APPEND);
file_put_contents($log, $isset." - существующих \n", FILE_APPEND);
file_put_contents($log, $need_update." - нужно обновить \n", FILE_APPEND);
file_put_contents($log, $not_update." - не обновлять \n", FILE_APPEND);


file_put_contents($log, "END \n", FILE_APPEND);

$path = $_SERVER['DOCUMENT_ROOT'].'/zoomos_data/items.txt';

file_put_contents($path, serialize($arr_add));

