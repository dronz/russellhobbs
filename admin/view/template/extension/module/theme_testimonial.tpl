<?php 
if(empty($module_bg_color)) $module_bg_color ="222222";
if(empty($module_title_color)) $module_title_color ="FFFFFF";
if(empty($module_testimonial_color)) $module_testimonial_color ="AAAAAA";
if(empty($module_name_color)) $module_name_color ="FFFFFF";
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-testimonial" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-testimonial" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-testimonial_title"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="testimonial[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($testimonial[$language['language_id']]) ? $testimonial[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-pr_id"><?php echo $entry_testimonials_per_row; ?></label>
            <div class="col-sm-10">
              <select name="pr_id" id="input-pr_id" class="form-control">
              <?php for ($i=1; $i<=3; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($pr_id==$i) {echo "selected=\"selected\"";}; ?>><?php echo $i; ?></option>
              <?php }; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_bg_color"><?php echo $entry_module_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_bg_color" id="module_bg_color" value="<?php echo $module_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_image_thumb"><?php echo $entry_module_image_thumb; ?></label>
            <div class="col-sm-10">
              <a href="" id="module_image_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $module_image_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="module_image_custom" value="<?php echo $module_image_custom; ?>" id="input-module_image_custom" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_title_color"><?php echo $entry_module_title_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_title_color" id="module_title_color" value="<?php echo $module_title_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_testimonial_color"><?php echo $entry_module_testimonial_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_testimonial_color" id="module_testimonial_color" value="<?php echo $module_testimonial_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_name_color"><?php echo $entry_module_name_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_name_color" id="module_name_color" value="<?php echo $module_name_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <br /><br />
          <legend>Testimonials</legend>
         <div class="row">
         <div class="col-sm-12">
         <table id="sections" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left"><?php echo $entry_photo; ?></td>
                <td class="text-left"><?php echo $entry_testimonial; ?></td>
                <td class="text-left" width="25%"><?php echo $entry_reviewer_name; ?></td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php $section_row = 1; ?>
              <?php foreach ($sections as $section) { ?>
              <tr id="section-row<?php echo $section_row; ?>">

                <td class="text-left">
                <a href="" id="thumb-image<?php echo $section_row; ?>" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo isset($section['image']) ? $section['image'] : $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="sections[<?php echo $section_row; ?>][thumb_image]" value="<?php echo isset($section['thumb_image']) ? $section['thumb_image'] : ''; ?>" id="input-image<?php echo $section_row; ?>" />
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="6" name="sections[<?php echo $section_row; ?>][testimonial_block][<?php echo $language['language_id']; ?>]" id="description-<?php echo $section_row; ?>-<?php echo $language['language_id']; ?>"><?php echo isset($section['testimonial_block'][$language['language_id']]) ? $section['testimonial_block'][$language['language_id']] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="1" name="sections[<?php echo $section_row; ?>][reviewer_name][<?php echo $language['language_id']; ?>]" id="description-<?php echo $section_row; ?>-<?php echo $language['language_id']; ?>"><?php echo isset($section['reviewer_name'][$language['language_id']]) ? $section['reviewer_name'][$language['language_id']] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>
                
                <td class="text-right">
                <button type="button" onclick="$('#section-row<?php echo $section_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                </td>
                
              </tr>
              <?php $section_row++; ?>
			  <?php } ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="3"></td>
                <td class="text-right"><button type="button" onclick="addRow();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </table>
         </div>
         </div>
          
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var section_row = <?php echo $section_row; ?>;

function addRow() {
	html  = '<tr id="section-row' + section_row + '">';

	html += '<td class="text-left">';
	html += '<a href="" id="thumb-image' + section_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="sections[' + section_row + '][thumb_image]" value="" id="input-image' + section_row + '" />';
	html += '  </td>';
	
	html += '<td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group">';
	html += '<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>';
	html += '<textarea name="sections[' + section_row + '][testimonial_block][<?php echo $language['language_id']; ?>]" id="description-' + section_row + '-<?php echo $language['language_id']; ?>" class="form-control" rows="6"></textarea>';
	html += '</div>';
	<?php } ?>
	html += '</td>';

	html += '<td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group">';
	html += '<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>';
	html += '<textarea name="sections[' + section_row + '][reviewer_name][<?php echo $language['language_id']; ?>]" id="description-' + section_row + '-<?php echo $language['language_id']; ?>" class="form-control" rows="1"></textarea>';
	html += '</div>';
	<?php } ?>
	html += '</td>';
	
	html += '  <td class="text-right"><button type="button" onclick="$(\'#section-row' + section_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#sections tbody').append(html);
	
	section_row++;
}
//--></script>
<style type="text/css">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
table {font-size:12px;}
</style>
<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>
<?php echo $footer; ?>