<?php
// Heading
$_['heading_title']     = 'SELLMORE Theme - Features Blocks';

// Text
$_['text_module']       = 'Modules';
$_['text_extension']    = 'Extensions';
$_['text_success']      = 'Success: You have modified SELLMORE Theme - Features Blocks module!';
$_['text_edit']         = 'Edit SELLMORE Theme - Features Blocks Module';

// Entry
$_['entry_name']        = 'Module Name';
$_['entry_awesome_font'] = 'Awesome Font';
$_['entry_title']       = 'Title';
$_['entry_subtitle']    = 'Subtitle';
$_['entry_description'] = 'Description';
$_['entry_status']      = 'Status';
$_['entry_features_blocks_per_row'] = 'Features Blocks in a row';
$_['entry_features_blocks_style'] = 'Features Blocks style';

$_['entry_module_bg_status'] = 'Show background:';
$_['entry_module_bg_color'] = 'Background color:';
$_['entry_module_image_thumb'] = 'Background image:';
$_['entry_module_title_color'] = 'Title color:';
$_['entry_module_subtitle_color'] = 'Subtitle color:';
$_['entry_module_icons_bg_color'] = 'Icons background color:';
$_['entry_module_icon_color'] = 'Awesome Font color:';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify SELLMORE Theme - Features Blocks module!';
$_['error_name']        = 'Module Name must be between 3 and 64 characters!';