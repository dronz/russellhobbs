
<style type="text/css">
.features<?php echo $module; ?> .f-title {
	color: <?php echo $module_title_color; ?>;
}
.features<?php echo $module; ?> span.f-subtitle {
	color: <?php echo $module_subtitle_color; ?>;
}
.features<?php echo $module; ?> span.f-icon {
	background-color: <?php echo $module_icons_bg_color; ?>;
}
.features<?php echo $module; ?> span.f-icon.fi1 i {color: <?php echo $module_icon_1_color; ?>;}
.features<?php echo $module; ?> span.f-icon.fi2 i {color: <?php echo $module_icon_2_color; ?>;}
.features<?php echo $module; ?> span.f-icon.fi3 i {color: <?php echo $module_icon_3_color; ?>;}
.features<?php echo $module; ?> span.f-icon.fi4 i {color: <?php echo $module_icon_4_color; ?>;}
<?php if($module_bg_status ==1) { ?>
.features<?php echo $module; ?> {
<?php if($module_bg_color !='') { ?>
	background-color: <?php echo $module_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($module_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $module_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
    background-position: center;
	background-size: cover;
}
<?php } ?>
</style>

  <div id="features" class="features<?php echo $module; ?> content-padd <?php echo $features_blocks_style; ?>">
  <div class="container">
    <div class="row full-width-container">
    
    <?php if($features_block['title-1']) { ?>
    <div class="theme-modal col-sm-12 col-md-<?php echo $bpr_id; ?> come-item">
    <a href="#" data-toggle="modal" data-target="#modal-fb1">
    <div class="f-content">
      <span class="f-icon fi1">
        <?php if($features_block['awesome-font-1'] !='') { ?>
        <i class="fa fa-<?php echo $features_block['awesome-font-1']; ?>"></i>
        <?php } else { ?>
        <i class="fa fa-paper-plane"></i>
		<?php } ?>
        <i class="fa fa-search"></i>
      </span>
    <span class="f-title"><?php echo $features_block['title-1']; ?></span>
    <?php if($features_block['subtitle-1']) { ?>
    <span class="f-subtitle subtitle"><?php echo $features_block['subtitle-1']; ?></span>
    <?php } ?>
    </div>
    </a>
    <div class="modal fade" id="modal-fb1" tabindex="-1" role="dialog" aria-labelledby="modal-fb1" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="content-footer-fb">
            <h2><?php echo $features_block['title-1']; ?></h2>
            <?php if($features_block['subtitle-1']) { ?>
            <p class="subtitle"><?php echo $features_block['subtitle-1']; ?></p>
            <?php } ?>
            <?php echo $module_description_1; ?>
          </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <?php } ?>
    
    <?php if($features_block['title-2']) { ?>
    <div class="theme-modal col-sm-12 col-md-<?php echo $bpr_id; ?> come-item">
    <a href="#" data-toggle="modal" data-target="#modal-fb2">
    <div class="f-content">
      <span class="f-icon fi2">
        <?php if($features_block['awesome-font-2'] !='') { ?>
        <i class="fa fa-<?php echo $features_block['awesome-font-2']; ?>"></i>
        <?php } else { ?>
        <i class="fa fa-paper-plane"></i>
		<?php } ?>
        <i class="fa fa-search"></i>
      </span>
    <span class="f-title"><?php echo $features_block['title-2']; ?></span>
    <?php if($features_block['subtitle-2']) { ?>
    <span class="f-subtitle subtitle"><?php echo $features_block['subtitle-2']; ?></span>
    <?php } ?>
    </div>
    </a>
    <div class="modal fade" id="modal-fb2" tabindex="-1" role="dialog" aria-labelledby="modal-fb2" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="content-footer-fb">
            <h2><?php echo $features_block['title-2']; ?></h2>
            <?php if($features_block['subtitle-2']) { ?>
            <p class="subtitle"><?php echo $features_block['subtitle-2']; ?></p>
            <?php } ?>
            <?php echo $module_description_2; ?>
          </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <?php } ?>
    
    <?php if($features_block['title-3']) { ?>
    <div class="theme-modal col-sm-12 col-md-<?php echo $bpr_id; ?> come-item">
    <a href="#" data-toggle="modal" data-target="#modal-fb3">
    <div class="f-content">
      <span class="f-icon fi3">
        <?php if($features_block['awesome-font-3'] !='') { ?>
        <i class="fa fa-<?php echo $features_block['awesome-font-3']; ?>"></i>
        <?php } else { ?>
        <i class="fa fa-paper-plane"></i>
		<?php } ?>
        <i class="fa fa-search"></i>
      </span>
    <span class="f-title"><?php echo $features_block['title-3']; ?></span>
    <?php if($features_block['subtitle-3']) { ?>
    <span class="f-subtitle subtitle"><?php echo $features_block['subtitle-3']; ?></span>
    <?php } ?>
    </div>
    </a>
    <div class="modal fade" id="modal-fb3" tabindex="-1" role="dialog" aria-labelledby="modal-fb3" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="content-footer-fb">
            <h2><?php echo $features_block['title-3']; ?></h2>
            <?php if($features_block['subtitle-3']) { ?>
            <p class="subtitle"><?php echo $features_block['subtitle-3']; ?></p>
            <?php } ?>
            <?php echo $module_description_3; ?>
          </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <?php } ?>
    
    <?php if($features_block['title-4']) { ?>
    <div class="theme-modal col-sm-12 col-md-<?php echo $bpr_id; ?> come-item">
    <a href="#" data-toggle="modal" data-target="#modal-fb4">
    <div class="f-content">
      <span class="f-icon fi4">
        <?php if($features_block['awesome-font-4'] !='') { ?>
        <i class="fa fa-<?php echo $features_block['awesome-font-4']; ?>"></i>
        <?php } else { ?>
        <i class="fa fa-paper-plane"></i>
		<?php } ?>
        <i class="fa fa-search"></i>
      </span>
    <span class="f-title"><?php echo $features_block['title-4']; ?></span>
    <?php if($features_block['subtitle-4']) { ?>
    <span class="f-subtitle subtitle"><?php echo $features_block['subtitle-4']; ?></span>
    <?php } ?>
    </div>
    </a>
    <div class="modal fade" id="modal-fb4" tabindex="-1" role="dialog" aria-labelledby="modal-fb4" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="content-footer-fb">
            <h2><?php echo $features_block['title-4']; ?></h2>
            <?php if($features_block['subtitle-4']) { ?>
            <p class="subtitle"><?php echo $features_block['subtitle-4']; ?></p>
            <?php } ?>
            <?php echo $module_description_4; ?>
          </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <?php } ?>
    
    </div>
  </div>
  </div>
