<?php
class ControllerExtensionModuleThemeFAQ extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/theme_faq');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$languages = $this->model_localisation_language->getLanguages();
		
		$this->load->model('extension/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('theme_faq', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_module_question_bg_color'] = $this->language->get('entry_module_question_bg_color');
		$data['entry_module_question_bg_color_hover'] = $this->language->get('entry_module_question_bg_color_hover');
		$data['entry_module_question_color'] = $this->language->get('entry_module_question_color');
		$data['entry_module_question_color_hover'] = $this->language->get('entry_module_question_color_hover');
		$data['entry_module_answer_bg_color'] = $this->language->get('entry_module_answer_bg_color');
		$data['entry_module_answer_color'] = $this->language->get('entry_module_answer_color');

		$data['entry_faq_answer'] = $this->language->get('entry_faq_answer');
		$data['entry_faq_question'] = $this->language->get('entry_faq_question');
		$data['entry_help_content'] = $this->language->get('entry_help_content');
		
		$data['button_add'] = $this->language->get('button_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
			
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/theme_faq', 'token=' . $this->session->data['token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/theme_faq', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true)
			);			
		}
		
		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/theme_faq', 'token=' . $this->session->data['token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/theme_faq', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true);
		}
		
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
		
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}
		
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}
		
		if (isset($this->request->post['faq'])) {
			$data['faq'] = $this->request->post['faq'];
		} elseif (!empty($module_info)) {
			$data['faq'] = $module_info['faq'];
		} else {
			$data['faq'] = '';
		}		
				
		if (isset($this->request->post['module_question_bg_color'])) {
			$data['module_question_bg_color'] = $this->request->post['module_question_bg_color'];
		} elseif (!empty($module_info)) {
			$data['module_question_bg_color'] = $module_info['module_question_bg_color'];
		} else {
			$data['module_question_bg_color'] = '';
		}
		
		if (isset($this->request->post['module_question_color'])) {
			$data['module_question_color'] = $this->request->post['module_question_color'];
		} elseif (!empty($module_info)) {
			$data['module_question_color'] = $module_info['module_question_color'];
		} else {
			$data['module_question_color'] = '';
		}
		
		if (isset($this->request->post['module_question_bg_color_hover'])) {
			$data['module_question_bg_color_hover'] = $this->request->post['module_question_bg_color_hover'];
		} elseif (!empty($module_info)) {
			$data['module_question_bg_color_hover'] = $module_info['module_question_bg_color_hover'];
		} else {
			$data['module_question_bg_color_hover'] = '';
		}
		
		if (isset($this->request->post['module_question_color_hover'])) {
			$data['module_question_color_hover'] = $this->request->post['module_question_color_hover'];
		} elseif (!empty($module_info)) {
			$data['module_question_color_hover'] = $module_info['module_question_color_hover'];
		} else {
			$data['module_question_color_hover'] = '';
		}
		
		if (isset($this->request->post['module_answer_bg_color'])) {
			$data['module_answer_bg_color'] = $this->request->post['module_answer_bg_color'];
		} elseif (!empty($module_info)) {
			$data['module_answer_bg_color'] = $module_info['module_answer_bg_color'];
		} else {
			$data['module_answer_bg_color'] = '';
		}
		
		if (isset($this->request->post['module_answer_color'])) {
			$data['module_answer_color'] = $this->request->post['module_answer_color'];
		} elseif (!empty($module_info)) {
			$data['module_answer_color'] = $module_info['module_answer_color'];
		} else {
			$data['module_answer_color'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}
		
		if (isset($this->request->post['sections'])) {
			$data['sections'] = $this->request->post['sections'];
		} elseif (!empty($module_info)) {
			$sections = $module_info['sections'];
		} else {
			$sections = array();
		}
		
		$data['sections'] = array();
		
		foreach ($sections as $section) {
			
			$data['sections'][] = array(
				'faq_question' => $section['faq_question'],
				'faq_answer' => $section['faq_answer']
			);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/theme_faq.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/theme_faq')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		return !$this->error;
	}
}