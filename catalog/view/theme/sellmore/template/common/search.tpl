
<?php if(($t1o_header_style == 'header-style-9') || ($t1o_header_style == 'header-style-10')) { ?>

	  <div class="mobs_searche_logo_cart search_jivo">
		<div id="search" class="header-search-bar">
			<input id="string_search" type="text" name="search" value="" placeholder="Поиск" class="form-control input-lg">
			<button id="search_butt_mob" type="button" class="btn btn-default btn-lg"><div class="button-i"><i class="fa fa-search"></i></div></button>
			<div id="search_visibl" type="button" class="btn btn-default btn-lg"><div class="button-i"><i class="fa fa-search"></i></div></div>
		</div>
<script>
    $("#search_visibl").click(function () {
          $("#string_search").animate({width:'toggle'},350);
          $("#search_butt_mob").addClass('search_butt_moba');
          $("#search_visibl").addClass('search_visibla');
    });
</script>
	  </div>

<?php } else { ?>

<a href="#" data-toggle="modal" data-target="#modal-search" class="btn search-block">
<div id="search-block" class="buttons-header theme-modal" data-toggle="tooltip" title="<?php echo $text_search; ?>">
<div class="button-i"><i class="fa fa-search"></i></div>
</div>
</a>
<div class="modal fade theme-modal" id="modal-search" tabindex="-1" role="dialog" aria-labelledby="modal-search" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      
      <div id="search" class="input-group">
        <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
        <span class="input-group-addon">
          <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
        </span>
      </div>
      
<?php if($t1o_header_popular_search_status == 1) { ?>  
<div id="popular-search">
    <?php echo $t1o_text_popular_search[$lang_id]; ?><br /><br />
<?php for ($i = 1; $i <= 20; $i++) { ?>
<?php if($t1o_header_popular_search[$i][$lang_id]['word']) { ?>                       
    <a href="index.php?route=product/search&search=<?php echo $t1o_header_popular_search[$i][$lang_id]['word']; ?>" class="btn btn-default popular-search-word">
    <?php echo $t1o_header_popular_search[$i][$lang_id]['word']; ?>
    </a>
<?php } ?>
<?php } ?>
</div>
<?php } ?>
      <a href="index.php?route=product/search" class="btn btn-primary advanced-search"><?php echo $t1o_text_advanced_search[$lang_id]; ?></a>
      </div>
    </div>
  </div>
</div>

<?php } ?>
