
<style type="text/css">
<?php if($banner_pro_hover_bg_color_status =='1') { ?>
#theme-banner-pro<?php echo $module; ?> .theme-banner-pro-bg {
	background-color: <?php echo $banner_pro_hover_bg_color; ?>;
}
#theme-banner-pro<?php echo $module; ?> .theme-banner-pro-item:hover .theme-banner-pro-bg {
	opacity: <?php echo $banner_pro_hover_bg_color_opacity; ?>;
}
<?php } ?>
#content #theme-banner-pro<?php echo $module; ?> {
<?php if($banner_pro_bg_color !='') { ?>
	background-color: <?php echo $banner_pro_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($banner_pro_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $banner_pro_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
}
<?php if($banner_pro_padding =='0') { ?>
#theme-banner-pro<?php echo $module; ?> {
	padding: 0;
}
#theme-banner-pro<?php echo $module; ?> .theme-banner-pro-item-wrapper {
	margin: 0;
}
.theme-banner-pro-wrapper {
	margin: 0;
}
<?php } else { ?>
#theme-banner-pro<?php echo $module; ?> {
	padding: 15px 30px;
}
#theme-banner-pro<?php echo $module; ?> .theme-banner-pro-item-wrapper {
	margin: 15px;
}
.theme-banner-pro-wrapper {
	margin: 0 -15px;
}
<?php } ?>
<?php if($banner_pro_title_shadow =='1') { ?>
#theme-banner-pro<?php echo $module; ?> h2 {
	text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.7);
}
<?php } ?>
</style>

<div id="theme-banner-pro<?php echo $module; ?>" class="panel panel-default theme-banner-pro">
<div>
<div class="theme-banner-pro-wrapper">



<?php if($banner_pro_item['status_1'] =='1') { ?>
<div class="col-sm-<?php echo $banner_pro_item['width_1']; ?> theme-banner-pro-item <?php echo $banner_pro_item['content_position_1']; ?> effect-<?php echo $banner_pro_item['hover_effect_1']; ?>">
  <div class="theme-banner-pro-item-wrapper">
  <a href="<?php echo $banner_pro_item[$lang_id]['url_1']; ?>">
  <img src="<?php echo $path_image . $banner_pro_item_image_custom_1 ?>" alt="<?php echo $banner_pro_item[$lang_id]['title_1']; ?>" title="<?php echo $banner_pro_item[$lang_id]['title_1']; ?>" />
  <span class="theme-banner-pro-bg"></span>
  <div class="theme-banner-pro-content-wrapper">
    <div class="theme-banner-pro-content-table">
      <div class="theme-banner-pro-content-table-cell">
        <?php if(($banner_pro_item[$lang_id]['label_1']) !='') { ?>
        <span class="btn btn-default theme-banner-pro-label" style="background-color: <?php echo $banner_pro_label_color_1; ?>;"><?php echo $banner_pro_item[$lang_id]['label_1']; ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['title_1']) !='') { ?>
        <h2 class="theme-banner-pro-title" style="color: <?php echo $banner_pro_title_color_1; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['title_1'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['subtitle_1']) !='') { ?>
        <span class="theme-banner-pro-subtitle subtitle" style="color: <?php echo $banner_pro_subtitle_color_1; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['subtitle_1'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['button_1']) !='') { ?>
        <span class="btn btn-<?php echo $banner_pro_item['button_style_1']; ?> theme-banner-pro-button"><?php echo $banner_pro_item[$lang_id]['button_1']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($banner_pro_item['status_2'] =='1') { ?>
<div class="col-sm-<?php echo $banner_pro_item['width_2']; ?> theme-banner-pro-item <?php echo $banner_pro_item['content_position_2']; ?> effect-<?php echo $banner_pro_item['hover_effect_2']; ?>">
  <div class="theme-banner-pro-item-wrapper">
  <a href="<?php echo $banner_pro_item[$lang_id]['url_2']; ?>">
  <img src="<?php echo $path_image . $banner_pro_item_image_custom_2 ?>" alt="<?php echo $banner_pro_item[$lang_id]['title_2']; ?>" title="<?php echo $banner_pro_item[$lang_id]['title_2']; ?>" />
  <span class="theme-banner-pro-bg"></span>
  <div class="theme-banner-pro-content-wrapper">
    <div class="theme-banner-pro-content-table">
      <div class="theme-banner-pro-content-table-cell">
        <?php if(($banner_pro_item[$lang_id]['label_2']) !='') { ?>
        <span class="btn btn-default theme-banner-pro-label" style="background-color: <?php echo $banner_pro_label_color_2; ?>;"><?php echo $banner_pro_item[$lang_id]['label_2']; ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['title_2']) !='') { ?>
        <h2 class="theme-banner-pro-title" style="color: <?php echo $banner_pro_title_color_2; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['title_2'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['subtitle_2']) !='') { ?>
        <span class="theme-banner-pro-subtitle subtitle" style="color: <?php echo $banner_pro_subtitle_color_2; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['subtitle_2'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['button_2']) !='') { ?>
        <span class="btn btn-<?php echo $banner_pro_item['button_style_2']; ?> theme-banner-pro-button"><?php echo $banner_pro_item[$lang_id]['button_2']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($banner_pro_item['status_3'] =='1') { ?>
<div class="col-sm-<?php echo $banner_pro_item['width_3']; ?> theme-banner-pro-item <?php echo $banner_pro_item['content_position_3']; ?> effect-<?php echo $banner_pro_item['hover_effect_3']; ?>">
  <div class="theme-banner-pro-item-wrapper">
  <a href="<?php echo $banner_pro_item[$lang_id]['url_3']; ?>">
  <img src="<?php echo $path_image . $banner_pro_item_image_custom_3 ?>" alt="<?php echo $banner_pro_item[$lang_id]['title_3']; ?>" title="<?php echo $banner_pro_item[$lang_id]['title_3']; ?>" />
  <span class="theme-banner-pro-bg"></span>
  <div class="theme-banner-pro-content-wrapper">
    <div class="theme-banner-pro-content-table">
      <div class="theme-banner-pro-content-table-cell">
        <?php if(($banner_pro_item[$lang_id]['label_3']) !='') { ?>
        <span class="btn btn-default theme-banner-pro-label" style="background-color: <?php echo $banner_pro_label_color_3; ?>;"><?php echo $banner_pro_item[$lang_id]['label_3']; ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['title_3']) !='') { ?>
        <h2 class="theme-banner-pro-title" style="color: <?php echo $banner_pro_title_color_3; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['title_3'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['subtitle_3']) !='') { ?>
        <span class="theme-banner-pro-subtitle subtitle" style="color: <?php echo $banner_pro_subtitle_color_3; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['subtitle_3'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['button_3']) !='') { ?>
        <span class="btn btn-<?php echo $banner_pro_item['button_style_3']; ?> theme-banner-pro-button"><?php echo $banner_pro_item[$lang_id]['button_3']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($banner_pro_item['status_4'] =='1') { ?>
<div class="col-sm-<?php echo $banner_pro_item['width_4']; ?> theme-banner-pro-item <?php echo $banner_pro_item['content_position_4']; ?> effect-<?php echo $banner_pro_item['hover_effect_4']; ?>">
  <div class="theme-banner-pro-item-wrapper">
  <a href="<?php echo $banner_pro_item[$lang_id]['url_4']; ?>">
  <img src="<?php echo $path_image . $banner_pro_item_image_custom_4 ?>" alt="<?php echo $banner_pro_item[$lang_id]['title_4']; ?>" title="<?php echo $banner_pro_item[$lang_id]['title_4']; ?>" />
  <span class="theme-banner-pro-bg"></span>
  <div class="theme-banner-pro-content-wrapper">
    <div class="theme-banner-pro-content-table">
      <div class="theme-banner-pro-content-table-cell">
        <?php if(($banner_pro_item[$lang_id]['label_4']) !='') { ?>
        <span class="btn btn-default theme-banner-pro-label" style="background-color: <?php echo $banner_pro_label_color_4; ?>;"><?php echo $banner_pro_item[$lang_id]['label_4']; ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['title_4']) !='') { ?>
        <h2 class="theme-banner-pro-title" style="color: <?php echo $banner_pro_title_color_4; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['title_4'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['subtitle_4']) !='') { ?>
        <span class="theme-banner-pro-subtitle subtitle" style="color: <?php echo $banner_pro_subtitle_color_4; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['subtitle_4'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['button_4']) !='') { ?>
        <span class="btn btn-<?php echo $banner_pro_item['button_style_4']; ?> theme-banner-pro-button"><?php echo $banner_pro_item[$lang_id]['button_4']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($banner_pro_item['status_5'] =='1') { ?>
<div class="col-sm-<?php echo $banner_pro_item['width_5']; ?> theme-banner-pro-item <?php echo $banner_pro_item['content_position_5']; ?> effect-<?php echo $banner_pro_item['hover_effect_5']; ?>">
  <div class="theme-banner-pro-item-wrapper">
  <a href="<?php echo $banner_pro_item[$lang_id]['url_5']; ?>">
  <img src="<?php echo $path_image . $banner_pro_item_image_custom_5 ?>" alt="<?php echo $banner_pro_item[$lang_id]['title_5']; ?>" title="<?php echo $banner_pro_item[$lang_id]['title_5']; ?>" />
  <span class="theme-banner-pro-bg"></span>
  <div class="theme-banner-pro-content-wrapper">
    <div class="theme-banner-pro-content-table">
      <div class="theme-banner-pro-content-table-cell">
        <?php if(($banner_pro_item[$lang_id]['label_5']) !='') { ?>
        <span class="btn btn-default theme-banner-pro-label" style="background-color: <?php echo $banner_pro_label_color_5; ?>;"><?php echo $banner_pro_item[$lang_id]['label_5']; ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['title_5']) !='') { ?>
        <h2 class="theme-banner-pro-title" style="color: <?php echo $banner_pro_title_color_5; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['title_5'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['subtitle_5']) !='') { ?>
        <span class="theme-banner-pro-subtitle subtitle" style="color: <?php echo $banner_pro_subtitle_color_5; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['subtitle_5'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['button_5']) !='') { ?>
        <span class="btn btn-<?php echo $banner_pro_item['button_style_5']; ?> theme-banner-pro-button"><?php echo $banner_pro_item[$lang_id]['button_5']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($banner_pro_item['status_6'] =='1') { ?>
<div class="col-sm-<?php echo $banner_pro_item['width_6']; ?> theme-banner-pro-item <?php echo $banner_pro_item['content_position_6']; ?> effect-<?php echo $banner_pro_item['hover_effect_6']; ?>">
  <div class="theme-banner-pro-item-wrapper">
  <a href="<?php echo $banner_pro_item[$lang_id]['url_6']; ?>">
  <img src="<?php echo $path_image . $banner_pro_item_image_custom_6 ?>" alt="<?php echo $banner_pro_item[$lang_id]['title_6']; ?>" title="<?php echo $banner_pro_item[$lang_id]['title_6']; ?>" />
  <span class="theme-banner-pro-bg"></span>
  <div class="theme-banner-pro-content-wrapper">
    <div class="theme-banner-pro-content-table">
      <div class="theme-banner-pro-content-table-cell">
        <?php if(($banner_pro_item[$lang_id]['label_6']) !='') { ?>
        <span class="btn btn-default theme-banner-pro-label" style="background-color: <?php echo $banner_pro_label_color_6; ?>;"><?php echo $banner_pro_item[$lang_id]['label_6']; ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['title_6']) !='') { ?>
        <h2 class="theme-banner-pro-title" style="color: <?php echo $banner_pro_title_color_6; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['title_6'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['subtitle_6']) !='') { ?>
        <span class="theme-banner-pro-subtitle subtitle" style="color: <?php echo $banner_pro_subtitle_color_6; ?>;"><?php echo htmlspecialchars_decode( $banner_pro_item[$lang_id]['subtitle_6'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($banner_pro_item[$lang_id]['button_6']) !='') { ?>
        <span class="btn btn-<?php echo $banner_pro_item['button_style_6']; ?> theme-banner-pro-button"><?php echo $banner_pro_item[$lang_id]['button_6']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

</div>
</div>
</div>
