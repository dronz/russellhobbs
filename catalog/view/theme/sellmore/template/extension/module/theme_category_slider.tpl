
<style type="text/css">
.category-slider-content<?php echo $module; ?> h2 {
	color: <?php echo $module_title_color; ?>;
}
.category-slider-content<?php echo $module; ?> .category-slider-subtitle {
	color: <?php echo $module_subtitle_color; ?>;
}
.module-style-1 .category-slider-content<?php echo $module; ?> .category-slider-subtitle {
<?php if($module_title_position =='right') { ?>
	padding-left: 20%;
<?php } else { ?>
    padding-right: 20%;
<?php } ?>
}
.category-slider-content<?php echo $module; ?> {
<?php if($module_bg_color !='') { ?>
	background-color: <?php echo $module_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($module_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $module_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
<?php if($module_title_position =='left') { ?>
	background-position: top left;
<?php } else { ?>
	background-position: top right;
<?php } ?>
}
.module-style-1 .category-slider-content<?php echo $module; ?> .panel-inline-title {
	float: <?php echo $module_title_position; ?>; 
}
<?php if($module_title_position =='right') { ?>
.module-style-1 .category-slider-content<?php echo $module; ?> h2, .module-style-1 .category-slider-content<?php echo $module; ?> .category-slider-subtitle {
	text-align: right;
}
.category-slider-content<?php echo $module; ?>.panel-inline-content .panel-inline-title .btn.btn-primary, .module-style-1 .category-slider-content<?php echo $module; ?> h2:before {
	right: 0;
	left: inherit;
	margin-right: 0;
}
<?php } ?>
<?php if($subcategories_status ==1) { ?>
.panel-inline-content .category-slider-item:hover .btn-default {
	top: 30px;
	bottom: auto;
}
.category-slider-item .subcat {
	position: absolute;
	bottom: -350px;
	left: 50%;
	transform: translateX(-50%);
	text-align: center;
	transition: all 0.25s ease-in-out 0s;
	opacity: 0;
}
.category-slider-item:hover .subcat {
	top: 100px;
	opacity: 1;
}
.category-slider-item .subcat ul {
	margin: 0;
	padding: 0;
}
.category-slider-item:hover .image {
	transition: all 0.25s ease-in-out 0s;
	opacity: 0;
}
.category-slider-item .subcat li {
	list-style: none;
}
.category-slider-item .subcat li a {
	margin-bottom: 25px;
}
.category-slider-item .subcat li.all {
	margin-top: 10px;
}
<?php } ?>
</style>

<div id="category-slider" class="panel panel-default panel-inline <?php echo $module_style; ?>">
<div class="category-slider-content<?php echo $module; ?> panel-inline-content row">
<?php if(($category_slider['title']) !='') { ?>
<div class="category-slider-title panel-inline-title col-sm-<?php echo $module_title_width; ?> full-width-container">
<h2><?php echo $category_slider['title']; ?></h2>
<?php if(($category_slider['subtitle']) !='') { ?>
  <div class="category-slider-subtitle panel-inline-subtitle subtitle"><?php echo $category_slider['subtitle']; ?></div>
<?php } ?>
</div>
<?php } ?>

  <?php if(($category_slider['title']) !='') { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> mgrb full-width-container">
  <?php } else { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> full-width">
  <?php } ?>
  
  <div class="category-slider-items<?php echo $module; ?>">  
    <?php foreach ($categories as $category) { ?>
    <div class="category-slider-item"> 
      <?php if ($category['thumb']) { ?>
      <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" /></a></div>
      <?php } ?>
      <a class="btn btn-default inline-name" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      <?php if($subcategories_status ==1) { ?>
      <div class="subcat">
        <?php for ($i = 0; $i < $subcategories_per_column;) { ?>
        <ul>
          <?php $j = $i + ceil($subcategories_per_column); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
          <li class="all"><a href="<?php echo $category['href']; ?>"><?php echo $category_slider['view_more']; ?></a></li>
        </ul>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
  
  </div>
  
</div>
</div>
<script type="text/javascript"><!--
$('.category-slider-items<?php echo $module; ?>').owlCarousel({
	items: <?php echo $ca_id; ?>,
	itemsDesktop : [1199, 3],
    itemsDesktopSmall : [979, 2],
    itemsTablet : [768, 2],
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>
