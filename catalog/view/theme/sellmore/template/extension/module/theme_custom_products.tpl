
<style type="text/css">
#theme-custom-products<?php echo $module; ?> h2 {
	color: <?php echo $module_title_color; ?>;
}
#theme-custom-products<?php echo $module; ?> .theme-custom-products-subtitle {
	color: <?php echo $module_subtitle_color; ?>;
<?php if($module_title_position =='right') { ?>
	padding-left: 25%;
<?php } else { ?>
    padding-right: 25%;
<?php } ?>
}
#theme-custom-products<?php echo $module; ?> {
<?php if($module_bg_color !='') { ?>
	background-color: <?php echo $module_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($module_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $module_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
<?php if($module_title_position =='left') { ?>
	background-position: top left;
<?php } else { ?>
	background-position: top right;
<?php } ?>
}
#theme-custom-products<?php echo $module; ?> .panel-inline-title {
	float: <?php echo $module_title_position; ?>; 
}
<?php if($module_title_position =='right') { ?>
#theme-custom-products<?php echo $module; ?> h2, #theme-custom-products<?php echo $module; ?> .theme-custom-products-subtitle {
	text-align: right;
}
#theme-custom-products<?php echo $module; ?>.panel-inline-content .panel-inline-title .btn.btn-primary, #theme-custom-products<?php echo $module; ?> h2:before {
	right: 0;
	left: inherit;
	margin-right: 0;
}
<?php } ?>
</style>

<div class="theme-custom-products panel panel-default panel-inline module-style-1 mgrb full-width-container">
<div id="theme-custom-products<?php echo $module; ?>" class="box-content theme-custom-products-content panel-inline-content">
  <?php if(($custom_products['title']) !='') { ?>
  <div class="theme-custom-products-title panel-inline-title col-sm-<?php echo $module_title_width; ?>">
  <h2><?php echo $custom_products['title']; ?></h2>
  <?php if(($custom_products['subtitle']) !='') { ?>
  <div class="theme-custom-products-subtitle panel-inline-subtitle subtitle"><?php echo $custom_products['subtitle']; ?></div>
  <?php } ?>
  <?php if(($custom_products['button']) !='') { ?>
  <a href="<?php echo $custom_products['button_url']; ?>" class="btn btn-primary"><?php echo $custom_products['button']; ?></a>
  <?php } ?>
  </div>
  <?php } ?>
  <?php if(($custom_products['title']) !='') { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?>">
  <?php } else { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> full-width">
  <?php } ?>

  <div class="theme-custom-products-items<?php echo $module; ?>">
  <?php foreach ($products as $product) { ?>
  <div class="product-layout-slider product-grid col-xs-12">
    <div class="product-thumb transition">
      <div class="image">     
            <?php if (($product['out_of_stock_quantity'] <= 0) && ($t1o_out_of_stock_badge_status ==1)) { ?>
            <span class="badge out-of-stock"><span><?php echo $product['out_of_stock_badge']; ?></span></span>
            <?php } ?> 
            <span class="badge-wrapper">
            <?php if($t1o_sale_badge_status ==1) { ?>	
            <?php if (($product['special'])&&($t1o_sale_badge_type == 0)) { ?>
            <span class="badge sale"><?php echo $t1o_text_sale[$lang_id]; ?></span>
            <?php } ?> 
            <?php if (($product['special'])&&($t1o_sale_badge_type == 1)) { ?>
            <?php 
            $val1 = preg_replace("/[^0-9.]/", "", $product['special']);
	        $val2 = preg_replace("/[^0-9.]/", "", $product['price']);
            ?>
            <?php
            $res = ($val1 / $val2) * 100;
            $res = 100 - $res;
            $res = round($res, 0);
            ?>
            <span class="badge sale">-<?php echo $res; ?>%</span>
            <?php } ?> 
            <?php } ?> 
      
            <?php if($t1o_new_badge_status ==1) { ?>	
            <?php
            $startDate1 = strtotime(mb_substr($product['newstart'], 0, 10));
            $endDate2 = strtotime(date("Y-m-d"));
            $days = ceil(($endDate2 / 86400)) - ceil(($startDate1 /86400));
            ?>
            <?php $newproductdays = 30; ?>
            <?php if ($days < $newproductdays) { ?>
            <span class="badge new"><?php echo $t1o_text_new_prod[$lang_id]; ?></span>
            <?php } ?>
            <?php } ?>
            </span>
            
            <?php if($t1o_category_prod_box_style =='product-box-style-3') { ?>
            <div class="flybar-top">  
            <div class="flybar-top-items">
            <p class="description"><?php echo $product['description']; ?></p>
            <div class="rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            </div>   
            </div>
            <?php } ?>
            
            <div class="flybar">  
            <div class="flybar-items">
            <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary"><i class="fa fa-shopping-bag"></i></button>
            <a class="btn btn-default quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="wishlist"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare"><i class="fa fa-retweet"></i></button>
            </div>   
            </div>
            
            <?php if ($product['thumb_swap']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" />
            <img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive thumb_swap img-<?php echo $t1d_img_style; ?>" />
            </a>
            <?php } else {?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" /></a>
            <?php } ?> 
      </div>
            
      <div class="caption">
              
                <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></div>
                <div class="product_box_brand"><?php if ($product['brand']) { ?><a href="<?php echo $product['brand_url']; ?>"><?php echo $product['brand']; ?></a><?php } ?></div>
                <p class="description"><?php echo $product['description']; ?></p>

                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                
                <div class="product-list-buttons">
                  <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary cart"><i class="fa fa-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
                  <a class="btn btn-default quickview list-quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>
                </div>
                
      </div>
    </div>
  </div>
  <?php } ?>
  
  </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
$('.theme-custom-products-items<?php echo $module; ?>').owlCarousel({
	items: <?php echo $pr_id; ?>,
	itemsDesktop : [1199, <?php echo $pr_id; ?>],
    itemsDesktopSmall : [979, 1],
    itemsTablet : [768, 2],
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>