
<style type="text/css">
.ei-slider<?php echo $module; ?>, .ei-slider<?php echo $module; ?> .ei-slider-large li, .ei-slider<?php echo $module; ?> .ei-slider-thumbs li {
	background-color: <?php echo $module_bg_color; ?>;
}
.ei-slider<?php echo $module; ?> .ei-title h2 a {
	color: <?php echo $module_product_name_color; ?>;
}
.ei-slider<?php echo $module; ?> .ei-title h3 a, .ei-slider<?php echo $module; ?> .ei-title h4 a .price-old {
	color: <?php echo $module_product_description_color; ?>;
}
.ei-slider<?php echo $module; ?> .ei-title h4 a {
	color: <?php echo $module_product_price_color; ?>;
}
.ei-slider<?php echo $module; ?> .ei-slider-thumbs li.ei-slider-element {
	background-color: <?php echo $module_product_active_border_color; ?>;
}
</style>

<div id="product-slider">
                <div id="ei-slider" class="ei-slider ei-slider<?php echo $module; ?>">
                    <ul class="ei-slider-large">
                    <?php foreach ($products as $product) { ?>
						<li>
                        <?php if ($product['thumb']) { ?>
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                        <?php } ?>
                            <div class="ei-title">
                                <h2><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                                <h3 class="subtitle"><a href="<?php echo $product['href']; ?>"><?php echo $product['description']; ?></a></h3>                                
                                <h4><a href="<?php echo $product['href']; ?>">
                                <?php if (!$product['special']) { ?>
                                <?php echo $product['price']; ?>
                                <?php } else { ?>
                                <span class="price-new"><?php echo $product['special']; ?> <span class="price-old" style="font-size:16px; text-decoration:line-through"><?php echo $product['price']; ?></span>
                                </span>
                                <?php } ?>
                                </a>
                                <br /><br /><br />
                                <a href="<?php echo $product['href']; ?>" class="btn btn-primary hidden-xs hidden-sm"><?php echo $t1o_text_shop_now[$lang_id]; ?></a>
                                </h4>
                            </div>
                        </li>
                    <?php } ?>
                    </ul>
                    <ul class="ei-slider-thumbs">
                        <li class="ei-slider-element">Current</li>
                    <?php foreach ($products as $product) { ?>
						<li><a href="#"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a><div class="thumb_arrow"></div></li>
                    <?php } ?> 
                    </ul>
                </div>
</div>
        <script type="text/javascript">
            $(function() {
                $('#ei-slider').eislideshow({
					autoplay			: true,
					slideshow_interval	: 5000,
					titlesFactor		: 0
                });
            });
        </script>
