
<style type="text/css">
#theme-gallery<?php echo $module; ?> h2 {
	color: <?php echo $module_title_color; ?>;
}
#theme-gallery<?php echo $module; ?> .theme-gallery-subtitle {
	color: <?php echo $module_subtitle_color; ?>;
}
.module-style-1 #theme-gallery<?php echo $module; ?> .theme-gallery-subtitle {
<?php if($module_title_position =='right') { ?>
	padding-left: 20%;
<?php } else { ?>
    padding-right: 20%;
<?php } ?>
}
#theme-gallery<?php echo $module; ?> {
<?php if($module_bg_color !='') { ?>
	background-color: <?php echo $module_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($module_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $module_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
<?php if($module_title_position =='left') { ?>
	background-position: top left;
<?php } else { ?>
	background-position: top right;
<?php } ?>
}
.module-style-1 #theme-gallery<?php echo $module; ?> .panel-inline-title {
	float: <?php echo $module_title_position; ?>; 
}
<?php if($module_title_position =='right') { ?>
.module-style-1 #theme-gallery<?php echo $module; ?> h2, .module-style-1 #theme-gallery<?php echo $module; ?> .theme-gallery-subtitle {
	text-align: right;
}
#theme-gallery<?php echo $module; ?>.panel-inline-content .panel-inline-title .btn.btn-primary, .module-style-1 #theme-gallery<?php echo $module; ?> h2:before {
	right: 0;
	left: inherit;
	margin-right: 0;
}
<?php } ?>
</style>

<div id="theme-gallery" class="panel panel-default panel-inline <?php echo $module_style; ?>">
<div id="theme-gallery<?php echo $module; ?>" class="box-content theme-gallery-content panel-inline-content">
  <?php if(($gallery['title']) !='') { ?>
  <div class="theme-gallery-title panel-inline-title col-sm-<?php echo $module_title_width; ?>">
  <h2><?php echo $gallery['title']; ?></h2>
  <?php if(($gallery['subtitle']) !='') { ?>
  <div class="theme-gallery-subtitle panel-inline-subtitle subtitle"><?php echo $gallery['subtitle']; ?></div>
  <?php } ?>
  <?php if(($gallery['button']) !='') { ?>
  <a href="<?php echo $gallery['button_url']; ?>" class="btn btn-primary" target="_blank"><?php echo $gallery['button']; ?></a>
  <?php } ?>
  </div>
  <?php } ?>
  <?php if(($gallery['title']) !='') { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> mgrb full-width-container">
  <?php } else { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> full-width">
  <?php } ?>
  <div class="theme-gallery-items<?php echo $module; ?>">
  <?php foreach ($banners as $banner) { ?>
  <div class="theme-gallery-item">
  <a href="<?php echo $banner['image']; ?>" title="<?php echo $banner['title']; ?>" class="thumbnail">
  <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" />
  <span class="theme-gallery-bg"></span>
  <span class="gallery-hover-box"><i class="fa fa-search"></i></span>
  </a>
  </div>
  <?php } ?>
  </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.theme-gallery-items<?php echo $module; ?>').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('.theme-gallery-items<?php echo $module; ?>').owlCarousel({
	items: <?php echo $pr_id; ?>,
	itemsDesktop : [1199, 2],
    itemsDesktopSmall : [979, 2],
    itemsTablet : [768, 2],
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>