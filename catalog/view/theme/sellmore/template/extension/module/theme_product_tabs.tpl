
<div class="tabs-products full-width-container">

<?php $tabs = 0; ?>
<div id="product-tabs" class="row content-padd">
        <div class="col-sm-12">
          <ul id="tabs-<?php echo $module; ?>" class="nav nav-tabs">
            <?php if($latest_products) { ?>
            <li class="active pt"><a href="#tab-latest-<?php echo $module; ?>" data-toggle="tab"><?php echo $t1o_text_latest[$lang_id]; ?></a></li>
            <?php } ?>
            <?php if($bestseller_products) { ?>
            <li class="pt"><a href="#tab-bestseller-<?php echo $module; ?>" data-toggle="tab"><?php echo $t1o_text_bestseller[$lang_id]; ?></a></li>
            <?php } ?>
            <?php if($featured_products) { ?>
            <li class="pt"><a href="#tab-featured-<?php echo $module; ?>" data-toggle="tab"><?php echo $t1o_text_featured[$lang_id]; ?></a></li>
            <?php } ?>
            <?php if($special_products) { ?>
            <li class="pt"><a href="#tab-special-<?php echo $module; ?>" data-toggle="tab"><?php echo $t1o_text_special[$lang_id]; ?></a></li>
            <?php } ?>
            <?php if($most_viewed_products) { ?>
            <li class="pt"><a href="#tab-most-viewed-<?php echo $module; ?>" data-toggle="tab"><?php echo $t1o_text_most_viewed[$lang_id]; ?></a></li>
            <?php } ?>
          </ul>


<div class="tab-content"> 


<?php if($latest_products){ ?>
<div id="tab-latest-<?php echo $module; ?>" class="row product-items fade in tab-pane active">
<div id="slider-latest-<?php echo $module; ?>">
  <?php foreach ($latest_products as $product) { ?>
  <?php if($t1o_product_tabs_style ==1) { ?>
  <div class="product-layout-slider product-grid col-xs-12">
  <?php } else { ?>
  <div class="product-layout-grid product-grid col-xs-12">
  <?php } ?>
    <div class="product-thumb transition">
      <div class="image">       
            <?php if (($product['out_of_stock_quantity'] <= 0) && ($t1o_out_of_stock_badge_status ==1)) { ?>
            <span class="badge out-of-stock"><span><?php echo $product['out_of_stock_badge']; ?></span></span>
            <?php } ?> 
            <span class="badge-wrapper">
            <?php if($t1o_sale_badge_status ==1) { ?>	
            <?php if (($product['special'])&&($t1o_sale_badge_type == 0)) { ?>
            <span class="badge sale"><?php echo $t1o_text_sale[$lang_id]; ?></span>
            <?php } ?> 
            <?php if (($product['special'])&&($t1o_sale_badge_type == 1)) { ?>
            <?php 
            $val1 = preg_replace("/[^0-9.]/", "", $product['special']);
	        $val2 = preg_replace("/[^0-9.]/", "", $product['price']);
            ?>
            <?php
            $res = ($val1 / $val2) * 100;
            $res = 100 - $res;
            $res = round($res, 0);
            ?>
            <span class="badge sale">-<?php echo $res; ?>%</span>
            <?php } ?> 
            <?php } ?> 
      
            <?php if($t1o_new_badge_status ==1) { ?>	
            <?php
            $startDate1 = strtotime(mb_substr($product['newstart'], 0, 10));
            $endDate2 = strtotime(date("Y-m-d"));
            $days = ceil(($endDate2 / 86400)) - ceil(($startDate1 /86400));
            ?>
            <?php $newproductdays = 30; ?>
            <?php if ($days < $newproductdays) { ?>
            <span class="badge new"><?php echo $t1o_text_new_prod[$lang_id]; ?></span>
            <?php } ?>
            <?php } ?>
            </span>
            
            <?php if($t1o_category_prod_box_style =='product-box-style-3') { ?>
            <div class="flybar-top">  
            <div class="flybar-top-items">
            <p class="description"><?php echo $product['description']; ?></p>
            <div class="rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            </div>   
            </div>
            <?php } ?>
      
            <div class="flybar">  
            <div class="flybar-items">
            <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary"><i class="fa fa-shopping-bag"></i></button>
            <a class="btn btn-default quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="wishlist"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare"><i class="fa fa-retweet"></i></button>
            </div>   
            </div>
            
            <?php if ($product['thumb_swap']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" />
            <img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive thumb_swap img-<?php echo $t1d_img_style; ?>" />
            </a>
            <?php } else {?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" /></a>
            <?php } ?> 
      </div>
      <div class="caption">
              
                <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></div>
                <div class="product_box_brand"><?php if ($product['brand']) { ?><a href="<?php echo $product['brand_url']; ?>"><?php echo $product['brand']; ?></a><?php } ?></div>
                <p class="description"><?php echo $product['description']; ?></p>

                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                
                <div class="product-list-buttons">
                  <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary cart"><i class="fa fa-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
                  <a class="btn btn-default quickview list-quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>
                </div>
                
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php if($t1o_product_tabs_style ==1) { ?>  
<script type="text/javascript"><!--
$('#slider-latest-<?php echo $module; ?>').owlCarousel({
	items: <?php echo $t1o_product_tabs_per_row; ?>,
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>
<?php } ?>
</div>

<?php } ?>

<?php if($bestseller_products){ ?>
 <div id="tab-bestseller-<?php echo $module; ?>" class="row product-items fade tab-pane">
 <div id="slider-bestseller-<?php echo $module; ?>">
  <?php foreach ($bestseller_products as $product) { ?>
  <?php if($t1o_product_tabs_style ==1) { ?>
  <div class="product-layout-slider product-grid col-xs-12">
  <?php } else { ?>
  <div class="product-layout-grid product-grid col-xs-12">
  <?php } ?>
    <div class="product-thumb transition">
      <div class="image">       
            <?php if (($product['out_of_stock_quantity'] <= 0) && ($t1o_out_of_stock_badge_status ==1)) { ?>
            <span class="badge out-of-stock"><span><?php echo $product['out_of_stock_badge']; ?></span></span>
            <?php } ?> 
            <span class="badge-wrapper">
            <?php if($t1o_sale_badge_status ==1) { ?>	
            <?php if (($product['special'])&&($t1o_sale_badge_type == 0)) { ?>
            <span class="badge sale"><?php echo $t1o_text_sale[$lang_id]; ?></span>
            <?php } ?> 
            <?php if (($product['special'])&&($t1o_sale_badge_type == 1)) { ?>
            <?php 
            $val1 = preg_replace("/[^0-9.]/", "", $product['special']);
	        $val2 = preg_replace("/[^0-9.]/", "", $product['price']);
            ?>
            <?php
            $res = ($val1 / $val2) * 100;
            $res = 100 - $res;
            $res = round($res, 0);
            ?>
            <span class="badge sale">-<?php echo $res; ?>%</span>
            <?php } ?> 
            <?php } ?> 
      
            <?php if($t1o_new_badge_status ==1) { ?>	
            <?php
            $startDate1 = strtotime(mb_substr($product['newstart'], 0, 10));
            $endDate2 = strtotime(date("Y-m-d"));
            $days = ceil(($endDate2 / 86400)) - ceil(($startDate1 /86400));
            ?>
            <?php $newproductdays = 30; ?>
            <?php if ($days < $newproductdays) { ?>
            <span class="badge new"><?php echo $t1o_text_new_prod[$lang_id]; ?></span>
            <?php } ?>
            <?php } ?>
            </span>
            
            <?php if($t1o_category_prod_box_style =='product-box-style-3') { ?>
            <div class="flybar-top">  
            <div class="flybar-top-items">
            <p class="description"><?php echo $product['description']; ?></p>
            <div class="rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            </div>   
            </div>
            <?php } ?>
      
            <div class="flybar">  
            <div class="flybar-items">
            <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary"><i class="fa fa-shopping-bag"></i></button>
            <a class="btn btn-default quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="wishlist"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare"><i class="fa fa-retweet"></i></button>
            </div>   
            </div>
            
            <?php if ($product['thumb_swap']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" />
            <img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive thumb_swap img-<?php echo $t1d_img_style; ?>" />
            </a>
            <?php } else {?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" /></a>
            <?php } ?> 
      </div>
      <div class="caption">
              
                <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></div>
                <div class="product_box_brand"><?php if ($product['brand']) { ?><a href="<?php echo $product['brand_url']; ?>"><?php echo $product['brand']; ?></a><?php } ?></div>
                <p class="description"><?php echo $product['description']; ?></p>

                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                
                <div class="product-list-buttons">
                  <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary cart"><i class="fa fa-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
                  <a class="btn btn-default quickview list-quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>
                </div>
                
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php if($t1o_product_tabs_style ==1) { ?>
<script type="text/javascript"><!--
$('#slider-bestseller-<?php echo $module; ?>').owlCarousel({
	items: <?php echo $t1o_product_tabs_per_row; ?>,
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>
<?php } ?>   
 </div>
<?php } ?>

<?php if($featured_products){ ?>
  <div id="tab-featured-<?php echo $module; ?>" class="row product-items fade tab-pane">
  <div id="slider-featured-<?php echo $module; ?>">
  <?php foreach ($featured_products as $product) { ?>
  <?php if($t1o_product_tabs_style ==1) { ?>
  <div class="product-layout-slider product-grid col-xs-12">
  <?php } else { ?>
  <div class="product-layout-grid product-grid col-xs-12">
  <?php } ?>
    <div class="product-thumb transition">
      <div class="image">       
            <?php if (($product['out_of_stock_quantity'] <= 0) && ($t1o_out_of_stock_badge_status ==1)) { ?>
            <span class="badge out-of-stock"><span><?php echo $product['out_of_stock_badge']; ?></span></span>
            <?php } ?> 
            <span class="badge-wrapper">
            <?php if($t1o_sale_badge_status ==1) { ?>	
            <?php if (($product['special'])&&($t1o_sale_badge_type == 0)) { ?>
            <span class="badge sale"><?php echo $t1o_text_sale[$lang_id]; ?></span>
            <?php } ?> 
            <?php if (($product['special'])&&($t1o_sale_badge_type == 1)) { ?>
            <?php 
            $val1 = preg_replace("/[^0-9.]/", "", $product['special']);
	        $val2 = preg_replace("/[^0-9.]/", "", $product['price']);
            ?>
            <?php
            $res = ($val1 / $val2) * 100;
            $res = 100 - $res;
            $res = round($res, 0);
            ?>
            <span class="badge sale">-<?php echo $res; ?>%</span>
            <?php } ?> 
            <?php } ?> 
      
            <?php if($t1o_new_badge_status ==1) { ?>	
            <?php
            $startDate1 = strtotime(mb_substr($product['newstart'], 0, 10));
            $endDate2 = strtotime(date("Y-m-d"));
            $days = ceil(($endDate2 / 86400)) - ceil(($startDate1 /86400));
            ?>
            <?php $newproductdays = 30; ?>
            <?php if ($days < $newproductdays) { ?>
            <span class="badge new"><?php echo $t1o_text_new_prod[$lang_id]; ?></span>
            <?php } ?>
            <?php } ?>
            </span>
            
            <?php if($t1o_category_prod_box_style =='product-box-style-3') { ?>
            <div class="flybar-top">  
            <div class="flybar-top-items">
            <p class="description"><?php echo $product['description']; ?></p>
            <div class="rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            </div>   
            </div>
            <?php } ?>
      
            <div class="flybar">  
            <div class="flybar-items">
            <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary"><i class="fa fa-shopping-bag"></i></button>
            <a class="btn btn-default quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="wishlist"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare"><i class="fa fa-retweet"></i></button>
            </div>   
            </div>
            
            <?php if ($product['thumb_swap']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" />
            <img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive thumb_swap img-<?php echo $t1d_img_style; ?>" />
            </a>
            <?php } else {?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" /></a>
            <?php } ?> 
      </div>
            
      <div class="caption">
              
                <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></div>
                <div class="product_box_brand"><?php if ($product['brand']) { ?><a href="<?php echo $product['brand_url']; ?>"><?php echo $product['brand']; ?></a><?php } ?></div>
                <p class="description"><?php echo $product['description']; ?></p>

                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                
                <div class="product-list-buttons">
                  <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary cart"><i class="fa fa-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
                  <a class="btn btn-default quickview list-quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>
                </div> 
                
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php if($t1o_product_tabs_style ==1) { ?>
<script type="text/javascript"><!--
$('#slider-featured-<?php echo $module; ?>').owlCarousel({
	items: <?php echo $t1o_product_tabs_per_row; ?>,
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script> 
<?php } ?> 
 </div>
<?php } ?>

<?php if($special_products){ ?>
 <div id="tab-special-<?php echo $module; ?>" class="row product-items fade tab-pane">
 <div id="slider-special-<?php echo $module; ?>">
  <?php foreach ($special_products as $product) { ?>
  <?php if($t1o_product_tabs_style ==1) { ?>
  <div class="product-layout-slider product-grid col-xs-12">
  <?php } else { ?>
  <div class="product-layout-grid product-grid col-xs-12">
  <?php } ?>
    <div class="product-thumb transition">
      <div class="image">       
            <?php if (($product['out_of_stock_quantity'] <= 0) && ($t1o_out_of_stock_badge_status ==1)) { ?>
            <span class="badge out-of-stock"><span><?php echo $product['out_of_stock_badge']; ?></span></span>
            <?php } ?> 
            <span class="badge-wrapper">
            <?php if($t1o_sale_badge_status ==1) { ?>	
            <?php if (($product['special'])&&($t1o_sale_badge_type == 0)) { ?>
            <span class="badge sale"><?php echo $t1o_text_sale[$lang_id]; ?></span>
            <?php } ?> 
            <?php if (($product['special'])&&($t1o_sale_badge_type == 1)) { ?>
            <?php 
            $val1 = preg_replace("/[^0-9.]/", "", $product['special']);
	        $val2 = preg_replace("/[^0-9.]/", "", $product['price']);
            ?>
            <?php
            $res = ($val1 / $val2) * 100;
            $res = 100 - $res;
            $res = round($res, 0);
            ?>
            <span class="badge sale">-<?php echo $res; ?>%</span>
            <?php } ?> 
            <?php } ?> 
      
            <?php if($t1o_new_badge_status ==1) { ?>	
            <?php
            $startDate1 = strtotime(mb_substr($product['newstart'], 0, 10));
            $endDate2 = strtotime(date("Y-m-d"));
            $days = ceil(($endDate2 / 86400)) - ceil(($startDate1 /86400));
            ?>
            <?php $newproductdays = 30; ?>
            <?php if ($days < $newproductdays) { ?>
            <span class="badge new"><?php echo $t1o_text_new_prod[$lang_id]; ?></span>
            <?php } ?>
            <?php } ?>
            </span>
            
            <?php if($t1o_category_prod_box_style =='product-box-style-3') { ?>
            <div class="flybar-top">  
            <div class="flybar-top-items">
            <p class="description"><?php echo $product['description']; ?></p>
            <div class="rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            </div>   
            </div>
            <?php } ?>
      
            <div class="flybar">  
            <div class="flybar-items">
            <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary"><i class="fa fa-shopping-bag"></i></button>
            <a class="btn btn-default quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="wishlist"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare"><i class="fa fa-retweet"></i></button>
            </div>   
            </div>
            
            <?php if ($product['thumb_swap']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" />
            <img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive thumb_swap img-<?php echo $t1d_img_style; ?>" />
            </a>
            <?php } else {?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" /></a>
            <?php } ?> 
      </div>
      <div class="caption">
              
                <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></div>
                <div class="product_box_brand"><?php if ($product['brand']) { ?><a href="<?php echo $product['brand_url']; ?>"><?php echo $product['brand']; ?></a><?php } ?></div>
                <p class="description"><?php echo $product['description']; ?></p>

                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                
                <div class="product-list-buttons">
                  <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary cart"><i class="fa fa-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
                  <a class="btn btn-default quickview list-quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>
                </div>
                
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php if($t1o_product_tabs_style ==1) { ?>  
<script type="text/javascript"><!--
$('#slider-special-<?php echo $module; ?>').owlCarousel({
	items: <?php echo $t1o_product_tabs_per_row; ?>,
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>
<?php } ?>
 </div>
<?php } ?>

<?php if($most_viewed_products){ ?>
<div id="tab-most-viewed-<?php echo $module; ?>" class="row product-items fade tab-pane">
<div id="slider-most-viewed-<?php echo $module; ?>">
  <?php foreach ($most_viewed_products as $product) { ?>
  <?php if($t1o_product_tabs_style ==1) { ?>
  <div class="product-layout-slider product-grid col-xs-12">
  <?php } else { ?>
  <div class="product-layout-grid product-grid col-xs-12">
  <?php } ?>
    <div class="product-thumb transition">
      <div class="image">       
            <?php if (($product['out_of_stock_quantity'] <= 0) && ($t1o_out_of_stock_badge_status ==1)) { ?>
            <span class="badge out-of-stock"><span><?php echo $product['out_of_stock_badge']; ?></span></span>
            <?php } ?>
            <span class="badge-wrapper">
            <?php if($t1o_sale_badge_status ==1) { ?>	
            <?php if (($product['special'])&&($t1o_sale_badge_type == 0)) { ?>
            <span class="badge sale"><?php echo $t1o_text_sale[$lang_id]; ?></span>
            <?php } ?> 
            <?php if (($product['special'])&&($t1o_sale_badge_type == 1)) { ?>
            <?php 
            $val1 = preg_replace("/[^0-9.]/", "", $product['special']);
	        $val2 = preg_replace("/[^0-9.]/", "", $product['price']);
            ?>
            <?php
            $res = ($val1 / $val2) * 100;
            $res = 100 - $res;
            $res = round($res, 0);
            ?>
            <span class="badge sale">-<?php echo $res; ?>%</span>
            <?php } ?> 
            <?php } ?> 
      
            <?php if($t1o_new_badge_status ==1) { ?>	
            <?php
            $startDate1 = strtotime(mb_substr($product['newstart'], 0, 10));
            $endDate2 = strtotime(date("Y-m-d"));
            $days = ceil(($endDate2 / 86400)) - ceil(($startDate1 /86400));
            ?>
            <?php $newproductdays = 30; ?>
            <?php if ($days < $newproductdays) { ?>
            <span class="badge new"><?php echo $t1o_text_new_prod[$lang_id]; ?></span>
            <?php } ?>
            <?php } ?>
            </span>
            
            <?php if($t1o_category_prod_box_style =='product-box-style-3') { ?>
            <div class="flybar-top">  
            <div class="flybar-top-items">
            <p class="description"><?php echo $product['description']; ?></p>
            <div class="rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            </div>   
            </div>
            <?php } ?>
      
            <div class="flybar">  
            <div class="flybar-items">
            <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary"><i class="fa fa-shopping-bag"></i></button>
            <a class="btn btn-default quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="wishlist"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare"><i class="fa fa-retweet"></i></button>
            </div>   
            </div>
            
            <?php if ($product['thumb_swap']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" />
            <img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive thumb_swap img-<?php echo $t1d_img_style; ?>" />
            </a>
            <?php } else {?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" /></a>
            <?php } ?> 
      </div>
      <div class="caption">
              
                <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></div>
                <div class="product_box_brand"><?php if ($product['brand']) { ?><a href="<?php echo $product['brand_url']; ?>"><?php echo $product['brand']; ?></a><?php } ?></div>
                <p class="description"><?php echo $product['description']; ?></p>
                
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                
                <div class="product-list-buttons">
                  <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary cart"><i class="fa fa-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
                  <a class="btn btn-default quickview list-quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>
                  <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>
                </div>
                
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php if($t1o_product_tabs_style ==1) { ?>  
<script type="text/javascript"><!--
$('#slider-most-viewed-<?php echo $module; ?>').owlCarousel({
	items: <?php echo $t1o_product_tabs_per_row; ?>,
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>
<?php } ?>
 
 </div>
<?php } ?>

</div>
</div>
</div>
</div>
