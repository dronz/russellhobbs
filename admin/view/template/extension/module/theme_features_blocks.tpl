<?php 
if(empty($module_bg_color)) $module_bg_color ="222222";
if(empty($module_title_color)) $module_title_color ="FFFFFF";
if(empty($module_subtitle_color)) $module_subtitle_color ="AAAAAA";
if(empty($module_icons_bg_color)) $module_icons_bg_color ="2A2A2A";
if(empty($module_icon_1_color)) $module_icon_1_color ="FFFFFF";
if(empty($module_icon_2_color)) $module_icon_2_color ="FFFFFF";
if(empty($module_icon_3_color)) $module_icon_3_color ="FFFFFF";
if(empty($module_icon_4_color)) $module_icon_4_color ="FFFFFF";
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-theme-features-blocks" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-form-theme-features-blocks" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-bpr_id"><?php echo $entry_features_blocks_per_row; ?></label>
            <div class="col-sm-10">
              <select name="bpr_id" id="input-bpr_id" class="form-control">
              <option value="12" <?php if ($bpr_id == '12') {echo "selected=\"selected\"";}; ?>>1</option>
              <option value="6" <?php if ($bpr_id == '6') {echo "selected=\"selected\"";}; ?>>2</option>
              <option value="4" <?php if ($bpr_id == '4') {echo "selected=\"selected\"";}; ?>>3</option>
              <option value="3" <?php if ($bpr_id == '3') {echo "selected=\"selected\"";}; ?>>4</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_blocks_style"><?php echo $entry_features_blocks_style; ?></label>
            <div class="col-sm-10">
              <select name="features_blocks_style" id="input-features_blocks_style" class="form-control">
                <option value="style-1" <?php if ($features_blocks_style == 'style-1') {echo "selected=\"selected\"";}; ?>>Style 1 - Left</option>
                <option value="style-2" <?php if ($features_blocks_style == 'style-2') {echo "selected=\"selected\"";}; ?>>Style 2 - Center</option>
                <option value="style-3" <?php if ($features_blocks_style == 'style-3') {echo "selected=\"selected\"";}; ?>>Style 3 - Column</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_bg_status"><?php echo $entry_module_bg_status; ?></label>
            <div class="col-sm-10">
              <select name="module_bg_status" id="input-module_bg_status" class="form-control">
                <option value="1" <?php if ($module_bg_status == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                <option value="0" <?php if ($module_bg_status == '0') {echo "selected=\"selected\"";}; ?>>No</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_bg_color"><?php echo $entry_module_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_bg_color" id="module_bg_color" value="<?php echo $module_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_image_thumb"><?php echo $entry_module_image_thumb; ?></label>
            <div class="col-sm-10">
              <a href="" id="module_image_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $module_image_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="module_image_custom" value="<?php echo $module_image_custom; ?>" id="input-module_image_custom" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_title_color"><?php echo $entry_module_title_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_title_color" id="module_title_color" value="<?php echo $module_title_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_subtitle_color"><?php echo $entry_module_subtitle_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_subtitle_color" id="module_subtitle_color" value="<?php echo $module_subtitle_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_icons_bg_color"><?php echo $entry_module_icons_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_icons_bg_color" id="module_icons_bg_color" value="<?php echo $module_icons_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <legend>Features Block 1</legend>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_awesome_font_1"><?php echo $entry_awesome_font; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][awesome-font-1]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['awesome-font-1'] : ''; ?>" placeholder="<?php echo $entry_awesome_font; ?>" class="form-control" />
						</div>
				<?php } ?>
                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span><br />
                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_icon_1_color"><?php echo $entry_module_icon_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_icon_1_color" id="module_icon_1_color" value="<?php echo $module_icon_1_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_title_1"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][title-1]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['title-1'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_subtitle_1"><?php echo $entry_subtitle; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][subtitle-1]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['subtitle-1'] : ''; ?>" placeholder="<?php echo $entry_subtitle; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>     
          <div class="tab-pane">
            <ul class="nav nav-tabs" id="language">
              <?php foreach ($languages as $language) { ?>
              <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($languages as $language) { ?>
              <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-description-1<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                  <div class="col-sm-10">
                    <textarea name="module_description_1[<?php echo $language['language_id']; ?>][description-1]" placeholder="<?php echo $entry_description; ?>" id="input-description-1<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($module_description_1[$language['language_id']]['description-1']) ? $module_description_1[$language['language_id']]['description-1'] : ''; ?></textarea>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          
          <legend>Features Block 2</legend>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_awesome_font_2"><?php echo $entry_awesome_font; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][awesome-font-2]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['awesome-font-2'] : ''; ?>" placeholder="<?php echo $entry_awesome_font; ?>" class="form-control" />
						</div>
				<?php } ?>
                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span><br />
                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_icon_2_color"><?php echo $entry_module_icon_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_icon_2_color" id="module_icon_2_color" value="<?php echo $module_icon_2_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_title_2"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][title-2]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['title-2'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_subtitle_2"><?php echo $entry_subtitle; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][subtitle-2]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['subtitle-2'] : ''; ?>" placeholder="<?php echo $entry_subtitle; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>     
          <div class="tab-pane">
            <ul class="nav nav-tabs" id="language">
              <?php foreach ($languages as $language) { ?>
              <li><a href="#language-2<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($languages as $language) { ?>
              <div class="tab-pane" id="language-2<?php echo $language['language_id']; ?>">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-description-2<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                  <div class="col-sm-10">
                    <textarea name="module_description_2[<?php echo $language['language_id']; ?>][description-2]" placeholder="<?php echo $entry_description; ?>" id="input-description-2<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($module_description_2[$language['language_id']]['description-2']) ? $module_description_2[$language['language_id']]['description-2'] : ''; ?></textarea>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          
          <legend>Features Block 3</legend>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_awesome_font_3"><?php echo $entry_awesome_font; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][awesome-font-3]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['awesome-font-3'] : ''; ?>" placeholder="<?php echo $entry_awesome_font; ?>" class="form-control" />
						</div>
				<?php } ?>
                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span><br />
                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_icon_3_color"><?php echo $entry_module_icon_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_icon_3_color" id="module_icon_3_color" value="<?php echo $module_icon_3_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_title_3"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][title-3]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['title-3'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_subtitle_3"><?php echo $entry_subtitle; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][subtitle-3]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['subtitle-3'] : ''; ?>" placeholder="<?php echo $entry_subtitle; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>     
          <div class="tab-pane">
            <ul class="nav nav-tabs" id="language">
              <?php foreach ($languages as $language) { ?>
              <li><a href="#language-3<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($languages as $language) { ?>
              <div class="tab-pane" id="language-3<?php echo $language['language_id']; ?>">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-description-3<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                  <div class="col-sm-10">
                    <textarea name="module_description_3[<?php echo $language['language_id']; ?>][description-3]" placeholder="<?php echo $entry_description; ?>" id="input-description-3<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($module_description_3[$language['language_id']]['description-3']) ? $module_description_3[$language['language_id']]['description-3'] : ''; ?></textarea>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          
          <legend>Features Block 4</legend>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_awesome_font_4"><?php echo $entry_awesome_font; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][awesome-font-4]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['awesome-font-4'] : ''; ?>" placeholder="<?php echo $entry_awesome_font; ?>" class="form-control" />
						</div>
				<?php } ?>
                <span class="k_help">Enter the name of an icon, for example: <b>car</b></span><br />
                <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_icon_4_color"><?php echo $entry_module_icon_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_icon_4_color" id="module_icon_4_color" value="<?php echo $module_icon_4_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_title_4"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][title-4]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['title-4'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-features_block_subtitle_4"><?php echo $entry_subtitle; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="features_block[<?php echo $language['language_id']; ?>][subtitle-4]" value="<?php echo isset($features_block[$language['language_id']]) ? $features_block[$language['language_id']]['subtitle-4'] : ''; ?>" placeholder="<?php echo $entry_subtitle; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>     
          <div class="tab-pane">
            <ul class="nav nav-tabs" id="language">
              <?php foreach ($languages as $language) { ?>
              <li><a href="#language-4<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($languages as $language) { ?>
              <div class="tab-pane" id="language-4<?php echo $language['language_id']; ?>">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-description-4<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                  <div class="col-sm-10">
                    <textarea name="module_description_4[<?php echo $language['language_id']; ?>][description-4]" placeholder="<?php echo $entry_description; ?>" id="input-description-4<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($module_description_4[$language['language_id']]['description-4']) ? $module_description_4[$language['language_id']]['description-4'] : ''; ?></textarea>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>  
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#input-description-1<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#input-description-2<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#input-description-3<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#input-description-4<?php echo $language['language_id']; ?>').summernote({height: 300});
<?php } ?>
//--></script> 
  <script type="text/javascript"><!--
$('#language li:first-child a').tab('show');
$('#language-2 li:first-child a').tab('show');
$('#language-3 li:first-child a').tab('show');
$('#language-4 li:first-child a').tab('show');
//--></script></div>
<style type="text/css">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
.panel-body legend {padding: 20px 0 15px;}
.k_help {color:#999999;font-size:12px;padding-left:5px;}
</style>
<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>
<?php echo $footer; ?>