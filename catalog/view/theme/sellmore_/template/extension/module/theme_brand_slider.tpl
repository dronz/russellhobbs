
<style type="text/css">
.brand-slider-content<?php echo $module; ?> h2 {
	color: <?php echo $module_title_color; ?>;
}
.brand-slider-content<?php echo $module; ?> .brand-slider-subtitle {
	color: <?php echo $module_subtitle_color; ?>;
}
.module-style-1 .brand-slider-content<?php echo $module; ?> .brand-slider-subtitle {
<?php if($module_title_position =='right') { ?>
	padding-left: 20%;
<?php } else { ?>
    padding-right: 20%;
<?php } ?>
}
.brand-slider-content<?php echo $module; ?> {
<?php if($module_bg_color !='') { ?>
	background-color: <?php echo $module_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($module_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $module_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
<?php if($module_title_position =='left') { ?>
	background-position: top left;
<?php } else { ?>
	background-position: top right;
<?php } ?>
}
.module-style-1 .brand-slider-content<?php echo $module; ?> .panel-inline-title {
	float: <?php echo $module_title_position; ?>; 
}
<?php if($module_title_position =='right') { ?>
.module-style-1 .brand-slider-content<?php echo $module; ?> h2, .module-style-1 .brand-slider-content<?php echo $module; ?> .brand-slider-subtitle {
	text-align: right;
}
.brand-slider-content<?php echo $module; ?>.panel-inline-content .panel-inline-title + .btn.btn-primary, .module-style-1 .brand-slider-content<?php echo $module; ?> h2:before {
	right: 0;
	left: inherit;
	margin-right: 0;
}
<?php } ?>
<?php if($brands_display_style =='1') { ?>
.panel-inline-content .brand-slider-item .btn-default.inline-name {
	display: none;
}
<?php } else { ?>
.panel-inline-content .brand-slider-item .image {
	margin-top: 0px;
	margin-bottom: 45px;
}
<?php } ?>
</style>

<div id="brand-slider" class="panel panel-default panel-inline <?php echo $module_style; ?>">
<div class="brand-slider-content<?php echo $module; ?> panel-inline-content row">
<?php if(($brand_slider['title']) !='') { ?>
<div class="brand-slider-title panel-inline-title col-sm-<?php echo $module_title_width; ?>">
<h2><?php echo $brand_slider['title']; ?></h2>
<?php if(($brand_slider['subtitle']) !='') { ?>
<div class="brand-slider-subtitle panel-inline-subtitle subtitle"><?php echo $brand_slider['subtitle']; ?></div>
<?php } ?>
<?php if(($brand_slider['button']) !='') { ?>
<a href="index.php?route=product/manufacturer" class="btn btn-primary"><?php echo $brand_slider['button']; ?></a>
<?php } ?>
</div>
<?php } ?>
<?php if(($brand_slider['title']) !='') { ?>
<div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> mgrb full-width-container">
<?php } else { ?>
<div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> full-width">
<?php } ?>
<div class="brand-slider-items<?php echo $module; ?>">  
<?php if ($manufacturers) { ?>
<?php foreach ($manufacturers as $manufacturer) { ?>
    <div class="brand-slider-item style-<?php echo $brands_display_style; ?>">
          <a href="<?php echo $manufacturer['href']; ?>">
          <?php if ($manufacturer['image']) { ?>
          <div class="image"><img src="<?php echo $manufacturer['image']; ?>" title="<?php echo $manufacturer['name']; ?>" alt="<?php echo $manufacturer['name']; ?>" /></div>
          <?php } else { ?>
          <div class="image"><i class="fa fa-camera"></i></div>
          <?php } ?>
          <div class="btn btn-default inline-name"><?php echo $manufacturer['name']; ?></div>
          </a>  
    </div>
<?php } ?>
<?php } ?>
</div>
</div>
</div>
</div>
<script type="text/javascript"><!--
$('.brand-slider-items<?php echo $module; ?>').owlCarousel({
	items: <?php echo $ca_id; ?>,
	autoPlay: 5000,
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>
