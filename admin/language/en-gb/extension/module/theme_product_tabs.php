<?php
// Heading
$_['heading_title']    = 'SELLMORE Theme - Product Tabs';

// Text
$_['text_module']      = 'Modules';
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified SELLMORE Theme - Product Tabs module!';
$_['text_edit']        = 'Edit SELLMORE Theme - Product Tabs Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_product']    = 'Featured Products';
$_['entry_limit']      = 'Limit';
$_['entry_image']      = 'Image (W x H) and Resize Type';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify SELLMORE Theme - Product Tabs module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';