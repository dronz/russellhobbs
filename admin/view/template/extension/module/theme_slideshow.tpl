<?php 
if(empty($slideshow_hover_bg_color)) $slideshow_hover_bg_color ="000000";
if(empty($slideshow_label_color_1)) $slideshow_label_color_1 ="16B778";
if(empty($slideshow_title_color_1)) $slideshow_title_color_1 ="222222";
if(empty($slideshow_subtitle_color_1)) $slideshow_subtitle_color_1 ="AAAAAA";
if(empty($slideshow_label_color_2)) $slideshow_label_color_2 ="16B778";
if(empty($slideshow_title_color_2)) $slideshow_title_color_2 ="222222";
if(empty($slideshow_subtitle_color_2)) $slideshow_subtitle_color_2 ="AAAAAA";
if(empty($slideshow_label_color_3)) $slideshow_label_color_3 ="16B778";
if(empty($slideshow_title_color_3)) $slideshow_title_color_3 ="222222";
if(empty($slideshow_subtitle_color_3)) $slideshow_subtitle_color_3 ="AAAAAA";
if(empty($slideshow_label_color_4)) $slideshow_label_color_4 ="16B778";
if(empty($slideshow_title_color_4)) $slideshow_title_color_4 ="222222";
if(empty($slideshow_subtitle_color_4)) $slideshow_subtitle_color_4 ="AAAAAA";
if(empty($slideshow_label_color_5)) $slideshow_label_color_5 ="16B778";
if(empty($slideshow_title_color_5)) $slideshow_title_color_5 ="222222";
if(empty($slideshow_subtitle_color_5)) $slideshow_subtitle_color_5 ="AAAAAA";
if(empty($slideshow_label_color_6)) $slideshow_label_color_6 ="16B778";
if(empty($slideshow_title_color_6)) $slideshow_title_color_6 ="222222";
if(empty($slideshow_subtitle_color_6)) $slideshow_subtitle_color_6 ="AAAAAA";
?>

<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-slideshow" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-slideshow" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>       
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-slideshow_hover_bg_color_status"><?php echo $entry_slideshow_hover_bg_color_status; ?></label>
            <div class="col-sm-10">
              <select name="slideshow_hover_bg_color_status" id="input-slideshow_hover_bg_color_status" class="form-control">
                <option value="1" <?php if ($slideshow_hover_bg_color_status == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                <option value="0" <?php if ($slideshow_hover_bg_color_status == '0') {echo "selected=\"selected\"";}; ?>>No</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-slideshow_hover_bg_color"><?php echo $entry_slideshow_hover_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="slideshow_hover_bg_color" id="slideshow_hover_bg_color" value="<?php echo $slideshow_hover_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-slideshow_hover_bg_color_opacity"><?php echo $entry_slideshow_hover_bg_color_opacity; ?></label>
            <div class="col-sm-10">
              <select name="slideshow_hover_bg_color_opacity" id="input-slideshow_hover_bg_color_opacity" class="form-control">
                <option value="0" <?php if ($slideshow_hover_bg_color_opacity == '0') {echo "selected=\"selected\"";}; ?>>0</option>
                <option value="0.1" <?php if ($slideshow_hover_bg_color_opacity == '0.1') {echo "selected=\"selected\"";}; ?>>0.1</option>
                <option value="0.2" <?php if ($slideshow_hover_bg_color_opacity == '0.2') {echo "selected=\"selected\"";}; ?>>0.2</option>
                <option value="0.3" <?php if ($slideshow_hover_bg_color_opacity == '0.3') {echo "selected=\"selected\"";}; ?>>0.3</option>
                <option value="0.4" <?php if ($slideshow_hover_bg_color_opacity == '0.4') {echo "selected=\"selected\"";}; ?>>0.4</option>
                <option value="0.5" <?php if ($slideshow_hover_bg_color_opacity == '0.5') {echo "selected=\"selected\"";}; ?>>0.5</option>
                <option value="0.6" <?php if ($slideshow_hover_bg_color_opacity == '0.6') {echo "selected=\"selected\"";}; ?>>0.6</option>
                <option value="0.7" <?php if ($slideshow_hover_bg_color_opacity == '0.7') {echo "selected=\"selected\"";}; ?>>0.7</option>
                <option value="0.8" <?php if ($slideshow_hover_bg_color_opacity == '0.8') {echo "selected=\"selected\"";}; ?>>0.8</option>
                <option value="0.9" <?php if ($slideshow_hover_bg_color_opacity == '0.9') {echo "selected=\"selected\"";}; ?>>0.9</option>
                <option value="1" <?php if ($slideshow_hover_bg_color_opacity == '1') {echo "selected=\"selected\"";}; ?>>1</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-slideshow_progress_bar"><?php echo $entry_slideshow_progress_bar; ?></label>
            <div class="col-sm-10">
              <select name="slideshow_progress_bar" id="input-slideshow_progress_bar" class="form-control">
                <option value="0" <?php if ($slideshow_progress_bar == '0') {echo "selected=\"selected\"";}; ?>>No</option>
                <option value="1" <?php if ($slideshow_progress_bar == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-slideshow_per_row"><?php echo $entry_slideshow_per_row; ?></label>
            <div class="col-sm-10">
              <select name="slideshow_per_row" id="input-slideshow_per_row" class="form-control">
                <option value="1" <?php if ($slideshow_per_row == '1') {echo "selected=\"selected\"";}; ?>>1</option>
                <option value="2" <?php if ($slideshow_per_row == '2') {echo "selected=\"selected\"";}; ?>>2</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-slideshow_time"><?php echo $entry_slideshow_time; ?></label>
            <div class="col-sm-10">
              <select name="slideshow_time" id="input-slideshow_time" class="form-control">
                <option value="1" <?php if ($slideshow_time == '1') {echo "selected=\"selected\"";}; ?>>1</option>
                <option value="2" <?php if ($slideshow_time == '2') {echo "selected=\"selected\"";}; ?>>2</option>
                <option value="3" <?php if ($slideshow_time == '3') {echo "selected=\"selected\"";}; ?>>3</option>
                <option value="4" <?php if ($slideshow_time == '4') {echo "selected=\"selected\"";}; ?>>4</option>
                <option value="5" <?php if ($slideshow_time == '5') {echo "selected=\"selected\"";}; ?>>5</option>
                <option value="6" <?php if ($slideshow_time == '6') {echo "selected=\"selected\"";}; ?>>6</option>
                <option value="7" <?php if ($slideshow_time == '7') {echo "selected=\"selected\"";}; ?>>7</option>
                <option value="8" <?php if ($slideshow_time == '8') {echo "selected=\"selected\"";}; ?>>8</option>
                <option value="9" <?php if ($slideshow_time == '9') {echo "selected=\"selected\"";}; ?>>9</option>
                <option value="10" <?php if ($slideshow_time == '10') {echo "selected=\"selected\"";}; ?>>10</option>
                <option value="11" <?php if ($slideshow_time == '11') {echo "selected=\"selected\"";}; ?>>11</option>
                <option value="12" <?php if ($slideshow_time == '12') {echo "selected=\"selected\"";}; ?>>12</option>
                <option value="13" <?php if ($slideshow_time == '13') {echo "selected=\"selected\"";}; ?>>13</option>
                <option value="14" <?php if ($slideshow_time == '14') {echo "selected=\"selected\"";}; ?>>14</option>
                <option value="15" <?php if ($slideshow_time == '15') {echo "selected=\"selected\"";}; ?>>15</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <br /><br />
          <legend>Banners</legend>
         <div class="row">
         <div class="col-sm-12">
         <table id="slideshow_items" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left" width="5%">Show</td>
                <td class="text-left" width="8%"><?php echo $entry_banner_image; ?></td>
                <td class="text-left" width="8%"><?php echo $entry_banner_main_image; ?></td>
                <td class="text-left" width="15%"><?php echo $entry_slideshow_url; ?></td>
                <td class="text-left"><?php echo $entry_slideshow_label; ?></td>
                <td class="text-left" width="15%"><?php echo $entry_slideshow_title; ?></td>
                <td class="text-left" width="15%"><?php echo $entry_slideshow_subtitle; ?></td>
                <td class="text-left"><?php echo $entry_slideshow_button; ?></td>
                <td class="text-left" width="10%"></td>
              </tr>
            </thead>
            <tbody>
              
              
              <tr>

                <td class="text-left">
                <select name="slideshow_item[status_1]" id="input-status_1" class="form-control">
                  <option value="0" <?php if ($slideshow_item['status_1'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($slideshow_item['status_1'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_image_thumb_1" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_image_thumb_1; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_image_custom_1" value="<?php echo $slideshow_item_image_custom_1; ?>" id="input-slideshow_item_image_custom_1" />
                <?php echo $entry_required; ?>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_main_image_thumb_1" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_main_image_thumb_1; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_main_image_custom_1" value="<?php echo $slideshow_item_main_image_custom_1; ?>" id="input-slideshow_item_main_image_custom_1" />
                <?php echo $entry_optional; ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][url_1]" id="url_1-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['url_1']) ? $slideshow_item[$language['language_id']]['url_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][label_1]" id="label_1-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['label_1']) ? $slideshow_item[$language['language_id']]['label_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_1"><?php echo $entry_slideshow_label_color; ?></label>
                <input type="text" name="slideshow_label_color_1" id="slideshow_label_color_1" value="<?php echo $slideshow_label_color_1; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][title_1]" id="title_1-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['title_1']) ? $slideshow_item[$language['language_id']]['title_1'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_1"><?php echo $entry_slideshow_title_color; ?></label>
                <input type="text" name="slideshow_title_color_1" id="slideshow_title_color_1" value="<?php echo $slideshow_title_color_1; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][subtitle_1]" id="subtitle_1-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['subtitle_1']) ? $slideshow_item[$language['language_id']]['subtitle_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_1"><?php echo $entry_slideshow_subtitle_color; ?></label>
                <input type="text" name="slideshow_subtitle_color_1" id="slideshow_subtitle_color_1" value="<?php echo $slideshow_subtitle_color_1; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][button_1]" id="button_1-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['button_1']) ? $slideshow_item[$language['language_id']]['button_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_1"><?php echo $entry_slideshow_button_style; ?></label>
                <select name="slideshow_item[button_style_1]" id="input-button_style_1" class="form-control">
                  <option value="primary" <?php if ($slideshow_item['button_style_1'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($slideshow_item['button_style_1'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_1"><?php echo $entry_slideshow_content_position; ?></label>
                <select name="slideshow_item[content_position_1]" id="input-content_position_1" class="form-control">
                    <option value="content-bottom-center" <?php if ($slideshow_item['content_position_1'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($slideshow_item['content_position_1'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($slideshow_item['content_position_1'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($slideshow_item['content_position_1'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($slideshow_item['content_position_1'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($slideshow_item['content_position_1'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($slideshow_item['content_position_1'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($slideshow_item['content_position_1'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($slideshow_item['content_position_1'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($slideshow_item['content_position_1'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($slideshow_item['content_position_1'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-main_image_animation_1"><?php echo $entry_slideshow_main_image_animation; ?></label>
                <select name="slideshow_item[main_image_animation_1]" id="input-main_image_animation_1" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['main_image_animation_1'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['main_image_animation_1'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['main_image_animation_1'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['main_image_animation_1'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['main_image_animation_1'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['main_image_animation_1'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['main_image_animation_1'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['main_image_animation_1'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['main_image_animation_1'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['main_image_animation_1'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['main_image_animation_1'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['main_image_animation_1'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['main_image_animation_1'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['main_image_animation_1'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['main_image_animation_1'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['main_image_animation_1'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['main_image_animation_1'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['main_image_animation_1'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['main_image_animation_1'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['main_image_animation_1'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['main_image_animation_1'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['main_image_animation_1'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['main_image_animation_1'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['main_image_animation_1'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['main_image_animation_1'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['main_image_animation_1'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-title_animation_1"><?php echo $entry_slideshow_title_animation; ?></label>
                <select name="slideshow_item[title_animation_1]" id="input-title_animation_1" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['title_animation_1'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['title_animation_1'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['title_animation_1'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['title_animation_1'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['title_animation_1'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['title_animation_1'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['title_animation_1'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['title_animation_1'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['title_animation_1'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['title_animation_1'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['title_animation_1'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['title_animation_1'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['title_animation_1'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['title_animation_1'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['title_animation_1'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['title_animation_1'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['title_animation_1'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['title_animation_1'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['title_animation_1'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['title_animation_1'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['title_animation_1'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['title_animation_1'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['title_animation_1'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['title_animation_1'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['title_animation_1'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['title_animation_1'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-subtitle_animation_1"><?php echo $entry_slideshow_subtitle_animation; ?></label>
                <select name="slideshow_item[subtitle_animation_1]" id="input-subtitle_animation_1" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['subtitle_animation_1'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['subtitle_animation_1'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['subtitle_animation_1'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['subtitle_animation_1'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['subtitle_animation_1'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['subtitle_animation_1'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['subtitle_animation_1'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['subtitle_animation_1'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['subtitle_animation_1'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['subtitle_animation_1'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['subtitle_animation_1'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['subtitle_animation_1'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['subtitle_animation_1'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['subtitle_animation_1'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['subtitle_animation_1'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['subtitle_animation_1'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['subtitle_animation_1'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['subtitle_animation_1'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['subtitle_animation_1'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['subtitle_animation_1'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['subtitle_animation_1'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['subtitle_animation_1'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['subtitle_animation_1'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['subtitle_animation_1'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['subtitle_animation_1'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['subtitle_animation_1'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-button_animation_1"><?php echo $entry_slideshow_button_animation; ?></label>
                <select name="slideshow_item[button_animation_1]" id="input-button_animation_1" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['button_animation_1'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['button_animation_1'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['button_animation_1'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['button_animation_1'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['button_animation_1'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['button_animation_1'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['button_animation_1'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['button_animation_1'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['button_animation_1'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['button_animation_1'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['button_animation_1'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['button_animation_1'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['button_animation_1'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['button_animation_1'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['button_animation_1'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['button_animation_1'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['button_animation_1'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['button_animation_1'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['button_animation_1'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['button_animation_1'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['button_animation_1'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['button_animation_1'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['button_animation_1'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['button_animation_1'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['button_animation_1'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['button_animation_1'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_1"><?php echo $entry_slideshow_hover_effect; ?></label>
                <select name="slideshow_item[hover_effect_1]" id="input-hover_effect_1" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($slideshow_item['hover_effect_1']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="slideshow_item[status_2]" id="input-status_2" class="form-control">
                  <option value="0" <?php if ($slideshow_item['status_2'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($slideshow_item['status_2'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_image_thumb_2" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_image_thumb_2; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_image_custom_2" value="<?php echo $slideshow_item_image_custom_2; ?>" id="input-slideshow_item_image_custom_2" />
                <?php echo $entry_required; ?>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_main_image_thumb_2" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_main_image_thumb_2; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_main_image_custom_2" value="<?php echo $slideshow_item_main_image_custom_2; ?>" id="input-slideshow_item_main_image_custom_2" />
                <?php echo $entry_optional; ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][url_2]" id="url_2-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['url_2']) ? $slideshow_item[$language['language_id']]['url_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][label_2]" id="label_2-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['label_2']) ? $slideshow_item[$language['language_id']]['label_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_2"><?php echo $entry_slideshow_label_color; ?></label>
                <input type="text" name="slideshow_label_color_2" id="slideshow_label_color_2" value="<?php echo $slideshow_label_color_2; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][title_2]" id="title_2-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['title_2']) ? $slideshow_item[$language['language_id']]['title_2'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_2"><?php echo $entry_slideshow_title_color; ?></label>
                <input type="text" name="slideshow_title_color_2" id="slideshow_title_color_2" value="<?php echo $slideshow_title_color_2; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][subtitle_2]" id="subtitle_2-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['subtitle_2']) ? $slideshow_item[$language['language_id']]['subtitle_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_2"><?php echo $entry_slideshow_subtitle_color; ?></label>
                <input type="text" name="slideshow_subtitle_color_2" id="slideshow_subtitle_color_2" value="<?php echo $slideshow_subtitle_color_2; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][button_2]" id="button_2-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['button_2']) ? $slideshow_item[$language['language_id']]['button_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_2"><?php echo $entry_slideshow_button_style; ?></label>
                <select name="slideshow_item[button_style_2]" id="input-button_style_2" class="form-control">
                  <option value="primary" <?php if ($slideshow_item['button_style_2'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($slideshow_item['button_style_2'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_2"><?php echo $entry_slideshow_content_position; ?></label>
                <select name="slideshow_item[content_position_2]" id="input-content_position_2" class="form-control">
                    <option value="content-bottom-center" <?php if ($slideshow_item['content_position_2'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($slideshow_item['content_position_2'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($slideshow_item['content_position_2'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($slideshow_item['content_position_2'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($slideshow_item['content_position_2'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($slideshow_item['content_position_2'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($slideshow_item['content_position_2'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($slideshow_item['content_position_2'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($slideshow_item['content_position_2'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($slideshow_item['content_position_2'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($slideshow_item['content_position_2'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-main_image_animation_2"><?php echo $entry_slideshow_main_image_animation; ?></label>
                <select name="slideshow_item[main_image_animation_2]" id="input-main_image_animation_2" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['main_image_animation_2'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['main_image_animation_2'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['main_image_animation_2'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['main_image_animation_2'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['main_image_animation_2'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['main_image_animation_2'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['main_image_animation_2'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['main_image_animation_2'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['main_image_animation_2'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['main_image_animation_2'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['main_image_animation_2'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['main_image_animation_2'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['main_image_animation_2'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['main_image_animation_2'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['main_image_animation_2'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['main_image_animation_2'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['main_image_animation_2'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['main_image_animation_2'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['main_image_animation_2'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['main_image_animation_2'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['main_image_animation_2'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['main_image_animation_2'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['main_image_animation_2'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['main_image_animation_2'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['main_image_animation_2'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['main_image_animation_2'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-title_animation_2"><?php echo $entry_slideshow_title_animation; ?></label>
                <select name="slideshow_item[title_animation_2]" id="input-title_animation_2" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['title_animation_2'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['title_animation_2'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['title_animation_2'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['title_animation_2'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['title_animation_2'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['title_animation_2'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['title_animation_2'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['title_animation_2'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['title_animation_2'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['title_animation_2'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['title_animation_2'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['title_animation_2'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['title_animation_2'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['title_animation_2'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['title_animation_2'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['title_animation_2'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['title_animation_2'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['title_animation_2'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['title_animation_2'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['title_animation_2'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['title_animation_2'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['title_animation_2'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['title_animation_2'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['title_animation_2'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['title_animation_2'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['title_animation_2'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-subtitle_animation_2"><?php echo $entry_slideshow_subtitle_animation; ?></label>
                <select name="slideshow_item[subtitle_animation_2]" id="input-subtitle_animation_2" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['subtitle_animation_2'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['subtitle_animation_2'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['subtitle_animation_2'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['subtitle_animation_2'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['subtitle_animation_2'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['subtitle_animation_2'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['subtitle_animation_2'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['subtitle_animation_2'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['subtitle_animation_2'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['subtitle_animation_2'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['subtitle_animation_2'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['subtitle_animation_2'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['subtitle_animation_2'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['subtitle_animation_2'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['subtitle_animation_2'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['subtitle_animation_2'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['subtitle_animation_2'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['subtitle_animation_2'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['subtitle_animation_2'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['subtitle_animation_2'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['subtitle_animation_2'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['subtitle_animation_2'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['subtitle_animation_2'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['subtitle_animation_2'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['subtitle_animation_2'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['subtitle_animation_2'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-button_animation_2"><?php echo $entry_slideshow_button_animation; ?></label>
                <select name="slideshow_item[button_animation_2]" id="input-button_animation_2" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['button_animation_2'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['button_animation_2'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['button_animation_2'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['button_animation_2'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['button_animation_2'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['button_animation_2'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['button_animation_2'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['button_animation_2'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['button_animation_2'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['button_animation_2'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['button_animation_2'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['button_animation_2'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['button_animation_2'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['button_animation_2'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['button_animation_2'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['button_animation_2'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['button_animation_2'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['button_animation_2'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['button_animation_2'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['button_animation_2'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['button_animation_2'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['button_animation_2'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['button_animation_2'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['button_animation_2'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['button_animation_2'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['button_animation_2'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_2"><?php echo $entry_slideshow_hover_effect; ?></label>
                <select name="slideshow_item[hover_effect_2]" id="input-hover_effect_2" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($slideshow_item['hover_effect_2']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="slideshow_item[status_3]" id="input-status_3" class="form-control">
                  <option value="0" <?php if ($slideshow_item['status_3'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($slideshow_item['status_3'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_image_thumb_3" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_image_thumb_3; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_image_custom_3" value="<?php echo $slideshow_item_image_custom_3; ?>" id="input-slideshow_item_image_custom_3" />
                <?php echo $entry_required; ?>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_main_image_thumb_3" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_main_image_thumb_3; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_main_image_custom_3" value="<?php echo $slideshow_item_main_image_custom_3; ?>" id="input-slideshow_item_main_image_custom_3" />
                <?php echo $entry_optional; ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][url_3]" id="url_3-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['url_3']) ? $slideshow_item[$language['language_id']]['url_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][label_3]" id="label_3-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['label_3']) ? $slideshow_item[$language['language_id']]['label_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_3"><?php echo $entry_slideshow_label_color; ?></label>
                <input type="text" name="slideshow_label_color_3" id="slideshow_label_color_3" value="<?php echo $slideshow_label_color_3; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][title_3]" id="title_3-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['title_3']) ? $slideshow_item[$language['language_id']]['title_3'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_3"><?php echo $entry_slideshow_title_color; ?></label>
                <input type="text" name="slideshow_title_color_3" id="slideshow_title_color_3" value="<?php echo $slideshow_title_color_3; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][subtitle_3]" id="subtitle_3-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['subtitle_3']) ? $slideshow_item[$language['language_id']]['subtitle_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_3"><?php echo $entry_slideshow_subtitle_color; ?></label>
                <input type="text" name="slideshow_subtitle_color_3" id="slideshow_subtitle_color_3" value="<?php echo $slideshow_subtitle_color_3; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][button_3]" id="button_3-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['button_3']) ? $slideshow_item[$language['language_id']]['button_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_3"><?php echo $entry_slideshow_button_style; ?></label>
                <select name="slideshow_item[button_style_3]" id="input-button_style_3" class="form-control">
                  <option value="primary" <?php if ($slideshow_item['button_style_3'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($slideshow_item['button_style_3'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_3"><?php echo $entry_slideshow_content_position; ?></label>
                <select name="slideshow_item[content_position_3]" id="input-content_position_3" class="form-control">
                    <option value="content-bottom-center" <?php if ($slideshow_item['content_position_3'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($slideshow_item['content_position_3'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($slideshow_item['content_position_3'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($slideshow_item['content_position_3'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($slideshow_item['content_position_3'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($slideshow_item['content_position_3'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($slideshow_item['content_position_3'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($slideshow_item['content_position_3'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($slideshow_item['content_position_3'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($slideshow_item['content_position_3'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($slideshow_item['content_position_3'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-main_image_animation_3"><?php echo $entry_slideshow_main_image_animation; ?></label>
                <select name="slideshow_item[main_image_animation_3]" id="input-main_image_animation_3" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['main_image_animation_3'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['main_image_animation_3'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['main_image_animation_3'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['main_image_animation_3'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['main_image_animation_3'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['main_image_animation_3'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['main_image_animation_3'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['main_image_animation_3'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['main_image_animation_3'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['main_image_animation_3'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['main_image_animation_3'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['main_image_animation_3'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['main_image_animation_3'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['main_image_animation_3'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['main_image_animation_3'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['main_image_animation_3'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['main_image_animation_3'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['main_image_animation_3'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['main_image_animation_3'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['main_image_animation_3'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['main_image_animation_3'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['main_image_animation_3'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['main_image_animation_3'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['main_image_animation_3'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['main_image_animation_3'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['main_image_animation_3'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-title_animation_3"><?php echo $entry_slideshow_title_animation; ?></label>
                <select name="slideshow_item[title_animation_3]" id="input-title_animation_3" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['title_animation_3'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['title_animation_3'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['title_animation_3'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['title_animation_3'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['title_animation_3'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['title_animation_3'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['title_animation_3'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['title_animation_3'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['title_animation_3'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['title_animation_3'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['title_animation_3'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['title_animation_3'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['title_animation_3'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['title_animation_3'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['title_animation_3'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['title_animation_3'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['title_animation_3'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['title_animation_3'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['title_animation_3'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['title_animation_3'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['title_animation_3'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['title_animation_3'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['title_animation_3'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['title_animation_3'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['title_animation_3'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['title_animation_3'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-subtitle_animation_3"><?php echo $entry_slideshow_subtitle_animation; ?></label>
                <select name="slideshow_item[subtitle_animation_3]" id="input-subtitle_animation_3" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['subtitle_animation_3'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['subtitle_animation_3'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['subtitle_animation_3'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['subtitle_animation_3'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['subtitle_animation_3'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['subtitle_animation_3'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['subtitle_animation_3'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['subtitle_animation_3'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['subtitle_animation_3'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['subtitle_animation_3'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['subtitle_animation_3'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['subtitle_animation_3'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['subtitle_animation_3'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['subtitle_animation_3'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['subtitle_animation_3'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['subtitle_animation_3'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['subtitle_animation_3'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['subtitle_animation_3'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['subtitle_animation_3'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['subtitle_animation_3'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['subtitle_animation_3'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['subtitle_animation_3'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['subtitle_animation_3'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['subtitle_animation_3'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['subtitle_animation_3'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['subtitle_animation_3'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-button_animation_3"><?php echo $entry_slideshow_button_animation; ?></label>
                <select name="slideshow_item[button_animation_3]" id="input-button_animation_3" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['button_animation_3'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['button_animation_3'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['button_animation_3'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['button_animation_3'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['button_animation_3'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['button_animation_3'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['button_animation_3'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['button_animation_3'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['button_animation_3'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['button_animation_3'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['button_animation_3'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['button_animation_3'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['button_animation_3'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['button_animation_3'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['button_animation_3'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['button_animation_3'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['button_animation_3'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['button_animation_3'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['button_animation_3'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['button_animation_3'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['button_animation_3'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['button_animation_3'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['button_animation_3'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['button_animation_3'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['button_animation_3'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['button_animation_3'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_3"><?php echo $entry_slideshow_hover_effect; ?></label>
                <select name="slideshow_item[hover_effect_3]" id="input-hover_effect_3" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($slideshow_item['hover_effect_3']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="slideshow_item[status_4]" id="input-status_4" class="form-control">
                  <option value="0" <?php if ($slideshow_item['status_4'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($slideshow_item['status_4'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_image_thumb_4" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_image_thumb_4; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_image_custom_4" value="<?php echo $slideshow_item_image_custom_4; ?>" id="input-slideshow_item_image_custom_4" />
                <?php echo $entry_required; ?>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_main_image_thumb_4" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_main_image_thumb_4; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_main_image_custom_4" value="<?php echo $slideshow_item_main_image_custom_4; ?>" id="input-slideshow_item_main_image_custom_4" />
                <?php echo $entry_optional; ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][url_4]" id="url_4-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['url_4']) ? $slideshow_item[$language['language_id']]['url_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][label_4]" id="label_4-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['label_4']) ? $slideshow_item[$language['language_id']]['label_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_4"><?php echo $entry_slideshow_label_color; ?></label>
                <input type="text" name="slideshow_label_color_4" id="slideshow_label_color_4" value="<?php echo $slideshow_label_color_4; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][title_4]" id="title_4-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['title_4']) ? $slideshow_item[$language['language_id']]['title_4'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_4"><?php echo $entry_slideshow_title_color; ?></label>
                <input type="text" name="slideshow_title_color_4" id="slideshow_title_color_4" value="<?php echo $slideshow_title_color_4; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][subtitle_4]" id="subtitle_4-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['subtitle_4']) ? $slideshow_item[$language['language_id']]['subtitle_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_4"><?php echo $entry_slideshow_subtitle_color; ?></label>
                <input type="text" name="slideshow_subtitle_color_4" id="slideshow_subtitle_color_4" value="<?php echo $slideshow_subtitle_color_4; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][button_4]" id="button_4-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['button_4']) ? $slideshow_item[$language['language_id']]['button_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_4"><?php echo $entry_slideshow_button_style; ?></label>
                <select name="slideshow_item[button_style_4]" id="input-button_style_4" class="form-control">
                  <option value="primary" <?php if ($slideshow_item['button_style_4'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($slideshow_item['button_style_4'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_4"><?php echo $entry_slideshow_content_position; ?></label>
                <select name="slideshow_item[content_position_4]" id="input-content_position_4" class="form-control">
                    <option value="content-bottom-center" <?php if ($slideshow_item['content_position_4'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($slideshow_item['content_position_4'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($slideshow_item['content_position_4'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($slideshow_item['content_position_4'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($slideshow_item['content_position_4'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($slideshow_item['content_position_4'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($slideshow_item['content_position_4'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($slideshow_item['content_position_4'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($slideshow_item['content_position_4'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($slideshow_item['content_position_4'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($slideshow_item['content_position_4'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-main_image_animation_4"><?php echo $entry_slideshow_main_image_animation; ?></label>
                <select name="slideshow_item[main_image_animation_4]" id="input-main_image_animation_4" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['main_image_animation_4'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['main_image_animation_4'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['main_image_animation_4'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['main_image_animation_4'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['main_image_animation_4'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['main_image_animation_4'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['main_image_animation_4'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['main_image_animation_4'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['main_image_animation_4'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['main_image_animation_4'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['main_image_animation_4'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['main_image_animation_4'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['main_image_animation_4'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['main_image_animation_4'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['main_image_animation_4'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['main_image_animation_4'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['main_image_animation_4'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['main_image_animation_4'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['main_image_animation_4'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['main_image_animation_4'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['main_image_animation_4'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['main_image_animation_4'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['main_image_animation_4'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['main_image_animation_4'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['main_image_animation_4'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['main_image_animation_4'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-title_animation_4"><?php echo $entry_slideshow_title_animation; ?></label>
                <select name="slideshow_item[title_animation_4]" id="input-title_animation_4" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['title_animation_4'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['title_animation_4'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['title_animation_4'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['title_animation_4'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['title_animation_4'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['title_animation_4'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['title_animation_4'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['title_animation_4'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['title_animation_4'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['title_animation_4'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['title_animation_4'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['title_animation_4'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['title_animation_4'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['title_animation_4'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['title_animation_4'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['title_animation_4'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['title_animation_4'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['title_animation_4'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['title_animation_4'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['title_animation_4'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['title_animation_4'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['title_animation_4'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['title_animation_4'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['title_animation_4'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['title_animation_4'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['title_animation_4'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-subtitle_animation_4"><?php echo $entry_slideshow_subtitle_animation; ?></label>
                <select name="slideshow_item[subtitle_animation_4]" id="input-subtitle_animation_4" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['subtitle_animation_4'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['subtitle_animation_4'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['subtitle_animation_4'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['subtitle_animation_4'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['subtitle_animation_4'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['subtitle_animation_4'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['subtitle_animation_4'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['subtitle_animation_4'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['subtitle_animation_4'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['subtitle_animation_4'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['subtitle_animation_4'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['subtitle_animation_4'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['subtitle_animation_4'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['subtitle_animation_4'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['subtitle_animation_4'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['subtitle_animation_4'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['subtitle_animation_4'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['subtitle_animation_4'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['subtitle_animation_4'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['subtitle_animation_4'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['subtitle_animation_4'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['subtitle_animation_4'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['subtitle_animation_4'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['subtitle_animation_4'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['subtitle_animation_4'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['subtitle_animation_4'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-button_animation_4"><?php echo $entry_slideshow_button_animation; ?></label>
                <select name="slideshow_item[button_animation_4]" id="input-button_animation_4" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['button_animation_4'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['button_animation_4'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['button_animation_4'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['button_animation_4'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['button_animation_4'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['button_animation_4'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['button_animation_4'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['button_animation_4'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['button_animation_4'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['button_animation_4'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['button_animation_4'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['button_animation_4'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['button_animation_4'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['button_animation_4'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['button_animation_4'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['button_animation_4'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['button_animation_4'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['button_animation_4'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['button_animation_4'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['button_animation_4'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['button_animation_4'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['button_animation_4'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['button_animation_4'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['button_animation_4'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['button_animation_4'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['button_animation_4'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_4"><?php echo $entry_slideshow_hover_effect; ?></label>
                <select name="slideshow_item[hover_effect_4]" id="input-hover_effect_4" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($slideshow_item['hover_effect_4']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="slideshow_item[status_5]" id="input-status_5" class="form-control">
                  <option value="0" <?php if ($slideshow_item['status_5'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($slideshow_item['status_5'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_image_thumb_5" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_image_thumb_5; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_image_custom_5" value="<?php echo $slideshow_item_image_custom_5; ?>" id="input-slideshow_item_image_custom_5" />
                <?php echo $entry_required; ?>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_main_image_thumb_5" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_main_image_thumb_5; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_main_image_custom_5" value="<?php echo $slideshow_item_main_image_custom_5; ?>" id="input-slideshow_item_main_image_custom_5" />
                <?php echo $entry_optional; ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][url_5]" id="url_5-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['url_5']) ? $slideshow_item[$language['language_id']]['url_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][label_5]" id="label_5-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['label_5']) ? $slideshow_item[$language['language_id']]['label_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_5"><?php echo $entry_slideshow_label_color; ?></label>
                <input type="text" name="slideshow_label_color_5" id="slideshow_label_color_5" value="<?php echo $slideshow_label_color_5; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][title_5]" id="title_5-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['title_5']) ? $slideshow_item[$language['language_id']]['title_5'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_5"><?php echo $entry_slideshow_title_color; ?></label>
                <input type="text" name="slideshow_title_color_5" id="slideshow_title_color_5" value="<?php echo $slideshow_title_color_5; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][subtitle_5]" id="subtitle_5-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['subtitle_5']) ? $slideshow_item[$language['language_id']]['subtitle_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_5"><?php echo $entry_slideshow_subtitle_color; ?></label>
                <input type="text" name="slideshow_subtitle_color_5" id="slideshow_subtitle_color_5" value="<?php echo $slideshow_subtitle_color_5; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][button_5]" id="button_5-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['button_5']) ? $slideshow_item[$language['language_id']]['button_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_5"><?php echo $entry_slideshow_button_style; ?></label>
                <select name="slideshow_item[button_style_5]" id="input-button_style_5" class="form-control">
                  <option value="primary" <?php if ($slideshow_item['button_style_5'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($slideshow_item['button_style_5'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_5"><?php echo $entry_slideshow_content_position; ?></label>
                <select name="slideshow_item[content_position_5]" id="input-content_position_5" class="form-control">
                    <option value="content-bottom-center" <?php if ($slideshow_item['content_position_5'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($slideshow_item['content_position_5'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($slideshow_item['content_position_5'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($slideshow_item['content_position_5'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($slideshow_item['content_position_5'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($slideshow_item['content_position_5'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($slideshow_item['content_position_5'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($slideshow_item['content_position_5'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($slideshow_item['content_position_5'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($slideshow_item['content_position_5'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($slideshow_item['content_position_5'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-main_image_animation_5"><?php echo $entry_slideshow_main_image_animation; ?></label>
                <select name="slideshow_item[main_image_animation_5]" id="input-main_image_animation_5" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['main_image_animation_5'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['main_image_animation_5'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['main_image_animation_5'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['main_image_animation_5'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['main_image_animation_5'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['main_image_animation_5'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['main_image_animation_5'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['main_image_animation_5'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['main_image_animation_5'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['main_image_animation_5'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['main_image_animation_5'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['main_image_animation_5'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['main_image_animation_5'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['main_image_animation_5'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['main_image_animation_5'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['main_image_animation_5'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['main_image_animation_5'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['main_image_animation_5'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['main_image_animation_5'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['main_image_animation_5'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['main_image_animation_5'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['main_image_animation_5'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['main_image_animation_5'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['main_image_animation_5'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['main_image_animation_5'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['main_image_animation_5'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-title_animation_5"><?php echo $entry_slideshow_title_animation; ?></label>
                <select name="slideshow_item[title_animation_5]" id="input-title_animation_5" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['title_animation_5'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['title_animation_5'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['title_animation_5'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['title_animation_5'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['title_animation_5'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['title_animation_5'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['title_animation_5'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['title_animation_5'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['title_animation_5'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['title_animation_5'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['title_animation_5'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['title_animation_5'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['title_animation_5'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['title_animation_5'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['title_animation_5'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['title_animation_5'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['title_animation_5'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['title_animation_5'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['title_animation_5'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['title_animation_5'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['title_animation_5'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['title_animation_5'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['title_animation_5'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['title_animation_5'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['title_animation_5'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['title_animation_5'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-subtitle_animation_5"><?php echo $entry_slideshow_subtitle_animation; ?></label>
                <select name="slideshow_item[subtitle_animation_5]" id="input-subtitle_animation_5" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['subtitle_animation_5'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['subtitle_animation_5'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['subtitle_animation_5'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['subtitle_animation_5'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['subtitle_animation_5'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['subtitle_animation_5'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['subtitle_animation_5'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['subtitle_animation_5'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['subtitle_animation_5'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['subtitle_animation_5'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['subtitle_animation_5'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['subtitle_animation_5'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['subtitle_animation_5'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['subtitle_animation_5'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['subtitle_animation_5'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['subtitle_animation_5'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['subtitle_animation_5'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['subtitle_animation_5'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['subtitle_animation_5'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['subtitle_animation_5'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['subtitle_animation_5'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['subtitle_animation_5'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['subtitle_animation_5'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['subtitle_animation_5'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['subtitle_animation_5'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['subtitle_animation_5'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-button_animation_5"><?php echo $entry_slideshow_button_animation; ?></label>
                <select name="slideshow_item[button_animation_5]" id="input-button_animation_5" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['button_animation_5'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['button_animation_5'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['button_animation_5'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['button_animation_5'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['button_animation_5'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['button_animation_5'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['button_animation_5'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['button_animation_5'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['button_animation_5'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['button_animation_5'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['button_animation_5'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['button_animation_5'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['button_animation_5'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['button_animation_5'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['button_animation_5'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['button_animation_5'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['button_animation_5'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['button_animation_5'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['button_animation_5'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['button_animation_5'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['button_animation_5'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['button_animation_5'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['button_animation_5'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['button_animation_5'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['button_animation_5'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['button_animation_5'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_5"><?php echo $entry_slideshow_hover_effect; ?></label>
                <select name="slideshow_item[hover_effect_5]" id="input-hover_effect_5" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($slideshow_item['hover_effect_5']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="slideshow_item[status_6]" id="input-status_6" class="form-control">
                  <option value="0" <?php if ($slideshow_item['status_6'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($slideshow_item['status_6'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_image_thumb_6" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_image_thumb_6; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_image_custom_6" value="<?php echo $slideshow_item_image_custom_6; ?>" id="input-slideshow_item_image_custom_6" />
                <?php echo $entry_required; ?>
                </td>
                
                <td class="text-left">
                <a href="" id="slideshow_item_main_image_thumb_6" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $slideshow_item_main_image_thumb_6; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="slideshow_item_main_image_custom_6" value="<?php echo $slideshow_item_main_image_custom_6; ?>" id="input-slideshow_item_main_image_custom_6" />
                <?php echo $entry_optional; ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][url_6]" id="url_6-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['url_6']) ? $slideshow_item[$language['language_id']]['url_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][label_6]" id="label_6-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['label_6']) ? $slideshow_item[$language['language_id']]['label_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_6"><?php echo $entry_slideshow_label_color; ?></label>
                <input type="text" name="slideshow_label_color_6" id="slideshow_label_color_6" value="<?php echo $slideshow_label_color_6; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][title_6]" id="title_6-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['title_6']) ? $slideshow_item[$language['language_id']]['title_6'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_6"><?php echo $entry_slideshow_title_color; ?></label>
                <input type="text" name="slideshow_title_color_6" id="slideshow_title_color_6" value="<?php echo $slideshow_title_color_6; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][subtitle_6]" id="subtitle_6-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['subtitle_6']) ? $slideshow_item[$language['language_id']]['subtitle_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_6"><?php echo $entry_slideshow_subtitle_color; ?></label>
                <input type="text" name="slideshow_subtitle_color_6" id="slideshow_subtitle_color_6" value="<?php echo $slideshow_subtitle_color_6; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="slideshow_item[<?php echo $language['language_id']; ?>][button_6]" id="button_6-<?php echo $language['language_id']; ?>"><?php echo isset($slideshow_item[$language['language_id']]['button_6']) ? $slideshow_item[$language['language_id']]['button_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_6"><?php echo $entry_slideshow_button_style; ?></label>
                <select name="slideshow_item[button_style_6]" id="input-button_style_6" class="form-control">
                  <option value="primary" <?php if ($slideshow_item['button_style_6'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($slideshow_item['button_style_6'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_6"><?php echo $entry_slideshow_content_position; ?></label>
                <select name="slideshow_item[content_position_6]" id="input-content_position_6" class="form-control">
                    <option value="content-bottom-center" <?php if ($slideshow_item['content_position_6'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($slideshow_item['content_position_6'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($slideshow_item['content_position_6'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($slideshow_item['content_position_6'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($slideshow_item['content_position_6'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($slideshow_item['content_position_6'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($slideshow_item['content_position_6'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($slideshow_item['content_position_6'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($slideshow_item['content_position_6'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($slideshow_item['content_position_6'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($slideshow_item['content_position_6'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-main_image_animation_6"><?php echo $entry_slideshow_main_image_animation; ?></label>
                <select name="slideshow_item[main_image_animation_6]" id="input-main_image_animation_6" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['main_image_animation_6'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['main_image_animation_6'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['main_image_animation_6'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['main_image_animation_6'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['main_image_animation_6'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['main_image_animation_6'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['main_image_animation_6'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['main_image_animation_6'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['main_image_animation_6'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['main_image_animation_6'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['main_image_animation_6'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['main_image_animation_6'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['main_image_animation_6'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['main_image_animation_6'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['main_image_animation_6'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['main_image_animation_6'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['main_image_animation_6'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['main_image_animation_6'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['main_image_animation_6'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['main_image_animation_6'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['main_image_animation_6'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['main_image_animation_6'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['main_image_animation_6'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['main_image_animation_6'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['main_image_animation_6'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['main_image_animation_6'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-title_animation_6"><?php echo $entry_slideshow_title_animation; ?></label>
                <select name="slideshow_item[title_animation_6]" id="input-title_animation_6" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['title_animation_6'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['title_animation_6'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['title_animation_6'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['title_animation_6'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['title_animation_6'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['title_animation_6'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['title_animation_6'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['title_animation_6'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['title_animation_6'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['title_animation_6'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['title_animation_6'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['title_animation_6'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['title_animation_6'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['title_animation_6'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['title_animation_6'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['title_animation_6'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['title_animation_6'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['title_animation_6'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['title_animation_6'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['title_animation_6'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['title_animation_6'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['title_animation_6'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['title_animation_6'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['title_animation_6'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['title_animation_6'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['title_animation_6'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-subtitle_animation_6"><?php echo $entry_slideshow_subtitle_animation; ?></label>
                <select name="slideshow_item[subtitle_animation_6]" id="input-subtitle_animation_6" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['subtitle_animation_6'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['subtitle_animation_6'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['subtitle_animation_6'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['subtitle_animation_6'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['subtitle_animation_6'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['subtitle_animation_6'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['subtitle_animation_6'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['subtitle_animation_6'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['subtitle_animation_6'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['subtitle_animation_6'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['subtitle_animation_6'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['subtitle_animation_6'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['subtitle_animation_6'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['subtitle_animation_6'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['subtitle_animation_6'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['subtitle_animation_6'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['subtitle_animation_6'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['subtitle_animation_6'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['subtitle_animation_6'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['subtitle_animation_6'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['subtitle_animation_6'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['subtitle_animation_6'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['subtitle_animation_6'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['subtitle_animation_6'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['subtitle_animation_6'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['subtitle_animation_6'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-button_animation_6"><?php echo $entry_slideshow_button_animation; ?></label>
                <select name="slideshow_item[button_animation_6]" id="input-button_animation_6" class="form-control">
                    <option value="fadeIn" <?php if ($slideshow_item['button_animation_6'] == 'fadeIn') {echo "selected=\"selected\"";}; ?>>fadeIn</option>
			    	<option value="fadeInLeft" <?php if ($slideshow_item['button_animation_6'] == 'fadeInLeft') {echo "selected=\"selected\"";}; ?>>fadeInLeft</option>
                    <option value="fadeInRight" <?php if ($slideshow_item['button_animation_6'] == 'fadeInRight') {echo "selected=\"selected\"";}; ?>>fadeInRight</option>
                    <option value="fadeInDown" <?php if ($slideshow_item['button_animation_6'] == 'fadeInDown') {echo "selected=\"selected\"";}; ?>>fadeInDown</option>
                    <option value="fadeInUp" <?php if ($slideshow_item['button_animation_6'] == 'fadeInUp') {echo "selected=\"selected\"";}; ?>>fadeInUp</option>
                    <option value="slideInLeft" <?php if ($slideshow_item['button_animation_6'] == 'slideInLeft') {echo "selected=\"selected\"";}; ?>>slideInLeft</option>
                    <option value="slideInRight" <?php if ($slideshow_item['button_animation_6'] == 'slideInRight') {echo "selected=\"selected\"";}; ?>>slideInRight</option>
                    <option value="slideInDown" <?php if ($slideshow_item['button_animation_6'] == 'slideInDown') {echo "selected=\"selected\"";}; ?>>slideInDown</option>
                    <option value="slideInUp" <?php if ($slideshow_item['button_animation_6'] == 'slideInUp') {echo "selected=\"selected\"";}; ?>>slideInUp</option>
                    <option value="zoomIn" <?php if ($slideshow_item['button_animation_6'] == 'zoomIn') {echo "selected=\"selected\"";}; ?>>zoomIn</option>
                    <option value="zoomInLeft" <?php if ($slideshow_item['button_animation_6'] == 'zoomInLeft') {echo "selected=\"selected\"";}; ?>>zoomInLeft</option>
                    <option value="zoomInRight" <?php if ($slideshow_item['button_animation_6'] == 'zoomInRight') {echo "selected=\"selected\"";}; ?>>zoomInRight</option>
                    <option value="zoomInDown" <?php if ($slideshow_item['button_animation_6'] == 'zoomInDown') {echo "selected=\"selected\"";}; ?>>zoomInDown</option>
                    <option value="zoomInUp" <?php if ($slideshow_item['button_animation_6'] == 'zoomInUp') {echo "selected=\"selected\"";}; ?>>zoomInUp</option>
                    <option value="rotateIn" <?php if ($slideshow_item['button_animation_6'] == 'rotateIn') {echo "selected=\"selected\"";}; ?>>rotateIn</option>
                    <option value="rotateInDownLeft" <?php if ($slideshow_item['button_animation_6'] == 'rotateInDownLeft') {echo "selected=\"selected\"";}; ?>>rotateInDownLeft</option>
                    <option value="rotateInDownRight" <?php if ($slideshow_item['button_animation_6'] == 'rotateInDownRight') {echo "selected=\"selected\"";}; ?>>rotateInDownRight</option>
                    <option value="rotateInUpLeft" <?php if ($slideshow_item['button_animation_6'] == 'rotateInUpLeft') {echo "selected=\"selected\"";}; ?>>rotateInUpLeft</option>
                    <option value="rotateInUpRight" <?php if ($slideshow_item['button_animation_6'] == 'rotateInUpRight') {echo "selected=\"selected\"";}; ?>>rotateInUpRight</option>
                    <option value="lightSpeedIn" <?php if ($slideshow_item['button_animation_6'] == 'lightSpeedIn') {echo "selected=\"selected\"";}; ?>>lightSpeedIn</option>
                    <option value="rollIn" <?php if ($slideshow_item['button_animation_6'] == 'rollIn') {echo "selected=\"selected\"";}; ?>>rollIn</option>
                    <option value="bounce" <?php if ($slideshow_item['button_animation_6'] == 'bounce') {echo "selected=\"selected\"";}; ?>>bounce</option>
                    <option value="flash" <?php if ($slideshow_item['button_animation_6'] == 'flash') {echo "selected=\"selected\"";}; ?>>flash</option>
                    <option value="pulse" <?php if ($slideshow_item['button_animation_6'] == 'pulse') {echo "selected=\"selected\"";}; ?>>pulse</option>
                    <option value="swing" <?php if ($slideshow_item['button_animation_6'] == 'swing') {echo "selected=\"selected\"";}; ?>>swing</option>
                    <option value="jello" <?php if ($slideshow_item['button_animation_6'] == 'jello') {echo "selected=\"selected\"";}; ?>>jello</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_6"><?php echo $entry_slideshow_hover_effect; ?></label>
                <select name="slideshow_item[hover_effect_6]" id="input-hover_effect_6" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($slideshow_item['hover_effect_6']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>

              
            </tbody>
            <tfoot>
              <tr>
                <td colspan="8" class="text-right"></td>
              </tr>
            </tfoot>
          </table>
         </div>
         </div>
          
        </form>
      </div>
    </div>
  </div>
</div>


<style type="text/css">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
table {font-size:12px;}
.table thead > tr > td, .table tbody > tr > td {vertical-align:top!important;}
</style>
<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>
<?php echo $footer; ?>