<?php echo $header; ?>
<div class="container full-width-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    
    <h1><?php echo $heading_title; ?></h1>
      
<?php if($t1o_contact_map_status == 1) { ?>  
  <h3><?php echo $text_location; ?></h3>    
  <div class="contact-map">
  <div id="map_div"></div>
  </div>
<script src="//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=<?php echo $t1o_contact_map_api; ?>" async="" defer="defer" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	//------- Google Maps ---------//
	// Creating a LatLng object containing the coordinate for the center of the map
	var latlng = new google.maps.LatLng(<?php echo $t1o_contact_map_ll; ?>);
	 
	// Creating an object literal containing the properties we want to pass to the map  
	var options = {  
		zoom: 15, // This number can be set to define the initial zoom level of the map
		center: latlng,
		mapTypeId: google.maps.MapTypeId.<?php echo $t1o_contact_map_type; ?> // This value can be set to define the map type ROADMAP/SATELLITE/HYBRID/TERRAIN
	};  
	// Calling the constructor, thereby initializing the map  
	var map = new google.maps.Map(document.getElementById('map_div'), options);  
	
	// Define Marker properties
	var image = new google.maps.MarkerImage(
		//Image file name
		'catalog/view/theme/sellmore/image/map_marker.png',
		// This marker is 129 pixels wide by 42 pixels tall.
		new google.maps.Size(57, 76),
		// The origin for this image is 0,0.
		new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 18,42.
		new google.maps.Point(30, 76)
	);

	// Add Marker
	var marker1 = new google.maps.Marker({
		position: new google.maps.LatLng(<?php echo $t1o_contact_map_ll; ?>), 
		map: map,
		icon: image // This path is the custom pin to be shown. Remove this line and the proceeding comma to use default pin
	});	
	
	// Add listener for a click on the pin
	google.maps.event.addListener(marker1, 'click', function() {
        infowindow1.open(map, marker1);
	    });

	// Add information window
	var infowindow1 = new google.maps.InfoWindow({  
		content:  '<div> </div>'
	}); 
	
});
</script> 
<?php } ?> 
      
      <div class="panel panel-default">
        <div class="panel-body content-padd">
          <div class="row">
            
            <div class="col-sm-4">
            <div class="contact-details-wrapper">
            
            <?php if ($image) { ?>
            <img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" class="img-thumbnail" />
            <br /><br />
            <?php } ?>
            
            <h4><?php echo $store; ?></h4>
            <address>
            <?php echo $address; ?>
            </address>
            
            <h4><?php echo $text_telephone; ?></h4>
            <?php echo $telephone; ?>
            <?php if ($fax) { ?>
            <br /><br />
            <h4><?php echo $text_fax; ?></h4>
            <?php echo $fax; ?>
            <?php } ?>
            
            <?php if ($open) { ?>
            <h4><?php echo $text_open; ?></h4>
            <?php echo $open; ?>
            <?php } ?>
            
            <?php if ($comment) { ?>
            <h4><?php echo $text_comment; ?></h4>
            <?php echo $comment; ?>
            <?php } ?>
            
            </div>
            </div>
            
            <div class="col-sm-8">
            <div class="contact-details-wrapper">
            
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <h3><?php echo $text_contact; ?></h3>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
            <div class="col-sm-10">
              <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
              <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
              <?php } ?>
            </div>
          </div>
          <?php echo $captcha; ?>
        </fieldset>
        <div class="buttons">
          <div class="pull-right">
            <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
          </div>
        </div>
      </form>
            
            </div>
            </div>

          </div>
        </div>
      </div>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
