
<footer <?php if($t1d_bg_image_f1_parallax == 1) { ?>data-stellar-background-ratio="0.5"<?php } ?>>

<?php if($t1o_custom_top_1_status == 1) { ?>
<div id="footer_custom_top_1">
<div class="container">
<div class="row">
<div class="col-sm-12 padd-t-b-40">
<?php echo html_entity_decode($t1o_custom_top_1_content[$lang_id], ENT_QUOTES, 'UTF-8');?>
</div>
</div>
</div>
</div>
<?php } ?>

<?php if($t1o_information_column_1_status == 1 || $t1o_information_column_2_status == 1 || $t1o_information_column_3_status == 1 || $t1o_custom_1_status == 1 || $t1o_custom_2_status == 1) { ?>
  <div id="information">
  <div class="container">
    <div class="row">
    <?php if($t1o_custom_1_status== 1) { ?>
      <div class="col-sm-<?php echo $t1o_custom_1_column_width; ?> col-xs-12">
        <?php if($t1o_custom_1_title[$lang_id]) { ?>
        <h5><?php echo $t1o_custom_1_title[$lang_id]; ?></h5>
        <?php } ?>
        <?php echo html_entity_decode($t1o_custom_1_content[$lang_id], ENT_QUOTES, 'UTF-8');?>
      </div>
      <?php } ?>
      
    <?php if($t1o_information_column_1_status == 1 || $t1o_information_column_2_status == 1 || $t1o_information_column_3_status == 1) { ?>
    <div id="information-block-containter" class="col-sm-<?php echo $t1o_information_block_width; ?> col-xs-12">
    <?php if($t1o_information_column_1_status == 1) { ?>
      <div class="col-sm-<?php echo 12 / ($t1o_information_column_1_status + $t1o_information_column_2_status + $t1o_information_column_3_status) ; ?> col-xs-12">
        <h5><?php echo $text_information; ?></h5>
        <?php if ($informations) { ?>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if($t1o_information_column_2_status == 1) { ?>
      <div class="col-sm-<?php echo 12 / ($t1o_information_column_1_status + $t1o_information_column_2_status + $t1o_information_column_3_status) ; ?> col-xs-12">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <?php if($t1o_i_c_2_1_status == 1) { ?><li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li><?php } ?>
          <?php if($t1o_i_c_2_2_status == 1) { ?><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><?php } ?>
          <?php if($t1o_i_c_2_3_status == 1) { ?><li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li><?php } ?>
          <?php if($t1o_i_c_2_4_status == 1) { ?><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><?php } ?>
          <?php if($t1o_i_c_2_5_status == 1) { ?><li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li><?php } ?>
        </ul>
      </div>
      <?php } ?>
      <?php if($t1o_information_column_3_status == 1) { ?>
      <div class="col-sm-<?php echo 12 / ($t1o_information_column_1_status + $t1o_information_column_2_status + $t1o_information_column_3_status) ; ?> col-xs-12">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <?php if($t1o_i_c_3_1_status == 1) { ?><li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li><?php } ?>
          <?php if($t1o_i_c_3_2_status == 1) { ?><li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li><?php } ?>
          <?php if($t1o_i_c_3_3_status == 1) { ?><li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li><?php } ?>
          <?php if($t1o_i_c_3_4_status == 1) { ?><li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li><?php } ?>
          <?php if($t1o_i_c_3_5_status == 1) { ?><li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li><?php } ?>
          <?php if($t1o_i_c_3_6_status == 1) { ?><li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li><?php } ?>
        </ul>
      </div>
      <?php } ?>
      </div>
      <?php } ?>
      
      
      <?php if($t1o_custom_2_status == 1) { ?>
      <div class="col-sm-<?php echo $t1o_custom_2_column_width; ?> col-xs-12">
        <?php if($t1o_custom_2_title[$lang_id]) { ?>
        <h5><?php echo $t1o_custom_2_title[$lang_id]; ?></h5>
        <?php } ?>
        
        <?php if($t1o_newsletter_status == 1) { ?>
        <div class="newsletter-block">                   
        <form action="<?php echo $t1o_newsletter_campaign_url; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
	        <label for="mce-EMAIL"><?php echo $t1o_newsletter_promo_text[$lang_id]; ?></label>
	        <input type="email" value="" name="EMAIL" class="email form-group form-control" id="mce-EMAIL" placeholder="<?php echo $t1o_newsletter_email[$lang_id]; ?>" required>
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_619755c76ad5fa60a96e52bec_c5fb795f8e" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="<?php echo $t1o_newsletter_subscribe[$lang_id]; ?>" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary"></div>
            </div>
        </form>
        </div>
        <?php } ?>
        
        <?php echo html_entity_decode($t1o_custom_2_content[$lang_id], ENT_QUOTES, 'UTF-8');?>
        
      </div>
      <?php } ?>
    </div>
  </div>
  </div>
  <?php } ?>
  
  <?php if($t1o_powered_status == 1 || $t1o_follow_us_status == 1 || $t1o_payment_block_status == 1) { ?>
  <div id="powered">
  <div class="container">
    <div class="row">
    
      <?php if($t1o_payment_block_status == 1) { ?>
      <div id="footer-payment-wrapper" class="col-sm-<?php echo 12 / ($t1o_payment_block_status + $t1o_powered_status + $t1o_follow_us_status) ; ?>">
        <ul id="footer-payment" class="list-inline">
            
        <?php if ($t1o_payment_block_custom_status == "1"): ?>
        <?php if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	        $path_image = $config_ssl . 'image/';
        } else {
        	$path_image = $config_url . 'image/';
        }
        if ($t1o_payment_block_custom != ''){ ?>   
        <?php if($t1o_payment_block_custom_url != ''): ?> 
		<li><a href="<?php echo $t1o_payment_block_custom_url; ?>" target="_blank">
			<img src="<?php echo $path_image . $t1o_payment_block_custom ?>" alt="Payment" title="Payment"></a>
        </li>
        <?php else: ?>
        <li>       
			<img src="<?php echo $path_image . $t1o_payment_block_custom ?>" alt="Payment" title="Payment">
        </li>
        <?php endif; ?>
		<?php } ?>
		<?php endif; ?>
        
        <?php if ($t1o_payment_paypal == "1"): ?>
		<li data-toggle="tooltip" title="PayPal">
        <?php if($t1o_payment_paypal_url != ''): ?>
            <a href="<?php echo $t1o_payment_paypal_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_paypal-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="PayPal" title="PayPal"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_paypal-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="PayPal" title="PayPal">
        <?php endif; ?>
        </li>
		<?php endif; ?>  
        
        <?php if ($t1o_payment_visa == "1"): ?>
        <li data-toggle="tooltip" title="Visa">
        <?php if($t1o_payment_visa_url != ''): ?>
			<a href="<?php echo $t1o_payment_visa_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_visa-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Visa" title="Visa"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_visa-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Visa" title="Visa">
        <?php endif; ?>
        </li>
		<?php endif; ?>   
        
        <?php if ($t1o_payment_mastercard == "1"): ?>
        <li data-toggle="tooltip" title="MasterCard">
        <?php if($t1o_payment_mastercard_url != ''): ?>
			<a href="<?php echo $t1o_payment_mastercard_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_mastercard-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="MasterCard" title="MasterCard"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_mastercard-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="MasterCard" title="MasterCard">
        <?php endif; ?>
        </li>
		<?php endif; ?> 
       
        <?php if ($t1o_payment_maestro == "1"): ?>
        <li data-toggle="tooltip" title="Maestro">
        <?php if($t1o_payment_maestro_url != ''): ?>
			<a href="<?php echo $t1o_payment_maestro_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_maestro-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Maestro" title="Maestro"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_maestro-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Maestro" title="Maestro">
        <?php endif; ?>
        </li>
		<?php endif; ?>
       
        <?php if ($t1o_payment_discover == "1"): ?>
        <li data-toggle="tooltip" title="Discover">
        <?php if($t1o_payment_discover_url != ''): ?>
			<a href="<?php echo $t1o_payment_discover_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_discover-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Discover" title="Discover"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_discover-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Discover" title="Discover">
        <?php endif; ?>
        </li>
		<?php endif; ?>                   
        
        <?php if ($t1o_payment_skrill == "1"): ?>
        <li data-toggle="tooltip" title="Skrill">
        <?php if($t1o_payment_skrill_url != ''): ?>
			<a href="<?php echo $t1o_payment_skrill_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_skrill-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Skrill" title="Skrill"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_skrill-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Skrill" title="Skrill">
        <?php endif; ?>
        </li>
		<?php endif; ?>   
        
        <?php if ($t1o_payment_american_express == "1"): ?>
        <li data-toggle="tooltip" title="American Express">
        <?php if($t1o_payment_american_express_url != ''): ?>
			<a href="<?php echo $t1o_payment_american_express_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_american_express-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="American Express" title="American Express"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_american_express-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="American Express" title="American Express">
        <?php endif; ?>
        </li>
		<?php endif; ?> 
                   
        <?php if ($t1o_payment_cirrus == "1"): ?>
        <li data-toggle="tooltip" title="Cirrus">
        <?php if($t1o_payment_cirrus_url != ''): ?>
			<a href="<?php echo $t1o_payment_cirrus_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_cirrus-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Cirrus" title="Cirrus"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_cirrus-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Cirrus" title="Cirrus">
        <?php endif; ?>
        </li>
		<?php endif; ?>   
        
        <?php if ($t1o_payment_delta == "1"): ?>
        <li data-toggle="tooltip" title="Delta">
        <?php if($t1o_payment_delta_url != ''): ?>
			<a href="<?php echo $t1o_payment_delta_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_delta-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Delta" title="Delta"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_delta-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Delta" title="Delta">
        <?php endif; ?>
        </li>
		<?php endif; ?>   
        
        <?php if ($t1o_payment_google == "1"): ?>
        <li data-toggle="tooltip" title="Google Wallet">
        <?php if($t1o_payment_google_url != ''): ?>
			<a href="<?php echo $t1o_payment_google_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_google-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Google Wallet" title="Google Wallet"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_google-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Google Wallet" title="Google Wallet">
        <?php endif; ?>
        </li>
		<?php endif; ?>
        
        <?php if ($t1o_payment_2co == "1"): ?>
        <li data-toggle="tooltip" title="2CheckOut">
        <?php if($t1o_payment_2co_url != ''): ?>
			<a href="<?php echo $t1o_payment_2co_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_2co-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="2CheckOut" title="2CheckOut"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_2co-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="2CheckOut" title="2CheckOut">
        <?php endif; ?>
        </li>
		<?php endif; ?> 
        
        <?php if ($t1o_payment_sage == "1"): ?>
        <li data-toggle="tooltip" title="Sage">
        <?php if($t1o_payment_sage_url != ''): ?>
			<a href="<?php echo $t1o_payment_sage_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_sage-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Sage" title="Sage"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_sage-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Sage" title="Sage">
        <?php endif; ?>
        </li>
		<?php endif; ?>   
        
        <?php if ($t1o_payment_solo == "1"): ?>
        <li data-toggle="tooltip" title="Solo">
        <?php if($t1o_payment_solo_url != ''): ?>
			<a href="<?php echo $t1o_payment_solo_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_solo-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Solo" title="Solo"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_solo-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Solo" title="Solo">
        <?php endif; ?>
        </li>
		<?php endif; ?> 
        
        <?php if ($t1o_payment_amazon == "1"): ?>
        <li data-toggle="tooltip" title="Amazon Payments">
        <?php if($t1o_payment_amazon_url != ''): ?>
			<a href="<?php echo $t1o_payment_amazon_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_amazon-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Amazon Payments" title="Amazon Payments"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_amazon-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Amazon Payments" title="Amazon Payments">
        <?php endif; ?>
        </li>
		<?php endif; ?>
        
        <?php if ($t1o_payment_western_union == "1"): ?>
        <li data-toggle="tooltip" title="Western Union">
        <?php if($t1o_payment_western_union_url != ''): ?>
			<a href="<?php echo $t1o_payment_western_union_url; ?>" target="_blank">
			<img src="catalog/view/theme/sellmore/image/payment/payment_image_western_union-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Western Union" title="Western Union"></a>
        <?php else: ?>
            <img src="catalog/view/theme/sellmore/image/payment/payment_image_western_union-<?php echo $t1d_f3_payment_images_style; ?>.png" alt="Western Union" title="Western Union">
        <?php endif; ?>
        </li>
		<?php endif; ?>
            
          </ul>
      </div>
      <?php } ?>
      
      <?php if($t1o_powered_status == 1) { ?>
      <div id="powered-content" class="col-sm-<?php echo 12 / ($t1o_payment_block_status + $t1o_powered_status + $t1o_follow_us_status) ; ?>">
      <?php echo html_entity_decode($t1o_powered_content[$lang_id], ENT_QUOTES, 'UTF-8');?>  
      </div>
      <?php } ?>
      
      <?php if($t1o_follow_us_status == 1) { ?>
      <div id="footer-social-wrapper" class="col-sm-<?php echo 12 / ($t1o_payment_block_status + $t1o_powered_status + $t1o_follow_us_status) ; ?>">
      <ul id="footer-social" class="list-inline">

            <?php if($t1o_facebook != '') { ?>
            <li data-toggle="tooltip" title="Facebook" class="facebook"><a href="<?php echo $t1o_facebook; ?>" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <?php } ?>
	        <?php if($t1o_twitter != '') { ?>
            <li data-toggle="tooltip" title="Twitter" class="twitter"><a href="<?php echo $t1o_twitter; ?>" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <?php } ?>
	        <?php if($t1o_googleplus != '') { ?>
            <li data-toggle="tooltip" title="Google+" class="google"><a href="<?php echo $t1o_googleplus; ?>" title="Google+" target="_blank"><i class="fa fa-google-plus"></i></a></li>
            <?php } ?>
	        <?php if($t1o_rss != '') { ?>
            <li data-toggle="tooltip" title="RSS" class="rrs"><a href="<?php echo $t1o_rss; ?>" title="RSS" target="_blank"><i class="fa fa-rss"></i></a></li>
            <?php } ?>
	        <?php if($t1o_pinterest != '') { ?>
            <li data-toggle="tooltip" title="Pinterest" class="pinterest"><a href="<?php echo $t1o_pinterest; ?>" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a></li>
            <?php } ?>
	        <?php if($t1o_vimeo != '') { ?>
            <li data-toggle="tooltip" title="Vimeo" class="vimeo"><a href="<?php echo $t1o_vimeo; ?>" title="Vimeo" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
            <?php } ?> 
	        <?php if($t1o_flickr != '') { ?>
            <li data-toggle="tooltip" title="Flickr" class="flickr"><a href="<?php echo $t1o_flickr; ?>" title="Flickr" target="_blank"><i class="fa fa-flickr"></i></a></li>
            <?php } ?>  
	        <?php if($t1o_linkedin != '') { ?>
            <li data-toggle="tooltip" title="LinkedIn" class="linkedin"><a href="<?php echo $t1o_linkedin; ?>" title="LinkedIn" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            <?php } ?>
	        <?php if($t1o_youtube != '') { ?>
           <li data-toggle="tooltip" title="YouTube" class="youtube"><a href="<?php echo $t1o_youtube; ?>" title="YouTube" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
            <?php } ?>
	        <?php if($t1o_dribbble != '') { ?>
            <li data-toggle="tooltip" title="Dribbble" class="dribbble"><a href="<?php echo $t1o_dribbble; ?>" title="Dribbble" target="_blank"><i class="fa fa-dribbble"></i></a></li>
            <?php } ?>
            <?php if($t1o_instagram != '') { ?>
            <li data-toggle="tooltip" title="Instagram" class="instagram"><a href="<?php echo $t1o_instagram; ?>" title="Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <?php } ?>   
            <?php if($t1o_behance != '') { ?>
            <li data-toggle="tooltip" title="Behance" class="behance"><a href="<?php echo $t1o_behance; ?>" title="Behance" target="_blank"><i class="fa fa-behance"></i></a></li>
            <?php } ?>   
            <?php if($t1o_skype != '') { ?>
            <li data-toggle="tooltip" title="Skype" class="skype"><a href="skype:<?php echo $t1o_skype; ?>" title="Skype" target="_blank"><i class="fa fa-skype"></i></a></li>
            <?php } ?>    
            <?php if($t1o_tumblr != '') { ?>
            <li data-toggle="tooltip" title="Tumblr" class="tumblr"><a href="<?php echo $t1o_tumblr; ?>" title="Tumblr" target="_blank"><i class="fa fa-tumblr"></i></a></li>
            <?php } ?>
            <?php if($t1o_reddit != '') { ?>
            <li data-toggle="tooltip" title="Reddit" class="reddit"><a href="<?php echo $t1o_reddit; ?>" title="Reddit" target="_blank"><i class="fa fa-reddit"></i></a></li>
            <?php } ?>
            <?php if($t1o_vk != '') { ?>
            <li data-toggle="tooltip" title="VK" class="vk"><a href="<?php echo $t1o_vk; ?>" title="VK" target="_blank"><i class="fa fa-vk"></i></a></li>
            <?php } ?>   
        
      </ul>
      </div>
      <?php } ?>
      
    </div>
  </div>
  </div>
  <?php } ?>
  
<?php if($t1o_custom_bottom_1_status == 1) { ?>
<div id="footer_custom_1">
<div class="container">
<div class="row">
<div class="col-sm-12 padd-t-b-30">
<?php echo html_entity_decode($t1o_custom_bottom_1_content[$lang_id], ENT_QUOTES, 'UTF-8');?>
</div>
</div>
</div>
</div>
<?php } ?>

</footer>



<script src="catalog/view/theme/sellmore/js/jquery.visible.min.js" type="text/javascript"></script>
<script type="text/javascript">

var win = $(window);

var allMods = $(".come-item");

allMods.each(function(i, el) {
    
  if ($(el).visible(true)) {
    $(el).addClass("already-visible"); 
  }
  
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    
    var el = $(el);
    
    if (el.visible(true)) {
      el.addClass("come-in"); 
    } else {
      el.removeClass("come-in already-visible");
    }
    
  });
  
}); 
</script>
<script>

    // Product List
	$('#list-view').click(function() {
		$('#content .product-layout > .clearfix').remove();
        
		$('#content .category-product-items').attr('class', 'row product-items category-product-items product-list-wrapper');
		$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');

		localStorage.setItem('display', 'list');
	});
	
	// Product Small List
	$('#small-list-view').click(function() {
		$('#content .product-layout > .clearfix').remove();
        
		$('#content .category-product-items').attr('class', 'row product-items category-product-items product-list-wrapper');
		$('#content .product-layout').attr('class', 'product-layout product-list product-small-list col-xs-12');

		localStorage.setItem('display', 'small-list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

			$('#content .category-product-items').attr('class', 'row product-items category-product-items product-grid-wrapper');
			$('#content .product-layout').attr('class', 'product-layout product-grid <?php echo $t1o_category_prod_box_style; ?> col-lg-<?php echo $t1o_product_grid_per_row; ?> col-md-<?php echo $t1o_product_grid_per_row; ?> col-sm-6 col-xs-6');
		
		 localStorage.setItem('display', 'grid');
	});
	
	// Product Gallery
	$('#gallery-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

			$('#content .category-product-items').attr('class', 'row product-items category-product-items product-gallery-wrapper');
			$('#content .product-layout').attr('class', 'product-layout product-gallery col-lg-<?php echo $t1o_product_grid_per_row; ?> col-md-<?php echo $t1o_product_grid_per_row; ?> col-sm-6 col-xs-6');

		 localStorage.setItem('display', 'gallery');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else if (localStorage.getItem('display') == 'small-list') {
		$('#small-list-view').trigger('click');
	} else if (localStorage.getItem('display') == 'gallery') {
		$('#gallery-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}
	
	// Module Grid
	cols2 = $('#column-right, #column-left').length;

		$('#content .product-layout-grid').attr('class', 'product-layout product-grid <?php echo $t1o_category_prod_box_style; ?> col-lg-<?php echo $t1o_product_grid_per_row; ?> col-md-<?php echo $t1o_product_grid_per_row; ?> col-sm-6 col-xs-6');
		$('.product-layout-slider').attr('class', 'product-layout-slider  product-grid <?php echo $t1o_category_prod_box_style; ?> col-xs-12');

</script>
<script src="catalog/view/theme/sellmore/js/tickerme.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$('#ticker').tickerme();
});
</script>
<script type="text/javascript" src="catalog/view/theme/sellmore/js/jquery.easing-1.3.min.js"></script>
<?php if($t1o_header_fixed_header_status ==1) { ?>
<script type="text/javascript" src="catalog/view/theme/sellmore/js/jquery.sticky.js"></script>
<?php } ?>
<?php if($t1o_header_auto_suggest_status ==1) { ?>
<link rel="stylesheet" property="stylesheet" type="text/css" href="catalog/view/theme/sellmore/stylesheet/livesearch.css" />
<script type="text/javascript" src="catalog/view/theme/sellmore/js/livesearch.js"></script>
<?php } ?>
<?php if($t1o_others_totop =='1') { ?>	
<link rel="stylesheet" property="stylesheet" type="text/css" href="catalog/view/theme/sellmore/stylesheet/ui.totop.css" />
<script type="text/javascript" src="catalog/view/theme/sellmore/js/jquery.ui.totop.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {	
		$().UItoTop({ easingType: 'easeOutQuart' });	
	});
</script>
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/sellmore/stylesheet/animate.css" />
<script type="text/javascript" src="catalog/view/theme/sellmore/js/quickview/quickview.js"></script>		
<link rel="stylesheet" property="stylesheet" href="catalog/view/theme/sellmore/js/quickview/fancybox/jquery.fancybox.css" />
<script src="catalog/view/theme/sellmore/js/quickview/fancybox/jquery.fancybox.pack.js"></script>
<script src="catalog/view/theme/sellmore/js/jquery.stellar.js" type="text/javascript"></script>
<?php if($t1o_eu_cookie_status == 1) { ?>
<script src="catalog/view/theme/sellmore/js/jquery.eucookiebar.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('.cookie-message').cookieBar({ closeButton : '.my-close-button' });
    });
</script>
<div class="cookie-message hidden-xs">
  <span class="cookie-img"><img src="catalog/view/theme/sellmore/image/321_cookie.png" alt="Cookie" title="Cookie"></span>
  <?php echo html_entity_decode($t1o_eu_cookie_message[$lang_id], ENT_QUOTES, 'UTF-8');?>
  <a class="my-close-button btn btn-primary" href="#"><?php echo $t1o_eu_cookie_close[$lang_id]; ?></a>
</div>
<?php } ?>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/sellmore/stylesheet/cloud-zoom.css" />
<script type="text/javascript" src="catalog/view/theme/sellmore/js/cloud-zoom.js"></script>
<link rel="stylesheet" property="stylesheet" type="text/css" href="catalog/view/javascript/jquery/magnific/magnific-popup.css" />	
<script type="text/javascript" src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>

<?php if($t1o_custom_js !='') { ?>
<?php echo htmlspecialchars_decode( $t1o_custom_js, ENT_QUOTES ); ?>
<?php } ?>

</div>
<?php if(($t1o_custom_bottom_2_status == 1) && ($t1o_layout_style == 'full-width')) { ?>
<div id="footer_custom_2" class="hidden-xs">
<div class="container">
<div class="row">
<div class="col-sm-12 padd-t-b-40">
<?php echo html_entity_decode($t1o_custom_bottom_2_content[$lang_id], ENT_QUOTES, 'UTF-8');?>
</div>
</div>
</div>
</div>
<?php } ?>
</body></html>