<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-theme-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-theme-banner" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner"><?php echo $entry_banner; ?></label>
            <div class="col-sm-10">
              <select name="banner_id" id="input-banner" class="form-control">
                <?php foreach ($banners as $banner) { ?>
                <?php if ($banner['banner_id'] == $banner_id) { ?>
                <option value="<?php echo $banner['banner_id']; ?>" selected="selected"><?php echo $banner['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $banner['banner_id']; ?>"><?php echo $banner['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-width"><?php echo $entry_width; ?></label>
            <div class="col-sm-10">
              <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
              <?php if ($error_width) { ?>
              <div class="text-danger"><?php echo $error_width; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-height"><?php echo $entry_height; ?></label>
            <div class="col-sm-10">
              <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
              <?php if ($error_height) { ?>
              <div class="text-danger"><?php echo $error_height; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_view"><?php echo $entry_banner_view; ?></label>
            <div class="col-sm-10">
              <select name="banner_view" id="input-banner_view" class="form-control">
                <option value="grid" <?php if ($banner_view == 'grid') {echo "selected=\"selected\"";}; ?>><?php echo $text_view_grid; ?></option>
				<option value="slider" <?php if ($banner_view == 'slider') {echo "selected=\"selected\"";}; ?>><?php echo $text_view_slider; ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-pr_id"><?php echo $entry_banner_per_row; ?></label>
            <div class="col-sm-10">
              <select name="pr_id" id="input-pr_id" class="form-control">
              <?php for ($i=1; $i<=5; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($pr_id==$i) {echo "selected=\"selected\"";}; ?>><?php echo $i; ?></option>
              <?php }; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-title_position"><?php echo $entry_title_position; ?></label>
            <div class="col-sm-10">
              <select name="title_position" id="input-title_position" class="form-control">
                <option value="title-bottom-center" <?php if ($title_position == 'title-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
				<option value="title-bottom-left" <?php if ($title_position == 'title-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                <option value="title-bottom-right" <?php if ($title_position == 'title-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                <option value="title-center" <?php if ($title_position == 'title-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
				<option value="title-left" <?php if ($title_position == 'title-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                <option value="title-right" <?php if ($title_position == 'title-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                <option value="title-top-center" <?php if ($title_position == 'title-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
				<option value="title-top-left" <?php if ($title_position == 'title-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                <option value="title-top-right" <?php if ($title_position == 'title-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-title_slide_status"><?php echo $entry_slide; ?></label>
            <div class="col-sm-10">
              <select name="title_slide_status" id="input-title_slide_status" class="form-control">
                <option value="1" <?php if ($title_slide_status == '1') {echo "selected=\"selected\"";}; ?>><?php echo $text_yes; ?></option>
				<option value="0" <?php if ($title_slide_status == '0') {echo "selected=\"selected\"";}; ?>><?php echo $text_no; ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>