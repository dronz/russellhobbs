
<style type="text/css">
#theme-lookbook<?php echo $module; ?> h2 {
	color: <?php echo $module_title_color; ?>;
}
#theme-lookbook<?php echo $module; ?> .theme-lookbook-subtitle {
	color: <?php echo $module_subtitle_color; ?>;
}
.module-style-1 #theme-lookbook<?php echo $module; ?> .theme-lookbook-subtitle {
<?php if($module_title_position =='right') { ?>
	padding-left: 20%;
<?php } else { ?>
    padding-right: 20%;
<?php } ?>
}
#theme-lookbook<?php echo $module; ?> {
<?php if($module_bg_color !='') { ?>
	background-color: <?php echo $module_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($module_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $module_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
<?php if($module_title_position =='left') { ?>
	background-position: top left;
<?php } else { ?>
	background-position: top right;
<?php } ?>
}
.module-style-1 #theme-lookbook<?php echo $module; ?> .panel-inline-title {
	float: <?php echo $module_title_position; ?>; 
}
<?php if($module_title_position =='right') { ?>
.module-style-1 #theme-lookbook<?php echo $module; ?> h2, .module-style-1 #theme-lookbook<?php echo $module; ?> .theme-lookbook-subtitle {
	text-align: right;
}
#theme-lookbook<?php echo $module; ?>.panel-inline-content .panel-inline-title .btn.btn-primary, .module-style-1 #theme-lookbook<?php echo $module; ?> h2:before {
	right: 0;
	left: inherit;
	margin-right: 0;
}
<?php } ?>
</style>


<div id="theme-lookbook" class="panel panel-default panel-inline <?php echo $module_style; ?>">
<div id="theme-lookbook<?php echo $module; ?>" class="box-content theme-lookbook-content panel-inline-content">
  <?php if(($lookbook['title']) !='') { ?>
  <div class="theme-lookbook-title panel-inline-title col-sm-<?php echo $module_title_width; ?> full-width-container">
  <h2><?php echo $lookbook['title']; ?></h2>
  <?php if(($lookbook['subtitle']) !='') { ?>
  <div class="theme-lookbook-subtitle panel-inline-subtitle subtitle"><?php echo $lookbook['subtitle']; ?></div>
  <?php } ?>
  </div>
  <?php } ?>
  <?php if(($lookbook['title']) !='') { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> mgrb full-width-container">
  <?php } else { ?>
  <div class="panel-inline-items col-sm-<?php echo $module_items_width; ?> full-width">
  <?php } ?>
  <div class="theme-lookbook-items<?php echo $module; ?>">
  <?php foreach ($banners as $banner) { ?>
  <div class="theme-lookbook-item">
  <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" />
  <span class="theme-lookbook-bg"></span>
  <a href="<?php echo $banner['link']; ?>">
  <div class="panel-inline-description">
  <span class="btn btn-default theme-lookbook-item-title"><?php echo $banner['title']; ?></span>
  <span class="btn btn-default theme-lookbook-item-title view-now"><?php echo $t1o_text_view[$lang_id]; ?></span>
  </div>
  </a>
  </div>
  <?php } ?>
  </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
$('.theme-lookbook-items<?php echo $module; ?>').owlCarousel({
	items: <?php echo $pr_id; ?>,
	itemsDesktop : [1199, <?php echo $pr_id; ?>],
    itemsDesktopSmall : [979, 2],
    itemsTablet : [768, 2],
	singleItem: false,
	scrollPerPage: false,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>