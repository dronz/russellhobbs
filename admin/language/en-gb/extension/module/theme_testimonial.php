<?php
// Heading
$_['heading_title']    = 'SELLMORE Theme - Testimonial';

// Text
$_['text_module']      = 'Modules';
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified SELLMORE Theme - Testimonial module!';
$_['text_edit']        = 'Edit SELLMORE Theme - Testimonial Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_dimension']  = 'Dimension (W x H) and Resize Type';
$_['entry_status']     = 'Status';
$_['entry_title']      = 'Title:'; 
$_['entry_testimonials_per_row'] = 'Testimonials in a row:';
$_['entry_module_bg_color'] = 'Background color:';
$_['entry_module_image_thumb'] = 'Background image:';
$_['entry_module_title_color'] = 'Title color:';
$_['entry_module_testimonial_color'] = 'Testimonial color:';
$_['entry_module_name_color'] = 'Name color:';
$_['entry_photo']         = 'Photo';
$_['entry_testimonial']   = 'Testimonial';
$_['entry_reviewer_name'] = 'Name';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify SELLMORE Theme - Testimonial module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';