<?php
class ControllerModuleZoomos extends Controller {
	public function index() {

error_reporting(E_ALL | E_STRICT) ;
            ini_set('display_errors', 'On');
            //require "config.php";

            ini_set('memory_limit', '1024M');
            set_time_limit(0);

			$ZOOMOS_KEY = 'opto.by-6V3IssAf2';
			
            $ZOOMOS_JSON_URL = "http://api.export.zoomos.by/pricelist?key=$ZOOMOS_KEY";

			$script_on = file_get_contents('zoomos_data/script_on.txt');
			if($script_on == '1'){
				//echo 'СКРИПТ ЕЩЕ РАБОТАЕТ'; exit;
			}
			
			file_put_contents('zoomos_data/script_on.txt', 1);			
 
			$log_categories = 'categories.txt';
			
			$arr_titles = array('_CATEGORY_','_ID_','_NAME_','_MODEL_','_SKU_','_MANUFACTURER_','_PRICE_', '_QUANTITY_','_SHORT_DESCRIPTION_','_DESCRIPTION_','_IMAGE_','_IMAGES_','_ATTRIBUTES_', '_UPC_', '_EAN_', '_MODEL_CODE_','_SEO_KEYWORD_', '_ISBN_', '_STOCK_STATUS_ID');

			$path = $_SERVER['DOCUMENT_ROOT'].'/zoomos_data/items.txt';
			$path_test = $_SERVER['DOCUMENT_ROOT'].'/zoomos_data/test.txt';

			$zoomos_data = file_get_contents($path);
			$zoomos_data = unserialize($zoomos_data);

			if(!count($zoomos_data)){
				echo 'Blank';
				exit;
			} 

			$step = 50;
			$c    = 1;
			$tc   = 0;
			$currency = 3;
			$was_add = false;
			$p_ids = array();
			$data_arr = array();
			$zoomos_new = array();
			$ik = 0;  
			$g = 0;
			$zoomos_categories = array();
			
			foreach($zoomos_data as $zoomos){
    
				if($c >= $step){
					$zoomos_new[] = $zoomos;
					$g++;
					continue;
				}

				$item_url = 'http://api.export.zoomos.by/item/'.$zoomos->id.'?key='.$ZOOMOS_KEY;

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($ch, CURLOPT_USERAGENT, $agent);
				curl_setopt($ch, CURLOPT_TIMEOUT, 60 * 60);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60 * 60);
				curl_setopt($ch, CURLOPT_URL, $item_url);

				$item_data = json_decode(curl_exec($ch));

				$need_update = 0;
				$data = array();
				if(isset($zoomos->id) && isset($zoomos->shopsId)){
					$sql = "SELECT zoomos_id, product_id FROM ".DB_PREFIX."product WHERE zoomos_id='".$zoomos->id."' OR product_id = '".$zoomos->shopsId."' LIMIT 1";					
				}else{
					$sql = "SELECT zoomos_id, product_id FROM ".DB_PREFIX."product WHERE zoomos_id='".$zoomos->id."' LIMIT 1";					
				}
				$res = $this->db->query($sql);

				if(isset($res->row) && $res->row){				
					$data['product_id'] = $res->row['product_id'];
					$need_update = 1;
				}else{
				//if(!in_array($zoomos->category->id, $zoomos_categories)){
					$sql = 'SELECT cd.name, c.parent_id, cd.category_id, c.zoomos_id FROM '.DB_PREFIX.'category as c LEFT JOIN '.DB_PREFIX.'category_description as cd ON(c.category_id = cd.category_id) WHERE c.zoomos_id=\''.$zoomos->category->id.'\'';
					$res_cat = $this->db->query($sql);

					if(!isset($res_cat->row['category_id'])){
							
							if(!in_array($zoomos->category->id, $zoomos_categories)){
								file_put_contents($log_categories, $item_data->category->parentCategory->name.' - '.$zoomos->category->name.' '.$zoomos->category->id. "\n", FILE_APPEND);
							}
							
							$zoomos_categories[] = $zoomos->category->id;
							$c+=0.5;
							continue;
					}
				}
				//echo '<pre>'; var_dump($data); echo '</pre>';
				//}else{
				//		continue;
				//}

				/*$categories = $this->getCategory($res->row, array());
				$categories = array_reverse($categories);*/
				
				$data['category_id'] = $res_cat->row['category_id'];
				$data['parent_id'] = $res_cat->row['parent_id'];

				//$data['category'] = $cats;

				if(isset($item_data->typePrefix) && $item_data->typePrefix){
					$data['name'] = $item_data->typePrefix;
				}else{
					$data['name'] = '';
				}
				if(isset($item_data->vendor->name) && isset($item_data->model)){
					$data['name']  .= ' ' .$item_data->vendor->name.' '.$item_data->model;
				}elseif(isset($item_data->vendor->name) || isset($item_data->model)){
					if(isset($item_data->model)){
						$data['name']  .= ' '.$item_data->model;
					}else{
						$data['name']  .= ' '.$item_data->vendor->name;
					}
				}

				$data['name'] = trim($data['name']);

				$data['model'] = '';
				if(isset($item_data->model)){    
					$data['model'] = $item_data->model;
				}
				
                if($zoomos->price >0){
                    $data['price'] = (float)$zoomos->price;
                }
				
				$data['quantity'] = 100;

				if($data['price'] > 0){
						$data['stock_status_id'] = 7;
				}else{
						$data['stock_status_id'] = 5;
				}				

				$data['sku'] = '';
				if(isset($zoomos->supplierInfo->itemId)){
					$data['sku'] = $zoomos->supplierInfo->itemId;
				}
				$data['url'] = $item_data->linkRewrite;

				$data['manufacturer'] = '';
				if(isset($item_data->vendor->name)){
					$data['manufacturer'] = $item_data->vendor->name;
				}
				$data['short_description'] = '';
				if(isset($item_data->shortDescriptionHTML)){
					$data['short_description'] = $item_data->shortDescriptionHTML;
				}
				$data['description'] = '';
				if(isset($item_data->fullDescriptionHTML)){
					$data['description'] = $item_data->fullDescriptionHTML;
				}

				$data['image'] = '';

				//if(isset($zoomos->image) && $zoomos->image){
					//$data['image'] = $zoomos->image;
				//}
		
				$data['images'] = '';
				if(isset($item_data->images)){
					if(isset($item_data->images[0])){
						$data['image'] = $item_data->images[0].'.jpg';
						foreach($item_data->images as $key =>$img){
							if($key == 0){
								continue;
							}
							$data['images'] .= $img.'.jpg,';
						}
						if($data['images']){
							$data['images'] = substr($data['images'], 0, -1);
						}
					}
				}

				$data['attr'] = '';

				if(isset($item_data->details->featuresBlocks)){
						$attr = '';
					foreach($item_data->details->featuresBlocks as $block){

						$grp = $block->name;
						foreach($block->features as $blc){
							$attr_name = $blc->name;
							$attr_val = '';
							foreach($blc->values as $val){
								$attr_val .= $val.' ';
							}
							$attr_val = substr($attr_val, 0, -1);
							$attr .=$grp.'|'.$attr_name.'|'.$attr_val."\n";
							//$attr .=$grp.'|'.$attr_name.'|'.$attr_val.'|1'."\n\r";
						}
					}
					$data['attr'] = $attr;

				}

				$data['seo_keyword'] = '';
				if(isset($zoomos->linkRewrite)){
						$data['seo_keyword'] = $zoomos->linkRewrite;
				}  	
				
				$data['warrantyInfoHTML'] = '';
				if(isset($item_data->warrantyInfoHTML)){
					$data['warrantyInfoHTML'] = $item_data->warrantyInfoHTML;
				}  	
				

				$data['zoomos_id'] = $zoomos->id;
				
				$data['shipping'] = 1;
				
				$data['dateUpdMillis'] = 0;
				if(isset($zoomos->itemDateUpdMillis)){
					$data['dateUpdMillis'] = $zoomos->itemDateUpdMillis;
				}else{
					$data['dateUpdMillis'] = $zoomos->itemDateAddMillis;
				}
				
				$data_arr[] = $data;
				$was_add = true;
				$c++;
				$tc++;
				
				if(!isset($data['name']) || !$data['name']){
						continue;
				}                

				//нужно обновить
				if($need_update){
					$a = $this->updateProduct($data);
					continue;
				}else{
					$this->addProduct($data);
				}
				
							
			}

            file_put_contents($path, serialize($zoomos_new));
            file_put_contents($path_test, 'Осталось:'.$g);
            //error_log("Конец скрипта", 0);
            echo 'FINISH';

			file_put_contents('zoomos_data/script_on.txt', 0);

            curl_close($ch);  			
			
	}
	
	
	function getCategory($category, $arr = array()){

		$arr[] = $category['name'];
		
		if(isset($category['parent_id']) && $category['parent_id']){
			$sql = 'SELECT cd.name, c.parent_id, c.category_id, c.zoomos_id FROM '.DB_PREFIX.'category as c LEFT JOIN '.DB_PREFIX.'category_description as cd ON(c.category_id = cd.category_id) WHERE c.category_id=\''.$category['parent_id'].'\'';		
			$res = $this->db->query($sql);
			$arr = $this->getCategory($res->row, $arr);
		}
		
		return $arr;
    
	}	
	
	private function updateProduct($data){

             $sql = '';
             if(isset($data['model'])){
                 $sql .= ' model = REPLACE(\'' . $this->db->escape($data['model']) . '\', \'"\', \'&quot;\'),';        
             }

             if(isset($data['sku'])) {
                     $sql .= ' sku = \'' . $this->db->escape($data['sku']) . '\',';
             }
			 
			if(isset($data['zoomos_id'])) {
					$sql .= ' zoomos_id = \'' . (int)$data['zoomos_id'] . '\',';
			} 			 

             if(isset($data['dateUpdMillis'])) {
                     $sql .= ' dateUpdMillis = \'' . (int)$data['dateUpdMillis'] . '\',';
             }                               

             if(isset($data['warrantyInfoHTML'])) {
                     $sql .= ' warrantyInfoHTML = \'' . $this->db->escape($data['warrantyInfoHTML']) . '\',';
             } 			 
			 
             if(isset($data['shipping'])) $sql .= ' shipping = \'' . $this->db->escape($data['shipping']) . '\',';                    

             if(isset($data['image'])){
                 //if($this->getRemoteFileSize(str_replace('.jpg', '', $data['image'])) > 3000){
                     $sql .= ' image = \'' .  $this->db->escape(html_entity_decode($this->saveImg($data['image'], $data['zoomos_id']), ENT_QUOTES, 'UTF-8')) . '\',';
                 //}else{
                 //    if($data['image']){
                 //        return false;
                 //    }
                 //}
            }


             $sql .= ' status = \'' . (int)1 . '\',';

             $sql .= ' tax_class_id = \'' . (int)0 . '\',';
             $sql .= ' minimum = \'' . (int)1 . '\',';
             $sql .= ' subtract = \'' . (int)0 . '\',';
             if(!isset($data['stock_status_id'])) {
                     $sql .= ' stock_status_id = \'' . (int)9 . '\',';
             }

             $sql .= ' weight_class_id = \'' . (int)1 . '\',';
             $sql .= ' length_class_id = \'' . (int)1 . '\',';
             
			 // _MANUFACTURER_
             //-------------------------------------------------------------------------
             if(isset($data['manufacturer']) ) {
                     $manufacturer_id = $this->getManufacturer($data['manufacturer']); 
                     $sql .= ' manufacturer_id = \'' . (int)$manufacturer_id . '\',';
             } else {
                     $sql .= ' manufacturer_id = \'' . (int)0 . '\','; // Last Field
             }

             $sql .= ' date_modified = NOW(),';
             
             $product_id = $data['product_id'];
             
            if(!empty($sql)) {
                    $sql = 'UPDATE `' . DB_PREFIX . 'product` SET ' . mb_substr($sql,0,-1) . ' WHERE product_id = \'' . (int)$product_id . '\'';
                    $this->db->query($sql);
            }
                                                                 

             // Add Product description by language_id
             //-------------------------------------------------------------------------
             $sql = '';

             if(isset($data['name'])) {
                     $sql .= ' name = REPLACE(\'' . $this->db->escape($data['name']) . '\', \'"\', \'&quot;\'),';
             }

            if(!empty($sql)) {
                    $result = $this->db->query('SELECT 1 FROM `' . DB_PREFIX . 'product_description` WHERE product_id = \'' . (int)$product_id . '\' AND language_id = \'' . (int)1 . '\'');

                    if($result->num_rows > 0 ) {
                            $sql = 'UPDATE `' . DB_PREFIX . 'product_description` SET ' . mb_substr($sql,0,-1)  . ' WHERE product_id = \'' . (int)$product_id . '\' AND language_id = \'' . (int)1 . '\'';
                    } else {
                            $sql = 'INSERT INTO `' . DB_PREFIX . 'product_description` SET ' . $sql  . ' product_id = \'' . (int)$product_id . '\', language_id = \'' . (int)1 . '\'';
                    }
                    $this->db->query($sql);
            }             


             // Add Product Images
             //-------------------------------------------------------------------------
             if(isset($data['images'])) {
                     $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_image` WHERE product_id = \'' . (int)$product_id . '\'');
                     if( !empty($data['images']) ) {
                             $images = explode(',', $data['images']);
                             foreach ($images as $image) {
                                     if(!empty($image) && $this->getRemoteFileSize(str_replace('.jpg', '', $image)) > 3000) {
                                             $sql = 'INSERT INTO `' . DB_PREFIX . 'product_image` SET product_id = \'' . (int)$product_id . '\', image = \'' . $this->db->escape(html_entity_decode($this->saveImg($image,$data['zoomos_id']), ENT_QUOTES, 'UTF-8')) . '\'';
                                             $this->db->query($sql);
                                     }
                             }
                     }
             }


             // Add Product Attributes
             //-------------------------------------------------------------------------
             if( isset($data['attr'])) {

                     $attributes = array();

                     $attributes = explode("\n", $data['attr']);

                     if(!empty($attributes)) {
                             $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_attribute` WHERE language_id = \'' . (int)1 . '\' AND product_id = \'' . (int)$product_id . '\'');

                             $tmp_product_attributes = array(); // added in v2.2.2a
                             foreach ($attributes as $attribute_date) {
                                     $attribute = explode('|', $attribute_date);
                                     if(count($attribute) == 3) {
                                             $attribute[0] = trim($attribute[0]);
                                             $attribute[1] = trim($attribute[1]);
                                             $attribute[2] = trim($attribute[2]);
                                             //$attribute[3] = trim($attribute[3]);
                                             // check
                                             if(!isset($this->attributes[mb_strtolower($attribute[0].$attribute[1])])) {
                                                     $attribute_id = $this->addProductAttribute($attribute[0], $attribute[1]);
                                             } else {
                                                     $attribute_id = $this->attributes[mb_strtolower($attribute[0].$attribute[1])];
                                             }

                                             // Add
                                             if (!in_array($product_id.'-'.$attribute_id.'-'.'1', $tmp_product_attributes) ) {
                                                     $this->db->query('INSERT INTO `' . DB_PREFIX . 'product_attribute` 
                                                             SET	product_id = \'' . (int)$product_id . '\',
                                                             attribute_id = \'' . (int)$attribute_id . '\',
                                                             language_id = \'' . (int)1 . '\',
                                                             text = \'' .$this->db->escape($attribute[2]) . '\'
                                                     ');
                                                     $tmp_product_attributes[] = $product_id.'-'.$attribute_id.'-'.'1';
                                             }
                                     }
                             }
                     }
             }  
             
             return TRUE;
             
        }
	
	function addProduct($data){
		$sql = '';
		if(isset($data['model'])){
			$sql .= ' model = REPLACE(\'' . $this->db->escape($data['model']) . '\', \'"\', \'&quot;\'),';        
		}

		if(isset($data['sku'])) {
				$sql .= ' sku = \'' . $this->db->escape($data['sku']) . '\',';
		}

		if(isset($data['dateUpdMillis'])) {
				$sql .= ' dateUpdMillis = \'' . (int)$data['dateUpdMillis'] . '\',';
		}
		
		if(isset($data['warrantyInfoHTML'])) {
				$sql .= ' warrantyInfoHTML = \'' . $this->db->escape($data['warrantyInfoHTML']) . '\',';
		} 			

		if(isset($data['zoomos_id'])) {
				$sql .= ' zoomos_id = \'' . (int)$data['zoomos_id'] . '\',';
		} 		
		
		if(isset($data['shipping'])) $sql .= ' shipping = \'' . $this->db->escape($data['shipping']) . '\',';                    
				
		if(isset($data['image'])){
			//if($this->getRemoteFileSize(str_replace('.jpg', '', $data['image'])) > 3000){
				$sql .= ' image = \'' .  $this->db->escape(html_entity_decode($this->saveImg($data['image'], $data['zoomos_id']), ENT_QUOTES, 'UTF-8')) . '\',';
			//}
		}

		$sql .= ' status = \'' . (int)1 . '\',';


		$sql .= ' tax_class_id = \'' . (int)0 . '\',';
		$sql .= ' minimum = \'' . (int)1 . '\',';
		$sql .= ' subtract = \'' . (int)0 . '\',';
		if(!isset($data['stock_status_id'])) {
				$sql .= ' stock_status_id = \'' . (int)9 . '\',';
		}

		$sql .= ' weight_class_id = \'' . (int)1 . '\',';
		$sql .= ' length_class_id = \'' . (int)1 . '\',';
		$sql .= ' date_added = NOW(),';
		$sql .= ' date_available = DATE_FORMAT(NOW(),\'%Y-%m-%d\'),';    

		if(isset($data['stock_status_id'])){ 
				$sql .= ' stock_status_id = \'' . (int)$data['stock_status_id'] . '\',';
		}   

		if(isset($data['quantity'])){ 
				$sql .= ' quantity = \'' . (int)$data['quantity'] . '\',';
		}   		
				
		
		if (isset($data['price']) ) {

				$price = preg_replace("/\\s+/iu", "", $data['price']);
				$price = preg_replace("/,/iu", ".", $price);
				$price = (float)$price;

				$sql .= ' price = \'' . number_format($price, 4, '.', ''). '\',';
		}			
		
		// _MANUFACTURER_
		//-------------------------------------------------------------------------
		if(isset($data['manufacturer']) ) {
				$manufacturer_id = $this->getManufacturer($data['manufacturer']); 
				$sql .= ' manufacturer_id = \'' . (int)$manufacturer_id . '\'';
		} else {
				$sql .= ' manufacturer_id = \'' . (int)0 . '\''; // Last Field
		}

		$sql = 'INSERT INTO `' . DB_PREFIX . 'product` SET ' . $sql;
		$this->db->query($sql);

		$product_id = $this->db->getLastId();


		//if (isset($this->setting['product_store'])) {
				//foreach ($this->setting['product_store'] as $store_id) {
						$store_id = 0;
						$this->db->query('INSERT INTO `' . DB_PREFIX . 'product_to_store` SET product_id = \'' . (int)$product_id . '\', store_id = \'' . (int)$store_id . '\'');
				//}
		//}                                                                       
		
		// Add Product description by language_id
		//-------------------------------------------------------------------------
		$sql = '';

		if(isset($data['name'])) {
				$sql .= ' name = REPLACE(\'' . $this->db->escape($data['name']) . '\', \'"\', \'&quot;\'),';
		}
		
		if(isset($data['description'])) {
				$sql .= ' description = \'' . $this->db->escape(htmlspecialchars($data['description'])) . '\',';
		}

		if(!empty($sql)) {
				$sql_inj = $sql;
				$sql = 'INSERT INTO `' . DB_PREFIX . 'product_description` SET ' . $sql . ' product_id = \'' . (int)$product_id . '\', language_id = \'' . (int)1 . '\'';
				$this->db->query($sql);                        
		}  
		
		// Add Product Images
		//-------------------------------------------------------------------------
		if(isset($data['images'])) {
				$this->db->query('DELETE FROM `' . DB_PREFIX . 'product_image` WHERE product_id = \'' . (int)$product_id . '\'');
				if( !empty($data['images']) ) {
						$images = explode(',', $data['images']);
						foreach ($images as $image) {
								//if(!empty($image) && $this->getRemoteFileSize(str_replace('.jpg', '', $image)) > 3000) {
		   //if(!empty($image) && $this->getRemoteFileSize(str_replace('.jpg', '', $image)) > 7000) {
				if(!empty($image)) {
										$sql = 'INSERT INTO `' . DB_PREFIX . 'product_image` SET product_id = \'' . (int)$product_id . '\', image = \'' . $this->db->escape(html_entity_decode($this->saveImg($image,$data['zoomos_id']), ENT_QUOTES, 'UTF-8')) . '\'';
										$this->db->query($sql);
								}
						}
				}
		}

		// Add Product Category
		//-------------------------------------------------------------------------
		if ($data['category_id']) {
				$this->db->query('DELETE FROM `' . DB_PREFIX . 'product_to_category` WHERE product_id = \'' . (int)$product_id . '\'');
				//$categories = array_unique($categories);
				//foreach ($categories as $category_id) {
						$this->db->query('INSERT INTO `' . DB_PREFIX . 'product_to_category` SET product_id = \'' . (int)$product_id . '\', category_id = \'' . (int)$data['category_id'] . '\', main_category=1');
				//}
				$categories = $this->getParentCategories($data['parent_id']);
				foreach ($categories as $category_id) {
						$this->db->query('INSERT INTO `' . DB_PREFIX . 'product_to_category` SET product_id = \'' . (int)$product_id . '\', category_id = \'' . (int)$category_id . '\'');
				}				
		}


		// Add Product Attributes
		//-------------------------------------------------------------------------
		if( isset($data['attr'])) {

				$attributes = array();

				$attributes = explode("\n", $data['attr']);

				if(!empty($attributes)) {
						$this->db->query('DELETE FROM `' . DB_PREFIX . 'product_attribute` WHERE language_id = \'' . (int)1 . '\' AND product_id = \'' . (int)$product_id . '\'');

						$tmp_product_attributes = array(); // added in v2.2.2a
						foreach ($attributes as $attribute_date) {
								$attribute = explode('|', $attribute_date);
								if(count($attribute) == 3) {
										$attribute[0] = trim($attribute[0]);
										$attribute[1] = trim($attribute[1]);
										$attribute[2] = trim($attribute[2]);
										//$attribute[3] = trim($attribute[3]);
										// check
										if(!isset($this->attributes[mb_strtolower($attribute[0].$attribute[1])])) {
												$attribute_id = $this->addProductAttribute($attribute[0], $attribute[1]);
										} else {
												$attribute_id = $this->attributes[mb_strtolower($attribute[0].$attribute[1])];
										}

										// Add
										if (!in_array($product_id.'-'.$attribute_id.'-'.'1', $tmp_product_attributes) ) {
												$this->db->query('INSERT INTO `' . DB_PREFIX . 'product_attribute` 
														SET	product_id = \'' . (int)$product_id . '\',
														attribute_id = \'' . (int)$attribute_id . '\',
														language_id = \'' . (int)1 . '\',
														text = \'' .$this->db->escape($attribute[2]) . '\'
												');
												$tmp_product_attributes[] = $product_id.'-'.$attribute_id.'-'.'1';
										}
								}
						}
				}
		}
		
		if(isset($data['seo_keyword'])) {
			//$this->db->query('DELETE FROM `' . DB_PREFIX . 'url_alias` WHERE query = \'product_id=' . (int)$product_id . '\'');
			$this->db->query('DELETE FROM `' . DB_PREFIX . 'url_alias` WHERE keyword = \''.$data['seo_keyword'].'\' ');
			$sql = 'INSERT INTO `' . DB_PREFIX . 'url_alias` SET query = \'product_id=' . (int)$product_id . '\', keyword = \'' . $this->db->escape($data['seo_keyword']) . '\'';
			$this->db->query($sql);

		}	
	
		
	}
	
	private function fsize($path){
		$fp = fopen($path,"r");
		$inf = stream_get_meta_data($fp);
		fclose($fp);
		
		foreach($inf["wrapper_data"] as $v){
			//echo '<pre>'; var_dump($v); echo '</pre>';
			if (stristr($v,"content-length")){
				$v = explode(":",$v);
				return trim($v[1]);
			}
		}
	}
        
    function getRemoteFileSize($url){
        ob_start();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        $ok = curl_exec($ch);
        curl_close($ch);
        $head = ob_get_contents();
        ob_end_clean();
        $regex = '/Content-Length:\s([0-9].+?)\s/';
        preg_match($regex, $head, $matches);
        return isset($matches[1]) ? $matches[1] : "unknown";
    }        

        // Add Manufacturer
	//-------------------------------------------------------------------------
	private function getManufacturer(&$name) {
		$name = trim($name, " \t\n");
		
		if(empty($name)) return 0;
		
		$result = $this->db->query('SELECT manufacturer_id FROM `' . DB_PREFIX . 'manufacturer` WHERE LOWER(name) = LOWER(\'' . $this->db->escape($name) . '\') LIMIT 1');
		
		if(isset($result->num_rows) AND $result->num_rows > 0 ) {
			return $result->row['manufacturer_id'];
		} else {
			$this->db->query('INSERT INTO `' . DB_PREFIX . 'manufacturer` SET name = \'' . $this->db->escape($name) . '\', sort_order = 0');
		
			$manufacturer_id = $this->db->getLastId();
            $store_id=0;
			$this->db->query('INSERT INTO `' . DB_PREFIX . 'manufacturer_to_store` SET manufacturer_id = \'' . (int)$manufacturer_id . '\', store_id = \'' . (int)$store_id . '\'');

			return $manufacturer_id;
		}
	} 
        
    private function saveImg($link,$add_cat = ''){
		if ((strpos(' '.$link, 'http:') ||  strpos(' '.$link, 'https:'))){
			$save_path = DIR_IMAGE.'/data/prd/'.$add_cat.'/';
                        $save_path2 = 'data/prd/'.$add_cat.'/';
                        
			if(!file_exists($save_path)) {
				mkdir($save_path);
			}
			
			$arr = parse_url($link);
			if ($arr['path']!=''){
				$tmp = explode('/',$arr['path']);
				$file = end($tmp);
				$file2 = str_replace('.jpg', '-new.jpg', $file);
				$save_path .= $file;

                //$this->load->model('tool/image' );
                                
				file_put_contents($save_path, file_get_contents($link));
				$new_filename = $this->resize2($save_path2.$file, $save_path2.$file2, 1200, 'auto');
				unlink($save_path);
				return $new_filename;
				// $ch = curl_init($link);
				// $fp = fopen($save_path, 'wb');
				// curl_setopt($ch, CURLOPT_FILE, $fp);
				// curl_setopt($ch, CURLOPT_HEADER, 0);
				// curl_exec($ch);
				// curl_close($ch);
				// fclose($fp);
				return 'data/prd/'.$add_cat.'/'.$file;
			}
			else{
				return $link;
			}
		}
		else{
			return $link;
		}
	} 
	
    public function resize2($filename, $filename2, $width, $height) {
		if (!is_file(DIR_IMAGE . $filename)) {
			return;
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$old_image = $filename;
		$new_image = $filename2;

		if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
			$path = '';

			$directories = explode('/', dirname(str_replace('../', '', $new_image)));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}
			}

			list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

			if ($height == 'auto') $height = $width/($width_orig/ $height_orig);
			if ($width == 'auto') $width = $height/($height_orig/ $width_orig);                        
                        
			if ($width_orig > $width || $height_orig > $height) {
				$image = new Image(DIR_IMAGE . $old_image);
				$image->resize($width, $height);
				$image->save(DIR_IMAGE . $new_image);
			} else {
				copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
			}
		}

		if ($this->request->server['HTTPS']) {
			return $new_image;
		} else {
			return $new_image;
		}
	}	


	// Add Attribute
	//-------------------------------------------------------------------------
	private function addProductAttribute($group_name, $attribute_name, $attribute_filter = '') {
		$query = $this->db->query('SELECT attribute_group_id FROM `' . DB_PREFIX . 'attribute_group_description` WHERE LOWER(name) = LOWER(\'' .$this->db->escape($group_name) . '\') AND language_id = \'' . (int)1 . '\' LIMIT 1');

		if(isset($query->row['attribute_group_id'])) {
			$attribute_group_id = $query->row['attribute_group_id'];
		} else {
			$this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute_group` SET sort_order = 1');
			$attribute_group_id = $this->db->getLastId();
			$this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute_group_description` 
				SET attribute_group_id = '.(int)$attribute_group_id.',
				language_id = \'' . (int)1 . '\',
				name = \'' .$this->db->escape($group_name) . '\'
			');
		}
		
		$query = $this->db->query('SELECT ad.attribute_id FROM `' . DB_PREFIX . 'attribute_description` ad 
			LEFT JOIN `' . DB_PREFIX . 'attribute` a ON (ad.attribute_id = a.attribute_id)
			WHERE LOWER(ad.name) = LOWER(\'' .$this->db->escape($attribute_name) . '\') AND ad.language_id = \'' . (int)1 . '\' AND a.attribute_group_id = \'' . (int)$attribute_group_id . '\' LIMIT 1
			');
		
		if(isset($query->row['attribute_id'])) {
			$attribute_id = $query->row['attribute_id'];
		} else {
			$this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute` SET sort_order = 1, attribute_group_id = '. (int)$attribute_group_id);
			$attribute_id = $this->db->getLastId();
			$this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute_description`
				SET attribute_id = '.(int)$attribute_id.',
				language_id = \'' . (int)1 . '\',
				name = \'' .$this->db->escape($attribute_name) . '\'
			');
		}

		return $attribute_id;
	}

	public function checkDuplicate(){
		$sql = 'SELECT zoomos_id, product_id FROM ' . DB_PREFIX . 'product WHERE product_id<3501';
		$products = $this->db->query($sql);
		foreach($products->rows as $row){
			$sql = "SELECT zoomos_id, product_id FROM " . DB_PREFIX . "product WHERE zoomos_id='".$row['zoomos_id']."'";
			$prd = $this->db->query($sql);
			/*if($prd->num_rows >1){			
				echo $prd->num_rows.'; product_id='.$prd->row['product_id'].'- zoomos_id='.$prd->row['zoomos_id'].'<br />';
			}*/
			if($this->config->get('mfilter_plus_version')) {
				require_once DIR_SYSTEM . 'library/mfilter_plus.php';
				
				Mfilter_Plus::getInstance($this)->updateProduct($row['product_id']);
			}			
		}
		
	}
	
	public function getParentCategories($category_id, $categories = array()){
		if($category_id){
			$sql = "SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE category_id=$category_id";
			$category = $this->db->query($sql);

			$categories[] = $category->row['category_id'];
			
			if($category->row['parent_id']){
				$categories = $this->getParentCategories($category->row['parent_id'], $categories);
			}
		}
		
		return $categories;
		
	}

public function test_text(){
	echo '123';
}
	
	
}