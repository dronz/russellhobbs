<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<table class="table table-striped table-bordered">
  <tr>
    <td class="text-right rating-td">
    <div class="rating-author"><strong><?php echo $review['author']; ?></strong></div>
    <div class="rating">
      <?php for ($i = 1; $i <= 5; $i++) { ?>
      <?php if ($review['rating'] < $i) { ?>
      <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
      <?php } else { ?>
      <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
      <?php } ?>
      <?php } ?>
    </div>
    <div class="rating-date"><?php echo $review['date_added']; ?></div>
    </td>
    <td>
    <p class="rating-text subtitle"><?php echo $review['text']; ?></p>
    </td>
  </tr>
</table>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
