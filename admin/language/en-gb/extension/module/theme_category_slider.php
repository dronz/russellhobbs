<?php
// Heading
$_['heading_title']    = 'SELLMORE Theme - Category Slider';

// Text
$_['text_module']      = 'Modules';
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified SELLMORE Theme - Category Slider module!';
$_['text_edit']        = 'Edit SELLMORE Theme - Category Slider Module';
$_['text_yes']         = 'Yes';
$_['text_no']          = 'No';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_status']     = 'Status';
$_['entry_title']      = 'Title:'; 
$_['entry_subtitle']   = 'Subtitle';
$_['entry_categories_per_row'] = 'Categories per row:';
$_['entry_subcategories_status'] = 'Show subcategories:';
$_['entry_subcategories_per_column'] = 'Subcategories per column:';
$_['entry_view_more']   = '"View More" text:';
$_['entry_module_style'] = 'Module Style:';
$_['entry_module_title_position'] = 'Title position:';
$_['entry_module_title_width'] = 'Title width:';
$_['entry_module_items_width'] = 'Items width:';
$_['entry_module_title_color'] = 'Title color:';
$_['entry_module_subtitle_color'] = 'Subtitle color:';
$_['entry_module_bg_color'] = 'Title background color:';
$_['entry_module_image_thumb'] = 'Title background image:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify SELLMORE Theme - Category Slider module!';