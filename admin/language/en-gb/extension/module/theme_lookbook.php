<?php
// Heading
$_['heading_title']    = 'SELLMORE Theme - LookBook';

// Text
$_['text_module']      = 'Modules';
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified SELLMORE Theme - LookBook module!';
$_['text_edit']        = 'Edit SELLMORE Theme - LookBook Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_banner']     = 'Banner';
$_['entry_dimension']  = 'Dimension (W x H) and Resize Type';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
$_['entry_title']      = 'Title:'; 
$_['entry_subtitle']   = 'Subtitle';
$_['entry_banner_per_row'] = 'Images in a row:';
$_['entry_module_style'] = 'Module Style:';
$_['entry_module_title_position'] = 'Title position:';
$_['entry_module_title_width'] = 'Title width:';
$_['entry_module_items_width'] = 'Items width:';
$_['entry_module_title_color'] = 'Title color:';
$_['entry_module_subtitle_color'] = 'Subtitle color:';
$_['entry_module_bg_color'] = 'Title background color:';
$_['entry_module_image_thumb'] = 'Title background image:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify SELLMORE Theme - LookBook module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';