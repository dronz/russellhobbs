
<div class="row panel panel-default product-module product-module-theme-featured <?php echo $products_style; ?> full-width-container">
  <?php if(($featured['title']) !='') { ?>
  <div class="panel-heading module-heading"><h2><?php echo $featured['title']; ?></h2></div>
  <?php } ?>
  <div class="row product-items-wrapper">
  <div class="product-items product-items-theme-featured product-items-0">
  <?php foreach ($products as $product) { ?>
  <div class="product-grid product-box-style-1 col-xs-12 col-sm-12 <?php if(($products_style) =='featured-product-view-1') { ?>col-md-12<?php } else { ?>col-md-6<?php } ?>">
    <div class="product-thumb transition">
      <div class="image col-xs-6">       
            <?php if (($product['out_of_stock_quantity'] <= 0) && ($t1o_out_of_stock_badge_status ==1)) { ?>
            <span class="badge out-of-stock"><span><?php echo $product['out_of_stock_badge']; ?></span></span>
            <?php } ?> 
            <span class="badge-wrapper">
            <?php if($t1o_sale_badge_status ==1) { ?>	
            <?php if (($product['special'])&&($t1o_sale_badge_type == 0)) { ?>
            <span class="badge sale"><?php echo $t1o_text_sale[$lang_id]; ?></span>
            <?php } ?> 
            <?php if (($product['special'])&&($t1o_sale_badge_type == 1)) { ?>
            <?php 
            $val1 = preg_replace("/[^0-9.]/", "", $product['special']);
	        $val2 = preg_replace("/[^0-9.]/", "", $product['price']);
            ?>
            <?php
            $res = ($val1 / $val2) * 100;
            $res = 100 - $res;
            $res = round($res, 0);
            ?>
            <span class="badge sale">-<?php echo $res; ?>%</span>
            <?php } ?> 
            <?php } ?> 
      
            <?php if($t1o_new_badge_status ==1) { ?>	
            <?php
            $startDate1 = strtotime(mb_substr($product['newstart'], 0, 10));
            $endDate2 = strtotime(date("Y-m-d"));
            $days = ceil(($endDate2 / 86400)) - ceil(($startDate1 /86400));
            ?>
            <?php $newproductdays = 30; ?>
            <?php if ($days < $newproductdays) { ?>
            <span class="badge new"><?php echo $t1o_text_new_prod[$lang_id]; ?></span>
            <?php } ?>
            <?php } ?>
            </span>

            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-<?php echo $t1d_img_style; ?>" />
            </a>
      </div>
      
      <div class="thumb-image col-xs-6">
            <?php if ($product['thumb_swap']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive thumb_swap img-<?php echo $t1d_img_style; ?>" />
            </a>
            <?php } ?>
            
            <div class="flybar">  
            <div class="flybar-items">
            <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary"><i class="fa fa-shopping-bag"></i></button>
            <a class="btn btn-default quickview" href="<?php echo $product['quickview']; ?>" data-toggle="tooltip" title="<?php echo $t1o_text_quickview[$lang_id]; ?>"><i class="fa fa-search"></i></a>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="wishlist"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare"><i class="fa fa-retweet"></i></button>
            </div>   
            </div>
      </div>
            
      <div class="caption col-xs-6">
                <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></div>
                <div class="product_box_brand"><?php if ($product['brand']) { ?><a href="<?php echo $product['brand_url']; ?>"><?php echo $product['brand']; ?></a><?php } ?></div>
                <p class="description"><?php echo $product['description']; ?></p>

                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>   
      </div>
    </div>
  </div>
  <?php } ?>
</div>

</div>
</div>
