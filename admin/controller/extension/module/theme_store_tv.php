<?php
class ControllerExtensionModuleThemeStoreTV extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/theme_store_tv');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$languages = $this->model_localisation_language->getLanguages();
		
		$this->load->model('extension/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('theme_store_tv', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_banner'] = $this->language->get('entry_banner');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_subtitle'] = $this->language->get('entry_subtitle');
		$data['entry_button'] = $this->language->get('entry_button');
		$data['entry_button_url'] = $this->language->get('entry_button_url');
		$data['entry_banner_per_row'] = $this->language->get('entry_banner_per_row');
		$data['entry_module_style'] = $this->language->get('entry_module_style');
		$data['entry_module_title_position'] = $this->language->get('entry_module_title_position');
		$data['entry_module_title_width'] = $this->language->get('entry_module_title_width');
		$data['entry_module_items_width'] = $this->language->get('entry_module_items_width');
		$data['entry_module_title_color'] = $this->language->get('entry_module_title_color');
		$data['entry_module_subtitle_color'] = $this->language->get('entry_module_subtitle_color');
		$data['entry_module_bg_color'] = $this->language->get('entry_module_bg_color');
		$data['entry_module_image_thumb'] = $this->language->get('entry_module_image_thumb');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
			
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
		
		if (isset($this->error['width'])) {
			$data['error_width'] = $this->error['width'];
		} else {
			$data['error_width'] = '';
		}
		
		if (isset($this->error['height'])) {
			$data['error_height'] = $this->error['height'];
		} else {
			$data['error_height'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/theme_store_tv', 'token=' . $this->session->data['token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/theme_store_tv', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true)
			);			
		}
		
		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/theme_store_tv', 'token=' . $this->session->data['token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/theme_store_tv', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true);
		}
		
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
		
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}
		
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}
				
		if (isset($this->request->post['banner_id'])) {
			$data['banner_id'] = $this->request->post['banner_id'];
		} elseif (!empty($module_info)) {
			$data['banner_id'] = $module_info['banner_id'];
		} else {
			$data['banner_id'] = '';
		}
		
		$this->load->model('design/banner');

		$data['banners'] = $this->model_design_banner->getBanners();
		
		if (isset($this->request->post['store_tv'])) {
			$data['store_tv'] = $this->request->post['store_tv'];
		} elseif (!empty($module_info)) {
			$data['store_tv'] = $module_info['store_tv'];
		} else {
			$data['store_tv'] = '';
		}	
		
		if (isset($this->request->post['width'])) {
			$data['width'] = $this->request->post['width'];
		} elseif (!empty($module_info)) {
			$data['width'] = $module_info['width'];
		} else {
			$data['width'] = '480';
		}	
			
		if (isset($this->request->post['height'])) {
			$data['height'] = $this->request->post['height'];
		} elseif (!empty($module_info)) {
			$data['height'] = $module_info['height'];
		} else {
			$data['height'] = '270';
		}		
		
		if (isset($this->request->post['pr_id'])) {
			$data['pr_id'] = $this->request->post['pr_id'];
		} elseif (!empty($module_info)) {
			$data['pr_id'] = $module_info['pr_id'];
		} else {
			$data['pr_id'] = '3';
		}
		
		if (isset($this->request->post['module_style'])) {
			$data['module_style'] = $this->request->post['module_style'];
		} elseif (!empty($module_info)) {
			$data['module_style'] = $module_info['module_style'];
		} else {
			$data['module_style'] = 'module-style-2';
		}
		
		if (isset($this->request->post['module_title_position'])) {
			$data['module_title_position'] = $this->request->post['module_title_position'];
		} elseif (!empty($module_info)) {
			$data['module_title_position'] = $module_info['module_title_position'];
		} else {
			$data['module_title_position'] = 'left';
		}
		
		if (isset($this->request->post['module_title_width'])) {
			$data['module_title_width'] = $this->request->post['module_title_width'];
		} elseif (!empty($module_info)) {
			$data['module_title_width'] = $module_info['module_title_width'];
		} else {
			$data['module_title_width'] = '4';
		}
		
		if (isset($this->request->post['module_items_width'])) {
			$data['module_items_width'] = $this->request->post['module_items_width'];
		} elseif (!empty($module_info)) {
			$data['module_items_width'] = $module_info['module_items_width'];
		} else {
			$data['module_items_width'] = '8';
		}
		
		if (isset($this->request->post['module_title_color'])) {
			$data['module_title_color'] = $this->request->post['module_title_color'];
		} elseif (!empty($module_info)) {
			$data['module_title_color'] = $module_info['module_title_color'];
		} else {
			$data['module_title_color'] = '';
		}
		
		if (isset($this->request->post['module_subtitle_color'])) {
			$data['module_subtitle_color'] = $this->request->post['module_subtitle_color'];
		} elseif (!empty($module_info)) {
			$data['module_subtitle_color'] = $module_info['module_subtitle_color'];
		} else {
			$data['module_subtitle_color'] = '';
		}
		
		if (isset($this->request->post['module_bg_color'])) {
			$data['module_bg_color'] = $this->request->post['module_bg_color'];
		} elseif (!empty($module_info)) {
			$data['module_bg_color'] = $module_info['module_bg_color'];
		} else {
			$data['module_bg_color'] = '';
		}
		
		if (isset($this->request->post['module_image_custom'])) {
			$data['module_image_custom'] = $this->request->post['module_image_custom'];
		} elseif (!empty($module_info)) {
			$data['module_image_custom'] = $module_info['module_image_custom'];
		} else {
			$data['module_image_custom'] = '';
		}
		
		$this->load->model('tool/image');
		
		if (isset($this->request->post['module_image_custom']) && is_file(DIR_IMAGE . $this->request->post['module_image_custom'])) {
			$data['module_image_thumb'] = $this->model_tool_image->resize($this->request->post['module_image_custom'], 100, 100);
		} elseif (!empty($module_info) && is_file(DIR_IMAGE . $module_info['module_image_custom'])) {
			$data['module_image_thumb'] = $this->model_tool_image->resize($module_info['module_image_custom'], 100, 100);
		} else {
			$data['module_image_thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/theme_store_tv.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/theme_store_tv')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}
				
		if (!$this->request->post['width']) {
			$this->error['width'] = $this->language->get('error_width');
		}
		
		if (!$this->request->post['height']) {
			$this->error['height'] = $this->language->get('error_height');
		}	

		return !$this->error;
	}
}