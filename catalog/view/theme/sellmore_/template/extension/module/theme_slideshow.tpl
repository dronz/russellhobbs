
<style type="text/css">

<?php if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
} ?>

<?php if($slideshow_hover_bg_color_status =='1') { ?>
#theme-slideshow<?php echo $module; ?> .theme-slideshow-bg {
	background-color: <?php echo $slideshow_hover_bg_color; ?>;
}
#theme-slideshow<?php echo $module; ?> .theme-slideshow-item:hover .theme-slideshow-bg {
	opacity: <?php echo $slideshow_hover_bg_color_opacity; ?>;
}
<?php } ?>

#theme-slideshow<?php echo $module; ?> .active .slideshow_1_main_image {
    animation: 0.7s <?php echo $slideshow_item['main_image_animation_1']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_1_title {
    animation: 0.7s <?php echo $slideshow_item['title_animation_1']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_1_subtitle {
    animation: 0.7s <?php echo $slideshow_item['subtitle_animation_1']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_1_button {
    animation: 0.7s <?php echo $slideshow_item['button_animation_1']; ?>;
}

<?php if($slideshow_item['status_2'] =='1') { ?>
#theme-slideshow<?php echo $module; ?> .active .slideshow_2_main_image {
    animation: 0.7s <?php echo $slideshow_item['main_image_animation_2']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_2_title {
    animation: 0.7s <?php echo $slideshow_item['title_animation_2']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_2_subtitle {
    animation: 0.7s <?php echo $slideshow_item['subtitle_animation_2']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_2_button {
    animation: 0.7s <?php echo $slideshow_item['button_animation_2']; ?>;
}
<?php } ?>
<?php if($slideshow_item['status_3'] =='1') { ?>
#theme-slideshow<?php echo $module; ?> .active .slideshow_3_main_image {
    animation: 0.7s <?php echo $slideshow_item['main_image_animation_3']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_3_title {
    animation: 0.7s <?php echo $slideshow_item['title_animation_3']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_3_subtitle {
    animation: 0.7s <?php echo $slideshow_item['subtitle_animation_3']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_3_button {
    animation: 0.7s <?php echo $slideshow_item['button_animation_3']; ?>;
}
<?php } ?>
<?php if($slideshow_item['status_4'] =='1') { ?>
#theme-slideshow<?php echo $module; ?> .active .slideshow_4_main_image {
    animation: 0.7s <?php echo $slideshow_item['main_image_animation_4']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_4_title {
    animation: 0.7s <?php echo $slideshow_item['title_animation_4']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_4_subtitle {
    animation: 0.7s <?php echo $slideshow_item['subtitle_animation_4']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_4_button {
    animation: 0.7s <?php echo $slideshow_item['button_animation_4']; ?>;
}
<?php } ?>
<?php if($slideshow_item['status_5'] =='1') { ?>
#theme-slideshow<?php echo $module; ?> .active .slideshow_5_main_image {
    animation: 0.7s <?php echo $slideshow_item['main_image_animation_5']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_5_title {
    animation: 0.7s <?php echo $slideshow_item['title_animation_5']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_5_subtitle {
    animation: 0.7s <?php echo $slideshow_item['subtitle_animation_5']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_5_button {
    animation: 0.7s <?php echo $slideshow_item['button_animation_5']; ?>;
}
<?php } ?>
<?php if($slideshow_item['status_6'] =='1') { ?>
#theme-slideshow<?php echo $module; ?> .active .slideshow_6_main_image {
    animation: 0.7s <?php echo $slideshow_item['main_image_animation_6']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_6_title {
    animation: 0.7s <?php echo $slideshow_item['title_animation_6']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_6_subtitle {
    animation: 0.7s <?php echo $slideshow_item['subtitle_animation_6']; ?>;
}
#theme-slideshow<?php echo $module; ?> .active .slideshow_6_button {
    animation: 0.7s <?php echo $slideshow_item['button_animation_6']; ?>;
}
<?php } ?>

@keyframes fadeInLeft {
  from {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
  to {
    -webkit-transform: none;
    transform: none;
  }
    0%   {opacity: 0;}
    25%  {opacity: 0;}
    50%  {opacity: 0;}
	75%  {opacity: 0;}
    100% {opacity: 1;}
}
@keyframes fadeInRight {
  from {
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
  }
  to {
    -webkit-transform: none;
    transform: none;
  }
    0%   {opacity: 0;}
    25%  {opacity: 0;}
    50%  {opacity: 0;}
	75%  {opacity: 0;}
    100% {opacity: 1;}
}

<?php if($slideshow_progress_bar =='1') { ?>
#bar {
  width: 0%;
  max-width: 100%;
  height: 4px;
}
#progressBar {
  width: 100%;
  background-color: transparent;
}
<?php } ?>

<?php if($slideshow_per_row =='2') { ?>
.theme-slideshow-content-wrapper {
	padding: 45px;
}
h2.theme-slideshow-title {
	font-size: 41px;
	margin: 0 0 10px 0;
}
.theme-slideshow-subtitle {
	font-size: 16px !important;
	line-height: 25px;
}
.theme-slideshow-button.btn {
	margin-top: 25px;
	padding: 10px 26px;
}
<?php } ?>

</style>

<div id="theme-slideshow<?php echo $module; ?>" class="panel panel-default theme-slideshow theme-slideshow-module">

<div class="theme-slideshow-wrapper<?php echo $module; ?>">

<?php if($slideshow_item['status_1'] =='1') { ?>
<div class="col-sm-12 theme-slideshow-item <?php echo $slideshow_item['content_position_1']; ?> effect-<?php echo $slideshow_item['hover_effect_1']; ?>">
  <div class="theme-slideshow-item-wrapper">
  <a href="<?php echo $slideshow_item[$lang_id]['url_1']; ?>">
  <img src="<?php echo $path_image . $slideshow_item_image_custom_1 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_1']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_1']; ?>" class="theme-slideshow-item-bg" />
  <span class="theme-slideshow-bg"></span>
  <?php if(($slideshow_item_main_image_custom_1) !='') { ?>
  <span class="theme-slideshow-main-image slideshow_1_main_image">
  <img src="<?php echo $path_image . $slideshow_item_main_image_custom_1 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_1']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_1']; ?>" />
  </span>
  <?php } ?>
  <div class="theme-slideshow-content-wrapper">
    <div class="theme-slideshow-content-table">
      <div class="theme-slideshow-content-table-cell">
        <?php if(($slideshow_item[$lang_id]['label_1']) !='') { ?>
        <span class="btn btn-default theme-slideshow-label" style="background-color: <?php echo $slideshow_label_color_1; ?>;"><?php echo $slideshow_item[$lang_id]['label_1']; ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['title_1']) !='') { ?>
        <h2 class="theme-slideshow-title slideshow_1_title" style="color: <?php echo $slideshow_title_color_1; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['title_1'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['subtitle_1']) !='') { ?>
        <span class="theme-slideshow-subtitle subtitle slideshow_1_subtitle" style="color: <?php echo $slideshow_subtitle_color_1; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['subtitle_1'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['button_1']) !='') { ?>
        <span class="btn btn-<?php echo $slideshow_item['button_style_1']; ?> theme-slideshow-button slideshow_1_button"><?php echo $slideshow_item[$lang_id]['button_1']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($slideshow_item['status_2'] =='1') { ?>
<div class="col-sm-12 theme-slideshow-item <?php echo $slideshow_item['content_position_2']; ?> effect-<?php echo $slideshow_item['hover_effect_2']; ?>">
  <div class="theme-slideshow-item-wrapper">
  <a href="<?php echo $slideshow_item[$lang_id]['url_2']; ?>">
  <img src="<?php echo $path_image . $slideshow_item_image_custom_2 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_2']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_2']; ?>" class="theme-slideshow-item-bg" />
  <span class="theme-slideshow-bg"></span>
  <?php if(($slideshow_item_main_image_custom_2) !='') { ?>
  <span class="theme-slideshow-main-image slideshow_2_main_image">
  <img src="<?php echo $path_image . $slideshow_item_main_image_custom_2 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_2']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_2']; ?>" />
  </span>
  <?php } ?>
  <div class="theme-slideshow-content-wrapper">
    <div class="theme-slideshow-content-table">
      <div class="theme-slideshow-content-table-cell">
        <?php if(($slideshow_item[$lang_id]['label_2']) !='') { ?>
        <span class="btn btn-default theme-slideshow-label" style="background-color: <?php echo $slideshow_label_color_2; ?>;"><?php echo $slideshow_item[$lang_id]['label_2']; ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['title_2']) !='') { ?>
        <h2 class="theme-slideshow-title slideshow_2_title" style="color: <?php echo $slideshow_title_color_2; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['title_2'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['subtitle_2']) !='') { ?>
        <span class="theme-slideshow-subtitle subtitle slideshow_2_subtitle" style="color: <?php echo $slideshow_subtitle_color_2; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['subtitle_2'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['button_2']) !='') { ?>
        <span class="btn btn-<?php echo $slideshow_item['button_style_2']; ?> theme-slideshow-button slideshow_2_button"><?php echo $slideshow_item[$lang_id]['button_2']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($slideshow_item['status_3'] =='1') { ?>
<div class="col-sm-12 theme-slideshow-item <?php echo $slideshow_item['content_position_3']; ?> effect-<?php echo $slideshow_item['hover_effect_3']; ?>">
  <div class="theme-slideshow-item-wrapper">
  <a href="<?php echo $slideshow_item[$lang_id]['url_3']; ?>">
  <img src="<?php echo $path_image . $slideshow_item_image_custom_3 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_3']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_3']; ?>" class="theme-slideshow-item-bg" />
  <span class="theme-slideshow-bg"></span>
  <?php if(($slideshow_item_main_image_custom_3) !='') { ?>
  <span class="theme-slideshow-main-image slideshow_3_main_image">
  <img src="<?php echo $path_image . $slideshow_item_main_image_custom_3 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_3']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_3']; ?>" />
  </span>
  <?php } ?>
  <div class="theme-slideshow-content-wrapper">
    <div class="theme-slideshow-content-table">
      <div class="theme-slideshow-content-table-cell">
        <?php if(($slideshow_item[$lang_id]['label_3']) !='') { ?>
        <span class="btn btn-default theme-slideshow-label" style="background-color: <?php echo $slideshow_label_color_3; ?>;"><?php echo $slideshow_item[$lang_id]['label_3']; ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['title_3']) !='') { ?>
        <h2 class="theme-slideshow-title slideshow_3_title" style="color: <?php echo $slideshow_title_color_3; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['title_3'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['subtitle_3']) !='') { ?>
        <span class="theme-slideshow-subtitle subtitle slideshow_3_subtitle" style="color: <?php echo $slideshow_subtitle_color_3; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['subtitle_3'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['button_3']) !='') { ?>
        <span class="btn btn-<?php echo $slideshow_item['button_style_3']; ?> theme-slideshow-button slideshow_3_button"><?php echo $slideshow_item[$lang_id]['button_3']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($slideshow_item['status_4'] =='1') { ?>
<div class="col-sm-12 theme-slideshow-item <?php echo $slideshow_item['content_position_4']; ?> effect-<?php echo $slideshow_item['hover_effect_4']; ?>">
  <div class="theme-slideshow-item-wrapper">
  <a href="<?php echo $slideshow_item[$lang_id]['url_4']; ?>">
  <img src="<?php echo $path_image . $slideshow_item_image_custom_4 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_4']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_4']; ?>" class="theme-slideshow-item-bg" />
  <span class="theme-slideshow-bg"></span>
  <?php if(($slideshow_item_main_image_custom_4) !='') { ?>
  <span class="theme-slideshow-main-image slideshow_4_main_image">
  <img src="<?php echo $path_image . $slideshow_item_main_image_custom_4 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_4']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_4']; ?>" />
  </span>
  <?php } ?>
  <div class="theme-slideshow-content-wrapper">
    <div class="theme-slideshow-content-table">
      <div class="theme-slideshow-content-table-cell">
        <?php if(($slideshow_item[$lang_id]['label_4']) !='') { ?>
        <span class="btn btn-default theme-slideshow-label" style="background-color: <?php echo $slideshow_label_color_4; ?>;"><?php echo $slideshow_item[$lang_id]['label_4']; ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['title_4']) !='') { ?>
        <h2 class="theme-slideshow-title slideshow_4_title" style="color: <?php echo $slideshow_title_color_4; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['title_4'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['subtitle_4']) !='') { ?>
        <span class="theme-slideshow-subtitle subtitle slideshow_4_subtitle" style="color: <?php echo $slideshow_subtitle_color_4; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['subtitle_4'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['button_4']) !='') { ?>
        <span class="btn btn-<?php echo $slideshow_item['button_style_4']; ?> theme-slideshow-button slideshow_4_button"><?php echo $slideshow_item[$lang_id]['button_4']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($slideshow_item['status_5'] =='1') { ?>
<div class="col-sm-12 theme-slideshow-item <?php echo $slideshow_item['content_position_5']; ?> effect-<?php echo $slideshow_item['hover_effect_5']; ?>">
  <div class="theme-slideshow-item-wrapper">
  <a href="<?php echo $slideshow_item[$lang_id]['url_5']; ?>">
  <img src="<?php echo $path_image . $slideshow_item_image_custom_5 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_5']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_5']; ?>" class="theme-slideshow-item-bg" />
  <span class="theme-slideshow-bg"></span>
  <?php if(($slideshow_item_main_image_custom_5) !='') { ?>
  <span class="theme-slideshow-main-image slideshow_5_main_image">
  <img src="<?php echo $path_image . $slideshow_item_main_image_custom_5 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_5']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_5']; ?>" />
  </span>
  <?php } ?>
  <div class="theme-slideshow-content-wrapper">
    <div class="theme-slideshow-content-table">
      <div class="theme-slideshow-content-table-cell">
        <?php if(($slideshow_item[$lang_id]['label_5']) !='') { ?>
        <span class="btn btn-default theme-slideshow-label" style="background-color: <?php echo $slideshow_label_color_5; ?>;"><?php echo $slideshow_item[$lang_id]['label_5']; ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['title_5']) !='') { ?>
        <h2 class="theme-slideshow-title slideshow_5_title" style="color: <?php echo $slideshow_title_color_5; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['title_5'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['subtitle_5']) !='') { ?>
        <span class="theme-slideshow-subtitle subtitle slideshow_5_subtitle" style="color: <?php echo $slideshow_subtitle_color_5; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['subtitle_5'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['button_5']) !='') { ?>
        <span class="btn btn-<?php echo $slideshow_item['button_style_5']; ?> theme-slideshow-button slideshow_5_button"><?php echo $slideshow_item[$lang_id]['button_5']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

<?php if($slideshow_item['status_6'] =='1') { ?>
<div class="col-sm-12 theme-slideshow-item <?php echo $slideshow_item['content_position_6']; ?> effect-<?php echo $slideshow_item['hover_effect_6']; ?>">
  <div class="theme-slideshow-item-wrapper">
  <a href="<?php echo $slideshow_item[$lang_id]['url_6']; ?>">
  <img src="<?php echo $path_image . $slideshow_item_image_custom_6 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_6']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_6']; ?>" class="theme-slideshow-item-bg" />
  <span class="theme-slideshow-bg"></span>
  <?php if(($slideshow_item_main_image_custom_6) !='') { ?>
  <span class="theme-slideshow-main-image slideshow_6_main_image">
  <img src="<?php echo $path_image . $slideshow_item_main_image_custom_6 ?>" alt="<?php echo $slideshow_item[$lang_id]['title_6']; ?>" title="<?php echo $slideshow_item[$lang_id]['title_6']; ?>" />
  </span>
  <?php } ?>
  <div class="theme-slideshow-content-wrapper">
    <div class="theme-slideshow-content-table">
      <div class="theme-slideshow-content-table-cell">
        <?php if(($slideshow_item[$lang_id]['label_6']) !='') { ?>
        <span class="btn btn-default theme-slideshow-label" style="background-color: <?php echo $slideshow_label_color_6; ?>;"><?php echo $slideshow_item[$lang_id]['label_6']; ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['title_6']) !='') { ?>
        <h2 class="theme-slideshow-title slideshow_6_title" style="color: <?php echo $slideshow_title_color_6; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['title_6'], ENT_QUOTES ); ?></h2>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['subtitle_6']) !='') { ?>
        <span class="theme-slideshow-subtitle subtitle slideshow_6_subtitle" style="color: <?php echo $slideshow_subtitle_color_6; ?>;"><?php echo htmlspecialchars_decode( $slideshow_item[$lang_id]['subtitle_6'], ENT_QUOTES ); ?></span>
        <?php } ?>
        <?php if(($slideshow_item[$lang_id]['button_6']) !='') { ?>
        <span class="btn btn-<?php echo $slideshow_item['button_style_6']; ?> theme-slideshow-button slideshow_6_button"><?php echo $slideshow_item[$lang_id]['button_6']; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
  </a>
  </div>
</div>
<?php } ?>

</div>

</div>




<script type="text/javascript"><!--


    $(document).ready(function() {
     
      var time = <?php echo $slideshow_time; ?>; // time in seconds
     
      var $progressBar,
          $bar, 
          $elem, 
          isPause, 
          tick,
          percentTime;
     
        //Init the carousel
        $(".theme-slideshow-wrapper<?php echo $module; ?>").owlCarousel({
          items: <?php echo $slideshow_per_row; ?>,
	      itemsDesktop: [1199,<?php echo $slideshow_per_row; ?>],
          itemsDesktopSmall: [980,1],
          itemsTablet: [768,1],
          itemsTabletSmall: false,
          itemsMobile: [479,1],
	      singleItem: false,
	      scrollPerPage: true,
	      pagination: false,
	      navigation: true,
	      navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	      transitionStyle: "fade",
	      addClassActive: true,
          afterInit: progressBar,
          afterMove: moved,
          startDragging: pauseOnDragging
        });
     
        //Init progressBar where elem is $("#owl-demo")
        function progressBar(elem){
          $elem = elem;
          //build progress bar elements
          buildProgressBar();
          //start counting
          start();
        }
     
        //create div#progressBar and div#bar then prepend to $("#owl-demo")
        function buildProgressBar(){
          $progressBar = $("<div>",{
            id:"progressBar"
          });
          $bar = $("<div>",{
            id:"bar"
          });
          $progressBar.append($bar).prependTo($elem);
        }
     
        function start() {
          //reset timer
          percentTime = 0;
          isPause = false;
          //run interval every 0.01 second
          tick = setInterval(interval, 10);
        };
     
        function interval() {
          if(isPause === false){
            percentTime += 1 / time;
            $bar.css({
               width: percentTime+"%"
             });
            //if percentTime is equal or greater than 100
            if(percentTime >= 100){
              //slide to next item 
              $elem.trigger('owl.next')
            }
          }
        }
     
        //pause while dragging 
        function pauseOnDragging(){
          isPause = true;
        }
     
        //moved callback
        function moved(){
          //clear interval
          clearTimeout(tick);
          //start again
          start();
        }
     
        //uncomment this to make pause on mouseover 
        // $elem.on('mouseover',function(){
        //   isPause = true;
        // })
        // $elem.on('mouseout',function(){
        //   isPause = false;
        // })
     
    });

--></script>



