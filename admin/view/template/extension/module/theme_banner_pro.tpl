<?php 
if(empty($banner_pro_bg_color)) $banner_pro_bg_color ="F2F2F2";
if(empty($banner_pro_hover_bg_color)) $banner_pro_hover_bg_color ="000000";
if(empty($banner_pro_label_color_1)) $banner_pro_label_color_1 ="16B778";
if(empty($banner_pro_title_color_1)) $banner_pro_title_color_1 ="222222";
if(empty($banner_pro_subtitle_color_1)) $banner_pro_subtitle_color_1 ="AAAAAA";
if(empty($banner_pro_label_color_2)) $banner_pro_label_color_2 ="16B778";
if(empty($banner_pro_title_color_2)) $banner_pro_title_color_2 ="222222";
if(empty($banner_pro_subtitle_color_2)) $banner_pro_subtitle_color_2 ="AAAAAA";
if(empty($banner_pro_label_color_3)) $banner_pro_label_color_3 ="16B778";
if(empty($banner_pro_title_color_3)) $banner_pro_title_color_3 ="222222";
if(empty($banner_pro_subtitle_color_3)) $banner_pro_subtitle_color_3 ="AAAAAA";
if(empty($banner_pro_label_color_4)) $banner_pro_label_color_4 ="16B778";
if(empty($banner_pro_title_color_4)) $banner_pro_title_color_4 ="222222";
if(empty($banner_pro_subtitle_color_4)) $banner_pro_subtitle_color_4 ="AAAAAA";
if(empty($banner_pro_label_color_5)) $banner_pro_label_color_5 ="16B778";
if(empty($banner_pro_title_color_5)) $banner_pro_title_color_5 ="222222";
if(empty($banner_pro_subtitle_color_5)) $banner_pro_subtitle_color_5 ="AAAAAA";
if(empty($banner_pro_label_color_6)) $banner_pro_label_color_6 ="16B778";
if(empty($banner_pro_title_color_6)) $banner_pro_title_color_6 ="222222";
if(empty($banner_pro_subtitle_color_6)) $banner_pro_subtitle_color_6 ="AAAAAA";
?>

<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-banner-pro" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner-pro" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>       
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_pro_bg_color"><?php echo $entry_banner_pro_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="banner_pro_bg_color" id="banner_pro_bg_color" value="<?php echo $banner_pro_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_pro_image_thumb"><?php echo $entry_banner_pro_image_thumb; ?></label>
            <div class="col-sm-10">
              <a href="" id="banner_pro_image_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $banner_pro_image_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="banner_pro_image_custom" value="<?php echo $banner_pro_image_custom; ?>" id="input-banner_pro_image_custom" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_pro_title_shadow"><?php echo $entry_banner_pro_title_shadow; ?></label>
            <div class="col-sm-10">
              <select name="banner_pro_title_shadow" id="input-banner_pro_title_shadow" class="form-control">
                <option value="0" <?php if ($banner_pro_title_shadow == '0') {echo "selected=\"selected\"";}; ?>>No</option>
                <option value="1" <?php if ($banner_pro_title_shadow == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_pro_hover_bg_color_status"><?php echo $entry_banner_pro_hover_bg_color_status; ?></label>
            <div class="col-sm-10">
              <select name="banner_pro_hover_bg_color_status" id="input-banner_pro_hover_bg_color_status" class="form-control">
                <option value="1" <?php if ($banner_pro_hover_bg_color_status == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                <option value="0" <?php if ($banner_pro_hover_bg_color_status == '0') {echo "selected=\"selected\"";}; ?>>No</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_pro_hover_bg_color"><?php echo $entry_banner_pro_hover_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="banner_pro_hover_bg_color" id="banner_pro_hover_bg_color" value="<?php echo $banner_pro_hover_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_pro_hover_bg_color_opacity"><?php echo $entry_banner_pro_hover_bg_color_opacity; ?></label>
            <div class="col-sm-10">
              <select name="banner_pro_hover_bg_color_opacity" id="input-banner_pro_hover_bg_color_opacity" class="form-control">
                <option value="0" <?php if ($banner_pro_hover_bg_color_opacity == '0') {echo "selected=\"selected\"";}; ?>>0</option>
                <option value="0.1" <?php if ($banner_pro_hover_bg_color_opacity == '0.1') {echo "selected=\"selected\"";}; ?>>0.1</option>
                <option value="0.2" <?php if ($banner_pro_hover_bg_color_opacity == '0.2') {echo "selected=\"selected\"";}; ?>>0.2</option>
                <option value="0.3" <?php if ($banner_pro_hover_bg_color_opacity == '0.3') {echo "selected=\"selected\"";}; ?>>0.3</option>
                <option value="0.4" <?php if ($banner_pro_hover_bg_color_opacity == '0.4') {echo "selected=\"selected\"";}; ?>>0.4</option>
                <option value="0.5" <?php if ($banner_pro_hover_bg_color_opacity == '0.5') {echo "selected=\"selected\"";}; ?>>0.5</option>
                <option value="0.6" <?php if ($banner_pro_hover_bg_color_opacity == '0.6') {echo "selected=\"selected\"";}; ?>>0.6</option>
                <option value="0.7" <?php if ($banner_pro_hover_bg_color_opacity == '0.7') {echo "selected=\"selected\"";}; ?>>0.7</option>
                <option value="0.8" <?php if ($banner_pro_hover_bg_color_opacity == '0.8') {echo "selected=\"selected\"";}; ?>>0.8</option>
                <option value="0.9" <?php if ($banner_pro_hover_bg_color_opacity == '0.9') {echo "selected=\"selected\"";}; ?>>0.9</option>
                <option value="1" <?php if ($banner_pro_hover_bg_color_opacity == '1') {echo "selected=\"selected\"";}; ?>>1</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner_pro_padding"><?php echo $entry_banner_pro_padding; ?></label>
            <div class="col-sm-10">
              <select name="banner_pro_padding" id="input-banner_pro_padding" class="form-control">
                <option value="0" <?php if ($banner_pro_padding == '0') {echo "selected=\"selected\"";}; ?>>0px</option>
                <option value="30" <?php if ($banner_pro_padding == '30') {echo "selected=\"selected\"";}; ?>>30px</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <br /><br />
          <legend>Banners</legend>
         <div class="row">
         <div class="col-sm-12">
         <table id="banner_pro_items" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left">Show</td>
                <td class="text-left"><?php echo $entry_banner_image; ?></td>
                <td class="text-left" width="15%"><?php echo $entry_banner_pro_url; ?></td>
                <td class="text-left" width="15%"><?php echo $entry_banner_pro_label; ?></td>
                <td class="text-left"><?php echo $entry_banner_pro_title; ?></td>
                <td class="text-left"><?php echo $entry_banner_pro_subtitle; ?></td>
                <td class="text-left" width="15%"><?php echo $entry_banner_pro_button; ?></td>
                <td class="text-left" width="10%"></td>
              </tr>
            </thead>
            <tbody>
              
              
              <tr>

                <td class="text-left">
                <select name="banner_pro_item[status_1]" id="input-status_1" class="form-control">
                  <option value="0" <?php if ($banner_pro_item['status_1'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($banner_pro_item['status_1'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="banner_pro_item_image_thumb_1" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $banner_pro_item_image_thumb_1; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="banner_pro_item_image_custom_1" value="<?php echo $banner_pro_item_image_custom_1; ?>" id="input-banner_pro_item_image_custom_1" />
                <br />
                <label class="control-label" for="input-width_1"><?php echo $entry_banner_pro_width; ?></label>
                <select name="banner_pro_item[width_1]" id="input-width_1" class="form-control">
                <?php for ($k=1; $k<=12; $k++) { ?>
                  <option value="<?php echo $k; ?>" <?php if ($banner_pro_item['width_1']==$k) {echo "selected=\"selected\"";}; ?>><?php echo $k; ?>/12</option>
                <?php }; ?>
                </select>
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][url_1]" id="url_1-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['url_1']) ? $banner_pro_item[$language['language_id']]['url_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][label_1]" id="label_1-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['label_1']) ? $banner_pro_item[$language['language_id']]['label_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_1"><?php echo $entry_banner_pro_label_color; ?></label>
                <input type="text" name="banner_pro_label_color_1" id="banner_pro_label_color_1" value="<?php echo $banner_pro_label_color_1; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][title_1]" id="title_1-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['title_1']) ? $banner_pro_item[$language['language_id']]['title_1'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_1"><?php echo $entry_banner_pro_title_color; ?></label>
                <input type="text" name="banner_pro_title_color_1" id="banner_pro_title_color_1" value="<?php echo $banner_pro_title_color_1; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][subtitle_1]" id="subtitle_1-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['subtitle_1']) ? $banner_pro_item[$language['language_id']]['subtitle_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_1"><?php echo $entry_banner_pro_subtitle_color; ?></label>
                <input type="text" name="banner_pro_subtitle_color_1" id="banner_pro_subtitle_color_1" value="<?php echo $banner_pro_subtitle_color_1; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][button_1]" id="button_1-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['button_1']) ? $banner_pro_item[$language['language_id']]['button_1'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_1"><?php echo $entry_banner_pro_button_style; ?></label>
                <select name="banner_pro_item[button_style_1]" id="input-button_style_1" class="form-control">
                  <option value="primary" <?php if ($banner_pro_item['button_style_1'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($banner_pro_item['button_style_1'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_1"><?php echo $entry_banner_pro_content_position; ?></label>
                <select name="banner_pro_item[content_position_1]" id="input-content_position_1" class="form-control">
                    <option value="content-bottom-center" <?php if ($banner_pro_item['content_position_1'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($banner_pro_item['content_position_1'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($banner_pro_item['content_position_1'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($banner_pro_item['content_position_1'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($banner_pro_item['content_position_1'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($banner_pro_item['content_position_1'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($banner_pro_item['content_position_1'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($banner_pro_item['content_position_1'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($banner_pro_item['content_position_1'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($banner_pro_item['content_position_1'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($banner_pro_item['content_position_1'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_1"><?php echo $entry_banner_pro_hover_effect; ?></label>
                <select name="banner_pro_item[hover_effect_1]" id="input-hover_effect_1" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($banner_pro_item['hover_effect_1']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="banner_pro_item[status_2]" id="input-status_2" class="form-control">
                  <option value="0" <?php if ($banner_pro_item['status_2'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($banner_pro_item['status_2'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="banner_pro_item_image_thumb_2" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $banner_pro_item_image_thumb_2; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="banner_pro_item_image_custom_2" value="<?php echo $banner_pro_item_image_custom_2; ?>" id="input-banner_pro_item_image_custom_2" />
                <br />
                <label class="control-label" for="input-width_2"><?php echo $entry_banner_pro_width; ?></label>
                <select name="banner_pro_item[width_2]" id="input-width_2" class="form-control">
                <?php for ($k=1; $k<=12; $k++) { ?>
                  <option value="<?php echo $k; ?>" <?php if ($banner_pro_item['width_2']==$k) {echo "selected=\"selected\"";}; ?>><?php echo $k; ?>/12</option>
                <?php }; ?>
                </select>
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][url_2]" id="url_2-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['url_2']) ? $banner_pro_item[$language['language_id']]['url_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][label_2]" id="label_2-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['label_2']) ? $banner_pro_item[$language['language_id']]['label_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_2"><?php echo $entry_banner_pro_label_color; ?></label>
                <input type="text" name="banner_pro_label_color_2" id="banner_pro_label_color_2" value="<?php echo $banner_pro_label_color_2; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][title_2]" id="title_2-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['title_2']) ? $banner_pro_item[$language['language_id']]['title_2'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_2"><?php echo $entry_banner_pro_title_color; ?></label>
                <input type="text" name="banner_pro_title_color_2" id="banner_pro_title_color_2" value="<?php echo $banner_pro_title_color_2; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][subtitle_2]" id="subtitle_2-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['subtitle_2']) ? $banner_pro_item[$language['language_id']]['subtitle_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_2"><?php echo $entry_banner_pro_subtitle_color; ?></label>
                <input type="text" name="banner_pro_subtitle_color_2" id="banner_pro_subtitle_color_2" value="<?php echo $banner_pro_subtitle_color_2; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][button_2]" id="button_2-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['button_2']) ? $banner_pro_item[$language['language_id']]['button_2'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_2"><?php echo $entry_banner_pro_button_style; ?></label>
                <select name="banner_pro_item[button_style_2]" id="input-button_style_2" class="form-control">
                  <option value="primary" <?php if ($banner_pro_item['button_style_2'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($banner_pro_item['button_style_2'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_2"><?php echo $entry_banner_pro_content_position; ?></label>
                <select name="banner_pro_item[content_position_2]" id="input-content_position_2" class="form-control">
                    <option value="content-bottom-center" <?php if ($banner_pro_item['content_position_2'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($banner_pro_item['content_position_2'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($banner_pro_item['content_position_2'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($banner_pro_item['content_position_2'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($banner_pro_item['content_position_2'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($banner_pro_item['content_position_2'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($banner_pro_item['content_position_2'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($banner_pro_item['content_position_2'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($banner_pro_item['content_position_2'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($banner_pro_item['content_position_2'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($banner_pro_item['content_position_2'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_2"><?php echo $entry_banner_pro_hover_effect; ?></label>
                <select name="banner_pro_item[hover_effect_2]" id="input-hover_effect_2" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($banner_pro_item['hover_effect_2']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="banner_pro_item[status_3]" id="input-status_3" class="form-control">
                  <option value="0" <?php if ($banner_pro_item['status_3'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($banner_pro_item['status_3'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="banner_pro_item_image_thumb_3" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $banner_pro_item_image_thumb_3; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="banner_pro_item_image_custom_3" value="<?php echo $banner_pro_item_image_custom_3; ?>" id="input-banner_pro_item_image_custom_3" />
                <br />
                <label class="control-label" for="input-width_3"><?php echo $entry_banner_pro_width; ?></label>
                <select name="banner_pro_item[width_3]" id="input-width_3" class="form-control">
                <?php for ($k=1; $k<=12; $k++) { ?>
                  <option value="<?php echo $k; ?>" <?php if ($banner_pro_item['width_3']==$k) {echo "selected=\"selected\"";}; ?>><?php echo $k; ?>/12</option>
                <?php }; ?>
                </select>
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][url_3]" id="url_3-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['url_3']) ? $banner_pro_item[$language['language_id']]['url_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][label_3]" id="label_3-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['label_3']) ? $banner_pro_item[$language['language_id']]['label_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_3"><?php echo $entry_banner_pro_label_color; ?></label>
                <input type="text" name="banner_pro_label_color_3" id="banner_pro_label_color_3" value="<?php echo $banner_pro_label_color_3; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][title_3]" id="title_3-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['title_3']) ? $banner_pro_item[$language['language_id']]['title_3'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_3"><?php echo $entry_banner_pro_title_color; ?></label>
                <input type="text" name="banner_pro_title_color_3" id="banner_pro_title_color_3" value="<?php echo $banner_pro_title_color_3; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][subtitle_3]" id="subtitle_3-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['subtitle_3']) ? $banner_pro_item[$language['language_id']]['subtitle_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_3"><?php echo $entry_banner_pro_subtitle_color; ?></label>
                <input type="text" name="banner_pro_subtitle_color_3" id="banner_pro_subtitle_color_3" value="<?php echo $banner_pro_subtitle_color_3; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][button_3]" id="button_3-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['button_3']) ? $banner_pro_item[$language['language_id']]['button_3'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_3"><?php echo $entry_banner_pro_button_style; ?></label>
                <select name="banner_pro_item[button_style_3]" id="input-button_style_3" class="form-control">
                  <option value="primary" <?php if ($banner_pro_item['button_style_3'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($banner_pro_item['button_style_3'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_3"><?php echo $entry_banner_pro_content_position; ?></label>
                <select name="banner_pro_item[content_position_3]" id="input-content_position_3" class="form-control">
                    <option value="content-bottom-center" <?php if ($banner_pro_item['content_position_3'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($banner_pro_item['content_position_3'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($banner_pro_item['content_position_3'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($banner_pro_item['content_position_3'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($banner_pro_item['content_position_3'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($banner_pro_item['content_position_3'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($banner_pro_item['content_position_3'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($banner_pro_item['content_position_3'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($banner_pro_item['content_position_3'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($banner_pro_item['content_position_3'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($banner_pro_item['content_position_3'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_3"><?php echo $entry_banner_pro_hover_effect; ?></label>
                <select name="banner_pro_item[hover_effect_3]" id="input-hover_effect_3" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($banner_pro_item['hover_effect_3']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="banner_pro_item[status_4]" id="input-status_4" class="form-control">
                  <option value="0" <?php if ($banner_pro_item['status_4'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($banner_pro_item['status_4'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="banner_pro_item_image_thumb_4" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $banner_pro_item_image_thumb_4; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="banner_pro_item_image_custom_4" value="<?php echo $banner_pro_item_image_custom_4; ?>" id="input-banner_pro_item_image_custom_4" />
                <br />
                <label class="control-label" for="input-width_4"><?php echo $entry_banner_pro_width; ?></label>
                <select name="banner_pro_item[width_4]" id="input-width_4" class="form-control">
                <?php for ($k=1; $k<=12; $k++) { ?>
                  <option value="<?php echo $k; ?>" <?php if ($banner_pro_item['width_4']==$k) {echo "selected=\"selected\"";}; ?>><?php echo $k; ?>/12</option>
                <?php }; ?>
                </select>
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][url_4]" id="url_4-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['url_4']) ? $banner_pro_item[$language['language_id']]['url_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][label_4]" id="label_4-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['label_4']) ? $banner_pro_item[$language['language_id']]['label_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_4"><?php echo $entry_banner_pro_label_color; ?></label>
                <input type="text" name="banner_pro_label_color_4" id="banner_pro_label_color_4" value="<?php echo $banner_pro_label_color_4; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][title_4]" id="title_4-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['title_4']) ? $banner_pro_item[$language['language_id']]['title_4'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_4"><?php echo $entry_banner_pro_title_color; ?></label>
                <input type="text" name="banner_pro_title_color_4" id="banner_pro_title_color_4" value="<?php echo $banner_pro_title_color_4; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][subtitle_4]" id="subtitle_4-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['subtitle_4']) ? $banner_pro_item[$language['language_id']]['subtitle_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_4"><?php echo $entry_banner_pro_subtitle_color; ?></label>
                <input type="text" name="banner_pro_subtitle_color_4" id="banner_pro_subtitle_color_4" value="<?php echo $banner_pro_subtitle_color_4; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][button_4]" id="button_4-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['button_4']) ? $banner_pro_item[$language['language_id']]['button_4'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_4"><?php echo $entry_banner_pro_button_style; ?></label>
                <select name="banner_pro_item[button_style_4]" id="input-button_style_4" class="form-control">
                  <option value="primary" <?php if ($banner_pro_item['button_style_4'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($banner_pro_item['button_style_4'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_4"><?php echo $entry_banner_pro_content_position; ?></label>
                <select name="banner_pro_item[content_position_4]" id="input-content_position_4" class="form-control">
                    <option value="content-bottom-center" <?php if ($banner_pro_item['content_position_4'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($banner_pro_item['content_position_4'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($banner_pro_item['content_position_4'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($banner_pro_item['content_position_4'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($banner_pro_item['content_position_4'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($banner_pro_item['content_position_4'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($banner_pro_item['content_position_4'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($banner_pro_item['content_position_4'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($banner_pro_item['content_position_4'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($banner_pro_item['content_position_4'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($banner_pro_item['content_position_4'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_4"><?php echo $entry_banner_pro_hover_effect; ?></label>
                <select name="banner_pro_item[hover_effect_4]" id="input-hover_effect_4" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($banner_pro_item['hover_effect_4']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="banner_pro_item[status_5]" id="input-status_5" class="form-control">
                  <option value="0" <?php if ($banner_pro_item['status_5'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($banner_pro_item['status_5'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="banner_pro_item_image_thumb_5" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $banner_pro_item_image_thumb_5; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="banner_pro_item_image_custom_5" value="<?php echo $banner_pro_item_image_custom_5; ?>" id="input-banner_pro_item_image_custom_5" />
                <br />
                <label class="control-label" for="input-width_5"><?php echo $entry_banner_pro_width; ?></label>
                <select name="banner_pro_item[width_5]" id="input-width_5" class="form-control">
                <?php for ($k=1; $k<=12; $k++) { ?>
                  <option value="<?php echo $k; ?>" <?php if ($banner_pro_item['width_5']==$k) {echo "selected=\"selected\"";}; ?>><?php echo $k; ?>/12</option>
                <?php }; ?>
                </select>
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][url_5]" id="url_5-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['url_5']) ? $banner_pro_item[$language['language_id']]['url_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][label_5]" id="label_5-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['label_5']) ? $banner_pro_item[$language['language_id']]['label_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_5"><?php echo $entry_banner_pro_label_color; ?></label>
                <input type="text" name="banner_pro_label_color_5" id="banner_pro_label_color_5" value="<?php echo $banner_pro_label_color_5; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][title_5]" id="title_5-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['title_5']) ? $banner_pro_item[$language['language_id']]['title_5'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_5"><?php echo $entry_banner_pro_title_color; ?></label>
                <input type="text" name="banner_pro_title_color_5" id="banner_pro_title_color_5" value="<?php echo $banner_pro_title_color_5; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][subtitle_5]" id="subtitle_5-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['subtitle_5']) ? $banner_pro_item[$language['language_id']]['subtitle_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_5"><?php echo $entry_banner_pro_subtitle_color; ?></label>
                <input type="text" name="banner_pro_subtitle_color_5" id="banner_pro_subtitle_color_5" value="<?php echo $banner_pro_subtitle_color_5; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][button_5]" id="button_5-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['button_5']) ? $banner_pro_item[$language['language_id']]['button_5'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_5"><?php echo $entry_banner_pro_button_style; ?></label>
                <select name="banner_pro_item[button_style_5]" id="input-button_style_5" class="form-control">
                  <option value="primary" <?php if ($banner_pro_item['button_style_5'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($banner_pro_item['button_style_5'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_5"><?php echo $entry_banner_pro_content_position; ?></label>
                <select name="banner_pro_item[content_position_5]" id="input-content_position_5" class="form-control">
                    <option value="content-bottom-center" <?php if ($banner_pro_item['content_position_5'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($banner_pro_item['content_position_5'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($banner_pro_item['content_position_5'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($banner_pro_item['content_position_5'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($banner_pro_item['content_position_5'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($banner_pro_item['content_position_5'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($banner_pro_item['content_position_5'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($banner_pro_item['content_position_5'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($banner_pro_item['content_position_5'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($banner_pro_item['content_position_5'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($banner_pro_item['content_position_5'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_5"><?php echo $entry_banner_pro_hover_effect; ?></label>
                <select name="banner_pro_item[hover_effect_5]" id="input-hover_effect_5" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($banner_pro_item['hover_effect_5']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>
              
              <tr>

                <td class="text-left">
                <select name="banner_pro_item[status_6]" id="input-status_6" class="form-control">
                  <option value="0" <?php if ($banner_pro_item['status_6'] == '0') {echo "selected=\"selected\"";}; ?>>No</option>
			      <option value="1" <?php if ($banner_pro_item['status_6'] == '1') {echo "selected=\"selected\"";}; ?>>Yes</option>
                </select>
                </td>
                
                <td class="text-left">
                <a href="" id="banner_pro_item_image_thumb_6" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $banner_pro_item_image_thumb_6; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="banner_pro_item_image_custom_6" value="<?php echo $banner_pro_item_image_custom_6; ?>" id="input-banner_pro_item_image_custom_6" />
                <br />
                <label class="control-label" for="input-width_6"><?php echo $entry_banner_pro_width; ?></label>
                <select name="banner_pro_item[width_6]" id="input-width_6" class="form-control">
                <?php for ($k=1; $k<=12; $k++) { ?>
                  <option value="<?php echo $k; ?>" <?php if ($banner_pro_item['width_6']==$k) {echo "selected=\"selected\"";}; ?>><?php echo $k; ?>/12</option>
                <?php }; ?>
                </select>
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][url_6]" id="url_6-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['url_6']) ? $banner_pro_item[$language['language_id']]['url_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][label_6]" id="label_6-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['label_6']) ? $banner_pro_item[$language['language_id']]['label_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-label_6"><?php echo $entry_banner_pro_label_color; ?></label>
                <input type="text" name="banner_pro_label_color_6" id="banner_pro_label_color_6" value="<?php echo $banner_pro_label_color_6; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][title_6]" id="title_6-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['title_6']) ? $banner_pro_item[$language['language_id']]['title_6'] : ''; ?></textarea>
                
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-title_color_6"><?php echo $entry_banner_pro_title_color; ?></label>
                <input type="text" name="banner_pro_title_color_6" id="banner_pro_title_color_6" value="<?php echo $banner_pro_title_color_6; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][subtitle_6]" id="subtitle_6-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['subtitle_6']) ? $banner_pro_item[$language['language_id']]['subtitle_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <br />
                <label class="control-label" for="input-subtitle_color_6"><?php echo $entry_banner_pro_subtitle_color; ?></label>
                <input type="text" name="banner_pro_subtitle_color_6" id="banner_pro_subtitle_color_6" value="<?php echo $banner_pro_subtitle_color_6; ?>" class="color {required:false,hash:true}" size="8" />
                </td>
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="4" name="banner_pro_item[<?php echo $language['language_id']; ?>][button_6]" id="button_6-<?php echo $language['language_id']; ?>"><?php echo isset($banner_pro_item[$language['language_id']]['button_6']) ? $banner_pro_item[$language['language_id']]['button_6'] : ''; ?></textarea>
                </div>
                <?php } ?>
                <label class="control-label" for="input-button_style_6"><?php echo $entry_banner_pro_button_style; ?></label>
                <select name="banner_pro_item[button_style_6]" id="input-button_style_6" class="form-control">
                  <option value="primary" <?php if ($banner_pro_item['button_style_6'] == 'primary') {echo "selected=\"selected\"";}; ?>>Primary</option>
			      <option value="default" <?php if ($banner_pro_item['button_style_6'] == 'default') {echo "selected=\"selected\"";}; ?>>Default</option>
                </select>
                </td>
                
                <td class="text-left">
                <label class="control-label" for="input-content_position_6"><?php echo $entry_banner_pro_content_position; ?></label>
                <select name="banner_pro_item[content_position_6]" id="input-content_position_6" class="form-control">
                    <option value="content-bottom-center" <?php if ($banner_pro_item['content_position_6'] == 'content-bottom-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bc; ?></option>
			    	<option value="content-bottom-left" <?php if ($banner_pro_item['content_position_6'] == 'content-bottom-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_bl; ?></option>
                    <option value="content-bottom-right" <?php if ($banner_pro_item['content_position_6'] == 'content-bottom-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_br; ?></option>
                    <option value="content-center" <?php if ($banner_pro_item['content_position_6'] == 'content-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_c; ?></option>
			    	<option value="content-left" <?php if ($banner_pro_item['content_position_6'] == 'content-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_l; ?></option>
                    <option value="content-right" <?php if ($banner_pro_item['content_position_6'] == 'content-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_r; ?></option>
                    <option value="content-top-center" <?php if ($banner_pro_item['content_position_6'] == 'content-top-center') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tc; ?></option>
			    	<option value="content-top-left" <?php if ($banner_pro_item['content_position_6'] == 'content-top-left') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tl; ?></option>
                    <option value="content-top-right" <?php if ($banner_pro_item['content_position_6'] == 'content-top-right') {echo "selected=\"selected\"";}; ?>><?php echo $text_position_tr; ?></option>
                    <option value="content-center-left" <?php if ($banner_pro_item['content_position_6'] == 'content-center-left') {echo "selected=\"selected\"";}; ?>>Center (shifted to the left)</option>
                    <option value="content-center-right" <?php if ($banner_pro_item['content_position_6'] == 'content-center-right') {echo "selected=\"selected\"";}; ?>>Center (shifted to the right)</option>
                </select>
                
                <label class="control-label" for="input-hover_effect_6"><?php echo $entry_banner_pro_hover_effect; ?></label>
                <select name="banner_pro_item[hover_effect_6]" id="input-hover_effect_6" class="form-control">
                <?php for ($j=1; $j<=10; $j++) { ?>
                  <option value="<?php echo $j; ?>" <?php if ($banner_pro_item['hover_effect_6']==$j) {echo "selected=\"selected\"";}; ?>>Effect <?php echo $j; ?></option>
                <?php }; ?>
                </select>
                </td>

              </tr>

              
            </tbody>
            <tfoot>
              <tr>
                <td colspan="8" class="text-right"></td>
              </tr>
            </tfoot>
          </table>
         </div>
         </div>
          
        </form>
      </div>
    </div>
  </div>
</div>


<style type="text/css">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
table {font-size:12px;}
.table thead > tr > td, .table tbody > tr > td {vertical-align:top!important;}
</style>
<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>
<?php echo $footer; ?>