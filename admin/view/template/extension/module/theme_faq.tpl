<?php 
if(empty($module_question_bg_color)) $module_question_bg_color ="F0F0F0";
if(empty($module_question_bg_color_hover)) $module_question_bg_color_hover ="16B778";
if(empty($module_question_color)) $module_question_color ="222222";
if(empty($module_question_color_hover)) $module_question_color_hover ="FFFFFF";
if(empty($module_answer_bg_color)) $module_answer_bg_color ="FFFFFF";
if(empty($module_answer_color)) $module_answer_color ="656565";
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-faq" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-faq" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-faq_title"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="faq[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($faq[$language['language_id']]) ? $faq[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_question_bg_color"><?php echo $entry_module_question_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_question_bg_color" id="module_question_bg_color" value="<?php echo $module_question_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_question_bg_color_hover"><?php echo $entry_module_question_bg_color_hover; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_question_bg_color_hover" id="module_question_bg_color_hover" value="<?php echo $module_question_bg_color_hover; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_question_color"><?php echo $entry_module_question_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_question_color" id="module_question_color" value="<?php echo $module_question_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_question_color_hover"><?php echo $entry_module_question_color_hover; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_question_color_hover" id="module_question_color_hover" value="<?php echo $module_question_color_hover; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_answer_bg_color"><?php echo $entry_module_answer_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_answer_bg_color" id="module_answer_bg_color" value="<?php echo $module_answer_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_answer_color"><?php echo $entry_module_answer_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_answer_color" id="module_answer_color" value="<?php echo $module_answer_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <br /><br />
          <legend>FAQs</legend>
         <div class="row">
         <div class="col-sm-12">
         <table id="sections" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left" width="35%"><?php echo $entry_faq_question; ?></td>
                <td class="text-left"><?php echo $entry_faq_answer; ?></td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php $section_row = 1; ?>
              <?php foreach ($sections as $section) { ?>
              <tr id="section-row<?php echo $section_row; ?>">
                
                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="1" name="sections[<?php echo $section_row; ?>][faq_question][<?php echo $language['language_id']; ?>]" id="description-<?php echo $section_row; ?>-<?php echo $language['language_id']; ?>"><?php echo isset($section['faq_question'][$language['language_id']]) ? $section['faq_question'][$language['language_id']] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>

                <td class="text-left">
                <?php foreach ($languages as $language) { ?>
                <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['language_id']; ?>" /></span>		
                <textarea class="form-control" rows="6" name="sections[<?php echo $section_row; ?>][faq_answer][<?php echo $language['language_id']; ?>]" id="description-<?php echo $section_row; ?>-<?php echo $language['language_id']; ?>"><?php echo isset($section['faq_answer'][$language['language_id']]) ? $section['faq_answer'][$language['language_id']] : ''; ?></textarea>
                </div>
                <?php } ?>
                </td>
                
                <td class="text-right">
                <button type="button" onclick="$('#section-row<?php echo $section_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                </td>
                
              </tr>
              <?php $section_row++; ?>
			  <?php } ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="text-right"><button type="button" onclick="addRow();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </table>
         </div>
         </div>
          
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var section_row = <?php echo $section_row; ?>;

function addRow() {
	html  = '<tr id="section-row' + section_row + '">';
	
	html += '<td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group">';
	html += '<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>';
	html += '<textarea name="sections[' + section_row + '][faq_question][<?php echo $language['language_id']; ?>]" id="description-' + section_row + '-<?php echo $language['language_id']; ?>" class="form-control" rows="1"></textarea>';
	html += '</div>';
	<?php } ?>
	html += '</td>';
	
	html += '<td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group">';
	html += '<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>';
	html += '<textarea name="sections[' + section_row + '][faq_answer][<?php echo $language['language_id']; ?>]" id="description-' + section_row + '-<?php echo $language['language_id']; ?>" class="form-control" rows="6"></textarea>';
	html += '</div>';
	<?php } ?>
	html += '</td>';
	
	html += '  <td class="text-right"><button type="button" onclick="$(\'#section-row' + section_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#sections tbody').append(html);
	
	section_row++;
}
//--></script>
<style type="text/css">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
table {font-size:12px;}
</style>
<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>
<?php echo $footer; ?>