<?php global $config; ?>

<style type="text/css">
.faq-block<?php echo $module; ?> .panel-title {
<?php if($module_question_bg_color !='') { ?>
	background-color: <?php echo $module_question_bg_color; ?>;
<?php } ?>
}
#content .faq-block<?php echo $module; ?> .subtitle, #column-left .faq-block<?php echo $module; ?> .subtitle, #column-right .faq-block<?php echo $module; ?> .subtitle {
	color: <?php echo $module_question_color; ?>;
}

.faq-block<?php echo $module; ?> a:hover .panel-title {
<?php if($module_question_bg_color_hover !='') { ?>
	background-color: <?php echo $module_question_bg_color_hover; ?>;
<?php } ?>
}
#content .faq-block<?php echo $module; ?> .subtitle:hover, #column-left .faq-block<?php echo $module; ?> .subtitle:hover, #column-right .faq-block<?php echo $module; ?> .subtitle:hover {
	color: <?php echo $module_question_color_hover; ?>;
}
.faq-block<?php echo $module; ?> .subtitle {
	transition: all 0.15s ease-in 0s;
}
.faq-block<?php echo $module; ?> .panel-collapse {
<?php if($module_question_bg_color !='') { ?>
	background-color: <?php echo $module_answer_bg_color; ?>;
<?php } ?>
    color: <?php echo $module_answer_color; ?>;
}
</style>

<?php if(($faq['title']) !='') { ?>
<div class="panel panel-default faq-block-heading">
  <div class="panel-heading module-heading"><h2><?php echo $faq['title']; ?></h2></div>
</div>
<?php } ?>

<div class="panel-group faq-block faq-block<?php echo $module; ?>" id="accordion<?php echo $module; ?>" role="tablist" aria-multiselectable="true">
<?php 
  $x = 0;
  foreach($sections as $section):
?>
  <div class="panel panel-default">
    <a role="button" data-toggle="collapse" data-parent="#accordion<?php echo $module; ?>" href="#collapse<?php echo $x; ?>" aria-expanded="true" aria-controls="collapse<?php echo $x; ?>">
    <div class="panel-title subtitle" role="tab" id="heading<?php echo $x; ?>">
          <?php echo $section['faq_question']; ?>
    </div>
    </a>
    <div id="collapse<?php echo $x; ?>" class="panel-collapse <?php echo ($x == 0 ? 'collapse in' : 'collapse'); ?>" role="tabpanel" aria-labelledby="heading<?php echo $x; ?>">
      <div class="panel-body">
        <?php echo $section['faq_answer']; ?>
      </div>
    </div>   
  </div>
<?php $x++; endforeach;?>
</div>
