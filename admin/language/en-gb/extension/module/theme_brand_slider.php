<?php
// Heading
$_['heading_title']    = 'SELLMORE Theme - Brand Slider';

// Text
$_['text_module']      = 'Modules';
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified SELLMORE Theme - Brand Slider module!';
$_['text_edit']        = 'Edit SELLMORE Theme - Brand Slider Module';
$_['text_brand_name']     = 'Name';
$_['text_brand_logo']     = 'Logo';
$_['text_brand_logo_name'] = 'Logo + Name';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_status']     = 'Status';
$_['entry_title']         = 'Title:'; 
$_['entry_subtitle']   = 'Subtitle';
$_['entry_button']     = 'Button';
$_['entry_brands_per_row'] = 'Brands per row:';
$_['entry_brands_display_style'] = 'Brands display style:';
$_['entry_banner_per_row'] = 'Images in a row:';
$_['entry_module_style'] = 'Module Style:';
$_['entry_module_title_position'] = 'Title position:';
$_['entry_module_title_width'] = 'Title width:';
$_['entry_module_items_width'] = 'Items width:';
$_['entry_module_title_color'] = 'Title color:';
$_['entry_module_subtitle_color'] = 'Subtitle color:';
$_['entry_module_bg_color'] = 'Title background color:';
$_['entry_module_image_thumb'] = 'Title background image:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify SELLMORE Theme - Brand Slider module!';