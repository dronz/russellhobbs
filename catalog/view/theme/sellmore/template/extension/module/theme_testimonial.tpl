
<style type="text/css">
#testimonial-block<?php echo $module; ?> h2 {
	color: <?php echo $module_title_color; ?>;
}
#testimonial-block<?php echo $module; ?> .testimonial-block-item-description {
	color: <?php echo $module_testimonial_color; ?>;
}
#testimonial-block<?php echo $module; ?> .testimonial-block-item-name {
	color: <?php echo $module_name_color; ?>;
}
#testimonial-block<?php echo $module; ?> {
<?php if($module_bg_color !='') { ?>
	background-color: <?php echo $module_bg_color; ?>;
<?php }	
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$path_image = $config_ssl . 'image/';
} else {
	$path_image = $config_url . 'image/';
}
if($module_image_custom !='') { ?>
	background-image: url("<?php echo $path_image . $module_image_custom ?>"); ?>;
<?php } else { ?>
	background-image: none;
<?php } ?>
    background-position: center;
}
</style>

<div id="testimonial-block" class="panel panel-default panel-inline module-style-2">
<div id="testimonial-block<?php echo $module; ?>" class="thumbnails box-content testimonial-block-content panel-inline-content">
  <?php if(($testimonial['title']) !='') { ?>
  <div class="testimonial-block-title panel-inline-title full-width-container"><h2><?php echo $testimonial['title']; ?></h2></div>
  <?php } ?>
  <div class="panel-inline-items-wrapper full-width-container">
  <div class="panel-inline-items testimonial-block-items<?php echo $module; ?>">
  <?php foreach($sections as $section){ ?>
  <div class="testimonial-block-item">
  <span class="testimonial-block-item-photo"><img src="<?php echo $section['image']; ?>" alt="" title="" /></span>
  <span class="testimonial-block-item-description subtitle"><?php echo $section['testimonial_block']; ?></span>
  <span class="testimonial-block-item-name">- <?php echo $section['reviewer_name']; ?> -</span>
  </div>
  <?php } ?>
  </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
$('.testimonial-block-items<?php echo $module; ?>').owlCarousel({
	items: <?php echo $pr_id; ?>,
	itemsDesktop : [1199, 2],
    itemsDesktopSmall : [979, 2],
    itemsTablet : [768, 1],
	autoPlay: 5000,
	singleItem: false,
	scrollPerPage: true,
	pagination: true,
	navigation: false,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>