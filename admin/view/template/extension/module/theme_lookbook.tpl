<?php 
if(empty($module_bg_color)) $module_bg_color ="EDEDED";
if(empty($module_title_color)) $module_title_color ="222222";
if(empty($module_subtitle_color)) $module_subtitle_color ="AAAAAA";
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-lookbook" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-lookbook" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-lookbook_title"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="lookbook[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($lookbook[$language['language_id']]) ? $lookbook[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-lookbook_subtitle"><?php echo $entry_subtitle; ?></label>
            <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
					<?php $language_id = $language['language_id']; ?>
						<div class="input-group pull-left">    
                            <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                            <input type="text" name="lookbook[<?php echo $language['language_id']; ?>][subtitle]" value="<?php echo isset($lookbook[$language['language_id']]) ? $lookbook[$language['language_id']]['subtitle'] : ''; ?>" placeholder="<?php echo $entry_subtitle; ?>" class="form-control" />
						</div>
				<?php } ?>
            </div>
          </div>     
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner"><?php echo $entry_banner; ?></label>
            <div class="col-sm-10">
              <select name="banner_id" id="input-banner" class="form-control">
                <?php foreach ($banners as $banner) { ?>
                <?php if ($banner['banner_id'] == $banner_id) { ?>
                <option value="<?php echo $banner['banner_id']; ?>" selected="selected"><?php echo $banner['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $banner['banner_id']; ?>"><?php echo $banner['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-width"><?php echo $entry_width; ?></label>
            <div class="col-sm-10">
              <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
              <?php if ($error_width) { ?>
              <div class="text-danger"><?php echo $error_width; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-height"><?php echo $entry_height; ?></label>
            <div class="col-sm-10">
              <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
              <?php if ($error_height) { ?>
              <div class="text-danger"><?php echo $error_height; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-pr_id"><?php echo $entry_banner_per_row; ?></label>
            <div class="col-sm-10">
              <select name="pr_id" id="input-pr_id" class="form-control">
              <?php for ($i=1; $i<=6; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($pr_id==$i) {echo "selected=\"selected\"";}; ?>><?php echo $i; ?></option>
              <?php }; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_style"><?php echo $entry_module_style; ?></label>
            <div class="col-sm-10">
              <select name="module_style" id="input-module_style" class="form-control">
                <option value="module-style-1" <?php if ($module_style == 'module-style-1') {echo "selected=\"selected\"";}; ?>>Style 1 - Inline</option>
                <option value="module-style-2" <?php if ($module_style == 'module-style-2') {echo "selected=\"selected\"";}; ?>>Style 2 - Classic</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_title_position"><?php echo $entry_module_title_position; ?></label>
            <div class="col-sm-10">
              <select name="module_title_position" id="input-module_title_position" class="form-control">
                <option value="left" <?php if ($module_title_position == 'left') {echo "selected=\"selected\"";}; ?>>Left</option>
                <option value="right" <?php if ($module_title_position == 'right') {echo "selected=\"selected\"";}; ?>>Right</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_title_width"><?php echo $entry_module_title_width; ?></label>
            <div class="col-sm-10">
              <select name="module_title_width" id="input-module_title_width" class="form-control">
              <?php for ($i=1; $i<=12; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($module_title_width==$i) {echo "selected=\"selected\"";}; ?>><?php echo $i; ?>/12</option>
              <?php }; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_items_width"><?php echo $entry_module_items_width; ?></label>
            <div class="col-sm-10">
              <select name="module_items_width" id="input-module_items_width" class="form-control">
              <?php for ($i=1; $i<=12; $i++) { ?>
                <option value="<?php echo $i; ?>" <?php if ($module_items_width==$i) {echo "selected=\"selected\"";}; ?>><?php echo $i; ?>/12</option>
              <?php }; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_title_color"><?php echo $entry_module_title_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_title_color" id="module_title_color" value="<?php echo $module_title_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_subtitle_color"><?php echo $entry_module_subtitle_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_subtitle_color" id="module_subtitle_color" value="<?php echo $module_subtitle_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_bg_color"><?php echo $entry_module_bg_color; ?></label>
            <div class="col-sm-10">
              <input type="text" name="module_bg_color" id="module_bg_color" value="<?php echo $module_bg_color; ?>" class="color {required:false,hash:true}" size="8" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-module_image_thumb"><?php echo $entry_module_image_thumb; ?></label>
            <div class="col-sm-10">
              <a href="" id="module_image_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $module_image_thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="module_image_custom" value="<?php echo $module_image_custom; ?>" id="input-module_image_custom" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
</style>
<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>
<?php echo $footer; ?>