
<div id="theme_banner<?php echo $module; ?>" class="panel panel-default theme-banner <?php echo $title_position; ?>">
<div class="theme_banner_items<?php echo $module; ?>">
  <?php foreach ($banners as $banner) { ?>
  <?php if ($banner['link']) { ?>
  <div class="theme-banner-item <?php if($banner_view =='grid') { ?>pr<?php echo $pr_id; ?> col-sm-4<?php } ?> slide-<?php echo $title_slide_status; ?>">
  <a href="<?php echo $banner['link']; ?>">
  <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" />
  <span class="theme-banner-bg"></span>
  <span class="btn btn-default theme-banner-title"><?php echo $banner['title']; ?></span>
  <span class="btn btn-default theme-banner-title view-now"><?php echo $t1o_text_view[$lang_id]; ?></span>
  </a>
  </div>
  <?php } else { ?>
  <div class="theme-banner-item <?php if($banner_view =='grid') { ?>pr<?php echo $pr_id; ?> col-sm-4<?php } ?> slide-<?php echo $title_slide_status; ?>">
  <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" />
  <span class="theme-banner-bg"></span>
  <span class="btn btn-default theme-banner-title no-link"><?php echo $banner['title']; ?></span>
  </div>
  <?php } ?>
  <?php } ?>
</div>
</div>

<?php if($banner_view =='slider') { ?>
<script type="text/javascript"><!--
$('.theme_banner_items<?php echo $module; ?>').owlCarousel({
	items: <?php echo $pr_id; ?>,
	autoPlay: 5000,
	singleItem: false,
	scrollPerPage: true,
	pagination: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
});
--></script>
<?php } ?>
